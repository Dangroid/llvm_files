source_filename = "test"
target datalayout = "e-p:32:32-f64:32:64-f80:32-n8:16:32-S128"

@es = internal unnamed_addr global i16 0, align 4
@eax = internal unnamed_addr global i32 0
@ecx = internal unnamed_addr global i32 0
@edx = internal unnamed_addr global i32 0
@ebx = internal unnamed_addr global i32 0
@esp = internal unnamed_addr global i32 0
@ebp = internal unnamed_addr global i32 0
@esi = internal unnamed_addr global i32 0
@edi = internal unnamed_addr global i32 0

declare i32 @unknown_4b2e() local_unnamed_addr

declare i32 @_CorExeMain() local_unnamed_addr

define i32 @function_4020b4() local_unnamed_addr {
dec_label_pc_4020b4:
  %v2_4020b4 = load i8, i8* bitcast (i32* @eax to i8*), align 4
  %v3_4020b4 = load i32, i32* @eax, align 4
  %v4_4020b4 = trunc i32 %v3_4020b4 to i8
  %v5_4020b4 = add i8 %v2_4020b4, %v4_4020b4
  %v21_4020b4 = inttoptr i32 %v3_4020b4 to i8*
  store i8 %v5_4020b4, i8* %v21_4020b4, align 1
  %v0_4020b6 = load i32, i32* @eax, align 4
  ret i32 %v0_4020b6
}

define i32 @function_4020c8() local_unnamed_addr {
dec_label_pc_4020c8:
  %v2_4020c8 = load i8, i8* bitcast (i32* @eax to i8*), align 4
  %v3_4020c8 = load i32, i32* @eax, align 4
  %v4_4020c8 = trunc i32 %v3_4020c8 to i8
  %v5_4020c8 = add i8 %v2_4020c8, %v4_4020c8
  %v21_4020c8 = inttoptr i32 %v3_4020c8 to i8*
  store i8 %v5_4020c8, i8* %v21_4020c8, align 1
  %v0_4020ca = load i32, i32* @eax, align 4
  ret i32 %v0_4020ca
}

define i32 @function_4020ce() local_unnamed_addr {
dec_label_pc_4020ce:
  %v0_4020ce = load i32, i32* @eax, align 4
  ret i32 %v0_4020ce
}

define i32 @function_4020d6() local_unnamed_addr {
dec_label_pc_4020d6:
  %v0_4020d6 = load i32, i32* @eax, align 4
  ret i32 %v0_4020d6
}

define i32 @function_40211d() local_unnamed_addr {
dec_label_pc_40211d:
  %v0_40211d = load i32, i32* @eax, align 4
  ret i32 %v0_40211d
}

define i32 @function_40214b() local_unnamed_addr {
dec_label_pc_40214b:
  %v4_40214b = load i32, i32* @eax, align 4
  ret i32 %v4_40214b
}

define i32 @function_402162() local_unnamed_addr {
dec_label_pc_402162:
  %v0_402162 = load i32, i32* @eax, align 4
  ret i32 %v0_402162
}

define i32 @function_40217c() local_unnamed_addr {
dec_label_pc_40217c:
  %v0_40217c = load i32, i32* @eax, align 4
  ret i32 %v0_40217c
}

define i32 @function_4022d2() local_unnamed_addr {
dec_label_pc_4022d2:
  %v0_4022d2 = load i32, i32* @eax, align 4
  ret i32 %v0_4022d2
}

define i32 @function_4022d6() local_unnamed_addr {
dec_label_pc_4022d6:
  %v0_4022d6 = load i32, i32* @eax, align 4
  ret i32 %v0_4022d6
}

define i32 @function_4022da() local_unnamed_addr {
dec_label_pc_4022da:
  %v0_4022da = load i32, i32* @eax, align 4
  ret i32 %v0_4022da
}

define i32 @function_4022ee() local_unnamed_addr {
dec_label_pc_4022ee:
  %v0_4022ee = load i32, i32* @eax, align 4
  ret i32 %v0_4022ee
}

define i32 @function_402316() local_unnamed_addr {
dec_label_pc_402316:
  %v0_402316 = load i32, i32* @eax, align 4
  ret i32 %v0_402316
}

define i32 @function_40236c() local_unnamed_addr {
dec_label_pc_40236c:
  %v0_40236c = load i32, i32* @eax, align 4
  ret i32 %v0_40236c
}

define i32 @function_4023aa() local_unnamed_addr {
dec_label_pc_4023aa:
  %v0_4023aa = load i32, i32* @eax, align 4
  ret i32 %v0_4023aa
}

define i32 @function_4023c2() local_unnamed_addr {
dec_label_pc_4023c2:
  %v4_4023c2 = load i32, i32* @eax, align 4
  ret i32 %v4_4023c2
}

define i32 @function_4023d5() local_unnamed_addr {
dec_label_pc_4023d5:
  %v0_4023d5 = load i32, i32* @eax, align 4
  ret i32 %v0_4023d5
}

define i32 @function_4023f5() local_unnamed_addr {
dec_label_pc_4023f5:
  %v0_4023f5 = load i32, i32* @eax, align 4
  ret i32 %v0_4023f5
}

define i32 @function_4023f9() local_unnamed_addr {
dec_label_pc_4023f9:
  %v0_4023f9 = load i32, i32* @eax, align 4
  ret i32 %v0_4023f9
}

define i32 @function_40240c() local_unnamed_addr {
dec_label_pc_40240c:
  %v0_40240c = load i32, i32* @eax, align 4
  ret i32 %v0_40240c
}

define i32 @function_40248b() local_unnamed_addr {
dec_label_pc_40248b:
  %v0_40248b = load i32, i32* @eax, align 4
  ret i32 %v0_40248b
}

define i32 @function_402517() local_unnamed_addr {
dec_label_pc_402517:
  %v1_402517 = load i32, i32* @eax, align 4
  ret i32 %v1_402517
}

define i32 @function_40253c() local_unnamed_addr {
dec_label_pc_40253c:
  %v0_40253c = load i32, i32* @eax, align 4
  ret i32 %v0_40253c
}

define i32 @function_402684() local_unnamed_addr {
dec_label_pc_402684:
  %v0_402684 = load i32, i32* @eax, align 4
  ret i32 %v0_402684
}

define i32 @function_402696() local_unnamed_addr {
dec_label_pc_402696:
  %v4_402696 = load i32, i32* @eax, align 4
  ret i32 %v4_402696
}

define i32 @function_40270a() local_unnamed_addr {
dec_label_pc_40270a:
  %v0_40270a = load i32, i32* @eax, align 4
  ret i32 %v0_40270a
}

define i32 @function_402717(i32 %arg1) local_unnamed_addr {
dec_label_pc_402717:
  %v0_402717 = load i32, i32* @eax, align 4
  %v1_402717 = trunc i32 %v0_402717 to i8
  %v1_402719 = inttoptr i32 %v0_402717 to i8*
  %v2_402719 = load i8, i8* %v1_402719, align 1
  %v5_402719 = add i8 %v2_402719, %v1_402717
  store i8 %v5_402719, i8* %v1_402719, align 1
  %v5_40271b = load i32, i32* @eax, align 4
  ret i32 %v5_40271b
}

define i32 @function_402737() local_unnamed_addr {
dec_label_pc_402737:
  %v0_402737 = load i32, i32* @eax, align 4
  ret i32 %v0_402737
}

define i32 @function_40275d() local_unnamed_addr {
dec_label_pc_40275d:
  %v0_40275d = load i32, i32* @eax, align 4
  ret i32 %v0_40275d
}

define i32 @function_4027f0() local_unnamed_addr {
dec_label_pc_4027f0:
  %v0_4027f0 = load i32, i32* @eax, align 4
  ret i32 %v0_4027f0
}

define i32 @function_402801(i32 %arg1) local_unnamed_addr {
dec_label_pc_402801:
  %v2_402801 = load i8, i8* bitcast (i32* @eax to i8*), align 4
  %v3_402801 = load i32, i32* @eax, align 4
  %v4_402801 = trunc i32 %v3_402801 to i8
  %v5_402801 = add i8 %v2_402801, %v4_402801
  %v21_402801 = inttoptr i32 %v3_402801 to i8*
  store i8 %v5_402801, i8* %v21_402801, align 1
  %v5_402803 = load i32, i32* @eax, align 4
  ret i32 %v5_402803
}

define i32 @function_40280a() local_unnamed_addr {
dec_label_pc_40280a:
  %v0_40280a = load i32, i32* @eax, align 4
  ret i32 %v0_40280a
}

define i32 @function_402816() local_unnamed_addr {
dec_label_pc_402816:
  %v0_402816 = load i32, i32* @eax, align 4
  ret i32 %v0_402816
}

define i32 @function_4028ba() local_unnamed_addr {
dec_label_pc_4028ba:
  %v0_4028ba = load i32, i32* @eax, align 4
  ret i32 %v0_4028ba
}

define i32 @function_4028cb(i32 %arg1) local_unnamed_addr {
dec_label_pc_4028cb:
  %v2_4028cb = load i8, i8* bitcast (i32* @eax to i8*), align 4
  %v3_4028cb = load i32, i32* @eax, align 4
  %v4_4028cb = trunc i32 %v3_4028cb to i8
  %v5_4028cb = add i8 %v2_4028cb, %v4_4028cb
  %v21_4028cb = inttoptr i32 %v3_4028cb to i8*
  store i8 %v5_4028cb, i8* %v21_4028cb, align 1
  %v5_4028cd = load i32, i32* @eax, align 4
  ret i32 %v5_4028cd
}

define i32 @function_4028dc() local_unnamed_addr {
dec_label_pc_4028dc:
  %v0_4028dc = load i32, i32* @eax, align 4
  ret i32 %v0_4028dc
}

define i32 @function_40296c(i32 %arg1) local_unnamed_addr {
dec_label_pc_40296c:
  %v5_40296c = load i32, i32* @eax, align 4
  ret i32 %v5_40296c
}

define i32 @function_402984() local_unnamed_addr {
dec_label_pc_402984:
  %v0_402984 = load i32, i32* @eax, align 4
  ret i32 %v0_402984
}

define i32 @function_4029ae() local_unnamed_addr {
dec_label_pc_4029ae:
  %v0_4029ae = load i32, i32* @eax, align 4
  ret i32 %v0_4029ae
}

define i32 @function_402a3d() local_unnamed_addr {
dec_label_pc_402a3d:
  %v0_402a3d = load i32, i32* @eax, align 4
  ret i32 %v0_402a3d
}

define i32 @function_402a45() local_unnamed_addr {
dec_label_pc_402a45:
  %v0_402a45 = load i32, i32* @eax, align 4
  ret i32 %v0_402a45
}

define i32 @function_402a76() local_unnamed_addr {
dec_label_pc_402a76:
  %v0_402a76 = load i32, i32* @eax, align 4
  ret i32 %v0_402a76
}

define i32 @function_402a8c() local_unnamed_addr {
dec_label_pc_402a8c:
  %v4_402a8c = load i32, i32* @eax, align 4
  ret i32 %v4_402a8c
}

define i32 @function_402a95(i32 %arg1) local_unnamed_addr {
dec_label_pc_402a95:
  %v5_402a95 = load i32, i32* @eax, align 4
  ret i32 %v5_402a95
}

define i32 @function_402b0a(i32 %arg1) local_unnamed_addr {
dec_label_pc_402b0a:
  %cf.global-to-local = alloca i1, align 1
  %v2_402b0a = load i8, i8* bitcast (i32* @eax to i8*), align 4
  %v3_402b0a = load i32, i32* @eax, align 4
  %v4_402b0a = trunc i32 %v3_402b0a to i8
  %v5_402b0a = load i1, i1* %cf.global-to-local, align 1
  %v6_402b0a = zext i1 %v5_402b0a to i8
  %v7_402b0a = add i8 %v2_402b0a, %v4_402b0a
  %v8_402b0a = add i8 %v7_402b0a, %v6_402b0a
  %v30_402b0a = inttoptr i32 %v3_402b0a to i8*
  store i8 %v8_402b0a, i8* %v30_402b0a, align 1
  %v2_402b0c = load i8, i8* bitcast (i32* @eax to i8*), align 4
  %v3_402b0c = load i32, i32* @eax, align 4
  %v4_402b0c = trunc i32 %v3_402b0c to i8
  %v5_402b0c = add i8 %v2_402b0c, %v4_402b0c
  %v10_402b0c = icmp ult i8 %v5_402b0c, %v2_402b0c
  store i1 %v10_402b0c, i1* %cf.global-to-local, align 1
  %v21_402b0c = inttoptr i32 %v3_402b0c to i8*
  store i8 %v5_402b0c, i8* %v21_402b0c, align 1
  %v5_402b0e = load i32, i32* @eax, align 4
  ret i32 %v5_402b0e
}

define i32 @function_402b12(i32 %arg1) local_unnamed_addr {
dec_label_pc_402b12:
  %v2_402b12 = load i8, i8* bitcast (i32* @eax to i8*), align 4
  %v3_402b12 = load i32, i32* @eax, align 4
  %v4_402b12 = trunc i32 %v3_402b12 to i8
  %v5_402b12 = add i8 %v2_402b12, %v4_402b12
  %v21_402b12 = inttoptr i32 %v3_402b12 to i8*
  store i8 %v5_402b12, i8* %v21_402b12, align 1
  %v2_402b14 = load i8, i8* bitcast (i32* @eax to i8*), align 4
  %v3_402b14 = load i32, i32* @eax, align 4
  %v4_402b14 = trunc i32 %v3_402b14 to i8
  %v5_402b14 = add i8 %v2_402b14, %v4_402b14
  %v21_402b14 = inttoptr i32 %v3_402b14 to i8*
  store i8 %v5_402b14, i8* %v21_402b14, align 1
  %v5_402b16 = load i32, i32* @eax, align 4
  ret i32 %v5_402b16
}

define i32 @function_402b19() local_unnamed_addr {
dec_label_pc_402b19:
  %v0_402b19 = load i32, i32* @eax, align 4
  ret i32 %v0_402b19
}

define i32 @function_402b32() local_unnamed_addr {
dec_label_pc_402b32:
  %stack_var_-4 = alloca i16, align 2
  %v2_402b34 = load i8, i8* bitcast (i32* @eax to i8*), align 4
  %v3_402b34 = load i32, i32* @eax, align 4
  %v4_402b34 = trunc i32 %v3_402b34 to i8
  %v5_402b34 = add i8 %v2_402b34, %v4_402b34
  %v21_402b34 = inttoptr i32 %v3_402b34 to i8*
  store i8 %v5_402b34, i8* %v21_402b34, align 1
  %v2_402b36 = load i8, i8* bitcast (i32* @edx to i8*), align 4
  %v3_402b36 = load i32, i32* @eax, align 4
  %v4_402b36 = trunc i32 %v3_402b36 to i8
  %v5_402b36 = add i8 %v2_402b36, %v4_402b36
  %v20_402b36 = load i32, i32* @edx, align 4
  %v21_402b36 = inttoptr i32 %v20_402b36 to i8*
  store i8 %v5_402b36, i8* %v21_402b36, align 1
  %v2_402b38 = load i8, i8* bitcast (i32* @ebx to i8*), align 4
  %v3_402b38 = load i32, i32* @eax, align 4
  %v4_402b38 = udiv i32 %v3_402b38, 256
  %v5_402b38 = trunc i32 %v4_402b38 to i8
  %v6_402b38 = sub i8 %v2_402b38, %v5_402b38
  %v21_402b38 = load i32, i32* @ebx, align 4
  %v22_402b38 = inttoptr i32 %v21_402b38 to i8*
  store i8 %v6_402b38, i8* %v22_402b38, align 1
  %v2_402b3a = load i8, i8* bitcast (i32* @eax to i8*), align 4
  %v3_402b3a = load i32, i32* @eax, align 4
  %v4_402b3a = trunc i32 %v3_402b3a to i8
  %v5_402b3a = add i8 %v2_402b3a, %v4_402b3a
  %v21_402b3a = inttoptr i32 %v3_402b3a to i8*
  store i8 %v5_402b3a, i8* %v21_402b3a, align 1
  %v0_402b3c = load i16, i16* @es, align 4
  %v0_402b3d = load i32, i32* @eax, align 4
  %v1_402b3d = udiv i32 %v0_402b3d, 256
  %v6_402b3d1 = or i32 %v1_402b3d, %v0_402b3d
  %v12_402b3d = mul i32 %v6_402b3d1, 256
  %v14_402b3d = and i32 %v12_402b3d, 65280
  %v15_402b3d = and i32 %v0_402b3d, -65281
  %v16_402b3d = or i32 %v14_402b3d, %v15_402b3d
  store i32 %v16_402b3d, i32* @eax, align 4
  store i16 %v0_402b3c, i16* %stack_var_-4, align 2
  %v1_402b40 = inttoptr i32 %v16_402b3d to i8*
  %v2_402b40 = load i8, i8* %v1_402b40, align 1
  %v4_402b40 = trunc i32 %v0_402b3d to i8
  %v5_402b40 = add i8 %v2_402b40, %v4_402b40
  store i8 %v5_402b40, i8* %v1_402b40, align 1
  %v0_402b42 = load i32, i32* @eax, align 4
  %v1_402b42 = inttoptr i32 %v0_402b42 to i8*
  %v2_402b42 = load i8, i8* %v1_402b42, align 1
  %v3_402b42 = load i32, i32* @ebx, align 4
  %v4_402b42 = udiv i32 %v3_402b42, 256
  %v5_402b42 = trunc i32 %v4_402b42 to i8
  %v6_402b42 = add i8 %v2_402b42, %v5_402b42
  store i8 %v6_402b42, i8* %v1_402b42, align 1
  store i32 956301311, i32* @ecx, align 4
  %v0_402b49 = load i16, i16* %stack_var_-4, align 2
  %v2_402b49 = call i32 @function_7ffffff(i16 %v0_402b49, i16 %v0_402b3c)
  store i32 %v2_402b49, i32* @eax, align 4
  ret i32 %v2_402b49
}

define i32 @function_402b5e() local_unnamed_addr {
dec_label_pc_402b5e:
  %v4_402b5e = load i32, i32* @eax, align 4
  ret i32 %v4_402b5e
}

define i32 @function_402b66() local_unnamed_addr {
dec_label_pc_402b66:
  %v0_402b66 = load i32, i32* @eax, align 4
  ret i32 %v0_402b66
}

define i32 @function_402bfe() local_unnamed_addr {
dec_label_pc_402bfe:
  %v4_402bfe = load i32, i32* @eax, align 4
  ret i32 %v4_402bfe
}

define i32 @function_402c07(i32 %arg1) local_unnamed_addr {
dec_label_pc_402c07:
  %v5_402c07 = load i32, i32* @eax, align 4
  ret i32 %v5_402c07
}

define i32 @function_402c12() local_unnamed_addr {
dec_label_pc_402c12:
  %v4_402c12 = load i32, i32* @eax, align 4
  ret i32 %v4_402c12
}

define i32 @function_402ca7(i32 %arg1) local_unnamed_addr {
dec_label_pc_402ca7:
  %v2_402caa = load i8, i8* bitcast (i32* @eax to i8*), align 4
  %v3_402caa = load i32, i32* @eax, align 4
  %v4_402caa = trunc i32 %v3_402caa to i8
  %v5_402caa = add i8 %v2_402caa, %v4_402caa
  %v21_402caa = inttoptr i32 %v3_402caa to i8*
  store i8 %v5_402caa, i8* %v21_402caa, align 1
  %v5_402cac = load i32, i32* @eax, align 4
  ret i32 %v5_402cac
}

define i32 @function_402caf() local_unnamed_addr {
dec_label_pc_402caf:
  %v0_402caf = load i32, i32* @eax, align 4
  ret i32 %v0_402caf
}

define i32 @function_402cbe() local_unnamed_addr {
dec_label_pc_402cbe:
  %v0_402cbe = load i32, i32* @eax, align 4
  ret i32 %v0_402cbe
}

define i32 @function_402cd0() local_unnamed_addr {
dec_label_pc_402cd0:
  %v0_402cd0 = load i32, i32* @eax, align 4
  ret i32 %v0_402cd0
}

define i32 @function_402cef(i32 %arg1) local_unnamed_addr {
dec_label_pc_402cef:
  %v5_402cef = load i32, i32* @eax, align 4
  ret i32 %v5_402cef
}

define i32 @function_402cf2() local_unnamed_addr {
dec_label_pc_402cf2:
  %v0_402cf2 = load i32, i32* @eax, align 4
  ret i32 %v0_402cf2
}

define i32 @function_402d09() local_unnamed_addr {
dec_label_pc_402d09:
  %v0_402d09 = load i32, i32* @eax, align 4
  ret i32 %v0_402d09
}

define i32 @function_402d76() local_unnamed_addr {
dec_label_pc_402d76:
  %v0_402d76 = load i32, i32* @eax, align 4
  ret i32 %v0_402d76
}

define i32 @function_402ee2() local_unnamed_addr {
dec_label_pc_402ee2:
  %v0_402ee2 = load i32, i32* @eax, align 4
  ret i32 %v0_402ee2
}

define i32 @function_402ee4() local_unnamed_addr {
dec_label_pc_402ee4:
  %cf.global-to-local = alloca i1, align 1
  %ss.global-to-local = alloca i16, align 2
  %v0_402ee4 = load i32, i32* @eax, align 4
  %v5_402ee4 = mul i32 %v0_402ee4, 2
  %v20_402ee4 = and i32 %v5_402ee4, 254
  %v22_402ee4 = and i32 %v0_402ee4, -256
  %v23_402ee4 = or i32 %v20_402ee4, %v22_402ee4
  store i32 %v23_402ee4, i32* @eax, align 4
  %v2_402ee6 = load i8, i8* bitcast (i32* @ebx to i8*), align 4
  %v3_402ee6 = load i32, i32* @ecx, align 4
  %v4_402ee6 = udiv i32 %v3_402ee6, 256
  %v5_402ee6 = trunc i32 %v4_402ee6 to i8
  %v6_402ee6 = add i8 %v2_402ee6, %v5_402ee6
  %v11_402ee6 = icmp ult i8 %v6_402ee6, %v2_402ee6
  store i1 %v11_402ee6, i1* %cf.global-to-local, align 1
  %v21_402ee6 = load i32, i32* @ebx, align 4
  %v22_402ee6 = inttoptr i32 %v21_402ee6 to i8*
  store i8 %v6_402ee6, i8* %v22_402ee6, align 1
  %v2_402ee8 = load i8, i8* bitcast (i32* @esi to i8*), align 4
  %v3_402ee8 = load i1, i1* %cf.global-to-local, align 1
  %v4_402ee8 = zext i1 %v3_402ee8 to i8
  %v5_402ee8 = add i8 %v2_402ee8, %v4_402ee8
  %v23_402ee8 = load i32, i32* @esi, align 4
  %v24_402ee8 = inttoptr i32 %v23_402ee8 to i8*
  store i8 %v5_402ee8, i8* %v24_402ee8, align 1
  %v2_402eeb = load i8, i8* bitcast (i32* @edx to i8*), align 4
  %v3_402eeb = load i32, i32* @ecx, align 4
  %v4_402eeb = trunc i32 %v3_402eeb to i8
  %v5_402eeb = add i8 %v2_402eeb, %v4_402eeb
  %v10_402eeb = icmp ult i8 %v5_402eeb, %v2_402eeb
  store i1 %v10_402eeb, i1* %cf.global-to-local, align 1
  %v20_402eeb = load i32, i32* @edx, align 4
  %v21_402eeb = inttoptr i32 %v20_402eeb to i8*
  store i8 %v5_402eeb, i8* %v21_402eeb, align 1
  %v2_402eed = load i32, i32* @ebx, align 4
  %v3_402eed = load i32, i32* @ecx, align 4
  %v4_402eed = load i1, i1* %cf.global-to-local, align 1
  %v5_402eed = zext i1 %v4_402eed to i32
  %v6_402eed = add i32 %v3_402eed, %v5_402eed
  %v7_402eed = sub i32 %v2_402eed, %v6_402eed
  %v17_402eed = sub i32 %v7_402eed, %v5_402eed
  %v18_402eed = icmp ult i32 %v2_402eed, %v17_402eed
  %v19_402eed = icmp ne i32 %v6_402eed, -1
  %v20_402eed = or i1 %v19_402eed, %v18_402eed
  %v21_402eed = icmp ult i32 %v2_402eed, %v6_402eed
  %v22_402eed = select i1 %v4_402eed, i1 %v20_402eed, i1 %v21_402eed
  store i1 %v22_402eed, i1* %cf.global-to-local, align 1
  %v37_402eed = inttoptr i32 %v2_402eed to i32*
  store i32 %v7_402eed, i32* %v37_402eed, align 4
  %v0_402eef = load i32, i32* @edi, align 4
  %v2_402eef = mul i32 %v0_402eef, 8
  %v3_402eef = add i32 %v0_402eef, 377421823
  %v4_402eef = add i32 %v3_402eef, %v2_402eef
  %v5_402eef = inttoptr i32 %v4_402eef to i8*
  %v6_402eef = load i8, i8* %v5_402eef, align 1
  %v7_402eef = load i32, i32* @eax, align 4
  %v8_402eef = udiv i32 %v7_402eef, 256
  %v9_402eef = trunc i32 %v8_402eef to i8
  %v15_402eef = icmp ult i8 %v6_402eef, %v9_402eef
  store i1 %v15_402eef, i1* %cf.global-to-local, align 1
  %v1_402ef6 = inttoptr i32 %v7_402eef to i8*
  %v2_402ef6 = load i8, i8* %v1_402ef6, align 1
  %v4_402ef6 = trunc i32 %v7_402eef to i8
  %v5_402ef6 = add i8 %v2_402ef6, %v4_402ef6
  store i8 %v5_402ef6, i8* %v1_402ef6, align 1
  %v0_402ef8 = load i32, i32* @ecx, align 4
  %v1_402ef8 = trunc i32 %v0_402ef8 to i8
  %v4_402ef8 = load i8, i8* bitcast (i32* @edx to i8*), align 4
  %v5_402ef8 = or i8 %v4_402ef8, %v1_402ef8
  store i1 false, i1* %cf.global-to-local, align 1
  %v11_402ef8 = zext i8 %v5_402ef8 to i32
  %v13_402ef8 = and i32 %v0_402ef8, -256
  %v14_402ef8 = or i32 %v13_402ef8, %v11_402ef8
  store i32 %v14_402ef8, i32* @ecx, align 4
  %v0_402efb = load i32, i32* @edx, align 4
  %v1_402efb = trunc i32 %v0_402efb to i8
  %v2_402efb = load i32, i32* @esi, align 4
  %v3_402efb = add i32 %v2_402efb, 56
  %v4_402efb = inttoptr i32 %v3_402efb to i8*
  %v5_402efb = load i8, i8* %v4_402efb, align 1
  %v6_402efb = sub i8 %v1_402efb, %v5_402efb
  %v11_402efb = icmp ugt i8 %v5_402efb, %v1_402efb
  store i1 %v11_402efb, i1* %cf.global-to-local, align 1
  %v21_402efb = zext i8 %v6_402efb to i32
  %v23_402efb = and i32 %v0_402efb, -256
  %v24_402efb = or i32 %v23_402efb, %v21_402efb
  %v25_402efb = inttoptr i32 %v24_402efb to i8*
  store i32 %v24_402efb, i32* @edx, align 4
  %v0_402efe = load i32, i32* @eax, align 4
  %v1_402efe = trunc i32 %v0_402efe to i8
  %v3_402efe = inttoptr i32 %v0_402efe to i8*
  %v4_402efe = load i8, i8* %v3_402efe, align 1
  %v5_402efe = add i8 %v4_402efe, %v1_402efe
  %v10_402efe = icmp ult i8 %v5_402efe, %v1_402efe
  store i1 %v10_402efe, i1* %cf.global-to-local, align 1
  %v20_402efe = zext i8 %v5_402efe to i32
  %v22_402efe = and i32 %v0_402efe, -256
  %v23_402efe = or i32 %v22_402efe, %v20_402efe
  store i32 %v23_402efe, i32* @eax, align 4
  %v1_402f00 = inttoptr i32 %v23_402efe to i8*
  %v2_402f00 = load i8, i8* %v1_402f00, align 1
  %v5_402f00 = add i8 %v2_402f00, %v5_402efe
  %v10_402f00 = icmp ult i8 %v5_402f00, %v2_402f00
  store i1 %v10_402f00, i1* %cf.global-to-local, align 1
  store i8 %v5_402f00, i8* %v1_402f00, align 1
  %v0_402f04 = load i32, i32* @ecx, align 4
  %v1_402f04 = udiv i32 %v0_402f04, 256
  %v2_402f04 = trunc i32 %v1_402f04 to i8
  %v3_402f04 = load i32, i32* @eax, align 4
  %v4_402f04 = inttoptr i32 %v3_402f04 to i8*
  %v5_402f04 = load i8, i8* %v4_402f04, align 1
  %v6_402f04 = add i8 %v5_402f04, %v2_402f04
  %v11_402f04 = icmp ult i8 %v6_402f04, %v2_402f04
  %v21_402f04 = zext i8 %v6_402f04 to i32
  %v23_402f04 = mul i32 %v21_402f04, 256
  %v24_402f04 = and i32 %v0_402f04, -65281
  %v25_402f04 = or i32 %v23_402f04, %v24_402f04
  store i32 %v25_402f04, i32* @ecx, align 4
  %v2_402f06 = zext i1 %v11_402f04 to i32
  %v3_402f06 = add i32 %v3_402f04, 705298432
  %v4_402f06 = add i32 %v3_402f06, %v2_402f06
  %v21_402f06 = icmp ule i32 %v4_402f06, %v3_402f04
  %v22_402f06 = icmp ugt i32 %v3_402f04, -705298433
  %v23_402f06 = select i1 %v11_402f04, i1 %v21_402f06, i1 %v22_402f06
  store i1 %v23_402f06, i1* %cf.global-to-local, align 1
  store i32 %v4_402f06, i32* @eax, align 4
  %v1_402f0b = inttoptr i32 %v4_402f06 to i8*
  %v2_402f0b = load i8, i8* %v1_402f0b, align 1
  %v4_402f0b = udiv i8 %v2_402f0b, 2
  store i8 %v4_402f0b, i8* %v1_402f0b, align 1
  %v12_402f0b = urem i8 %v2_402f0b, 2
  %v13_402f0b = icmp ne i8 %v12_402f0b, 0
  store i1 %v13_402f0b, i1* %cf.global-to-local, align 1
  %v0_402f0d = load i32, i32* @eax, align 4
  %v1_402f0d = inttoptr i32 %v0_402f0d to i8*
  %v2_402f0d = load i8, i8* %v1_402f0d, align 1
  %v4_402f0d = trunc i32 %v0_402f0d to i8
  %v5_402f0d = add i8 %v2_402f0d, %v4_402f0d
  %v10_402f0d = icmp ult i8 %v5_402f0d, %v2_402f0d
  store i1 %v10_402f0d, i1* %cf.global-to-local, align 1
  store i8 %v5_402f0d, i8* %v1_402f0d, align 1
  %v0_402f10 = load i32, i32* @eax, align 4
  %v1_402f10 = trunc i32 %v0_402f10 to i8
  %v3_402f10 = inttoptr i32 %v0_402f10 to i8*
  %v4_402f10 = load i8, i8* %v3_402f10, align 1
  %v5_402f10 = sub i8 %v1_402f10, %v4_402f10
  %v20_402f10 = zext i8 %v5_402f10 to i32
  %v22_402f10 = and i32 %v0_402f10, -256
  %v23_402f10 = or i32 %v22_402f10, %v20_402f10
  store i32 %v23_402f10, i32* @eax, align 4
  %v2_402f13 = load i8, i8* bitcast (i32* @esi to i8*), align 4
  %v4_402f13 = udiv i32 %v0_402f10, 256
  %v5_402f13 = trunc i32 %v4_402f13 to i8
  %v6_402f13 = add i8 %v2_402f13, %v5_402f13
  %v11_402f13 = icmp ult i8 %v6_402f13, %v2_402f13
  store i1 %v11_402f13, i1* %cf.global-to-local, align 1
  %v21_402f13 = load i32, i32* @esi, align 4
  %v22_402f13 = inttoptr i32 %v21_402f13 to i8*
  store i8 %v6_402f13, i8* %v22_402f13, align 1
  %v3_402f15 = load i8, i8* %v25_402efb, align 1
  %v4_402f15 = load i32, i32* @eax, align 4
  %v5_402f15 = trunc i32 %v4_402f15 to i8
  %v11_402f15 = icmp ult i8 %v3_402f15, %v5_402f15
  store i1 %v11_402f15, i1* %cf.global-to-local, align 1
  %v1_402f17 = inttoptr i32 %v4_402f15 to i8*
  %v2_402f17 = load i8, i8* %v1_402f17, align 1
  %v5_402f17 = add i8 %v2_402f17, %v5_402f15
  store i8 %v5_402f17, i8* %v1_402f17, align 1
  %v2_402f19 = load i8, i8* bitcast (i32* @esi to i8*), align 4
  %v3_402f19 = load i32, i32* @eax, align 4
  %v4_402f19 = udiv i32 %v3_402f19, 256
  %v5_402f19 = trunc i32 %v4_402f19 to i8
  %v6_402f19 = add i8 %v2_402f19, %v5_402f19
  %v11_402f19 = icmp ult i8 %v6_402f19, %v2_402f19
  store i1 %v11_402f19, i1* %cf.global-to-local, align 1
  %v21_402f19 = load i32, i32* @esi, align 4
  %v22_402f19 = inttoptr i32 %v21_402f19 to i8*
  store i8 %v6_402f19, i8* %v22_402f19, align 1
  %v4_402f33 = udiv i32 %v0_402efb, 256
  br label %dec_label_pc_402f1d

dec_label_pc_402f1d:                              ; preds = %dec_label_pc_402f1d, %dec_label_pc_402ee4
  %v0_402f1d = load i32, i32* @eax, align 4
  %v1_402f1d = trunc i32 %v0_402f1d to i8
  %v3_402f1d = inttoptr i32 %v0_402f1d to i8*
  %v4_402f1d = load i8, i8* %v3_402f1d, align 1
  %v5_402f1d = sub i8 %v1_402f1d, %v4_402f1d
  %v20_402f1d = zext i8 %v5_402f1d to i32
  %v22_402f1d = and i32 %v0_402f1d, -256
  %v23_402f1d = or i32 %v22_402f1d, %v20_402f1d
  store i32 %v23_402f1d, i32* @eax, align 4
  %v2_402f1f = load i8, i8* bitcast (i32* @esi to i8*), align 4
  %v4_402f1f = udiv i32 %v0_402f1d, 256
  %v5_402f1f = trunc i32 %v4_402f1f to i8
  %v6_402f1f = add i8 %v2_402f1f, %v5_402f1f
  %v11_402f1f = icmp ult i8 %v6_402f1f, %v2_402f1f
  store i1 %v11_402f1f, i1* %cf.global-to-local, align 1
  %v21_402f1f = load i32, i32* @esi, align 4
  %v22_402f1f = inttoptr i32 %v21_402f1f to i8*
  store i8 %v6_402f1f, i8* %v22_402f1f, align 1
  %v3_402f21 = load i8, i8* %v25_402efb, align 1
  %v4_402f21 = load i32, i32* @eax, align 4
  %v5_402f21 = trunc i32 %v4_402f21 to i8
  %v11_402f21 = icmp ult i8 %v3_402f21, %v5_402f21
  store i1 %v11_402f21, i1* %cf.global-to-local, align 1
  %v1_402f23 = inttoptr i32 %v4_402f21 to i8*
  %v2_402f23 = load i8, i8* %v1_402f23, align 1
  %v5_402f23 = add i8 %v2_402f23, %v5_402f21
  store i8 %v5_402f23, i8* %v1_402f23, align 1
  %v2_402f25 = load i8, i8* bitcast (i32* @esi to i8*), align 4
  %v3_402f25 = load i32, i32* @eax, align 4
  %v4_402f25 = udiv i32 %v3_402f25, 256
  %v5_402f25 = trunc i32 %v4_402f25 to i8
  %v6_402f25 = add i8 %v2_402f25, %v5_402f25
  %v11_402f25 = icmp ult i8 %v6_402f25, %v2_402f25
  store i1 %v11_402f25, i1* %cf.global-to-local, align 1
  %v21_402f25 = load i32, i32* @esi, align 4
  %v22_402f25 = inttoptr i32 %v21_402f25 to i8*
  store i8 %v6_402f25, i8* %v22_402f25, align 1
  %v0_402f27 = load i16, i16* %ss.global-to-local, align 2
  %v1_402f27 = load i32, i32* @esp, align 4
  %v2_402f27 = add i32 %v1_402f27, -2
  %v3_402f27 = inttoptr i32 %v2_402f27 to i16*
  store i16 %v0_402f27, i16* %v3_402f27, align 2
  %v0_402f28 = load i16, i16* %ss.global-to-local, align 2
  %v2_402f28 = add i32 %v1_402f27, -4
  %v3_402f28 = inttoptr i32 %v2_402f28 to i16*
  store i16 %v0_402f28, i16* %v3_402f28, align 2
  %v0_402f29 = load i32, i32* @eax, align 4
  %v1_402f29 = trunc i32 %v0_402f29 to i8
  %v3_402f29 = inttoptr i32 %v0_402f29 to i8*
  %v4_402f29 = load i8, i8* %v3_402f29, align 1
  %v5_402f29 = sub i8 %v1_402f29, %v4_402f29
  %v20_402f29 = zext i8 %v5_402f29 to i32
  %v22_402f29 = and i32 %v0_402f29, -256
  %v23_402f29 = or i32 %v22_402f29, %v20_402f29
  store i32 %v23_402f29, i32* @eax, align 4
  %v2_402f2b = load i8, i8* bitcast (i32* @esi to i8*), align 4
  %v5_402f2b = add i8 %v2_402f2b, %v5_402f29
  %v10_402f2b = icmp ult i8 %v5_402f2b, %v2_402f2b
  store i1 %v10_402f2b, i1* %cf.global-to-local, align 1
  %v20_402f2b = load i32, i32* @esi, align 4
  %v21_402f2b = inttoptr i32 %v20_402f2b to i8*
  store i8 %v5_402f2b, i8* %v21_402f2b, align 1
  %v0_402f2d = load i32, i32* @eax, align 4
  %v1_402f2d = trunc i32 %v0_402f2d to i8
  %v3_402f2d = inttoptr i32 %v0_402f2d to i8*
  %v4_402f2d = load i8, i8* %v3_402f2d, align 1
  %v5_402f2d = sub i8 %v1_402f2d, %v4_402f2d
  %v20_402f2d = zext i8 %v5_402f2d to i32
  %v22_402f2d = and i32 %v0_402f2d, -256
  %v23_402f2d = or i32 %v22_402f2d, %v20_402f2d
  store i32 %v23_402f2d, i32* @eax, align 4
  %v2_402f2f = load i8, i8* bitcast (i32* @ebx to i8*), align 4
  %v6_402f2f = add i8 %v2_402f2f, %v6_402efb
  %v21_402f2f = load i32, i32* @ebx, align 4
  %v22_402f2f = inttoptr i32 %v21_402f2f to i8*
  store i8 %v6_402f2f, i8* %v22_402f2f, align 1
  %v2_402f31 = load i8, i8* bitcast (i32* @esi to i8*), align 4
  %v3_402f31 = load i32, i32* @eax, align 4
  %v4_402f31 = trunc i32 %v3_402f31 to i8
  %v5_402f31 = xor i8 %v2_402f31, %v4_402f31
  %v11_402f31 = load i32, i32* @esi, align 4
  %v12_402f31 = inttoptr i32 %v11_402f31 to i8*
  store i8 %v5_402f31, i8* %v12_402f31, align 1
  %v0_402f33 = load i32, i32* @eax, align 4
  %v6_402f33 = add i32 %v0_402f33, %v4_402f33
  %v21_402f33 = urem i32 %v6_402f33, 256
  %v23_402f33 = and i32 %v0_402f33, -256
  %v24_402f33 = or i32 %v21_402f33, %v23_402f33
  store i32 %v24_402f33, i32* @eax, align 4
  %v1_402f35 = inttoptr i32 %v24_402f33 to i32*
  %v2_402f35 = load i32, i32* %v1_402f35, align 4
  %v4_402f35 = add i32 %v24_402f33, %v2_402f35
  store i32 %v4_402f35, i32* %v1_402f35, align 4
  %v2_402f37 = load i8, i8* bitcast (i32* @esi to i8*), align 4
  %v3_402f37 = load i32, i32* @ecx, align 4
  %v4_402f37 = trunc i32 %v3_402f37 to i8
  %v5_402f37 = add i8 %v2_402f37, %v4_402f37
  %v10_402f37 = icmp ult i8 %v5_402f37, %v2_402f37
  store i1 %v10_402f37, i1* %cf.global-to-local, align 1
  %v20_402f37 = load i32, i32* @esi, align 4
  %v21_402f37 = inttoptr i32 %v20_402f37 to i8*
  store i8 %v5_402f37, i8* %v21_402f37, align 1
  %v0_402f39 = load i32, i32* @eax, align 4
  %v1_402f39 = inttoptr i32 %v0_402f39 to i8*
  %v2_402f39 = load i8, i8* %v1_402f39, align 1
  %v4_402f39 = trunc i32 %v0_402f39 to i8
  %v5_402f39 = add i8 %v2_402f39, %v4_402f39
  %v10_402f39 = icmp ult i8 %v5_402f39, %v2_402f39
  store i1 %v10_402f39, i1* %cf.global-to-local, align 1
  store i8 %v5_402f39, i8* %v1_402f39, align 1
  %v0_402f3b = load i32, i32* @eax, align 4
  %v1_402f3b = inttoptr i32 %v0_402f3b to i32*
  %v2_402f3b = load i32, i32* %v1_402f3b, align 4
  %v3_402f3b = load i32, i32* @edi, align 4
  %v4_402f3b = load i1, i1* %cf.global-to-local, align 1
  %v5_402f3b = zext i1 %v4_402f3b to i32
  %v6_402f3b = add i32 %v3_402f3b, %v2_402f3b
  %v7_402f3b = add i32 %v6_402f3b, %v5_402f3b
  %v26_402f3b = icmp ule i32 %v7_402f3b, %v2_402f3b
  %v27_402f3b = icmp ult i32 %v6_402f3b, %v2_402f3b
  %v28_402f3b = select i1 %v4_402f3b, i1 %v26_402f3b, i1 %v27_402f3b
  store i1 %v28_402f3b, i1* %cf.global-to-local, align 1
  store i32 %v7_402f3b, i32* %v1_402f3b, align 4
  %v0_402f3d = load i32, i32* @eax, align 4
  %v1_402f3d = trunc i32 %v0_402f3d to i8
  %v3_402f3d = inttoptr i32 %v0_402f3d to i8*
  %v4_402f3d = load i8, i8* %v3_402f3d, align 1
  %v5_402f3d = add i8 %v4_402f3d, %v1_402f3d
  %v10_402f3d = icmp ult i8 %v5_402f3d, %v1_402f3d
  store i1 %v10_402f3d, i1* %cf.global-to-local, align 1
  %v20_402f3d = zext i8 %v5_402f3d to i32
  %v22_402f3d = and i32 %v0_402f3d, -256
  %v23_402f3d = or i32 %v22_402f3d, %v20_402f3d
  store i32 %v23_402f3d, i32* @eax, align 4
  %v1_402f3f = inttoptr i32 %v23_402f3d to i8*
  %v2_402f3f = load i8, i8* %v1_402f3f, align 1
  %v5_402f3f = add i8 %v2_402f3f, %v5_402f3d
  %v10_402f3f = icmp ult i8 %v5_402f3f, %v2_402f3f
  store i1 %v10_402f3f, i1* %cf.global-to-local, align 1
  store i8 %v5_402f3f, i8* %v1_402f3f, align 1
  %v0_402f41 = load i16, i16* %ss.global-to-local, align 2
  %v1_402f41 = load i32, i32* @esp, align 4
  %v2_402f41 = add i32 %v1_402f41, -2
  %v3_402f41 = inttoptr i32 %v2_402f41 to i16*
  store i16 %v0_402f41, i16* %v3_402f41, align 2
  %v3_402f43 = load i8, i8* %v25_402efb, align 1
  %v4_402f43 = load i32, i32* @ecx, align 4
  %v5_402f43 = trunc i32 %v4_402f43 to i8
  %v6_402f43 = and i8 %v3_402f43, %v5_402f43
  store i1 false, i1* %cf.global-to-local, align 1
  store i8 %v6_402f43, i8* %v25_402efb, align 1
  %v0_402f45 = load i32, i32* @eax, align 4
  %v1_402f45 = inttoptr i32 %v0_402f45 to i8*
  %v2_402f45 = load i8, i8* %v1_402f45, align 1
  %v4_402f45 = trunc i32 %v0_402f45 to i8
  %v5_402f45 = add i8 %v2_402f45, %v4_402f45
  %v10_402f45 = icmp ult i8 %v5_402f45, %v2_402f45
  store i1 %v10_402f45, i1* %cf.global-to-local, align 1
  store i8 %v5_402f45, i8* %v1_402f45, align 1
  %v0_402f47 = load i32, i32* @eax, align 4
  %v1_402f47 = inttoptr i32 %v0_402f47 to i8*
  %v2_402f47 = load i8, i8* %v1_402f47, align 1
  %v3_402f47 = load i32, i32* @ebx, align 4
  %v4_402f47 = udiv i32 %v3_402f47, 256
  %v5_402f47 = trunc i32 %v4_402f47 to i8
  %v6_402f47 = add i8 %v2_402f47, %v5_402f47
  %v11_402f47 = icmp ult i8 %v6_402f47, %v2_402f47
  store i1 %v11_402f47, i1* %cf.global-to-local, align 1
  store i8 %v6_402f47, i8* %v1_402f47, align 1
  %v0_402f4b = load i32, i32* @eax, align 4
  %v1_402f4b = inttoptr i32 %v0_402f4b to i8*
  %v2_402f4b = load i8, i8* %v1_402f4b, align 1
  %v4_402f4b = trunc i32 %v0_402f4b to i8
  %v5_402f4b = add i8 %v2_402f4b, %v4_402f4b
  store i8 %v5_402f4b, i8* %v1_402f4b, align 1
  %v0_402f4d = load i32, i32* @ecx, align 4
  %v1_402f4d = udiv i32 %v0_402f4d, 256
  %v5_402f4d1 = and i32 %v1_402f4d, %v0_402f4d
  %v5_402f4d = trunc i32 %v5_402f4d1 to i8
  store i1 false, i1* %cf.global-to-local, align 1
  %v7_402f4d = icmp slt i8 %v5_402f4d, 0
  %v11_402f4d = mul i32 %v5_402f4d1, 256
  %v13_402f4d = and i32 %v11_402f4d, 65280
  %v14_402f4d = and i32 %v0_402f4d, -65281
  %v15_402f4d = or i32 %v13_402f4d, %v14_402f4d
  store i32 %v15_402f4d, i32* @ecx, align 4
  %v1_402f4f = icmp eq i1 %v7_402f4d, false
  br i1 %v1_402f4f, label %dec_label_pc_402f1d, label %dec_label_pc_402f51

dec_label_pc_402f51:                              ; preds = %dec_label_pc_402f1d
  %v2_402f51 = trunc i32 %v24_402efb to i16
  %v3_402f51 = call i32 @__asm_insd(i16 %v2_402f51)
  %v4_402f51 = load i32, i32* @edi, align 4
  %v5_402f51 = inttoptr i32 %v4_402f51 to i32*
  store i32 %v3_402f51, i32* %v5_402f51, align 4
  %v0_402f52 = load i32, i32* @eax, align 4
  %v1_402f52 = load i1, i1* %cf.global-to-local, align 1
  %v3_402f52 = select i1 %v1_402f52, i32 17193, i32 17192
  %v4_402f52 = sub i32 %v0_402f52, %v3_402f52
  store i32 %v4_402f52, i32* @eax, align 4
  %v0_402f57 = load i16, i16* @es, align 4
  %v1_402f57 = load i32, i32* @esp, align 4
  %v2_402f57 = add i32 %v1_402f57, -2
  %v3_402f57 = inttoptr i32 %v2_402f57 to i16*
  store i16 %v0_402f57, i16* %v3_402f57, align 2
  %v0_402f58 = load i32, i32* @eax, align 4
  %v1_402f58 = trunc i32 %v0_402f58 to i8
  %v2_402f58 = or i8 %v1_402f58, 32
  %v8_402f58 = zext i8 %v2_402f58 to i32
  %v10_402f58 = and i32 %v0_402f58, -939524352
  %v11_402f58 = or i32 %v10_402f58, %v8_402f58
  %v1_402f5a = or i32 %v11_402f58, 939524096
  store i1 false, i1* %cf.global-to-local, align 1
  store i32 %v1_402f5a, i32* @eax, align 4
  %v0_402f5f = load i32, i32* @ebx, align 4
  %v2_402f5f = load i32, i32* @esi, align 4
  %v5_402f5f = add i32 %v2_402f5f, %v0_402f5f
  %tmp235 = urem i32 %v5_402f5f, 65536
  %v6_402f5f = inttoptr i32 %tmp235 to i8*
  %v7_402f5f = load i8, i8* %v6_402f5f, align 1
  %v10_402f5f = add i8 %v7_402f5f, %v2_402f58
  %v15_402f5f = icmp ult i8 %v10_402f5f, %v7_402f5f
  store i1 %v15_402f5f, i1* %cf.global-to-local, align 1
  store i8 %v10_402f5f, i8* %v6_402f5f, align 1
  %v0_402f62 = load i32, i32* @eax, align 4
  %v1_402f62 = inttoptr i32 %v0_402f62 to i8*
  %v2_402f62 = load i8, i8* %v1_402f62, align 1
  %v3_402f62 = load i32, i32* @ebx, align 4
  %v4_402f62 = udiv i32 %v3_402f62, 256
  %v5_402f62 = trunc i32 %v4_402f62 to i8
  %v6_402f62 = add i8 %v2_402f62, %v5_402f62
  store i8 %v6_402f62, i8* %v1_402f62, align 1
  %v0_402f64 = load i32, i32* @eax, align 4
  %v1_402f64 = load i32, i32* @ecx, align 4
  %v2_402f64 = inttoptr i32 %v1_402f64 to i32*
  %v3_402f64 = load i32, i32* %v2_402f64, align 4
  %v4_402f64 = or i32 %v3_402f64, %v0_402f64
  store i1 false, i1* %cf.global-to-local, align 1
  %v7_402f64 = trunc i32 %v4_402f64 to i8
  store i32 %v4_402f64, i32* @eax, align 4
  %v1_402f66 = inttoptr i32 %v4_402f64 to i8*
  %v2_402f66 = load i8, i8* %v1_402f66, align 1
  %v5_402f66 = add i8 %v2_402f66, %v7_402f64
  store i8 %v5_402f66, i8* %v1_402f66, align 1
  %v0_402f68 = load i32, i32* @ebx, align 4
  %v2_402f68 = load i32, i32* @ecx, align 4
  %v4_402f682 = and i32 %v2_402f68, %v0_402f68
  store i1 false, i1* %cf.global-to-local, align 1
  %v10_402f68 = urem i32 %v4_402f682, 256
  %v12_402f68 = and i32 %v0_402f68, -256
  %v13_402f68 = or i32 %v10_402f68, %v12_402f68
  store i32 %v13_402f68, i32* @ebx, align 4
  %v0_402f6a = call i32 @function_28589d3b()
  store i32 %v0_402f6a, i32* @eax, align 4
  ret i32 %v0_402f6a
}

define i32 @function_402f6f() local_unnamed_addr {
dec_label_pc_402f6f:
  %cf.global-to-local = alloca i1, align 1
  %st7.global-to-local = alloca x86_fp80, align 4
  %stack_var_-4 = alloca i16, align 2
  %stack_var_-2 = alloca i16, align 2
  %v0_402f6f = load i32, i32* @ebx, align 4
  %v1_402f6f = add i32 %v0_402f6f, 1
  store i32 %v1_402f6f, i32* @ebx, align 4
  %v2_402f70 = load i8, i8* bitcast (i32* @eax to i8*), align 4
  %v3_402f70 = load i32, i32* @eax, align 4
  %v4_402f70 = trunc i32 %v3_402f70 to i8
  %v5_402f70 = add i8 %v2_402f70, %v4_402f70
  %v10_402f70 = icmp ult i8 %v5_402f70, %v2_402f70
  store i1 %v10_402f70, i1* %cf.global-to-local, align 1
  %v21_402f70 = inttoptr i32 %v3_402f70 to i8*
  store i8 %v5_402f70, i8* %v21_402f70, align 1
  %v0_402f72 = load i16, i16* @es, align 4
  store i16 %v0_402f72, i16* %stack_var_-2, align 2
  %v0_402f73 = load i32, i32* @eax, align 4
  %v4_402f73 = load i1, i1* %cf.global-to-local, align 1
  %v5_402f73 = zext i1 %v4_402f73 to i32
  %v6_402f73 = mul i32 %v0_402f73, 2
  %v7_402f73 = or i32 %v6_402f73, %v5_402f73
  %v20_402f73 = trunc i32 %v7_402f73 to i8
  %v26_402f73 = icmp ule i32 %v7_402f73, %v0_402f73
  %v27_402f73 = icmp ult i32 %v6_402f73, %v0_402f73
  %v28_402f73 = select i1 %v4_402f73, i1 %v26_402f73, i1 %v27_402f73
  store i1 %v28_402f73, i1* %cf.global-to-local, align 1
  store i32 %v7_402f73, i32* @eax, align 4
  %v1_402f76 = inttoptr i32 %v7_402f73 to i8*
  %v2_402f76 = load i8, i8* %v1_402f76, align 1
  %v6_402f76 = zext i1 %v28_402f73 to i8
  %v7_402f76 = add i8 %v2_402f76, %v20_402f73
  %v8_402f76 = add i8 %v7_402f76, %v6_402f76
  %v26_402f76 = icmp ule i8 %v8_402f76, %v2_402f76
  %v27_402f76 = icmp ult i8 %v7_402f76, %v2_402f76
  %v28_402f76 = select i1 %v28_402f73, i1 %v26_402f76, i1 %v27_402f76
  store i1 %v28_402f76, i1* %cf.global-to-local, align 1
  store i8 %v8_402f76, i8* %v1_402f76, align 1
  %v0_402f78 = load i32, i32* @eax, align 4
  %v1_402f78 = inttoptr i32 %v0_402f78 to i8*
  %v2_402f78 = load i8, i8* %v1_402f78, align 1
  %v4_402f78 = trunc i32 %v0_402f78 to i8
  %v5_402f78 = add i8 %v2_402f78, %v4_402f78
  %v10_402f78 = icmp ult i8 %v5_402f78, %v2_402f78
  store i1 %v10_402f78, i1* %cf.global-to-local, align 1
  store i8 %v5_402f78, i8* %v1_402f78, align 1
  %v0_402f7a = load i32, i32* @ebx, align 4
  %v1_402f7a = inttoptr i32 %v0_402f7a to i8*
  %v2_402f7a = load i8, i8* %v1_402f7a, align 1
  %v3_402f7a = load i32, i32* @ecx, align 4
  %v4_402f7a = trunc i32 %v3_402f7a to i8
  %v10_402f7a = icmp ult i8 %v2_402f7a, %v4_402f7a
  store i1 %v10_402f7a, i1* %cf.global-to-local, align 1
  %v0_402f7d = load i32, i32* @eax, align 4
  %v1_402f7d = inttoptr i32 %v0_402f7d to i8*
  %v2_402f7d = load i8, i8* %v1_402f7d, align 1
  %v4_402f7d = trunc i32 %v0_402f7d to i8
  %v5_402f7d = add i8 %v2_402f7d, %v4_402f7d
  %v10_402f7d = icmp ult i8 %v5_402f7d, %v2_402f7d
  store i1 %v10_402f7d, i1* %cf.global-to-local, align 1
  store i8 %v5_402f7d, i8* %v1_402f7d, align 1
  %v0_402f7f = load i32, i32* @eax, align 4
  %v1_402f7f = inttoptr i32 %v0_402f7f to i8*
  %v2_402f7f = load i8, i8* %v1_402f7f, align 1
  %v3_402f7f = load i32, i32* @ecx, align 4
  %v4_402f7f = udiv i32 %v3_402f7f, 256
  %v5_402f7f = trunc i32 %v4_402f7f to i8
  %v6_402f7f = or i8 %v2_402f7f, %v5_402f7f
  store i1 false, i1* %cf.global-to-local, align 1
  store i8 %v6_402f7f, i8* %v1_402f7f, align 1
  %v0_402f81 = load i32, i32* @eax, align 4
  %v1_402f81 = inttoptr i32 %v0_402f81 to i8*
  %v2_402f81 = load i8, i8* %v1_402f81, align 1
  %v4_402f81 = trunc i32 %v0_402f81 to i8
  %v5_402f81 = add i8 %v2_402f81, %v4_402f81
  store i8 %v5_402f81, i8* %v1_402f81, align 1
  %v0_402f84 = load i16, i16* @es, align 4
  store i16 %v0_402f84, i16* %stack_var_-4, align 2
  %v1_402f85 = load i32, i32* @eax, align 4
  %v2_402f85 = inttoptr i32 %v1_402f85 to i32*
  %v2_402f87 = load i32, i32* %v2_402f85, align 4
  %v4_402f87 = or i32 %v2_402f87, %v1_402f85
  store i1 false, i1* %cf.global-to-local, align 1
  store i32 %v4_402f87, i32* %v2_402f85, align 4
  %v0_402f89 = load i32, i32* @eax, align 4
  %v1_402f89 = inttoptr i32 %v0_402f89 to i8*
  %v2_402f89 = load i8, i8* %v1_402f89, align 1
  %v4_402f89 = trunc i32 %v0_402f89 to i8
  %v5_402f89 = add i8 %v2_402f89, %v4_402f89
  store i8 %v5_402f89, i8* %v1_402f89, align 1
  %v2_402f8b = load i8, i8* bitcast (i32* @edx to i8*), align 4
  %v3_402f8b = load i32, i32* @ebx, align 4
  %v4_402f8b = udiv i32 %v3_402f8b, 256
  %v5_402f8b = trunc i32 %v4_402f8b to i8
  %v11_402f8b = icmp ult i8 %v2_402f8b, %v5_402f8b
  store i1 %v11_402f8b, i1* %cf.global-to-local, align 1
  %v0_402f8d = load i32, i32* @eax, align 4
  %v1_402f8d = inttoptr i32 %v0_402f8d to i8*
  %v2_402f8d = load i8, i8* %v1_402f8d, align 1
  %v4_402f8d = trunc i32 %v0_402f8d to i8
  %v5_402f8d = add i8 %v2_402f8d, %v4_402f8d
  %v10_402f8d = icmp ult i8 %v5_402f8d, %v2_402f8d
  store i1 %v10_402f8d, i1* %cf.global-to-local, align 1
  store i8 %v5_402f8d, i8* %v1_402f8d, align 1
  %v0_402f8f = load i32, i32* @eax, align 4
  %v1_402f8f = inttoptr i32 %v0_402f8f to i8*
  %v2_402f8f = load i8, i8* %v1_402f8f, align 1
  %v3_402f8f = load i32, i32* @ebx, align 4
  %v4_402f8f = udiv i32 %v3_402f8f, 256
  %v5_402f8f = trunc i32 %v4_402f8f to i8
  %v6_402f8f = add i8 %v2_402f8f, %v5_402f8f
  %v11_402f8f = icmp ult i8 %v6_402f8f, %v2_402f8f
  store i1 %v11_402f8f, i1* %cf.global-to-local, align 1
  store i8 %v6_402f8f, i8* %v1_402f8f, align 1
  %v1_402f91 = load x86_fp80, x86_fp80* %st7.global-to-local, align 4
  %v2_402f91 = load i32, i32* @eax, align 4
  %v3_402f91 = inttoptr i32 %v2_402f91 to i16*
  %v4_402f91 = load i16, i16* %v3_402f91, align 2
  %v5_402f91 = sitofp i16 %v4_402f91 to x86_fp80
  %v6_402f91 = fadd x86_fp80 %v1_402f91, %v5_402f91
  store x86_fp80 %v6_402f91, x86_fp80* %st7.global-to-local, align 4
  %v1_402f93 = inttoptr i32 %v2_402f91 to i8*
  %v2_402f93 = load i8, i8* %v1_402f93, align 1
  %v4_402f93 = trunc i32 %v2_402f91 to i8
  %v5_402f93 = add i8 %v2_402f93, %v4_402f93
  store i8 %v5_402f93, i8* %v1_402f93, align 1
  %v0_402f95 = load i32, i32* @eax, align 4
  %v2_402f95 = load i32, i32* @edx, align 4
  %v3_402f95 = udiv i32 %v2_402f95, 256
  %v5_402f951 = and i32 %v3_402f95, %v0_402f95
  store i1 false, i1* %cf.global-to-local, align 1
  %v11_402f95 = urem i32 %v5_402f951, 256
  %v13_402f95 = and i32 %v0_402f95, -256
  %v14_402f95 = or i32 %v11_402f95, %v13_402f95
  store i32 %v14_402f95, i32* @eax, align 4
  %v0_402f97 = load i16, i16* %stack_var_-4, align 2
  %v1_402f97 = load i16, i16* %stack_var_-2, align 2
  %v2_402f97 = call i32 @function_28589d68(i16 %v0_402f97, i16 %v1_402f97)
  store i32 %v2_402f97, i32* @eax, align 4
  ret i32 %v2_402f97
}

define i32 @function_402f9c() local_unnamed_addr {
dec_label_pc_402f9c:
  %v0_402f9c = load i32, i32* @ebx, align 4
  %v1_402f9c = add i32 %v0_402f9c, 1
  store i32 %v1_402f9c, i32* @ebx, align 4
  %v2_402f9d = load i8, i8* bitcast (i32* @eax to i8*), align 4
  %v3_402f9d = load i32, i32* @eax, align 4
  %v4_402f9d = trunc i32 %v3_402f9d to i8
  %v5_402f9d = add i8 %v2_402f9d, %v4_402f9d
  %v21_402f9d = inttoptr i32 %v3_402f9d to i8*
  store i8 %v5_402f9d, i8* %v21_402f9d, align 1
  %v0_402fa0 = load i32, i32* @eax, align 4
  %v1_402fa0 = or i32 %v0_402fa0, 1056
  store i32 %v1_402fa0, i32* @eax, align 4
  %v1_402fa5 = inttoptr i32 %v1_402fa0 to i8*
  %v2_402fa5 = load i8, i8* %v1_402fa5, align 1
  %v3_402fa5 = load i32, i32* @ebx, align 4
  %v4_402fa5 = udiv i32 %v3_402fa5, 256
  %v5_402fa5 = trunc i32 %v4_402fa5 to i8
  %v6_402fa5 = add i8 %v2_402fa5, %v5_402fa5
  store i8 %v6_402fa5, i8* %v1_402fa5, align 1
  %v0_402fa8 = load i32, i32* @eax, align 4
  %v1_402fa8 = inttoptr i32 %v0_402fa8 to i8*
  %v2_402fa8 = load i8, i8* %v1_402fa8, align 1
  %v4_402fa8 = trunc i32 %v0_402fa8 to i8
  %v5_402fa8 = add i8 %v2_402fa8, %v4_402fa8
  store i8 %v5_402fa8, i8* %v1_402fa8, align 1
  %v0_402faa = load i32, i32* @eax, align 4
  %v1_402faa = inttoptr i32 %v0_402faa to i8*
  %v2_402faa = load i8, i8* %v1_402faa, align 1
  %v4_402faa = udiv i32 %v0_402faa, 256
  %v5_402faa = trunc i32 %v4_402faa to i8
  %v6_402faa = add i8 %v2_402faa, %v5_402faa
  store i8 %v6_402faa, i8* %v1_402faa, align 1
  %v0_402fac = call i32 @__asm_iretd()
  store i32 %v0_402fac, i32* @eax, align 4
  %v0_402fad = call i32 @function_285c9d7e()
  store i32 %v0_402fad, i32* @eax, align 4
  ret i32 %v0_402fad
}

define i32 @function_402fb2() local_unnamed_addr {
dec_label_pc_402fb2:
  %cf.global-to-local = alloca i1, align 1
  %stack_var_-2 = alloca i16, align 2
  %v0_402fb2 = load i32, i32* @ebx, align 4
  %v1_402fb2 = add i32 %v0_402fb2, 1
  store i32 %v1_402fb2, i32* @ebx, align 4
  %v2_402fb3 = load i8, i8* bitcast (i32* @eax to i8*), align 4
  %v3_402fb3 = load i32, i32* @eax, align 4
  %v4_402fb3 = trunc i32 %v3_402fb3 to i8
  %v5_402fb3 = add i8 %v2_402fb3, %v4_402fb3
  %v10_402fb3 = icmp ult i8 %v5_402fb3, %v2_402fb3
  store i1 %v10_402fb3, i1* %cf.global-to-local, align 1
  %v21_402fb3 = inttoptr i32 %v3_402fb3 to i8*
  store i8 %v5_402fb3, i8* %v21_402fb3, align 1
  %v0_402fb5 = load i16, i16* @es, align 4
  store i16 %v0_402fb5, i16* %stack_var_-2, align 2
  %v0_402fb6 = load i32, i32* @eax, align 4
  %v1_402fb6 = load i32, i32* inttoptr (i32 79416 to i32*), align 8
  %v2_402fb6 = load i1, i1* %cf.global-to-local, align 1
  %v3_402fb6 = zext i1 %v2_402fb6 to i32
  %v4_402fb6 = add i32 %v1_402fb6, %v0_402fb6
  %v5_402fb6 = add i32 %v4_402fb6, %v3_402fb6
  %v24_402fb6 = icmp ule i32 %v5_402fb6, %v0_402fb6
  %v25_402fb6 = icmp ult i32 %v4_402fb6, %v0_402fb6
  %v26_402fb6 = select i1 %v2_402fb6, i1 %v24_402fb6, i1 %v25_402fb6
  store i1 %v26_402fb6, i1* %cf.global-to-local, align 1
  store i32 %v5_402fb6, i32* @eax, align 4
  %v1_402fbc = inttoptr i32 %v5_402fb6 to i8*
  %v2_402fbc = load i8, i8* %v1_402fbc, align 1
  %v4_402fbc = udiv i32 %v5_402fb6, 256
  %v5_402fbc = trunc i32 %v4_402fbc to i8
  %v6_402fbc = add i8 %v2_402fbc, %v5_402fbc
  %v11_402fbc = icmp ult i8 %v6_402fbc, %v2_402fbc
  store i1 %v11_402fbc, i1* %cf.global-to-local, align 1
  store i8 %v6_402fbc, i8* %v1_402fbc, align 1
  %v0_402fbe = load i32, i32* @eax, align 4
  %v1_402fbe = trunc i32 %v0_402fbe to i8
  %v3_402fbe = inttoptr i32 %v0_402fbe to i8*
  %v4_402fbe = load i8, i8* %v3_402fbe, align 1
  %v5_402fbe = or i8 %v4_402fbe, %v1_402fbe
  store i1 false, i1* %cf.global-to-local, align 1
  %v11_402fbe = zext i8 %v5_402fbe to i32
  %v13_402fbe = and i32 %v0_402fbe, -256
  %v14_402fbe = or i32 %v13_402fbe, %v11_402fbe
  store i32 %v14_402fbe, i32* @eax, align 4
  %v1_402fc0 = inttoptr i32 %v14_402fbe to i8*
  %v2_402fc0 = load i8, i8* %v1_402fc0, align 1
  %v5_402fc0 = add i8 %v2_402fc0, %v5_402fbe
  store i8 %v5_402fc0, i8* %v1_402fc0, align 1
  %v2_402fc2 = load i8, i8* bitcast (i32* @esi to i8*), align 4
  %v3_402fc2 = add i8 %v2_402fc2, -1
  %v16_402fc2 = load i32, i32* @esi, align 4
  %v17_402fc2 = inttoptr i32 %v16_402fc2 to i8*
  store i8 %v3_402fc2, i8* %v17_402fc2, align 1
  %v2_402fc4 = load i16, i16* %stack_var_-2, align 2
  store i16 %v2_402fc4, i16* @es, align 4
  %v0_402fc5 = load i32, i32* @edx, align 4
  %v3_402fc5 = load i32, i32* @ebx, align 4
  %v4_402fc5 = and i32 %v3_402fc5, 65280
  %v1_402fc52 = add i32 %v4_402fc5, %v0_402fc5
  %v23_402fc5 = and i32 %v1_402fc52, 65280
  %v24_402fc5 = and i32 %v0_402fc5, -65281
  %v25_402fc5 = or i32 %v23_402fc5, %v24_402fc5
  store i32 %v25_402fc5, i32* @edx, align 4
  %v0_402fc7 = load i32, i32* @eax, align 4
  %v1_402fc7 = trunc i32 %v0_402fc7 to i8
  %v2_402fc7 = or i8 %v1_402fc7, 7
  store i1 false, i1* %cf.global-to-local, align 1
  %v8_402fc7 = zext i8 %v2_402fc7 to i32
  %v10_402fc7 = and i32 %v0_402fc7, -256
  %v11_402fc7 = or i32 %v10_402fc7, %v8_402fc7
  store i32 %v11_402fc7, i32* @eax, align 4
  %v0_402fc9 = load i32, i32* @ebp, align 4
  %v1_402fc9 = add i32 %v0_402fc9, 18
  %v2_402fc9 = inttoptr i32 %v1_402fc9 to i8*
  %v3_402fc9 = load i8, i8* %v2_402fc9, align 1
  %v6_402fc9 = add i8 %v3_402fc9, %v2_402fc7
  %v11_402fc9 = icmp ult i8 %v6_402fc9, %v3_402fc9
  store i1 %v11_402fc9, i1* %cf.global-to-local, align 1
  store i8 %v6_402fc9, i8* %v2_402fc9, align 1
  %v0_402fcc = load i32, i32* @eax, align 4
  %v1_402fcc = inttoptr i32 %v0_402fcc to i8*
  %v2_402fcc = load i8, i8* %v1_402fcc, align 1
  %v4_402fcc = trunc i32 %v0_402fcc to i8
  %v5_402fcc = add i8 %v2_402fcc, %v4_402fcc
  %v10_402fcc = icmp ult i8 %v5_402fcc, %v2_402fcc
  store i1 %v10_402fcc, i1* %cf.global-to-local, align 1
  store i8 %v5_402fcc, i8* %v1_402fcc, align 1
  %v0_402fce = load i32, i32* @edi, align 4
  %v2_402fce = mul i32 %v0_402fce, 8
  %v3_402fce = add i32 %v0_402fce, -1
  %v4_402fce = add i32 %v3_402fce, %v2_402fce
  %v5_402fce = inttoptr i32 %v4_402fce to i8*
  %v6_402fce = load i8, i8* %v5_402fce, align 1
  %v7_402fce = load i32, i32* @ecx, align 4
  %v8_402fce = trunc i32 %v7_402fce to i8
  %v9_402fce = add i8 %v6_402fce, %v8_402fce
  %v14_402fce = icmp ult i8 %v9_402fce, %v6_402fce
  store i1 %v14_402fce, i1* %cf.global-to-local, align 1
  store i8 %v9_402fce, i8* %v5_402fce, align 1
  %v0_402fd2 = load i32, i32* @eax, align 4
  ret i32 %v0_402fd2
}

define i32 @function_403025() local_unnamed_addr {
dec_label_pc_403025:
  %v0_403025 = load i32, i32* @eax, align 4
  ret i32 %v0_403025
}

define i32 @function_40303d() local_unnamed_addr {
dec_label_pc_40303d:
  %v0_40303d = load i32, i32* @eax, align 4
  ret i32 %v0_40303d
}

define i32 @function_403052() local_unnamed_addr {
dec_label_pc_403052:
  %v4_403052 = load i32, i32* @eax, align 4
  ret i32 %v4_403052
}

define i32 @function_403064() local_unnamed_addr {
dec_label_pc_403064:
  %v0_403064 = load i32, i32* @eax, align 4
  ret i32 %v0_403064
}

define i32 @function_403078() local_unnamed_addr {
dec_label_pc_403078:
  %v2_403078 = load i8, i8* bitcast (i32* @eax to i8*), align 4
  %v3_403078 = load i32, i32* @eax, align 4
  %v4_403078 = trunc i32 %v3_403078 to i8
  %v5_403078 = add i8 %v2_403078, %v4_403078
  %v21_403078 = inttoptr i32 %v3_403078 to i8*
  store i8 %v5_403078, i8* %v21_403078, align 1
  %v0_40307a = call i32 @function_a940307d()
  store i32 %v0_40307a, i32* @eax, align 4
  ret i32 %v0_40307a
}

define i32 @function_403095() local_unnamed_addr {
dec_label_pc_403095:
  %v0_403095 = load i32, i32* @eax, align 4
  ret i32 %v0_403095
}

define i32 @function_4030cb() local_unnamed_addr {
dec_label_pc_4030cb:
  %v0_4030cb = load i32, i32* @eax, align 4
  ret i32 %v0_4030cb
}

define i32 @function_4030e3() local_unnamed_addr {
dec_label_pc_4030e3:
  %v0_4030e3 = load i32, i32* @eax, align 4
  ret i32 %v0_4030e3
}

define i32 @function_40311f() local_unnamed_addr {
dec_label_pc_40311f:
  %v0_40311f = load i32, i32* @eax, align 4
  ret i32 %v0_40311f
}

define i32 @function_40312a() local_unnamed_addr {
dec_label_pc_40312a:
  %v4_40312a = load i32, i32* @eax, align 4
  ret i32 %v4_40312a
}

define i32 @function_403165() local_unnamed_addr {
dec_label_pc_403165:
  %v0_403165 = load i32, i32* @eax, align 4
  ret i32 %v0_403165
}

define i32 @function_403168() local_unnamed_addr {
dec_label_pc_403168:
  %v0_403168 = call i32 @function_285d9f39()
  store i32 %v0_403168, i32* @eax, align 4
  ret i32 %v0_403168
}

define i32 @function_40316d(i16 %arg1) local_unnamed_addr {
dec_label_pc_40316d:
  %cf.global-to-local = alloca i1, align 1
  %cs.global-to-local = alloca i16, align 2
  %eflags.global-to-local = alloca i32, align 4
  %v0_40316d = load i32, i32* @eax, align 4
  %v13_40316d = and i32 %v0_40316d, -256
  store i32 %v13_40316d, i32* @eax, align 4
  %v2_40316f = load i8, i8* bitcast (i32* @esi to i8*), align 4
  store i1 false, i1* %cf.global-to-local, align 1
  %v20_40316f = load i32, i32* @esi, align 4
  %v21_40316f = inttoptr i32 %v20_40316f to i8*
  store i8 %v2_40316f, i8* %v21_40316f, align 1
  %v0_403171 = load i32, i32* @edx, align 4
  %v1_403171 = add i32 %v0_403171, -23
  %v2_403171 = inttoptr i32 %v1_403171 to i8*
  %v3_403171 = load i8, i8* %v2_403171, align 1
  %v4_403171 = load i32, i32* @ecx, align 4
  %v5_403171 = trunc i32 %v4_403171 to i8
  %v6_403171 = and i8 %v3_403171, %v5_403171
  store i1 false, i1* %cf.global-to-local, align 1
  store i8 %v6_403171, i8* %v2_403171, align 1
  %v0_403174 = call i32 @__asm_int3()
  store i32 %v0_403174, i32* @eax, align 4
  %v0_403175 = load i32, i32* @edx, align 4
  %v1_403175 = trunc i32 %v0_403175 to i16
  %v2_403175 = call i32 @__asm_insd(i16 %v1_403175)
  %v3_403175 = load i32, i32* @edi, align 4
  %v4_403175 = inttoptr i32 %v3_403175 to i32*
  store i32 %v2_403175, i32* %v4_403175, align 4
  %v0_403176 = load i32, i32* @ebp, align 4
  %v1_403176 = load i32, i32* @eax, align 4
  %v2_403176 = inttoptr i32 %v1_403176 to i32*
  %v3_403176 = load i32, i32* %v2_403176, align 4
  %v4_403176 = load i1, i1* %cf.global-to-local, align 1
  %v5_403176 = zext i1 %v4_403176 to i32
  %v6_403176 = add i32 %v3_403176, %v5_403176
  %v7_403176 = sub i32 %v0_403176, %v6_403176
  %v17_403176 = sub i32 %v7_403176, %v5_403176
  %v18_403176 = icmp ult i32 %v0_403176, %v17_403176
  %v19_403176 = icmp ne i32 %v6_403176, -1
  %v20_403176 = or i1 %v19_403176, %v18_403176
  %v21_403176 = icmp ult i32 %v0_403176, %v6_403176
  %v22_403176 = select i1 %v4_403176, i1 %v20_403176, i1 %v21_403176
  store i1 %v22_403176, i1* %cf.global-to-local, align 1
  store i32 %v7_403176, i32* @ebp, align 4
  %v1_403178 = trunc i32 %v1_403176 to i8
  %v3_403178 = inttoptr i32 %v1_403176 to i8*
  %v4_403178 = load i8, i8* %v3_403178, align 1
  %v5_403178 = xor i8 %v4_403178, %v1_403178
  %v11_403178 = zext i8 %v5_403178 to i32
  %v13_403178 = and i32 %v1_403176, -256
  %v14_403178 = or i32 %v13_403178, %v11_403178
  store i32 %v14_403178, i32* @eax, align 4
  %v2_40317a = load i8, i8* bitcast (i32* @esi to i8*), align 4
  %v5_40317a = add i8 %v2_40317a, %v5_403178
  %v10_40317a = icmp ult i8 %v5_40317a, %v2_40317a
  store i1 %v10_40317a, i1* %cf.global-to-local, align 1
  %v20_40317a = load i32, i32* @esi, align 4
  %v21_40317a = inttoptr i32 %v20_40317a to i8*
  store i8 %v5_40317a, i8* %v21_40317a, align 1
  %v0_40317c = load i1, i1* %cf.global-to-local, align 1
  %v1_40317c = icmp eq i1 %v0_40317c, false
  %v0_403196 = load i32, i32* @eax, align 4
  %v1_403196 = inttoptr i32 %v0_403196 to i8*
  %v2_403196 = load i8, i8* %v1_403196, align 1
  %v4_403196 = trunc i32 %v0_403196 to i8
  %v5_403196 = add i8 %v2_403196, %v4_403196
  %v10_403196 = icmp ult i8 %v5_403196, %v2_403196
  store i1 %v10_403196, i1* %cf.global-to-local, align 1
  store i8 %v5_403196, i8* %v1_403196, align 1
  br i1 %v1_40317c, label %dec_label_pc_403196, label %dec_label_pc_40317e

dec_label_pc_40317e:                              ; preds = %dec_label_pc_40316d
  %v0_403180 = load i32, i32* @ebx, align 4
  %v1_403180 = udiv i32 %v0_403180, 256
  %v2_403180 = trunc i32 %v1_403180 to i8
  %v3_403180 = load i32, i32* @edx, align 4
  %v4_403180 = add i32 %v3_403180, 6
  %v5_403180 = inttoptr i32 %v4_403180 to i8*
  %v6_403180 = load i8, i8* %v5_403180, align 1
  %v7_403180 = or i8 %v6_403180, %v2_403180
  %v13_403180 = zext i8 %v7_403180 to i32
  %v15_403180 = mul i32 %v13_403180, 256
  %v16_403180 = and i32 %v0_403180, -65281
  %v17_403180 = or i32 %v15_403180, %v16_403180
  store i32 %v17_403180, i32* @ebx, align 4
  %v2_403183 = load i32, i32* @edi, align 4
  %v4_403183 = or i32 %v2_403183, %v3_403180
  store i1 false, i1* %cf.global-to-local, align 1
  %v12_403183 = inttoptr i32 %v2_403183 to i32*
  store i32 %v4_403183, i32* %v12_403183, align 4
  %v0_403185 = load i32, i32* @eax, align 4
  %v1_403185 = inttoptr i32 %v0_403185 to i16*
  %v2_403185 = load i16, i16* %v1_403185, align 2
  %v3_403185 = load i32, i32* @ecx, align 4
  %v4_403185 = trunc i32 %v3_403185 to i16
  call void @__asm_arpl(i16 %v2_403185, i16 %v4_403185)
  %v0_403187 = load i32, i32* @eax, align 4
  %v1_40318a = trunc i32 %v0_403187 to i8
  %v2_40318a = add i8 %v1_40318a, 96
  %v15_40318a = zext i8 %v2_40318a to i32
  %v17_40318a = and i32 %v0_403187, -65536
  %v18_40318a = or i32 %v17_40318a, %v15_40318a
  %v2_40318c = or i32 %v18_40318a, 39936
  store i32 %v2_40318c, i32* @eax, align 4
  %v2_40318e = load i8, i8* bitcast (i32* @esi to i8*), align 4
  %v5_40318e = and i8 %v2_40318e, %v2_40318a
  store i1 false, i1* %cf.global-to-local, align 1
  %v11_40318e = load i32, i32* @esi, align 4
  %v12_40318e = inttoptr i32 %v11_40318e to i8*
  store i8 %v5_40318e, i8* %v12_40318e, align 1
  %v0_403190 = load i32, i32* @eax, align 4
  %v1_403190 = inttoptr i32 %v0_403190 to i8*
  %v2_403190 = load i8, i8* %v1_403190, align 1
  %v4_403190 = trunc i32 %v0_403190 to i8
  %v5_403190 = add i8 %v2_403190, %v4_403190
  %v10_403190 = icmp ult i8 %v5_403190, %v2_403190
  store i1 %v10_403190, i1* %cf.global-to-local, align 1
  store i8 %v5_403190, i8* %v1_403190, align 1
  %v0_403192 = load i32, i32* @eax, align 4
  %v1_403192 = inttoptr i32 %v0_403192 to i8*
  %v2_403192 = load i8, i8* %v1_403192, align 1
  %v3_403192 = load i32, i32* @ebx, align 4
  %v4_403192 = udiv i32 %v3_403192, 256
  %v5_403192 = trunc i32 %v4_403192 to i8
  %v6_403192 = add i8 %v2_403192, %v5_403192
  %v11_403192 = icmp ult i8 %v6_403192, %v2_403192
  store i1 %v11_403192, i1* %cf.global-to-local, align 1
  store i8 %v6_403192, i8* %v1_403192, align 1
  %v23_403192 = load i32, i32* @eax, align 4
  ret i32 %v23_403192

dec_label_pc_403196:                              ; preds = %dec_label_pc_40316d
  %v2_403199 = load i32, i32* @ebx, align 4
  %v3_403199 = load i32, i32* @edx, align 4
  %v4_403199 = or i32 %v3_403199, %v2_403199
  %v12_403199 = inttoptr i32 %v2_403199 to i32*
  store i32 %v4_403199, i32* %v12_403199, align 4
  %v6_40319c = load i32, i32* @ebx, align 4
  %v7_40319c = trunc i32 %v6_40319c to i8
  %v2_4031a0 = load i8, i8* bitcast (i32* @edi to i8*), align 4
  %v5_4031a0 = add i8 %v2_4031a0, %v7_40319c
  %v20_4031a0 = load i32, i32* @edi, align 4
  %v21_4031a0 = inttoptr i32 %v20_4031a0 to i8*
  store i8 %v5_4031a0, i8* %v21_4031a0, align 1
  %v0_4031a2 = load i32, i32* @eax, align 4
  %v1_4031a2 = or i32 %v0_4031a2, -3528
  store i32 %v1_4031a2, i32* @eax, align 4
  %v2_4031a7 = load i32, i32* @esi, align 4
  %v3_4031a7 = add i32 %v2_4031a7, 1
  %v18_4031a7 = inttoptr i32 %v2_4031a7 to i32*
  store i32 %v3_4031a7, i32* %v18_4031a7, align 4
  %v1_4031a9 = load i32, i32* @eax, align 4
  %v3_4031ab = add i32 %v1_4031a9, 939524096
  %v22_4031ab = icmp ugt i32 %v1_4031a9, -939524097
  store i1 %v22_4031ab, i1* %cf.global-to-local, align 1
  store i32 %v3_4031ab, i32* @eax, align 4
  %v0_4031b0 = load i32, i32* %eflags.global-to-local, align 4
  call void @__asm_into(i32 %v0_4031b0)
  %v0_4031b1 = load i32, i32* @eax, align 4
  %v1_4031b1 = inttoptr i32 %v0_4031b1 to i32*
  %v2_4031b1 = load i32, i32* %v1_4031b1, align 4
  %v4_4031b1 = add i32 %v2_4031b1, %v0_4031b1
  store i32 %v4_4031b1, i32* %v1_4031b1, align 4
  %v2_4031b3 = load i8, i8* bitcast (i32* @edi to i8*), align 4
  %v3_4031b3 = load i32, i32* @ebx, align 4
  %v4_4031b3 = trunc i32 %v3_4031b3 to i8
  %v5_4031b3 = add i8 %v2_4031b3, %v4_4031b3
  %v20_4031b3 = load i32, i32* @edi, align 4
  %v21_4031b3 = inttoptr i32 %v20_4031b3 to i8*
  store i8 %v5_4031b3, i8* %v21_4031b3, align 1
  %v0_4031b5 = load i32, i32* @edx, align 4
  %v1_4031b5 = trunc i32 %v0_4031b5 to i8
  %v4_4031b5 = load i8, i8* bitcast (i32* @ebx to i8*), align 4
  %v5_4031b5 = or i8 %v4_4031b5, %v1_4031b5
  store i1 false, i1* %cf.global-to-local, align 1
  %v11_4031b5 = zext i8 %v5_4031b5 to i32
  %v13_4031b5 = and i32 %v0_4031b5, -256
  %v14_4031b5 = or i32 %v13_4031b5, %v11_4031b5
  store i32 %v14_4031b5, i32* @edx, align 4
  %v0_4031b7 = load i16, i16* @es, align 4
  %v1_4031b7 = load i32, i32* @esp, align 4
  %v2_4031b7 = add i32 %v1_4031b7, -2
  %v3_4031b7 = inttoptr i32 %v2_4031b7 to i16*
  store i16 %v0_4031b7, i16* %v3_4031b7, align 2
  %v0_4031b8 = load i32, i32* @eax, align 4
  %v1_4031b8 = add i32 %v0_4031b8, 2
  %v2_4031b8 = inttoptr i32 %v1_4031b8 to i8*
  %v3_4031b8 = load i8, i8* %v2_4031b8, align 1
  %v5_4031b8 = trunc i32 %v0_4031b8 to i8
  %v11_4031b8 = icmp ult i8 %v3_4031b8, %v5_4031b8
  store i1 %v11_4031b8, i1* %cf.global-to-local, align 1
  %v1_4031bb = inttoptr i32 %v0_4031b8 to i8*
  %v2_4031bb = load i8, i8* %v1_4031bb, align 1
  %v5_4031bb = add i8 %v2_4031bb, %v5_4031b8
  store i8 %v5_4031bb, i8* %v1_4031bb, align 1
  %v0_4031bd = load i32, i32* @esp, align 4
  %v1_4031bd = inttoptr i32 %v0_4031bd to i16*
  %v0_4031be = load i16, i16* %cs.global-to-local, align 2
  store i16 %v0_4031be, i16* %v1_4031bd, align 2
  %v0_4031bf = load i32, i32* @edx, align 4
  %v1_4031bf = trunc i32 %v0_4031bf to i8
  %v3_4031bf = udiv i32 %v0_4031bf, 256
  %v4_4031bf = trunc i32 %v3_4031bf to i8
  %v10_4031bf = icmp ult i8 %v1_4031bf, %v4_4031bf
  store i1 %v10_4031bf, i1* %cf.global-to-local, align 1
  %v20_4031bf = load i32, i32* @eax, align 4
  ret i32 %v20_4031bf
}

define i32 @function_4031c4() local_unnamed_addr {
dec_label_pc_4031c4:
  %cf.global-to-local = alloca i1, align 1
  %v0_4031c4 = load i32, i32* @edx, align 4
  %v2_4031c4 = add i32 %v0_4031c4, -27584
  %v3_4031c4 = inttoptr i32 %v2_4031c4 to i32*
  %v4_4031c4 = load i32, i32* %v3_4031c4, align 4
  %v5_4031c4 = load i32, i32* @eax, align 4
  %v6_4031c4 = load i1, i1* %cf.global-to-local, align 1
  %v7_4031c4 = zext i1 %v6_4031c4 to i32
  %v8_4031c4 = add i32 %v5_4031c4, %v4_4031c4
  %v9_4031c4 = add i32 %v8_4031c4, %v7_4031c4
  %v28_4031c4 = icmp ule i32 %v9_4031c4, %v4_4031c4
  %v29_4031c4 = icmp ult i32 %v8_4031c4, %v4_4031c4
  %v30_4031c4 = select i1 %v6_4031c4, i1 %v28_4031c4, i1 %v29_4031c4
  store i1 %v30_4031c4, i1* %cf.global-to-local, align 1
  store i32 %v9_4031c4, i32* %v3_4031c4, align 4
  %v0_4031cb = load i32, i32* @eax, align 4
  ret i32 %v0_4031cb
}

define i32 @function_4031cd(i16 %arg1) local_unnamed_addr {
dec_label_pc_4031cd:
  %cf.global-to-local = alloca i1, align 1
  store i16 %arg1, i16* @es, align 4
  %v2_4031ce = load i8, i8* bitcast (i32* @eax to i8*), align 4
  %v3_4031ce = load i32, i32* @eax, align 4
  %v4_4031ce = trunc i32 %v3_4031ce to i8
  %v5_4031ce = add i8 %v2_4031ce, %v4_4031ce
  %v21_4031ce = inttoptr i32 %v3_4031ce to i8*
  store i8 %v5_4031ce, i8* %v21_4031ce, align 1
  %v2_4031d0 = load i8, i8* bitcast (i32* @edi to i8*), align 4
  %v3_4031d0 = load i32, i32* @edx, align 4
  %v4_4031d0 = trunc i32 %v3_4031d0 to i8
  %v5_4031d0 = add i8 %v2_4031d0, %v4_4031d0
  %v20_4031d0 = load i32, i32* @edi, align 4
  %v21_4031d0 = inttoptr i32 %v20_4031d0 to i8*
  store i8 %v5_4031d0, i8* %v21_4031d0, align 1
  %v0_4031d2 = load i32, i32* @ecx, align 4
  %v3_4031d8 = load i32, i32* @eax, align 4
  %v5_4031da = mul i32 %v3_4031d8, 2
  %v20_4031da = and i32 %v5_4031da, 254
  %v22_4031da = and i32 %v3_4031d8, -256
  %v23_4031da = or i32 %v20_4031da, %v22_4031da
  store i32 %v23_4031da, i32* @eax, align 4
  %tmp233 = trunc i32 %v0_4031d2 to i8
  %v5_4031dc = mul i8 %tmp233, 2
  %v10_4031dc = icmp ult i8 %v5_4031dc, %tmp233
  store i1 %v10_4031dc, i1* %cf.global-to-local, align 1
  %v21_4031dc = inttoptr i32 %v0_4031d2 to i8*
  store i8 %v5_4031dc, i8* %v21_4031dc, align 1
  %v0_4031de = load i32, i32* @edx, align 4
  %v5_4031de = load i1, i1* %cf.global-to-local, align 1
  %v6_4031de = zext i1 %v5_4031de to i32
  %v7_4031de = add i32 %v0_4031de, %v6_4031de
  %v8_4031de2 = mul i32 %v7_4031de, 256
  %v1_4031de3 = sub i32 %v0_4031de, %v8_4031de2
  %v38_4031de = and i32 %v1_4031de3, 65280
  %v39_4031de = and i32 %v0_4031de, -65281
  %v40_4031de = or i32 %v38_4031de, %v39_4031de
  store i32 %v40_4031de, i32* @edx, align 4
  %v0_4031e0 = load i32, i32* @eax, align 4
  %v1_4031e0 = or i32 %v0_4031e0, 2080
  store i1 false, i1* %cf.global-to-local, align 1
  store i32 %v1_4031e0, i32* @eax, align 4
  %v1_4031e5 = inttoptr i32 %v1_4031e0 to i8*
  %v2_4031e5 = load i8, i8* %v1_4031e5, align 1
  %v3_4031e5 = load i32, i32* @ebx, align 4
  %v4_4031e5 = udiv i32 %v3_4031e5, 256
  %v5_4031e5 = trunc i32 %v4_4031e5 to i8
  %v6_4031e5 = add i8 %v2_4031e5, %v5_4031e5
  store i8 %v6_4031e5, i8* %v1_4031e5, align 1
  %v0_4031e7 = load i32, i32* @eax, align 4
  %v1_4031e7 = load i32, i32* @edi, align 4
  store i32 %v1_4031e7, i32* @eax, align 4
  store i32 %v0_4031e7, i32* @edi, align 4
  %v4_4031e8 = add i32 %v1_4031e7, %v0_4031e7
  %v21_4031e8 = inttoptr i32 %v1_4031e7 to i32*
  store i32 %v4_4031e8, i32* %v21_4031e8, align 4
  %v0_4031ea = load i32, i32* @eax, align 4
  %v1_4031ea = trunc i32 %v0_4031ea to i8
  %v2_4031ea = load i32, i32* @edx, align 4
  %v3_4031ea = trunc i32 %v2_4031ea to i8
  %v4_4031ea = add i8 %v3_4031ea, %v1_4031ea
  %v9_4031ea = icmp ult i8 %v4_4031ea, %v1_4031ea
  store i1 %v9_4031ea, i1* %cf.global-to-local, align 1
  %v19_4031ea = zext i8 %v4_4031ea to i32
  %v21_4031ea = and i32 %v0_4031ea, -256
  %v22_4031ea = or i32 %v21_4031ea, %v19_4031ea
  store i32 %v22_4031ea, i32* @eax, align 4
  %v1_4031ec = inttoptr i32 %v22_4031ea to i8*
  %v2_4031ec = load i8, i8* %v1_4031ec, align 1
  %v5_4031ec = add i8 %v2_4031ec, %v4_4031ea
  %v10_4031ec = icmp ult i8 %v5_4031ec, %v2_4031ec
  store i1 %v10_4031ec, i1* %cf.global-to-local, align 1
  store i8 %v5_4031ec, i8* %v1_4031ec, align 1
  %v0_4031f0 = load i32, i32* @eax, align 4
  %v1_4031f0 = inttoptr i32 %v0_4031f0 to i8*
  %v2_4031f0 = load i8, i8* %v1_4031f0, align 1
  %v3_4031f0 = load i32, i32* @edx, align 4
  %v4_4031f0 = udiv i32 %v3_4031f0, 256
  %v5_4031f0 = trunc i32 %v4_4031f0 to i8
  %v6_4031f0 = sub i8 %v2_4031f0, %v5_4031f0
  %v11_4031f0 = icmp ult i8 %v2_4031f0, %v5_4031f0
  store i1 %v11_4031f0, i1* %cf.global-to-local, align 1
  store i8 %v6_4031f0, i8* %v1_4031f0, align 1
  %v0_4031f3 = load i32, i32* @eax, align 4
  %v1_4031f3 = inttoptr i32 %v0_4031f3 to i8*
  %v2_4031f3 = load i8, i8* %v1_4031f3, align 1
  %v4_4031f3 = trunc i32 %v0_4031f3 to i8
  %v5_4031f3 = add i8 %v2_4031f3, %v4_4031f3
  %v10_4031f3 = icmp ult i8 %v5_4031f3, %v2_4031f3
  store i1 %v10_4031f3, i1* %cf.global-to-local, align 1
  store i8 %v5_4031f3, i8* %v1_4031f3, align 1
  %v0_4031f6 = load i32, i32* @edi, align 4
  %v1_4031f6 = inttoptr i32 %v0_4031f6 to i8*
  %v2_4031f6 = load i8, i8* %v1_4031f6, align 1
  %v3_4031f6 = load i32, i32* @ecx, align 4
  %v4_4031f6 = udiv i32 %v3_4031f6, 256
  %v5_4031f6 = trunc i32 %v4_4031f6 to i8
  %v6_4031f6 = sub i8 %v2_4031f6, %v5_4031f6
  %v11_4031f6 = icmp ult i8 %v2_4031f6, %v5_4031f6
  store i1 %v11_4031f6, i1* %cf.global-to-local, align 1
  store i8 %v6_4031f6, i8* %v1_4031f6, align 1
  %v0_4031f8 = load i32, i32* @eax, align 4
  %v1_4031f8 = inttoptr i32 %v0_4031f8 to i8*
  %v2_4031f8 = load i8, i8* %v1_4031f8, align 1
  %v4_4031f8 = trunc i32 %v0_4031f8 to i8
  %v5_4031f8 = add i8 %v2_4031f8, %v4_4031f8
  store i8 %v5_4031f8, i8* %v1_4031f8, align 1
  %v0_4031fb = load i32, i32* @ecx, align 4
  %v1_4031fb = load i32, i32* @eax, align 4
  %v3_4031fb = add i32 %v1_4031fb, %v0_4031fb
  %v4_4031fb = inttoptr i32 %v3_4031fb to i32*
  %v5_4031fb = load i32, i32* %v4_4031fb, align 4
  %v12_4031fb = icmp ult i32 %v5_4031fb, %v0_4031fb
  store i1 %v12_4031fb, i1* %cf.global-to-local, align 1
  %v1_4031fe = inttoptr i32 %v1_4031fb to i8*
  %v2_4031fe = load i8, i8* %v1_4031fe, align 1
  %v4_4031fe = trunc i32 %v1_4031fb to i8
  %v5_4031fe = add i8 %v2_4031fe, %v4_4031fe
  store i8 %v5_4031fe, i8* %v1_4031fe, align 1
  %v2_403200 = load i8, i8* bitcast (i32* @ecx to i8*), align 4
  %v3_403200 = load i32, i32* @eax, align 4
  %v4_403200 = trunc i32 %v3_403200 to i8
  %v5_403200 = and i8 %v2_403200, %v4_403200
  store i1 false, i1* %cf.global-to-local, align 1
  %v11_403200 = load i32, i32* @ecx, align 4
  %v12_403200 = inttoptr i32 %v11_403200 to i8*
  store i8 %v5_403200, i8* %v12_403200, align 1
  %v0_403203 = load i32, i32* @eax, align 4
  %v1_403203 = inttoptr i32 %v0_403203 to i8*
  %v2_403203 = load i8, i8* %v1_403203, align 1
  %v4_403203 = trunc i32 %v0_403203 to i8
  %v5_403203 = add i8 %v2_403203, %v4_403203
  %v10_403203 = icmp ult i8 %v5_403203, %v2_403203
  store i1 %v10_403203, i1* %cf.global-to-local, align 1
  store i8 %v5_403203, i8* %v1_403203, align 1
  %v0_403205 = load i32, i32* @eax, align 4
  %v1_403205 = inttoptr i32 %v0_403205 to i8*
  %v2_403205 = load i8, i8* %v1_403205, align 1
  %v3_403205 = load i32, i32* @ecx, align 4
  %v4_403205 = udiv i32 %v3_403205, 256
  %v5_403205 = trunc i32 %v4_403205 to i8
  %v6_403205 = add i8 %v2_403205, %v5_403205
  %v11_403205 = icmp ult i8 %v6_403205, %v2_403205
  store i1 %v11_403205, i1* %cf.global-to-local, align 1
  store i8 %v6_403205, i8* %v1_403205, align 1
  %v0_403207 = load i32, i32* @eax, align 4
  %v1_403207 = inttoptr i32 %v0_403207 to i8*
  %v2_403207 = load i8, i8* %v1_403207, align 1
  %v4_403207 = trunc i32 %v0_403207 to i8
  %v5_403207 = xor i8 %v2_403207, %v4_403207
  store i8 %v5_403207, i8* %v1_403207, align 1
  %v2_403209 = load i8, i8* bitcast (i32* @esi to i8*), align 4
  %v3_403209 = load i32, i32* @eax, align 4
  %v4_403209 = trunc i32 %v3_403209 to i8
  %v5_403209 = add i8 %v2_403209, %v4_403209
  %v10_403209 = icmp ult i8 %v5_403209, %v2_403209
  store i1 %v10_403209, i1* %cf.global-to-local, align 1
  %v20_403209 = load i32, i32* @esi, align 4
  %v21_403209 = inttoptr i32 %v20_403209 to i8*
  store i8 %v5_403209, i8* %v21_403209, align 1
  %v0_40320b = load i32, i32* @edx, align 4
  %v1_40320b = udiv i32 %v0_40320b, 256
  %v2_40320b = trunc i32 %v1_40320b to i8
  %v4_40320b = add i32 %v0_40320b, 1
  %v5_40320b = inttoptr i32 %v4_40320b to i8*
  %v6_40320b = load i8, i8* %v5_40320b, align 1
  %v12_40320b = icmp ugt i8 %v6_40320b, %v2_40320b
  store i1 %v12_40320b, i1* %cf.global-to-local, align 1
  %v0_40320e = load i32, i32* @eax, align 4
  %v1_40320e = inttoptr i32 %v0_40320e to i8*
  %v2_40320e = load i8, i8* %v1_40320e, align 1
  %v4_40320e = trunc i32 %v0_40320e to i8
  %v5_40320e = add i8 %v2_40320e, %v4_40320e
  %v10_40320e = icmp ult i8 %v5_40320e, %v2_40320e
  store i1 %v10_40320e, i1* %cf.global-to-local, align 1
  store i8 %v5_40320e, i8* %v1_40320e, align 1
  %v0_403210 = load i32, i32* @ebp, align 4
  %v1_403210 = add i32 %v0_403210, 1
  %v2_403210 = inttoptr i32 %v1_403210 to i8*
  %v3_403210 = load i8, i8* %v2_403210, align 1
  %v4_403210 = load i32, i32* @ecx, align 4
  %v5_403210 = udiv i32 %v4_403210, 256
  %v6_403210 = trunc i32 %v5_403210 to i8
  %v12_403210 = icmp ult i8 %v3_403210, %v6_403210
  store i1 %v12_403210, i1* %cf.global-to-local, align 1
  %v0_403213 = load i32, i32* @eax, align 4
  %v1_403213 = inttoptr i32 %v0_403213 to i8*
  %v2_403213 = load i8, i8* %v1_403213, align 1
  %v4_403213 = trunc i32 %v0_403213 to i8
  %v5_403213 = add i8 %v2_403213, %v4_403213
  store i8 %v5_403213, i8* %v1_403213, align 1
  %v2_403215 = load i32, i32* @eax, align 4
  %v1_403217 = inttoptr i32 %v2_403215 to i32*
  %v2_403217 = load i32, i32* %v1_403217, align 4
  %v4_403217 = add i32 %v2_403217, %v2_403215
  store i32 %v4_403217, i32* %v1_403217, align 4
  %v2_403219 = load i8, i8* bitcast (i32* @ecx to i8*), align 4
  %v3_403219 = load i32, i32* @ecx, align 4
  %v4_403219 = trunc i32 %v3_403219 to i8
  %v5_403219 = add i8 %v2_403219, %v4_403219
  %v10_403219 = icmp ult i8 %v5_403219, %v2_403219
  store i1 %v10_403219, i1* %cf.global-to-local, align 1
  %v21_403219 = inttoptr i32 %v3_403219 to i8*
  store i8 %v5_403219, i8* %v21_403219, align 1
  %v0_40321b = load i32, i32* inttoptr (i32 -32962 to i32*), align 4
  %v1_40321b = load i32, i32* @eax, align 4
  %v2_40321b = load i1, i1* %cf.global-to-local, align 1
  %v3_40321b = zext i1 %v2_40321b to i32
  %v4_40321b = add i32 %v1_40321b, %v0_40321b
  %v5_40321b = add i32 %v4_40321b, %v3_40321b
  %v24_40321b = icmp ule i32 %v5_40321b, %v0_40321b
  %v25_40321b = icmp ult i32 %v4_40321b, %v0_40321b
  %v26_40321b = select i1 %v2_40321b, i1 %v24_40321b, i1 %v25_40321b
  store i1 %v26_40321b, i1* %cf.global-to-local, align 1
  store i32 %v5_40321b, i32* inttoptr (i32 -32962 to i32*), align 4
  %v0_403221 = load i32, i32* @eax, align 4
  ret i32 %v0_403221
}

define i32 @function_403279() local_unnamed_addr {
dec_label_pc_403279:
  %v0_403279 = call i32 @function_2856a04a()
  store i32 %v0_403279, i32* @eax, align 4
  ret i32 %v0_403279
}

define i32 @function_403307(i16 %arg1) local_unnamed_addr {
dec_label_pc_403307:
  %cf.global-to-local = alloca i1, align 1
  %stack_var_0 = alloca i16, align 2
  store i16 %arg1, i16* %stack_var_0, align 2
  %v0_403307 = load i32, i32* @eax, align 4
  %v1_403307 = add i32 %v0_403307, 654311422
  %v2_403307 = inttoptr i32 %v1_403307 to i8*
  %v3_403307 = load i8, i8* %v2_403307, align 1
  %v4_403307 = load i32, i32* @ecx, align 4
  %v5_403307 = udiv i32 %v4_403307, 256
  %v6_403307 = trunc i32 %v5_403307 to i8
  %v12_403307 = icmp ult i8 %v3_403307, %v6_403307
  store i1 %v12_403307, i1* %cf.global-to-local, align 1
  %v3_40330d = mul i32 %v0_403307, 2
  %v4_40330d = inttoptr i32 %v3_40330d to i8*
  %v5_40330d = load i8, i8* %v4_40330d, align 2
  %v6_40330d = load i32, i32* @edx, align 4
  %v7_40330d = trunc i32 %v6_40330d to i8
  %v8_40330d = and i8 %v5_40330d, %v7_40330d
  store i8 %v8_40330d, i8* %v4_40330d, align 2
  %v2_403310 = load i8, i8* bitcast (i32* @eax to i8*), align 4
  %v3_403310 = load i32, i32* @eax, align 4
  %v4_403310 = trunc i32 %v3_403310 to i8
  %v5_403310 = add i8 %v2_403310, %v4_403310
  %v21_403310 = inttoptr i32 %v3_403310 to i8*
  store i8 %v5_403310, i8* %v21_403310, align 1
  %v2_403315 = load i8, i8* bitcast (i32* @eax to i8*), align 4
  %v3_403315 = load i32, i32* @eax, align 4
  %v4_403315 = trunc i32 %v3_403315 to i8
  %v5_403315 = add i8 %v2_403315, %v4_403315
  %v10_403315 = icmp ult i8 %v5_403315, %v2_403315
  store i1 %v10_403315, i1* %cf.global-to-local, align 1
  %v21_403315 = inttoptr i32 %v3_403315 to i8*
  store i8 %v5_403315, i8* %v21_403315, align 1
  %v0_403318 = load i32, i32* @eax, align 4
  %v3_403318 = load i32, i32* @esi, align 4
  %v4_403318 = load i1, i1* %cf.global-to-local, align 1
  %v5_403318 = zext i1 %v4_403318 to i32
  %v6_403318 = add i32 %v3_403318, %v0_403318
  %v7_403318 = add i32 %v6_403318, %v5_403318
  %v26_403318 = icmp ule i32 %v7_403318, %v0_403318
  %v27_403318 = icmp ult i32 %v6_403318, %v0_403318
  %v28_403318 = select i1 %v4_403318, i1 %v26_403318, i1 %v27_403318
  store i1 %v28_403318, i1* %cf.global-to-local, align 1
  store i32 %v7_403318, i32* @eax, align 4
  %v1_40331a = inttoptr i32 %v7_403318 to i8*
  %v2_40331a = load i8, i8* %v1_40331a, align 1
  %v3_40331a = load i32, i32* @edx, align 4
  %v4_40331a = trunc i32 %v3_40331a to i8
  %v5_40331a = and i8 %v2_40331a, %v4_40331a
  store i1 false, i1* %cf.global-to-local, align 1
  store i8 %v5_40331a, i8* %v1_40331a, align 1
  %v0_40331c = load i32, i32* @eax, align 4
  %v1_40331c = inttoptr i32 %v0_40331c to i8*
  %v2_40331c = load i8, i8* %v1_40331c, align 1
  %v4_40331c = trunc i32 %v0_40331c to i8
  %v5_40331c = add i8 %v2_40331c, %v4_40331c
  store i8 %v5_40331c, i8* %v1_40331c, align 1
  %v2_40331e = load i8, i8* bitcast (i32* @edi to i8*), align 4
  %v3_40331e = load i32, i32* @edx, align 4
  %v4_40331e = trunc i32 %v3_40331e to i8
  %v5_40331e = add i8 %v2_40331e, %v4_40331e
  %v20_40331e = load i32, i32* @edi, align 4
  %v21_40331e = inttoptr i32 %v20_40331e to i8*
  store i8 %v5_40331e, i8* %v21_40331e, align 1
  %v0_403320 = load i32, i32* @ebx, align 4
  %v1_403320 = trunc i32 %v0_403320 to i8
  %v4_403320 = load i8, i8* bitcast (i32* @ebp to i8*), align 4
  %v10_403320 = icmp ugt i8 %v4_403320, %v1_403320
  store i1 %v10_403320, i1* %cf.global-to-local, align 1
  %v0_403323 = load i32, i32* @eax, align 4
  %v1_403323 = inttoptr i32 %v0_403323 to i8*
  %v2_403323 = load i8, i8* %v1_403323, align 1
  %v4_403323 = trunc i32 %v0_403323 to i8
  %v5_403323 = add i8 %v2_403323, %v4_403323
  %v10_403323 = icmp ult i8 %v5_403323, %v2_403323
  store i1 %v10_403323, i1* %cf.global-to-local, align 1
  store i8 %v5_403323, i8* %v1_403323, align 1
  %v2_403325 = load i8, i8* bitcast (i32* @ebx to i8*), align 4
  %v3_403325 = load i32, i32* @edx, align 4
  %v4_403325 = trunc i32 %v3_403325 to i8
  %v5_403325 = load i1, i1* %cf.global-to-local, align 1
  %v6_403325.neg = sext i1 %v5_403325 to i8
  %v7_403325.neg = sub i8 %v2_403325, %v4_403325
  %v8_403325 = add i8 %v7_403325.neg, %v6_403325.neg
  %v36_403325 = load i32, i32* @ebx, align 4
  %v37_403325 = inttoptr i32 %v36_403325 to i8*
  store i8 %v8_403325, i8* %v37_403325, align 1
  %v0_403328 = load i16, i16* @es, align 4
  store i16 %v0_403328, i16* %stack_var_0, align 2
  %v2_403329 = load i8, i8* bitcast (i32* @edx to i8*), align 4
  %v3_403329 = load i32, i32* @edx, align 4
  %v4_403329 = trunc i32 %v3_403329 to i8
  %v5_403329 = and i8 %v2_403329, %v4_403329
  store i1 false, i1* %cf.global-to-local, align 1
  %v12_403329 = inttoptr i32 %v3_403329 to i8*
  store i8 %v5_403329, i8* %v12_403329, align 1
  %v0_40332b = load i32, i32* @eax, align 4
  %v1_40332b = inttoptr i32 %v0_40332b to i8*
  %v2_40332b = load i8, i8* %v1_40332b, align 1
  %v4_40332b = trunc i32 %v0_40332b to i8
  %v5_40332b = add i8 %v2_40332b, %v4_40332b
  %v10_40332b = icmp ult i8 %v5_40332b, %v2_40332b
  store i1 %v10_40332b, i1* %cf.global-to-local, align 1
  store i8 %v5_40332b, i8* %v1_40332b, align 1
  %v0_40332d = load i32, i32* @eax, align 4
  %v1_40332d = inttoptr i32 %v0_40332d to i8*
  %v2_40332d = load i8, i8* %v1_40332d, align 1
  %v3_40332d = load i32, i32* @ebx, align 4
  %v4_40332d = udiv i32 %v3_40332d, 256
  %v5_40332d = trunc i32 %v4_40332d to i8
  %v6_40332d = add i8 %v2_40332d, %v5_40332d
  %v11_40332d = icmp ult i8 %v6_40332d, %v2_40332d
  store i1 %v11_40332d, i1* %cf.global-to-local, align 1
  store i8 %v6_40332d, i8* %v1_40332d, align 1
  %v0_40332f = load i32, i32* @edi, align 4
  %v1_40332f = add i32 %v0_40332f, -1
  store i32 %v1_40332f, i32* @edi, align 4
  %v0_403330 = load i32, i32* @eax, align 4
  %v1_403330 = inttoptr i32 %v0_403330 to i8*
  %v2_403330 = load i8, i8* %v1_403330, align 1
  %v4_403330 = trunc i32 %v0_403330 to i8
  %v5_403330 = add i8 %v2_403330, %v4_403330
  %v10_403330 = icmp ult i8 %v5_403330, %v2_403330
  store i1 %v10_403330, i1* %cf.global-to-local, align 1
  store i8 %v5_403330, i8* %v1_403330, align 1
  %v0_403332 = load i32, i32* @eax, align 4
  %v1_403332 = inttoptr i32 %v0_403332 to i8*
  %v2_403332 = load i8, i8* %v1_403332, align 1
  %v3_403332 = load i32, i32* @ebx, align 4
  %v4_403332 = udiv i32 %v3_403332, 256
  %v5_403332 = trunc i32 %v4_403332 to i8
  %v6_403332 = add i8 %v2_403332, %v5_403332
  %v11_403332 = icmp ult i8 %v6_403332, %v2_403332
  store i1 %v11_403332, i1* %cf.global-to-local, align 1
  store i8 %v6_403332, i8* %v1_403332, align 1
  %v0_403334 = load i32, i32* @eax, align 4
  %v1_403334 = inttoptr i32 %v0_403334 to i32*
  %v2_403334 = load i32, i32* %v1_403334, align 4
  store i32 %v2_403334, i32* @eax, align 4
  %v1_403336 = inttoptr i32 %v2_403334 to i8*
  %v2_403336 = load i8, i8* %v1_403336, align 1
  %v4_403336 = trunc i32 %v2_403334 to i8
  %v5_403336 = add i8 %v2_403336, %v4_403336
  %v10_403336 = icmp ult i8 %v5_403336, %v2_403336
  store i1 %v10_403336, i1* %cf.global-to-local, align 1
  store i8 %v5_403336, i8* %v1_403336, align 1
  %v0_403338 = load i32, i32* @eax, align 4
  %v1_403338 = add i32 %v0_403338, 510512361
  %v2_403338 = inttoptr i32 %v1_403338 to i8*
  %v3_403338 = load i8, i8* %v2_403338, align 1
  %v4_403338 = load i32, i32* @ecx, align 4
  %v5_403338 = trunc i32 %v4_403338 to i8
  %v6_403338 = and i8 %v3_403338, %v5_403338
  store i8 %v6_403338, i8* %v2_403338, align 1
  %v2_40333e = load i8, i8* bitcast (i32* @edx to i8*), align 4
  %v3_40333e = load i32, i32* @edx, align 4
  %v4_40333e = udiv i32 %v3_40333e, 256
  %v5_40333e = trunc i32 %v4_40333e to i8
  %v6_40333e = sub i8 %v2_40333e, %v5_40333e
  %v11_40333e = icmp ult i8 %v2_40333e, %v5_40333e
  store i1 %v11_40333e, i1* %cf.global-to-local, align 1
  %v22_40333e = inttoptr i32 %v3_40333e to i8*
  store i8 %v6_40333e, i8* %v22_40333e, align 1
  %v0_403340 = load i32, i32* @eax, align 4
  %v1_403340 = inttoptr i32 %v0_403340 to i8*
  %v2_403340 = load i8, i8* %v1_403340, align 1
  %v4_403340 = trunc i32 %v0_403340 to i8
  %v5_403340 = add i8 %v2_403340, %v4_403340
  store i8 %v5_403340, i8* %v1_403340, align 1
  %v0_403343 = load i32, i32* @ecx, align 4
  %v1_403343 = trunc i32 %v0_403343 to i8
  %v5_403343 = mul i8 %v1_403343, 2
  %v10_403343 = icmp ult i8 %v5_403343, %v1_403343
  store i1 %v10_403343, i1* %cf.global-to-local, align 1
  %v20_403343 = zext i8 %v5_403343 to i32
  %v22_403343 = and i32 %v0_403343, -256
  %v23_403343 = or i32 %v22_403343, %v20_403343
  store i32 %v23_403343, i32* @ecx, align 4
  %v3_403346 = select i1 %v10_403343, i32 255, i32 0
  %v4_403346 = load i32, i32* @eax, align 4
  %v5_403346 = and i32 %v4_403346, -256
  %v6_403346 = or i32 %v5_403346, %v3_403346
  store i32 %v6_403346, i32* @eax, align 4
  %v1_403347 = add i32 %v23_403343, 536870914
  %v2_403347 = inttoptr i32 %v1_403347 to i8*
  %v3_403347 = load i8, i8* %v2_403347, align 2
  %v4_403347 = load i32, i32* @ebx, align 4
  %v5_403347 = trunc i32 %v4_403347 to i8
  %v6_403347 = and i8 %v3_403347, %v5_403347
  store i1 false, i1* %cf.global-to-local, align 1
  store i8 %v6_403347, i8* %v2_403347, align 2
  %v0_40334d = load i16, i16* %stack_var_0, align 2
  %v1_40334d = call i32 @function_28000002(i16 %v0_40334d)
  store i32 %v1_40334d, i32* @eax, align 4
  ret i32 %v1_40334d
}

define i32 @function_403374() local_unnamed_addr {
dec_label_pc_403374:
  %v0_403374 = load i32, i32* @eax, align 4
  ret i32 %v0_403374
}

define i32 @function_403396() local_unnamed_addr {
dec_label_pc_403396:
  %v4_403396 = load i32, i32* @eax, align 4
  ret i32 %v4_403396
}

define i32 @function_4033a7() local_unnamed_addr {
dec_label_pc_4033a7:
  %v0_4033a7 = call i32 @function_47fffffe()
  store i32 %v0_4033a7, i32* @eax, align 4
  ret i32 %v0_4033a7
}

define i32 @function_4033ae() local_unnamed_addr {
dec_label_pc_4033ae:
  %v4_4033ae = load i32, i32* @eax, align 4
  ret i32 %v4_4033ae
}

define i32 @function_4033b2() local_unnamed_addr {
dec_label_pc_4033b2:
  %v0_4033b6 = load i32, i32* @eax, align 4
  ret i32 %v0_4033b6
}

define i32 @function_4033da() local_unnamed_addr {
dec_label_pc_4033da:
  %v4_4033da = load i32, i32* @eax, align 4
  ret i32 %v4_4033da
}

define i32 @function_4033e9() local_unnamed_addr {
dec_label_pc_4033e9:
  %v0_4033e9 = load i32, i32* @eax, align 4
  ret i32 %v0_4033e9
}

define i32 @function_403423() local_unnamed_addr {
dec_label_pc_403423:
  %v4_403423 = load i32, i32* @eax, align 4
  ret i32 %v4_403423
}

define i32 @function_40343b() local_unnamed_addr {
dec_label_pc_40343b:
  %v0_40343b = load i32, i32* @eax, align 4
  ret i32 %v0_40343b
}

define i32 @function_40343f() local_unnamed_addr {
dec_label_pc_40343f:
  %v0_40343f = load i32, i32* @eax, align 4
  ret i32 %v0_40343f
}

define i32 @function_403553() local_unnamed_addr {
dec_label_pc_403553:
  %v0_403553 = load i32, i32* @eax, align 4
  ret i32 %v0_403553
}

define i32 @function_403593(i32 %arg1) local_unnamed_addr {
dec_label_pc_403593:
  %v0_403593 = load i32, i32* @ebx, align 4
  %v1_403599 = udiv i32 %v0_403593, 256
  %v2_403599 = trunc i32 %v1_403599 to i8
  %v5_403599 = load i8, i8* bitcast (i32* @eax to i8*), align 4
  %v6_403599 = or i8 %v5_403599, %v2_403599
  %v12_403599 = zext i8 %v6_403599 to i32
  %v14_403599 = mul i32 %v12_403599, 256
  %v15_403599 = and i32 %v0_403593, -65281
  %v16_403599 = or i32 %v14_403599, %v15_403599
  store i32 %v16_403599, i32* @ebx, align 4
  %v0_40359b = load i32, i32* @eax, align 4
  %v6_40359b3 = and i32 %v0_40359b, 14
  %v7_40359b = icmp ugt i32 %v6_40359b3, 9
  %v9_40359b = add i32 %v0_40359b, 10
  %v11_40359b = select i1 %v7_40359b, i32 %v9_40359b, i32 %v0_40359b
  %v10_40359b = sext i1 %v7_40359b to i32
  %v15_40359b = urem i32 %v11_40359b, 16
  %v18_40359b = and i32 %v0_40359b, -65536
  %v19_40359b = or i32 %v15_40359b, %v18_40359b
  %v12_40359b1 = mul i32 %v10_40359b, 256
  %v3_40359b2 = add i32 %v12_40359b1, %v0_40359b
  %v22_40359b = and i32 %v3_40359b2, 65280
  %v24_40359b = or i32 %v19_40359b, %v22_40359b
  store i32 %v24_40359b, i32* @eax, align 4
  %v1_40359c = inttoptr i32 %v24_40359b to i8*
  %v2_40359c = load i8, i8* %v1_40359c, align 1
  %v4_40359c = trunc i32 %v15_40359b to i8
  %v5_40359c = add i8 %v2_40359c, %v4_40359c
  store i8 %v5_40359c, i8* %v1_40359c, align 1
  %v0_40359e = load i8, i8* inttoptr (i32 -3016 to i8*), align 8
  %v1_40359e = load i32, i32* @ebx, align 4
  %v2_40359e = trunc i32 %v1_40359e to i8
  %v3_40359e = add i8 %v0_40359e, %v2_40359e
  store i8 %v3_40359e, i8* inttoptr (i32 -3016 to i8*), align 8
  %v0_4035a6 = load i32, i32* @ebx, align 4
  %v1_4035a6 = udiv i32 %v0_4035a6, 256
  %v2_4035a6 = trunc i32 %v1_4035a6 to i8
  %v3_4035a6 = load i32, i32* @eax, align 4
  %v4_4035a6 = inttoptr i32 %v3_4035a6 to i8*
  %v5_4035a6 = load i8, i8* %v4_4035a6, align 1
  %v6_4035a6 = or i8 %v5_4035a6, %v2_4035a6
  %v12_4035a6 = zext i8 %v6_4035a6 to i32
  %v14_4035a6 = mul i32 %v12_4035a6, 256
  %v15_4035a6 = and i32 %v0_4035a6, -65281
  %v16_4035a6 = or i32 %v14_4035a6, %v15_4035a6
  store i32 %v16_4035a6, i32* @ebx, align 4
  ret i32 %v3_4035a6
}

define i32 @function_4035ab() local_unnamed_addr {
dec_label_pc_4035ab:
  %v0_4035ab = load i32, i32* @eax, align 4
  ret i32 %v0_4035ab
}

define i32 @function_4035cb() local_unnamed_addr {
dec_label_pc_4035cb:
  %v0_4035cb = load i32, i32* @eax, align 4
  ret i32 %v0_4035cb
}

define i32 @function_4035e7() local_unnamed_addr {
dec_label_pc_4035e7:
  %v0_4035e7 = load i32, i32* @eax, align 4
  ret i32 %v0_4035e7
}

define i32 @function_403603() local_unnamed_addr {
dec_label_pc_403603:
  %v0_403603 = load i32, i32* @eax, align 4
  ret i32 %v0_403603
}

define i32 @function_403605(i32 %arg1) local_unnamed_addr {
dec_label_pc_403605:
  %v0_403605 = load i32, i32* @eax, align 4
  %v1_403605 = trunc i32 %v0_403605 to i8
  %v1_403607 = inttoptr i32 %v0_403605 to i8*
  %v2_403607 = load i8, i8* %v1_403607, align 1
  %v5_403607 = add i8 %v2_403607, %v1_403605
  store i8 %v5_403607, i8* %v1_403607, align 1
  %v5_40360f = load i32, i32* @eax, align 4
  ret i32 %v5_40360f
}

define i32 @function_40367b() local_unnamed_addr {
dec_label_pc_40367b:
  %v0_40367b = load i32, i32* @eax, align 4
  ret i32 %v0_40367b
}

define i32 @function_4036b2() local_unnamed_addr {
dec_label_pc_4036b2:
  %v0_4036b2 = load i32, i32* @eax, align 4
  ret i32 %v0_4036b2
}

define i32 @function_4036db() local_unnamed_addr {
dec_label_pc_4036db:
  %v0_4036db = load i32, i32* @eax, align 4
  ret i32 %v0_4036db
}

define i32 @function_4036f9() local_unnamed_addr {
dec_label_pc_4036f9:
  %v2_4036f9 = load i32, i32* @esi, align 4
  %v3_4036f9 = add i32 %v2_4036f9, 1
  %v18_4036f9 = inttoptr i32 %v2_4036f9 to i32*
  store i32 %v3_4036f9, i32* %v18_4036f9, align 4
  %v0_4036fb = load i32, i32* @ebp, align 4
  %v1_4036fb = add i32 %v0_4036fb, 1
  store i32 %v1_4036fb, i32* @ebp, align 4
  %v2_4036fc = load i8, i8* bitcast (i32* @eax to i8*), align 4
  %v3_4036fc = load i32, i32* @eax, align 4
  %v4_4036fc = trunc i32 %v3_4036fc to i8
  %v5_4036fc = or i8 %v2_4036fc, %v4_4036fc
  %v12_4036fc = inttoptr i32 %v3_4036fc to i8*
  store i8 %v5_4036fc, i8* %v12_4036fc, align 1
  %v2_4036fe = load i8, i8* bitcast (i32* @eax to i8*), align 4
  %v3_4036fe = load i32, i32* @eax, align 4
  %v4_4036fe = trunc i32 %v3_4036fe to i8
  %v5_4036fe = add i8 %v2_4036fe, %v4_4036fe
  %v21_4036fe = inttoptr i32 %v3_4036fe to i8*
  store i8 %v5_4036fe, i8* %v21_4036fe, align 1
  %v0_403700 = load i32, i32* @eax, align 4
  ret i32 %v0_403700
}

define i32 @function_40370b() local_unnamed_addr {
dec_label_pc_40370b:
  %v4_40370b = load i32, i32* @eax, align 4
  ret i32 %v4_40370b
}

define i32 @function_40370f() local_unnamed_addr {
dec_label_pc_40370f:
  %v4_40370f = load i32, i32* @eax, align 4
  ret i32 %v4_40370f
}

define i32 @function_40371c() local_unnamed_addr {
dec_label_pc_40371c:
  %v0_40371c = load i32, i32* @eax, align 4
  ret i32 %v0_40371c
}

define i32 @function_40371f() local_unnamed_addr {
dec_label_pc_40371f:
  %v0_40371f = load i32, i32* @eax, align 4
  ret i32 %v0_40371f
}

define i32 @function_40372b(i32 %arg1) local_unnamed_addr {
dec_label_pc_40372b:
  %v5_40372b = load i32, i32* @eax, align 4
  ret i32 %v5_40372b
}

define i32 @function_4037e9() local_unnamed_addr {
dec_label_pc_4037e9:
  %v0_4037e9 = load i32, i32* @eax, align 4
  ret i32 %v0_4037e9
}

define i32 @function_4037fe() local_unnamed_addr {
dec_label_pc_4037fe:
  %v0_4037fe = load i32, i32* @eax, align 4
  ret i32 %v0_4037fe
}

define i32 @function_40380a() local_unnamed_addr {
dec_label_pc_40380a:
  %v0_40380a = load i32, i32* @eax, align 4
  ret i32 %v0_40380a
}

define i32 @function_403842() local_unnamed_addr {
dec_label_pc_403842:
  %v0_403842 = load i32, i32* @eax, align 4
  ret i32 %v0_403842
}

define i32 @function_40384e(i32 %arg1) local_unnamed_addr {
dec_label_pc_40384e:
  %v5_40384e = load i32, i32* @eax, align 4
  ret i32 %v5_40384e
}

define i32 @function_40396c() local_unnamed_addr {
dec_label_pc_40396c:
  %v0_40396c = load i32, i32* @eax, align 4
  ret i32 %v0_40396c
}

define i32 @function_403986() local_unnamed_addr {
dec_label_pc_403986:
  %v4_403986 = load i32, i32* @eax, align 4
  ret i32 %v4_403986
}

define i32 @function_4039a2() local_unnamed_addr {
dec_label_pc_4039a2:
  %v0_4039a2 = load i32, i32* @eax, align 4
  ret i32 %v0_4039a2
}

define i32 @function_4039ad() local_unnamed_addr {
dec_label_pc_4039ad:
  %v0_4039ad = load i32, i32* @eax, align 4
  ret i32 %v0_4039ad
}

define i32 @function_4039bf() local_unnamed_addr {
dec_label_pc_4039bf:
  %v0_4039bf = load i32, i32* @eax, align 4
  ret i32 %v0_4039bf
}

define i32 @function_403ac4() local_unnamed_addr {
dec_label_pc_403ac4:
  %v0_403ac4 = load i32, i32* @esi, align 4
  %v1_403ac4 = add i32 %v0_403ac4, 251658240
  %v2_403ac4 = inttoptr i32 %v1_403ac4 to i32*
  %v3_403ac4 = load i32, i32* %v2_403ac4, align 4
  %v4_403ac4 = add i32 %v3_403ac4, 1
  store i32 %v4_403ac4, i32* %v2_403ac4, align 4
  %v2_403aca = load i8, i8* bitcast (i32* @eax to i8*), align 4
  %v3_403aca = load i32, i32* @eax, align 4
  %v4_403aca = trunc i32 %v3_403aca to i8
  %v5_403aca = add i8 %v2_403aca, %v4_403aca
  %v21_403aca = inttoptr i32 %v3_403aca to i8*
  store i8 %v5_403aca, i8* %v21_403aca, align 1
  %v0_403acc = load i32, i32* @edi, align 4
  %v2_403acc = mul i32 %v0_403acc, 8
  %v3_403acc = add i32 %v0_403acc, -1
  %v4_403acc = add i32 %v3_403acc, %v2_403acc
  %v5_403acc = inttoptr i32 %v4_403acc to i8*
  %v6_403acc = load i8, i8* %v5_403acc, align 1
  %v7_403acc = load i32, i32* @edx, align 4
  %v8_403acc = udiv i32 %v7_403acc, 256
  %v9_403acc = trunc i32 %v8_403acc to i8
  %v10_403acc = add i8 %v6_403acc, %v9_403acc
  store i8 %v10_403acc, i8* %v5_403acc, align 1
  %v0_403ad0 = load i32, i32* @eax, align 4
  ret i32 %v0_403ad0
}

define i32 @function_403bbc() local_unnamed_addr {
dec_label_pc_403bbc:
  %v0_403bbc = load i32, i32* @eax, align 4
  ret i32 %v0_403bbc
}

define i32 @function_403bbf() local_unnamed_addr {
dec_label_pc_403bbf:
  %v0_403bbf = load i32, i32* @eax, align 4
  ret i32 %v0_403bbf
}

define i32 @function_403bd3() local_unnamed_addr {
dec_label_pc_403bd3:
  %v0_403bd3 = load i32, i32* @eax, align 4
  ret i32 %v0_403bd3
}

define i32 @function_403be6() local_unnamed_addr {
dec_label_pc_403be6:
  %v0_403be6 = load i32, i32* @eax, align 4
  ret i32 %v0_403be6
}

define i32 @function_403bf2() local_unnamed_addr {
dec_label_pc_403bf2:
  %v0_403bf2 = load i32, i32* @eax, align 4
  ret i32 %v0_403bf2
}

define i32 @function_403bff() local_unnamed_addr {
dec_label_pc_403bff:
  %v4_403bff = load i32, i32* @eax, align 4
  ret i32 %v4_403bff
}

define i32 @function_403c01(i32 %arg1, i32 %arg2, i32 %arg3) local_unnamed_addr {
dec_label_pc_403c01:
  %cf.global-to-local = alloca i1, align 1
  %stack_var_6 = alloca i32, align 4
  store i32 %arg3, i32* %stack_var_6, align 4
  %stack_var_0 = alloca i32, align 4
  store i32 %arg1, i32* %stack_var_0, align 4
  %v2_403c01 = load i8, i8* bitcast (i32* @eax to i8*), align 4
  %v3_403c01 = load i32, i32* @eax, align 4
  %v4_403c01 = trunc i32 %v3_403c01 to i8
  %v5_403c01 = add i8 %v2_403c01, %v4_403c01
  %v21_403c01 = inttoptr i32 %v3_403c01 to i8*
  store i8 %v5_403c01, i8* %v21_403c01, align 1
  %v2_403c03 = load i8, i8* bitcast (i32* @ebx to i8*), align 4
  %v3_403c03 = load i32, i32* @edx, align 4
  %v4_403c03 = trunc i32 %v3_403c03 to i8
  %v5_403c03 = add i8 %v2_403c03, %v4_403c03
  %v20_403c03 = load i32, i32* @ebx, align 4
  %v21_403c03 = inttoptr i32 %v20_403c03 to i8*
  store i8 %v5_403c03, i8* %v21_403c03, align 1
  %v2_403c05 = load i8, i8* bitcast (i32* @edi to i8*), align 4
  %v3_403c05 = load i32, i32* @eax, align 4
  %v4_403c05 = trunc i32 %v3_403c05 to i8
  %v5_403c05 = xor i8 %v2_403c05, %v4_403c05
  %v11_403c05 = load i32, i32* @edi, align 4
  %v12_403c05 = inttoptr i32 %v11_403c05 to i8*
  store i8 %v5_403c05, i8* %v12_403c05, align 1
  %v2_403c07 = load i8, i8* bitcast (i32* @eax to i8*), align 4
  %v3_403c07 = load i32, i32* @edx, align 4
  %v4_403c07 = trunc i32 %v3_403c07 to i8
  %v5_403c07 = add i8 %v2_403c07, %v4_403c07
  %v20_403c07 = load i32, i32* @eax, align 4
  %v21_403c07 = inttoptr i32 %v20_403c07 to i8*
  store i8 %v5_403c07, i8* %v21_403c07, align 1
  %v0_403c09 = load i32, i32* @eax, align 4
  %v4_403c09 = mul i32 %v0_403c09, 2
  store i32 %v4_403c09, i32* @eax, align 4
  %v2_403c0b = load i8, i8* bitcast (i32* @ecx to i8*), align 4
  %v3_403c0b = load i32, i32* @edx, align 4
  %v4_403c0b = trunc i32 %v3_403c0b to i8
  %v5_403c0b = add i8 %v2_403c0b, %v4_403c0b
  %v10_403c0b = icmp ult i8 %v5_403c0b, %v2_403c0b
  store i1 %v10_403c0b, i1* %cf.global-to-local, align 1
  %v20_403c0b = load i32, i32* @ecx, align 4
  %v21_403c0b = inttoptr i32 %v20_403c0b to i8*
  store i8 %v5_403c0b, i8* %v21_403c0b, align 1
  %v0_403c0d = load i32, i32* @eax, align 4
  %v1_403c0d = inttoptr i32 %v0_403c0d to i8*
  %v2_403c0d = load i8, i8* %v1_403c0d, align 1
  %v4_403c0d = trunc i32 %v0_403c0d to i8
  %v5_403c0d = add i8 %v2_403c0d, %v4_403c0d
  %v10_403c0d = icmp ult i8 %v5_403c0d, %v2_403c0d
  store i1 %v10_403c0d, i1* %cf.global-to-local, align 1
  store i8 %v5_403c0d, i8* %v1_403c0d, align 1
  %v0_403c0f = load i32, i32* @eax, align 4
  %v1_403c0f = inttoptr i32 %v0_403c0f to i32*
  %v2_403c0f = load i32, i32* %v1_403c0f, align 4
  %v3_403c0f = load i32, i32* @edi, align 4
  %v4_403c0f = load i1, i1* %cf.global-to-local, align 1
  %v5_403c0f = zext i1 %v4_403c0f to i32
  %v6_403c0f = add i32 %v3_403c0f, %v2_403c0f
  %v7_403c0f = add i32 %v6_403c0f, %v5_403c0f
  %v26_403c0f = icmp ule i32 %v7_403c0f, %v2_403c0f
  %v27_403c0f = icmp ult i32 %v6_403c0f, %v2_403c0f
  %v28_403c0f = select i1 %v4_403c0f, i1 %v26_403c0f, i1 %v27_403c0f
  store i1 %v28_403c0f, i1* %cf.global-to-local, align 1
  store i32 %v7_403c0f, i32* %v1_403c0f, align 4
  %v0_403c11 = load i32, i32* @eax, align 4
  %v1_403c11 = trunc i32 %v0_403c11 to i8
  %v3_403c11 = inttoptr i32 %v0_403c11 to i8*
  %v4_403c11 = load i8, i8* %v3_403c11, align 1
  %v5_403c11 = add i8 %v4_403c11, %v1_403c11
  %v10_403c11 = icmp ult i8 %v5_403c11, %v1_403c11
  store i1 %v10_403c11, i1* %cf.global-to-local, align 1
  %v20_403c11 = zext i8 %v5_403c11 to i32
  %v22_403c11 = and i32 %v0_403c11, -256
  %v23_403c11 = or i32 %v22_403c11, %v20_403c11
  store i32 %v23_403c11, i32* @eax, align 4
  %v1_403c13 = inttoptr i32 %v23_403c11 to i8*
  %v2_403c13 = load i8, i8* %v1_403c13, align 1
  %v5_403c13 = add i8 %v2_403c13, %v5_403c11
  store i8 %v5_403c13, i8* %v1_403c13, align 1
  %v2_403c18 = load i8, i8* bitcast (i32* @edi to i8*), align 4
  %v3_403c18 = load i32, i32* @eax, align 4
  %v4_403c18 = trunc i32 %v3_403c18 to i8
  %v5_403c18 = sub i8 %v2_403c18, %v4_403c18
  %v20_403c18 = load i32, i32* @edi, align 4
  %v21_403c18 = inttoptr i32 %v20_403c18 to i8*
  store i8 %v5_403c18, i8* %v21_403c18, align 1
  %v2_403c1b = load i8, i8* bitcast (i32* @esi to i8*), align 4
  %v3_403c1b = load i32, i32* @eax, align 4
  %v4_403c1b = trunc i32 %v3_403c1b to i8
  %v5_403c1b = add i8 %v2_403c1b, %v4_403c1b
  %v20_403c1b = load i32, i32* @esi, align 4
  %v21_403c1b = inttoptr i32 %v20_403c1b to i8*
  store i8 %v5_403c1b, i8* %v21_403c1b, align 1
  store i1 false, i1* %cf.global-to-local, align 1
  %v0_403c1f = load i32, i32* @eax, align 4
  %v1_403c1f = trunc i32 %v0_403c1f to i8
  %v3_403c1f = inttoptr i32 %v0_403c1f to i8*
  %v4_403c1f = load i8, i8* %v3_403c1f, align 1
  %v5_403c1f = add i8 %v4_403c1f, %v1_403c1f
  %v20_403c1f = zext i8 %v5_403c1f to i32
  %v22_403c1f = and i32 %v0_403c1f, -256
  %v23_403c1f = or i32 %v22_403c1f, %v20_403c1f
  store i32 %v23_403c1f, i32* @eax, align 4
  %v2_403c21 = load i8, i8* bitcast (i32* @esi to i8*), align 4
  %v4_403c21 = udiv i32 %v0_403c1f, 256
  %v5_403c21 = trunc i32 %v4_403c21 to i8
  %v6_403c21 = add i8 %v2_403c21, %v5_403c21
  %v21_403c21 = load i32, i32* @esi, align 4
  %v22_403c21 = inttoptr i32 %v21_403c21 to i8*
  store i8 %v6_403c21, i8* %v22_403c21, align 1
  %v2_403c23 = load i8, i8* bitcast (i32* @edi to i8*), align 4
  %v3_403c23 = load i32, i32* @eax, align 4
  %v4_403c23 = trunc i32 %v3_403c23 to i8
  %v5_403c23 = and i8 %v2_403c23, %v4_403c23
  store i1 false, i1* %cf.global-to-local, align 1
  %v11_403c23 = load i32, i32* @edi, align 4
  %v12_403c23 = inttoptr i32 %v11_403c23 to i8*
  store i8 %v5_403c23, i8* %v12_403c23, align 1
  %v0_403c25 = load i32, i32* @eax, align 4
  %v1_403c25 = inttoptr i32 %v0_403c25 to i8*
  %v2_403c25 = load i8, i8* %v1_403c25, align 1
  %v4_403c25 = trunc i32 %v0_403c25 to i8
  %v5_403c25 = add i8 %v2_403c25, %v4_403c25
  %v10_403c25 = icmp ult i8 %v5_403c25, %v2_403c25
  store i1 %v10_403c25, i1* %cf.global-to-local, align 1
  store i8 %v5_403c25, i8* %v1_403c25, align 1
  %v0_403c27 = load i32, i32* @eax, align 4
  %v1_403c27 = inttoptr i32 %v0_403c27 to i8*
  %v2_403c27 = load i8, i8* %v1_403c27, align 1
  %v3_403c27 = load i32, i32* @ebx, align 4
  %v4_403c27 = udiv i32 %v3_403c27, 256
  %v5_403c27 = trunc i32 %v4_403c27 to i8
  %v6_403c27 = add i8 %v2_403c27, %v5_403c27
  store i8 %v6_403c27, i8* %v1_403c27, align 1
  %v0_403c29 = load i32, i32* @eax, align 4
  %v1_403c29 = trunc i32 %v0_403c29 to i8
  %v4_403c29 = load i8, i8* bitcast (i32* @ecx to i8*), align 4
  %v5_403c29 = xor i8 %v4_403c29, %v1_403c29
  store i1 false, i1* %cf.global-to-local, align 1
  %v11_403c29 = zext i8 %v5_403c29 to i32
  %v13_403c29 = and i32 %v0_403c29, -256
  %v14_403c29 = or i32 %v13_403c29, %v11_403c29
  store i32 %v14_403c29, i32* @eax, align 4
  %v1_403c2b = inttoptr i32 %v14_403c29 to i8*
  %v2_403c2b = load i8, i8* %v1_403c2b, align 1
  %v5_403c2b = add i8 %v2_403c2b, %v5_403c29
  %v10_403c2b = icmp ult i8 %v5_403c2b, %v2_403c2b
  store i1 %v10_403c2b, i1* %cf.global-to-local, align 1
  store i8 %v5_403c2b, i8* %v1_403c2b, align 1
  %v7_403c2d = icmp sgt i8 %v5_403c2b, 0
  %v0_403c2f = load i32, i32* @eax, align 4
  %v1_403c2f = inttoptr i32 %v0_403c2f to i8*
  %v2_403c2f = load i8, i8* %v1_403c2f, align 1
  %v4_403c2f = trunc i32 %v0_403c2f to i8
  %v5_403c2f = add i8 %v2_403c2f, %v4_403c2f
  %v10_403c2f = icmp ult i8 %v5_403c2f, %v2_403c2f
  store i1 %v10_403c2f, i1* %cf.global-to-local, align 1
  store i8 %v5_403c2f, i8* %v1_403c2f, align 1
  %v0_403c31 = load i32, i32* @eax, align 4
  br i1 %v7_403c2d, label %dec_label_pc_403c2f, label %dec_label_pc_403c36

dec_label_pc_403c2f:                              ; preds = %dec_label_pc_403c01
  %v2_403c31 = add i32 %v0_403c31, 13
  %v16_403c31 = urem i32 %v2_403c31, 256
  %v18_403c31 = and i32 %v0_403c31, -256
  %v19_403c31 = or i32 %v16_403c31, %v18_403c31
  store i32 %v19_403c31, i32* @eax, align 4
  %v2_403c33 = load i8, i8* bitcast (i32* @ebx to i8*), align 4
  %v3_403c33 = load i32, i32* @edx, align 4
  %v4_403c33 = trunc i32 %v3_403c33 to i8
  %v5_403c33 = and i8 %v2_403c33, %v4_403c33
  store i1 false, i1* %cf.global-to-local, align 1
  %v11_403c33 = load i32, i32* @ebx, align 4
  %v12_403c33 = inttoptr i32 %v11_403c33 to i8*
  store i8 %v5_403c33, i8* %v12_403c33, align 1
  %v13_403c33 = load i32, i32* @eax, align 4
  ret i32 %v13_403c33

dec_label_pc_403c36:                              ; preds = %dec_label_pc_403c01
  %v1_403c3a = inttoptr i32 %v0_403c31 to i32*
  %v2_403c3a = load i32, i32* %v1_403c3a, align 4
  %v4_403c3a = add i32 %v2_403c3a, %v0_403c31
  store i32 %v4_403c3a, i32* %v1_403c3a, align 4
  %v2_403c3c = load i8, i8* bitcast (i32* @ecx to i8*), align 4
  %v3_403c3c = load i32, i32* @ecx, align 4
  %v4_403c3c = trunc i32 %v3_403c3c to i8
  %v5_403c3c = add i8 %v2_403c3c, %v4_403c3c
  %v10_403c3c = icmp ult i8 %v5_403c3c, %v2_403c3c
  store i1 %v10_403c3c, i1* %cf.global-to-local, align 1
  %v21_403c3c = inttoptr i32 %v3_403c3c to i8*
  store i8 %v5_403c3c, i8* %v21_403c3c, align 1
  %v0_403c3f = load i32, i32* @eax, align 4
  %v1_403c3f = add i32 %v0_403c3f, -109
  %v2_403c3f = inttoptr i32 %v1_403c3f to i8*
  %v3_403c3f = load i8, i8* %v2_403c3f, align 1
  %v4_403c3f = load i32, i32* @ebx, align 4
  %v5_403c3f = trunc i32 %v4_403c3f to i8
  %v6_403c3f = load i1, i1* %cf.global-to-local, align 1
  %v7_403c3f = zext i1 %v6_403c3f to i8
  %v8_403c3f = add i8 %v7_403c3f, %v5_403c3f
  %v9_403c3f = sub i8 %v3_403c3f, %v8_403c3f
  %v19_403c3f = sub i8 %v9_403c3f, %v7_403c3f
  %v20_403c3f = icmp ult i8 %v3_403c3f, %v19_403c3f
  %v21_403c3f = icmp ne i8 %v8_403c3f, -1
  %v22_403c3f = or i1 %v21_403c3f, %v20_403c3f
  %v23_403c3f = icmp ult i8 %v3_403c3f, %v8_403c3f
  %v24_403c3f = select i1 %v6_403c3f, i1 %v22_403c3f, i1 %v23_403c3f
  store i1 %v24_403c3f, i1* %cf.global-to-local, align 1
  store i8 %v9_403c3f, i8* %v2_403c3f, align 1
  %v0_403c43 = load i32, i32* @edx, align 4
  %v1_403c43 = add i32 %v0_403c43, 8
  %v2_403c43 = inttoptr i32 %v1_403c43 to i8*
  %v3_403c43 = load i8, i8* %v2_403c43, align 1
  %v4_403c43 = load i32, i32* @eax, align 4
  %v5_403c43 = udiv i32 %v4_403c43, 256
  %v6_403c43 = trunc i32 %v5_403c43 to i8
  %v7_403c43 = load i1, i1* %cf.global-to-local, align 1
  %v8_403c43 = zext i1 %v7_403c43 to i8
  %v9_403c43 = add i8 %v3_403c43, %v6_403c43
  %v10_403c43 = add i8 %v9_403c43, %v8_403c43
  store i8 %v10_403c43, i8* %v2_403c43, align 1
  %v2_403c46 = load i32, i32* %stack_var_0, align 4
  %v1_403c47 = trunc i32 %v2_403c46 to i8
  %v2_403c47 = or i8 %v1_403c47, 32
  store i1 false, i1* %cf.global-to-local, align 1
  %v8_403c47 = zext i8 %v2_403c47 to i32
  %v10_403c47 = and i32 %v2_403c46, -256
  %v11_403c47 = or i32 %v10_403c47, %v8_403c47
  store i32 %v11_403c47, i32* @eax, align 4
  %v1_403c4a = inttoptr i32 %v11_403c47 to i8*
  %v2_403c4a = load i8, i8* %v1_403c4a, align 1
  %v5_403c4a = add i8 %v2_403c4a, %v2_403c47
  store i8 %v5_403c4a, i8* %v1_403c4a, align 1
  %v2_403c4c = load i8, i8* bitcast (i32* @edi to i8*), align 4
  %v3_403c4c = load i32, i32* @edx, align 4
  %v4_403c4c = trunc i32 %v3_403c4c to i8
  %v5_403c4c = add i8 %v2_403c4c, %v4_403c4c
  %v10_403c4c = icmp ult i8 %v5_403c4c, %v2_403c4c
  store i1 %v10_403c4c, i1* %cf.global-to-local, align 1
  %v20_403c4c = load i32, i32* @edi, align 4
  %v21_403c4c = inttoptr i32 %v20_403c4c to i8*
  store i8 %v5_403c4c, i8* %v21_403c4c, align 1
  %v0_403c4e = load i32, i32* @ecx, align 4
  %v1_403c4e = trunc i32 %v0_403c4e to i8
  %v3_403c4e = load i32, i32* @eax, align 4
  %v5_403c4e = add i32 %v3_403c4e, %v0_403c4e
  %v6_403c4e = inttoptr i32 %v5_403c4e to i8*
  %v7_403c4e = load i8, i8* %v6_403c4e, align 1
  %v13_403c4e = icmp ugt i8 %v7_403c4e, %v1_403c4e
  store i1 %v13_403c4e, i1* %cf.global-to-local, align 1
  %v1_403c51 = inttoptr i32 %v3_403c4e to i8*
  %v2_403c51 = load i8, i8* %v1_403c51, align 1
  %v4_403c51 = trunc i32 %v3_403c4e to i8
  %v5_403c51 = add i8 %v2_403c51, %v4_403c51
  %v10_403c51 = icmp ult i8 %v5_403c51, %v2_403c51
  store i1 %v10_403c51, i1* %cf.global-to-local, align 1
  store i8 %v5_403c51, i8* %v1_403c51, align 1
  %v0_403c53 = load i32, i32* @eax, align 4
  %v1_403c53 = load i1, i1* %cf.global-to-local, align 1
  %v3_403c53 = select i1 %v1_403c53, i32 136317204, i32 136317203
  %v4_403c53 = sub i32 %v0_403c53, %v3_403c53
  %v18_403c53 = icmp ult i32 %v0_403c53, %v3_403c53
  %v19_403c53 = or i1 %v1_403c53, %v18_403c53
  store i1 %v19_403c53, i1* %cf.global-to-local, align 1
  %v29_403c53 = trunc i32 %v4_403c53 to i8
  store i32 %v4_403c53, i32* @eax, align 4
  %v1_403c59 = inttoptr i32 %v4_403c53 to i8*
  %v2_403c59 = load i8, i8* %v1_403c59, align 1
  %v5_403c59 = add i8 %v2_403c59, %v29_403c53
  %v10_403c59 = icmp ult i8 %v5_403c59, %v2_403c59
  store i1 %v10_403c59, i1* %cf.global-to-local, align 1
  store i8 %v5_403c59, i8* %v1_403c59, align 1
  %v0_403c5b = load i32, i32* @eax, align 4
  %v1_403c5b = inttoptr i32 %v0_403c5b to i8*
  %v2_403c5b = load i8, i8* %v1_403c5b, align 1
  %v3_403c5b = load i32, i32* @ebx, align 4
  %v4_403c5b = udiv i32 %v3_403c5b, 256
  %v5_403c5b = trunc i32 %v4_403c5b to i8
  %v6_403c5b = add i8 %v2_403c5b, %v5_403c5b
  %v11_403c5b = icmp ult i8 %v6_403c5b, %v2_403c5b
  store i1 %v11_403c5b, i1* %cf.global-to-local, align 1
  store i8 %v6_403c5b, i8* %v1_403c5b, align 1
  %v0_403c5d = load i32, i32* @eax, align 4
  %v1_403c5d = inttoptr i32 %v0_403c5d to i8*
  %v2_403c5d = load i8, i8* %v1_403c5d, align 1
  %v3_403c5d = add i8 %v2_403c5d, 1
  store i8 %v3_403c5d, i8* %v1_403c5d, align 1
  %v0_403c5f = load i32, i32* @eax, align 4
  %v1_403c5f = inttoptr i32 %v0_403c5f to i8*
  %v2_403c5f = load i8, i8* %v1_403c5f, align 1
  %v4_403c5f = trunc i32 %v0_403c5f to i8
  %v5_403c5f = add i8 %v2_403c5f, %v4_403c5f
  %v10_403c5f = icmp ult i8 %v5_403c5f, %v2_403c5f
  store i1 %v10_403c5f, i1* %cf.global-to-local, align 1
  store i8 %v5_403c5f, i8* %v1_403c5f, align 1
  %v0_403c61 = load i32, i32* @edi, align 4
  %v1_403c61 = add i32 %v0_403c61, 385875968
  %v2_403c61 = inttoptr i32 %v1_403c61 to i8*
  %v3_403c61 = load i8, i8* %v2_403c61, align 1
  %v4_403c61 = load i32, i32* @edx, align 4
  %v5_403c61 = trunc i32 %v4_403c61 to i8
  %v11_403c61 = icmp ult i8 %v3_403c61, %v5_403c61
  %v0_403c67 = load i32, i32* @eax, align 4
  %v1_403c67 = inttoptr i32 %v0_403c67 to i32*
  %v2_403c67 = load i32, i32* %v1_403c67, align 4
  %v3_403c67 = load i32, i32* @ecx, align 4
  %v5_403c67 = zext i1 %v11_403c61 to i32
  %v6_403c67 = add i32 %v3_403c67, %v2_403c67
  %v7_403c67 = add i32 %v6_403c67, %v5_403c67
  %v26_403c67 = icmp ule i32 %v7_403c67, %v2_403c67
  %v27_403c67 = icmp ult i32 %v6_403c67, %v2_403c67
  %v28_403c67 = select i1 %v11_403c61, i1 %v26_403c67, i1 %v27_403c67
  store i32 %v7_403c67, i32* %v1_403c67, align 4
  %v2_403c69 = load i32, i32* %stack_var_6, align 4
  store i32 %v2_403c69, i32* @eax, align 4
  %v0_403c6a = load i32, i32* @ecx, align 4
  %v2_403c6a = inttoptr i32 %v2_403c69 to i32*
  %v3_403c6a = load i32, i32* %v2_403c6a, align 4
  %v5_403c6a = zext i1 %v28_403c67 to i32
  %v6_403c6a = add i32 %v3_403c6a, %v0_403c6a
  %v7_403c6a = add i32 %v6_403c6a, %v5_403c6a
  %v20_403c6a = trunc i32 %v7_403c6a to i8
  %v26_403c6a = icmp ule i32 %v7_403c6a, %v0_403c6a
  %v27_403c6a = icmp ult i32 %v6_403c6a, %v0_403c6a
  %v28_403c6a = select i1 %v28_403c67, i1 %v26_403c6a, i1 %v27_403c6a
  store i1 %v28_403c6a, i1* %cf.global-to-local, align 1
  store i32 %v7_403c6a, i32* @ecx, align 4
  %v1_403c6c = inttoptr i32 %v7_403c6a to i8*
  %v2_403c6c = load i8, i8* %v1_403c6c, align 1
  %v5_403c6c = and i8 %v2_403c6c, %v20_403c6a
  store i1 false, i1* %cf.global-to-local, align 1
  store i8 %v5_403c6c, i8* %v1_403c6c, align 1
  %v0_403c6e = load i32, i32* @eax, align 4
  %v1_403c6e = inttoptr i32 %v0_403c6e to i8*
  %v2_403c6e = load i8, i8* %v1_403c6e, align 1
  %v4_403c6e = trunc i32 %v0_403c6e to i8
  %v5_403c6e = add i8 %v2_403c6e, %v4_403c6e
  %v10_403c6e = icmp ult i8 %v5_403c6e, %v2_403c6e
  store i1 %v10_403c6e, i1* %cf.global-to-local, align 1
  store i8 %v5_403c6e, i8* %v1_403c6e, align 1
  %v0_403c70 = load i32, i32* @eax, align 4
  %v1_403c70 = inttoptr i32 %v0_403c70 to i8*
  %v2_403c70 = load i8, i8* %v1_403c70, align 1
  %v3_403c70 = load i32, i32* @ebx, align 4
  %v4_403c70 = udiv i32 %v3_403c70, 256
  %v5_403c70 = trunc i32 %v4_403c70 to i8
  %v6_403c70 = add i8 %v2_403c70, %v5_403c70
  %v11_403c70 = icmp ult i8 %v6_403c70, %v2_403c70
  store i1 %v11_403c70, i1* %cf.global-to-local, align 1
  store i8 %v6_403c70, i8* %v1_403c70, align 1
  %v0_403c72 = call i32 @function_38403c77()
  store i32 %v0_403c72, i32* @eax, align 4
  ret i32 %v0_403c72
}

define i32 @function_403d17() local_unnamed_addr {
dec_label_pc_403d17:
  %v4_403d17 = load i32, i32* @eax, align 4
  ret i32 %v4_403d17
}

define i32 @function_403d51() local_unnamed_addr {
dec_label_pc_403d51:
  %v0_403d51 = load i32, i32* @eax, align 4
  ret i32 %v0_403d51
}

define i32 @function_403d73() local_unnamed_addr {
dec_label_pc_403d73:
  %v0_403d73 = load i32, i32* @eax, align 4
  ret i32 %v0_403d73
}

define i32 @function_403d83() local_unnamed_addr {
dec_label_pc_403d83:
  %v2_403d83 = load i32, i32* @eax, align 4
  %v3_403d83 = add i32 %v2_403d83, -1
  %v18_403d83 = inttoptr i32 %v2_403d83 to i32*
  store i32 %v3_403d83, i32* %v18_403d83, align 4
  %v2_403d86 = load i8, i8* bitcast (i32* @eax to i8*), align 4
  %v3_403d86 = load i32, i32* @eax, align 4
  %v4_403d86 = trunc i32 %v3_403d86 to i8
  %v5_403d86 = add i8 %v2_403d86, %v4_403d86
  %v21_403d86 = inttoptr i32 %v3_403d86 to i8*
  store i8 %v5_403d86, i8* %v21_403d86, align 1
  %v4_403d8b = load i32, i32* @eax, align 4
  ret i32 %v4_403d8b
}

define i32 @function_403da7() local_unnamed_addr {
dec_label_pc_403da7:
  %v0_403da7 = load i32, i32* @eax, align 4
  ret i32 %v0_403da7
}

define i32 @function_403e01() local_unnamed_addr {
dec_label_pc_403e01:
  %v0_403e01 = load i32, i32* @eax, align 4
  ret i32 %v0_403e01
}

define i32 @function_403e08(i32 %arg1) local_unnamed_addr {
dec_label_pc_403e08:
  %v0_403e08 = load i32, i32* @eax, align 4
  %v1_403e08 = udiv i32 %v0_403e08, 256
  %v6_403e08 = add i32 %v1_403e08, %v0_403e08
  %v21_403e08 = mul i32 %v6_403e08, 256
  %v23_403e08 = and i32 %v21_403e08, 65280
  %v24_403e08 = and i32 %v0_403e08, -65281
  %v25_403e08 = or i32 %v23_403e08, %v24_403e08
  store i32 %v25_403e08, i32* @eax, align 4
  ret i32 %v25_403e08
}

define i32 @function_403e46() local_unnamed_addr {
dec_label_pc_403e46:
  %v0_403e46 = load i32, i32* @eax, align 4
  ret i32 %v0_403e46
}

define i32 @function_403e85() local_unnamed_addr {
dec_label_pc_403e85:
  %v0_403e85 = load i32, i32* @eax, align 4
  ret i32 %v0_403e85
}

define i32 @function_403ea2() local_unnamed_addr {
dec_label_pc_403ea2:
  %v0_403ea2 = load i32, i32* @eax, align 4
  ret i32 %v0_403ea2
}

define i32 @function_403eb5() local_unnamed_addr {
dec_label_pc_403eb5:
  %v0_403eb5 = load i32, i32* @eax, align 4
  ret i32 %v0_403eb5
}

define i32 @function_403ee1() local_unnamed_addr {
dec_label_pc_403ee1:
  %v0_403ee1 = load i32, i32* @eax, align 4
  ret i32 %v0_403ee1
}

define i32 @function_403ee4() local_unnamed_addr {
dec_label_pc_403ee4:
  %cf.global-to-local = alloca i1, align 1
  %v0_403ee4 = load i32, i32* @ecx, align 4
  %v4_403ee4 = load i1, i1* %cf.global-to-local, align 1
  %v5_403ee4 = zext i1 %v4_403ee4 to i32
  %v6_403ee4 = mul i32 %v0_403ee4, 2
  %v7_403ee4 = or i32 %v6_403ee4, %v5_403ee4
  %v26_403ee4 = icmp ule i32 %v7_403ee4, %v0_403ee4
  %v27_403ee4 = icmp ult i32 %v6_403ee4, %v0_403ee4
  %v28_403ee4 = select i1 %v4_403ee4, i1 %v26_403ee4, i1 %v27_403ee4
  store i1 %v28_403ee4, i1* %cf.global-to-local, align 1
  store i32 %v7_403ee4, i32* @ecx, align 4
  %v0_403ee6 = load i32, i32* @edi, align 4
  %v2_403ee6 = mul i32 %v0_403ee6, 8
  %v3_403ee6 = add i32 %v0_403ee6, 539426815
  %v4_403ee6 = add i32 %v3_403ee6, %v2_403ee6
  %v5_403ee6 = inttoptr i32 %v4_403ee6 to i8*
  %v6_403ee6 = load i8, i8* %v5_403ee6, align 1
  %v7_403ee6 = load i32, i32* @ebx, align 4
  %v8_403ee6 = trunc i32 %v7_403ee6 to i8
  %v14_403ee6 = icmp ult i8 %v6_403ee6, %v8_403ee6
  %v2_403eed = load i32, i32* @eax, align 4
  %v7_403eed = sext i1 %v14_403ee6 to i32
  %v37_403eed = inttoptr i32 %v2_403eed to i32*
  store i32 %v7_403eed, i32* %v37_403eed, align 4
  %v2_403eef = load i8, i8* bitcast (i32* @eax to i8*), align 4
  %v3_403eef = load i32, i32* @eax, align 4
  %v4_403eef = trunc i32 %v3_403eef to i8
  %v5_403eef = add i8 %v2_403eef, %v4_403eef
  %v21_403eef = inttoptr i32 %v3_403eef to i8*
  store i8 %v5_403eef, i8* %v21_403eef, align 1
  %v2_403ef1 = load i8, i8* bitcast (i32* @esi to i8*), align 4
  %v3_403ef1 = load i32, i32* @eax, align 4
  %v4_403ef1 = trunc i32 %v3_403ef1 to i8
  %v5_403ef1 = sub i8 %v2_403ef1, %v4_403ef1
  %v20_403ef1 = load i32, i32* @esi, align 4
  %v21_403ef1 = inttoptr i32 %v20_403ef1 to i8*
  store i8 %v5_403ef1, i8* %v21_403ef1, align 1
  %v2_403ef4 = load i8, i8* bitcast (i32* @esi to i8*), align 4
  %v3_403ef4 = load i32, i32* @eax, align 4
  %v4_403ef4 = trunc i32 %v3_403ef4 to i8
  %v5_403ef4 = add i8 %v2_403ef4, %v4_403ef4
  %v10_403ef4 = icmp ult i8 %v5_403ef4, %v2_403ef4
  store i1 %v10_403ef4, i1* %cf.global-to-local, align 1
  %v20_403ef4 = load i32, i32* @esi, align 4
  %v21_403ef4 = inttoptr i32 %v20_403ef4 to i8*
  store i8 %v5_403ef4, i8* %v21_403ef4, align 1
  %v0_403ef6 = load i32, i32* @eax, align 4
  %v1_403ef6 = udiv i32 %v0_403ef6, 256
  %v2_403ef6 = trunc i32 %v1_403ef6 to i8
  %v3_403ef6 = load i32, i32* @esi, align 4
  %v4_403ef6 = load i32, i32* @edi, align 4
  %v5_403ef6 = mul i32 %v4_403ef6, 8
  %v6_403ef6 = add i32 %v3_403ef6, -1
  %v7_403ef6 = add i32 %v6_403ef6, %v5_403ef6
  %v8_403ef6 = inttoptr i32 %v7_403ef6 to i8*
  %v9_403ef6 = load i8, i8* %v8_403ef6, align 1
  %v15_403ef6 = icmp ugt i8 %v9_403ef6, %v2_403ef6
  store i1 %v15_403ef6, i1* %cf.global-to-local, align 1
  ret i32 %v0_403ef6
}

define i32 @function_403f1b() local_unnamed_addr {
dec_label_pc_403f1b:
  %v0_403f1b = load i32, i32* @eax, align 4
  ret i32 %v0_403f1b
}

define i32 @function_403fd6() local_unnamed_addr {
dec_label_pc_403fd6:
  %v4_403fd6 = load i32, i32* @eax, align 4
  ret i32 %v4_403fd6
}

define i32 @function_403fdc(i32 %arg1) local_unnamed_addr {
dec_label_pc_403fdc:
  %v2_403fdd = load i8, i8* bitcast (i32* @eax to i8*), align 4
  %v3_403fdd = load i32, i32* @eax, align 4
  %v4_403fdd = trunc i32 %v3_403fdd to i8
  %v5_403fdd = add i8 %v2_403fdd, %v4_403fdd
  %v21_403fdd = inttoptr i32 %v3_403fdd to i8*
  store i8 %v5_403fdd, i8* %v21_403fdd, align 1
  %v2_403fdf = load i8, i8* bitcast (i32* @eax to i8*), align 4
  %v3_403fdf = load i32, i32* @ebx, align 4
  %v4_403fdf = udiv i32 %v3_403fdf, 256
  %v5_403fdf = trunc i32 %v4_403fdf to i8
  %v6_403fdf = add i8 %v2_403fdf, %v5_403fdf
  %v21_403fdf = load i32, i32* @eax, align 4
  %v22_403fdf = inttoptr i32 %v21_403fdf to i8*
  store i8 %v6_403fdf, i8* %v22_403fdf, align 1
  %v2_403fe1 = load i8, i8* bitcast (i32* @eax to i8*), align 4
  %v3_403fe1 = load i32, i32* @eax, align 4
  %v4_403fe1 = trunc i32 %v3_403fe1 to i8
  %v5_403fe1 = add i8 %v2_403fe1, %v4_403fe1
  %v21_403fe1 = inttoptr i32 %v3_403fe1 to i8*
  store i8 %v5_403fe1, i8* %v21_403fe1, align 1
  %v2_403fe4 = load i8, i8* bitcast (i32* @eax to i8*), align 4
  %v3_403fe4 = load i32, i32* @ebx, align 4
  %v4_403fe4 = udiv i32 %v3_403fe4, 256
  %v5_403fe4 = trunc i32 %v4_403fe4 to i8
  %v6_403fe4 = add i8 %v2_403fe4, %v5_403fe4
  %v21_403fe4 = load i32, i32* @eax, align 4
  %v22_403fe4 = inttoptr i32 %v21_403fe4 to i8*
  store i8 %v6_403fe4, i8* %v22_403fe4, align 1
  %v5_403fe6 = load i32, i32* @eax, align 4
  ret i32 %v5_403fe6
}

define i32 @function_403ffd() local_unnamed_addr {
dec_label_pc_403ffd:
  %v0_403ffd = load i32, i32* @eax, align 4
  ret i32 %v0_403ffd
}

define i32 @function_403fff() local_unnamed_addr {
dec_label_pc_403fff:
  %v0_403fff = load i32, i32* @eax, align 4
  %v1_403fff = trunc i32 %v0_403fff to i8
  %v5_403fff = mul i8 %v1_403fff, 2
  %v20_403fff = zext i8 %v5_403fff to i32
  %v22_403fff = and i32 %v0_403fff, -256
  %v23_403fff = or i32 %v22_403fff, %v20_403fff
  store i32 %v23_403fff, i32* @eax, align 4
  %v1_404001 = inttoptr i32 %v23_403fff to i8*
  %v2_404001 = load i8, i8* %v1_404001, align 2
  %v5_404001 = add i8 %v2_404001, %v5_403fff
  store i8 %v5_404001, i8* %v1_404001, align 2
  %v2_404003 = load i8, i8* bitcast (i32* @esi to i8*), align 4
  %v3_404003 = add i8 %v2_404003, -1
  %v16_404003 = load i32, i32* @esi, align 4
  %v17_404003 = inttoptr i32 %v16_404003 to i8*
  store i8 %v3_404003, i8* %v17_404003, align 1
  %v0_404005 = load i32, i32* @eax, align 4
  %v1_404005 = inttoptr i32 %v0_404005 to i32*
  %v2_404005 = load i32, i32* %v1_404005, align 4
  %v4_404005 = add i32 %v2_404005, %v0_404005
  store i32 %v4_404005, i32* %v1_404005, align 4
  %v0_404007 = load i32, i32* @ecx, align 4
  %v1_404007 = load i32, i32* @eax, align 4
  %v3_404007 = add i32 %v1_404007, %v0_404007
  %v4_404007 = inttoptr i32 %v3_404007 to i8*
  %v5_404007 = load i8, i8* %v4_404007, align 1
  %v6_404007 = add i8 %v5_404007, -1
  store i8 %v6_404007, i8* %v4_404007, align 1
  %v0_40400a = load i32, i32* @ebp, align 4
  %v1_40400a = add i32 %v0_40400a, 9
  %v2_40400a = inttoptr i32 %v1_40400a to i8*
  %v3_40400a = load i8, i8* %v2_40400a, align 1
  %v4_40400a = load i32, i32* @eax, align 4
  %v5_40400a = trunc i32 %v4_40400a to i8
  %v6_40400a = add i8 %v3_40400a, %v5_40400a
  store i8 %v6_40400a, i8* %v2_40400a, align 1
  %v0_40400d = load i32, i32* @eax, align 4
  %v1_40400d = inttoptr i32 %v0_40400d to i8*
  %v2_40400d = load i8, i8* %v1_40400d, align 1
  %v4_40400d = trunc i32 %v0_40400d to i8
  %v5_40400d = add i8 %v2_40400d, %v4_40400d
  store i8 %v5_40400d, i8* %v1_40400d, align 1
  %v0_40400f = load i32, i32* @edi, align 4
  %v2_40400f = mul i32 %v0_40400f, 8
  %v3_40400f = add i32 %v0_40400f, -1
  %v4_40400f = add i32 %v3_40400f, %v2_40400f
  %v5_40400f = inttoptr i32 %v4_40400f to i8*
  %v6_40400f = load i8, i8* %v5_40400f, align 1
  %v7_40400f = load i32, i32* @ebx, align 4
  %v8_40400f = udiv i32 %v7_40400f, 256
  %v9_40400f = trunc i32 %v8_40400f to i8
  %v10_40400f = add i8 %v6_40400f, %v9_40400f
  store i8 %v10_40400f, i8* %v5_40400f, align 1
  %v0_404013 = load i32, i32* @eax, align 4
  ret i32 %v0_404013
}

define i32 @function_40401b() local_unnamed_addr {
dec_label_pc_40401b:
  %v0_40401b = load i32, i32* @eax, align 4
  ret i32 %v0_40401b
}

define i32 @function_40403e() local_unnamed_addr {
dec_label_pc_40403e:
  %v0_40403e = load i32, i32* @eax, align 4
  ret i32 %v0_40403e
}

define i32 @function_404044(i32 %arg1, i32 %arg2, i32 %arg3, i32 %arg4) local_unnamed_addr {
dec_label_pc_404044:
  %cf.global-to-local = alloca i1, align 1
  %ds.global-to-local = alloca i16, align 2
  %ss.global-to-local = alloca i16, align 2
  %zf.global-to-local = alloca i1, align 1
  %stack_var_-8 = alloca i8*, align 4
  %stack_var_-6 = alloca i16, align 2
  %stack_var_0 = alloca i32, align 4
  store i32 %arg1, i32* %stack_var_0, align 4
  %v2_404044 = load i32, i32* @eax, align 4
  %v3_404044 = add i32 %v2_404044, 1
  %v18_404044 = inttoptr i32 %v2_404044 to i32*
  store i32 %v3_404044, i32* %v18_404044, align 4
  %v2_404046 = load i8, i8* bitcast (i32* @eax to i8*), align 4
  %v3_404046 = load i32, i32* @eax, align 4
  %v4_404046 = trunc i32 %v3_404046 to i8
  %v5_404046 = add i8 %v2_404046, %v4_404046
  %v21_404046 = inttoptr i32 %v3_404046 to i8*
  store i8 %v5_404046, i8* %v21_404046, align 1
  %v2_404048 = load i32, i32* %stack_var_0, align 4
  store i32 %v2_404048, i32* @edx, align 4
  %v3_404049 = load i32, i32* @eax, align 4
  %v4_404049 = trunc i32 %v3_404049 to i8
  %v5_40404b = mul i8 %v4_404049, 2
  %v21_40404b = inttoptr i32 %v3_404049 to i8*
  store i8 %v5_40404b, i8* %v21_40404b, align 1
  %v2_40404d = load i8, i8* bitcast (i32* @esi to i8*), align 4
  %v3_40404d = load i32, i32* @eax, align 4
  %v4_40404d = udiv i32 %v3_40404d, 256
  %v5_40404d = trunc i32 %v4_40404d to i8
  %v6_40404d = add i8 %v2_40404d, %v5_40404d
  %v21_40404d = load i32, i32* @esi, align 4
  %v22_40404d = inttoptr i32 %v21_40404d to i8*
  store i8 %v6_40404d, i8* %v22_40404d, align 1
  %v0_404050 = load i32, i32* @edx, align 4
  %v3_404050 = load i32, i32* @ebx, align 4
  %v4_404050 = and i32 %v3_404050, 65280
  %v1_40405036 = add i32 %v4_404050, %v0_404050
  %v23_404050 = and i32 %v1_40405036, 65280
  %v24_404050 = and i32 %v0_404050, -65281
  %v25_404050 = or i32 %v23_404050, %v24_404050
  store i32 %v25_404050, i32* @edx, align 4
  %v2_404052 = load i32, i32* @eax, align 4
  %v10_404052 = inttoptr i32 %v2_404052 to i32*
  store i32 %v2_404052, i32* %v10_404052, align 4
  %v0_404054 = load i32, i32* @edx, align 4
  %v3_404054 = load i32, i32* @ebx, align 4
  %v4_404054 = and i32 %v3_404054, 65280
  %v1_40405438 = add i32 %v4_404054, %v0_404054
  %v23_404054 = and i32 %v1_40405438, 65280
  %v24_404054 = and i32 %v0_404054, -65281
  %v25_404054 = or i32 %v23_404054, %v24_404054
  %v26_404054 = inttoptr i32 %v25_404054 to i8*
  store i32 %v25_404054, i32* @edx, align 4
  %v2_404056 = load i32, i32* @ecx, align 4
  %v3_404056 = load i32, i32* @eax, align 4
  %v4_404056 = or i32 %v3_404056, %v2_404056
  %v12_404056 = inttoptr i32 %v2_404056 to i32*
  store i32 %v4_404056, i32* %v12_404056, align 4
  %v2_404058 = load i8, i8* bitcast (i32* @eax to i8*), align 4
  %v3_404058 = load i32, i32* @ecx, align 4
  %v4_404058 = udiv i32 %v3_404058, 256
  %v5_404058 = trunc i32 %v4_404058 to i8
  %v6_404058 = add i8 %v2_404058, %v5_404058
  %v21_404058 = load i32, i32* @eax, align 4
  %v22_404058 = inttoptr i32 %v21_404058 to i8*
  store i8 %v6_404058, i8* %v22_404058, align 1
  %v0_40405a = load i16, i16* %ds.global-to-local, align 2
  %v4_40405a = sext i16 %v0_40405a to i32
  store i32 %v4_40405a, i32* %stack_var_0, align 4
  %v2_40405b = load i8, i8* bitcast (i32* @eax to i8*), align 4
  %v3_40405b = load i32, i32* @eax, align 4
  %v4_40405b = trunc i32 %v3_40405b to i8
  %v5_40405b = add i8 %v2_40405b, %v4_40405b
  %v10_40405b = icmp ult i8 %v5_40405b, %v2_40405b
  store i1 %v10_40405b, i1* %cf.global-to-local, align 1
  %v15_40405b = icmp eq i8 %v5_40405b, 0
  store i1 %v15_40405b, i1* %zf.global-to-local, align 1
  %v21_40405b = inttoptr i32 %v3_40405b to i8*
  store i8 %v5_40405b, i8* %v21_40405b, align 1
  %v0_40405d = load i32, i32* @ecx, align 4
  %v1_40405d = udiv i32 %v0_40405d, 256
  %v2_40405d = trunc i32 %v1_40405d to i8
  %v6_40405d = load i8, i8* %v26_404054, align 1
  %v7_40405d = or i8 %v6_40405d, %v2_40405d
  %v13_40405d = zext i8 %v7_40405d to i32
  %v15_40405d = mul i32 %v13_40405d, 256
  %v16_40405d = and i32 %v0_40405d, -65281
  %v17_40405d = or i32 %v15_40405d, %v16_40405d
  store i32 %v17_40405d, i32* @ecx, align 4
  %v2_40405f = load i8, i8* bitcast (i32* @esi to i8*), align 4
  %v3_40405f = load i32, i32* @eax, align 4
  %v4_40405f = udiv i32 %v3_40405f, 256
  %v5_40405f = trunc i32 %v4_40405f to i8
  %v6_40405f = add i8 %v2_40405f, %v5_40405f
  %v21_40405f = load i32, i32* @esi, align 4
  %v22_40405f = inttoptr i32 %v21_40405f to i8*
  store i8 %v6_40405f, i8* %v22_40405f, align 1
  %v4_404061 = load i32, i32* @eax, align 4
  %v5_404061 = trunc i32 %v4_404061 to i8
  %v5_404063 = mul i8 %v5_404061, 2
  %v21_404063 = inttoptr i32 %v4_404061 to i8*
  store i8 %v5_404063, i8* %v21_404063, align 1
  %v2_404065 = load i8, i8* bitcast (i32* @esi to i8*), align 4
  %v3_404065 = load i32, i32* @eax, align 4
  %v4_404065 = udiv i32 %v3_404065, 256
  %v5_404065 = trunc i32 %v4_404065 to i8
  %v6_404065 = add i8 %v2_404065, %v5_404065
  %v21_404065 = load i32, i32* @esi, align 4
  %v22_404065 = inttoptr i32 %v21_404065 to i8*
  store i8 %v6_404065, i8* %v22_404065, align 1
  %v0_404069 = load i32, i32* @eax, align 4
  %v22_404069 = and i32 %v0_404069, -256
  store i32 %v22_404069, i32* @eax, align 4
  %v2_40406b = load i8, i8* bitcast (i32* @esi to i8*), align 4
  %v4_40406b = udiv i32 %v0_404069, 256
  %v5_40406b = trunc i32 %v4_40406b to i8
  %v6_40406b = add i8 %v2_40406b, %v5_40406b
  %v11_40406b = icmp ult i8 %v6_40406b, %v2_40406b
  store i1 %v11_40406b, i1* %cf.global-to-local, align 1
  %v16_40406b = icmp eq i8 %v6_40406b, 0
  store i1 %v16_40406b, i1* %zf.global-to-local, align 1
  %v21_40406b = load i32, i32* @esi, align 4
  %v22_40406b = inttoptr i32 %v21_40406b to i8*
  store i8 %v6_40406b, i8* %v22_40406b, align 1
  %v3_40406d = load i8, i8* %v26_404054, align 1
  %v4_40406d = load i32, i32* @eax, align 4
  %v5_40406d = trunc i32 %v4_40406d to i8
  %v11_40406d = icmp ult i8 %v3_40406d, %v5_40406d
  store i1 %v11_40406d, i1* %cf.global-to-local, align 1
  %v16_40406d = icmp eq i8 %v3_40406d, %v5_40406d
  store i1 %v16_40406d, i1* %zf.global-to-local, align 1
  %v1_40406f = inttoptr i32 %v4_40406d to i8*
  %v2_40406f = load i8, i8* %v1_40406f, align 1
  %v5_40406f = add i8 %v2_40406f, %v5_40406d
  store i8 %v5_40406f, i8* %v1_40406f, align 1
  %v2_404071 = load i8, i8* bitcast (i32* @esi to i8*), align 4
  %v3_404071 = load i32, i32* @eax, align 4
  %v4_404071 = udiv i32 %v3_404071, 256
  %v5_404071 = trunc i32 %v4_404071 to i8
  %v6_404071 = add i8 %v2_404071, %v5_404071
  %v11_404071 = icmp ult i8 %v6_404071, %v2_404071
  store i1 %v11_404071, i1* %cf.global-to-local, align 1
  %v16_404071 = icmp eq i8 %v6_404071, 0
  store i1 %v16_404071, i1* %zf.global-to-local, align 1
  %v21_404071 = load i32, i32* @esi, align 4
  %v22_404071 = inttoptr i32 %v21_404071 to i8*
  store i8 %v6_404071, i8* %v22_404071, align 1
  %v0_404075 = load i32, i32* @eax, align 4
  %v1_404075 = trunc i32 %v0_404075 to i8
  %v3_404075 = inttoptr i32 %v0_404075 to i8*
  %v4_404075 = load i8, i8* %v3_404075, align 1
  %v5_404075 = sub i8 %v1_404075, %v4_404075
  %v10_404075 = icmp ugt i8 %v4_404075, %v1_404075
  store i1 %v10_404075, i1* %cf.global-to-local, align 1
  %v15_404075 = icmp eq i8 %v5_404075, 0
  store i1 %v15_404075, i1* %zf.global-to-local, align 1
  %v20_404075 = zext i8 %v5_404075 to i32
  %v22_404075 = and i32 %v0_404075, -256
  %v23_404075 = or i32 %v22_404075, %v20_404075
  store i32 %v23_404075, i32* @eax, align 4
  %v0_404077 = load i32, i32* @esi, align 4
  %v1_404077 = add i32 %v0_404077, 56
  %v2_404077 = inttoptr i32 %v1_404077 to i8*
  %v3_404077 = load i8, i8* %v2_404077, align 1
  %v6_404077 = add i8 %v3_404077, %v5_404075
  %v11_404077 = icmp ult i8 %v6_404077, %v3_404077
  store i1 %v11_404077, i1* %cf.global-to-local, align 1
  %v16_404077 = icmp eq i8 %v6_404077, 0
  store i1 %v16_404077, i1* %zf.global-to-local, align 1
  store i8 %v6_404077, i8* %v2_404077, align 1
  %v0_40407a = load i32, i32* @eax, align 4
  %v1_40407a = trunc i32 %v0_40407a to i8
  %v3_40407a = inttoptr i32 %v0_40407a to i8*
  %v4_40407a = load i8, i8* %v3_40407a, align 1
  %v5_40407a = add i8 %v4_40407a, %v1_40407a
  %v10_40407a = icmp ult i8 %v5_40407a, %v1_40407a
  store i1 %v10_40407a, i1* %cf.global-to-local, align 1
  %v15_40407a = icmp eq i8 %v5_40407a, 0
  store i1 %v15_40407a, i1* %zf.global-to-local, align 1
  %v20_40407a = zext i8 %v5_40407a to i32
  %v22_40407a = and i32 %v0_40407a, -256
  %v23_40407a = or i32 %v22_40407a, %v20_40407a
  %v24_40407a = inttoptr i32 %v23_40407a to i8*
  store i32 %v23_40407a, i32* @eax, align 4
  %v3_40407c = load i8, i8* %v24_40407a, align 1
  %v7_40407c = add i8 %v3_40407c, %v5_40407a
  %v12_40407c = icmp ult i8 %v7_40407c, %v3_40407c
  store i1 %v12_40407c, i1* %cf.global-to-local, align 1
  %v17_40407c = icmp eq i8 %v7_40407c, 0
  store i1 %v17_40407c, i1* %zf.global-to-local, align 1
  store i8 %v7_40407c, i8* %v24_40407a, align 1
  %v0_40407e = load i16, i16* %ss.global-to-local, align 2
  store i16 %v0_40407e, i16* %stack_var_-6, align 2
  %v0_404080 = load i32, i32* @ecx, align 4
  %v1_404080 = inttoptr i32 %v0_404080 to i8*
  %v2_404080 = load i8, i8* %v1_404080, align 1
  %v3_404080 = add i8 %v2_404080, -1
  %v11_404080 = icmp eq i8 %v3_404080, 0
  store i1 %v11_404080, i1* %zf.global-to-local, align 1
  store i8 %v3_404080, i8* %v1_404080, align 1
  %v3_404082 = load i8, i8* %v24_40407a, align 1
  %v7_404082 = add i8 %v3_404082, %v5_40407a
  %v12_404082 = icmp ult i8 %v7_404082, %v3_404082
  store i1 %v12_404082, i1* %cf.global-to-local, align 1
  %v17_404082 = icmp eq i8 %v7_404082, 0
  store i1 %v17_404082, i1* %zf.global-to-local, align 1
  store i8 %v7_404082, i8* %v24_40407a, align 1
  %v2_404084 = trunc i32 %v25_404054 to i16
  %v5_404084 = load i32, i32* @esi, align 4
  call void @__asm_outsd(i16 %v2_404084, i32 %v5_404084)
  %v2_404085 = load i16, i16* %stack_var_-6, align 2
  store i16 %v2_404085, i16* %ds.global-to-local, align 2
  %v3_404086 = load i8, i8* %v24_40407a, align 1
  %v7_404086 = add i8 %v3_404086, %v5_40407a
  %v12_404086 = icmp ult i8 %v7_404086, %v3_404086
  store i1 %v12_404086, i1* %cf.global-to-local, align 1
  %v17_404086 = icmp eq i8 %v7_404086, 0
  store i1 %v17_404086, i1* %zf.global-to-local, align 1
  store i8 %v7_404086, i8* %v24_40407a, align 1
  %v0_404088 = load i32, i32* @ecx, align 4
  %v1_404088 = udiv i32 %v0_404088, 256
  %v2_404088 = trunc i32 %v1_404088 to i8
  %v6_404088 = load i8, i8* %v26_404054, align 1
  %v7_404088 = or i8 %v6_404088, %v2_404088
  store i1 false, i1* %cf.global-to-local, align 1
  %v8_404088 = icmp eq i8 %v7_404088, 0
  store i1 %v8_404088, i1* %zf.global-to-local, align 1
  %v13_404088 = zext i8 %v7_404088 to i32
  %v15_404088 = mul i32 %v13_404088, 256
  %v16_404088 = and i32 %v0_404088, -65281
  %v17_404088 = or i32 %v15_404088, %v16_404088
  store i32 %v17_404088, i32* @ecx, align 4
  %v3_40408a = load i8, i8* %v24_40407a, align 1
  %v7_40408a = add i8 %v3_40408a, %v5_40407a
  store i8 %v7_40408a, i8* %v24_40407a, align 1
  %v2_40408c = add i32 %v25_404054, 1
  %v0_40408d = load i32, i32* @ebx, align 4
  %v4_40408d = inttoptr i32 %v0_40408d to i8*
  store i8* %v4_40408d, i8** %stack_var_-8, align 4
  store i32 %v2_40408c, i32* @edx, align 4
  %tmp321 = inttoptr i32 %v23_40407a to i32*
  %v3_404090 = load i32, i32* %tmp321, align 4
  %v6_404090 = add i32 %v3_404090, %v23_40407a
  %v6_404092 = add i32 %v6_404090, %v23_40407a
  %v11_404092 = icmp ult i32 %v6_404092, %v6_404090
  store i1 %v11_404092, i1* %cf.global-to-local, align 1
  %v16_404092 = icmp eq i32 %v6_404092, 0
  store i1 %v16_404092, i1* %zf.global-to-local, align 1
  store i32 %v6_404092, i32* %tmp321, align 4
  %v3_404094 = load i8, i8* %v24_40407a, align 1
  %factor = mul i8 %v5_40407a, 2
  %v7_404096 = add i8 %v3_404094, %factor
  store i8 %v7_404096, i8* %v24_40407a, align 1
  store i1 false, i1* %cf.global-to-local, align 1
  store i1 %v15_40407a, i1* %zf.global-to-local, align 1
  store i32 %v23_40407a, i32* @eax, align 4
  %v3_40409a = load i8, i8* %v24_40407a, align 1
  %v7_40409a = add i8 %v3_40409a, %v5_40407a
  %v12_40409a = icmp ult i8 %v7_40409a, %v3_40409a
  store i1 %v12_40409a, i1* %cf.global-to-local, align 1
  %v17_40409a = icmp eq i8 %v7_40409a, 0
  store i1 %v17_40409a, i1* %zf.global-to-local, align 1
  store i8 %v7_40409a, i8* %v24_40407a, align 1
  %v0_40409c = load i1, i1* %cf.global-to-local, align 1
  %v1_40409c = load i1, i1* %zf.global-to-local, align 1
  %v2_40409c = or i1 %v0_40409c, %v1_40409c
  br i1 %v2_40409c, label %dec_label_pc_4040d0, label %dec_label_pc_4040c5

dec_label_pc_4040c5:                              ; preds = %dec_label_pc_404044
  %v2_40409e = load i8, i8* bitcast (i32* @esi to i8*), align 4
  %v3_40409e = load i32, i32* @ecx, align 4
  %v4_40409e = udiv i32 %v3_40409e, 256
  %v5_40409e = trunc i32 %v4_40409e to i8
  %v6_40409e = xor i8 %v2_40409e, %v5_40409e
  %v12_40409e = load i32, i32* @esi, align 4
  %v13_40409e = inttoptr i32 %v12_40409e to i8*
  store i8 %v6_40409e, i8* %v13_40409e, align 1
  %v2_4040a1 = xor i32 %v23_40407a, 926037808
  store i1 false, i1* %cf.global-to-local, align 1
  %v3_4040a1 = icmp eq i32 %v2_4040a1, 0
  store i1 %v3_4040a1, i1* %zf.global-to-local, align 1
  %v5_4040a1 = trunc i32 %v2_4040a1 to i8
  store i32 %v2_4040a1, i32* @eax, align 4
  %v1_4040a6 = inttoptr i32 %v2_4040a1 to i8*
  %v2_4040a6 = load i8, i8* %v1_4040a6, align 1
  %v5_4040a6 = add i8 %v2_4040a6, %v5_4040a1
  %v10_4040a6 = icmp ult i8 %v5_4040a6, %v2_4040a6
  store i1 %v10_4040a6, i1* %cf.global-to-local, align 1
  %v15_4040a6 = icmp eq i8 %v5_4040a6, 0
  store i1 %v15_4040a6, i1* %zf.global-to-local, align 1
  store i8 %v5_4040a6, i8* %v1_4040a6, align 1
  %v0_4040a8 = load i32, i32* @eax, align 4
  %v1_4040a8 = inttoptr i32 %v0_4040a8 to i8*
  %v2_4040a8 = load i8, i8* %v1_4040a8, align 1
  %v4_4040a8 = trunc i32 %v0_4040a8 to i8
  %v5_4040a8 = add i8 %v2_4040a8, %v4_4040a8
  store i8 %v5_4040a8, i8* %v1_4040a8, align 1
  %v0_4040aa = load i32, i32* @eax, align 4
  %v1_4040aa = add i32 %v0_4040aa, 27648
  %v11_4040aa = trunc i32 %v1_4040aa to i8
  %v1_4040af = udiv i32 %v1_4040aa, 256
  %v2_4040af = trunc i32 %v1_4040af to i8
  %v3_4040af = load i32, i32* @ebx, align 4
  %v4_4040af = udiv i32 %v3_4040af, 256
  %v5_4040af = trunc i32 %v4_4040af to i8
  %v6_4040af = add i8 %v5_4040af, %v2_4040af
  %v11_4040af = icmp ult i8 %v6_4040af, %v2_4040af
  store i1 %v11_4040af, i1* %cf.global-to-local, align 1
  %v16_4040af = icmp eq i8 %v6_4040af, 0
  store i1 %v16_4040af, i1* %zf.global-to-local, align 1
  %v21_4040af = zext i8 %v6_4040af to i32
  %v23_4040af = mul i32 %v21_4040af, 256
  %v24_4040af = and i32 %v1_4040aa, -65281
  %v25_4040af = or i32 %v23_4040af, %v24_4040af
  %v26_4040af = inttoptr i32 %v25_4040af to i8*
  store i32 %v25_4040af, i32* @eax, align 4
  %v3_4040b1 = load i8, i8* %v26_4040af, align 1
  %v7_4040b1 = or i8 %v3_4040b1, %v11_4040aa
  store i8 %v7_4040b1, i8* %v26_4040af, align 1
  %v2_4040b3 = load i8, i8* bitcast (i32* @ebx to i8*), align 4
  %v7_4040b3 = add i8 %v2_4040b3, %v6_4040af
  %v12_4040b3 = icmp ult i8 %v7_4040b3, %v2_4040b3
  store i1 %v12_4040b3, i1* %cf.global-to-local, align 1
  %v17_4040b3 = icmp eq i8 %v7_4040b3, 0
  store i1 %v17_4040b3, i1* %zf.global-to-local, align 1
  %v22_4040b3 = load i32, i32* @ebx, align 4
  %v23_4040b3 = inttoptr i32 %v22_4040b3 to i8*
  store i8 %v7_4040b3, i8* %v23_4040b3, align 1
  %v2_4040b7 = add i32 %v25_4040af, 9
  %v3_4040b7 = inttoptr i32 %v2_4040b7 to i8*
  %v4_4040b7 = load i8, i8* %v3_4040b7, align 1
  %v5_4040b7 = load i32, i32* @ecx, align 4
  %v6_4040b7 = udiv i32 %v5_4040b7, 256
  %v7_4040b7 = trunc i32 %v6_4040b7 to i8
  %v8_4040b7 = add i8 %v4_4040b7, %v7_4040b7
  %v13_4040b7 = icmp ult i8 %v8_4040b7, %v4_4040b7
  store i1 %v13_4040b7, i1* %cf.global-to-local, align 1
  %v18_4040b7 = icmp eq i8 %v8_4040b7, 0
  store i1 %v18_4040b7, i1* %zf.global-to-local, align 1
  store i8 %v8_4040b7, i8* %v3_4040b7, align 1
  %v3_4040ba = load i8, i8* %v26_4040af, align 1
  %v7_4040ba = add i8 %v3_4040ba, %v11_4040aa
  %v12_4040ba = icmp ult i8 %v7_4040ba, %v3_4040ba
  store i1 %v12_4040ba, i1* %cf.global-to-local, align 1
  %v17_4040ba = icmp eq i8 %v7_4040ba, 0
  store i1 %v17_4040ba, i1* %zf.global-to-local, align 1
  store i8 %v7_4040ba, i8* %v26_4040af, align 1
  %v2_4040bd = load i8*, i8** %stack_var_-8, align 4
  %v3_4040bd = ptrtoint i8* %v2_4040bd to i32
  %v4_4040bd = trunc i32 %v3_4040bd to i16
  store i16 %v4_4040bd, i16* @es, align 4
  %v3_4040be = load i8, i8* %v26_4040af, align 1
  %v7_4040be = add i8 %v3_4040be, %v11_4040aa
  store i8 %v7_4040be, i8* %v26_4040af, align 1
  %v0_4040c0 = load i32, i32* @edx, align 4
  %v1_4040c0 = load i32, i32* @ebx, align 4
  %v2_4040c0 = add i32 %v1_4040c0, 116
  %v3_4040c0 = inttoptr i32 %v2_4040c0 to i32*
  %v4_4040c0 = load i32, i32* %v3_4040c0, align 4
  %v5_4040c0 = and i32 %v4_4040c0, %v0_4040c0
  store i1 false, i1* %cf.global-to-local, align 1
  %v6_4040c0 = icmp eq i32 %v5_4040c0, 0
  store i1 %v6_4040c0, i1* %zf.global-to-local, align 1
  store i32 %v5_4040c0, i32* @edx, align 4
  %v2_4040c5 = trunc i32 %v5_4040c0 to i16
  %v5_4040c5 = load i8, i8* bitcast (i32* @esi to i8*), align 4
  call void @__asm_outsb(i16 %v2_4040c5, i8 %v5_4040c5)
  %v3_4040c9 = load i8, i8* %v26_4040af, align 1
  %v7_4040c9 = add i8 %v3_4040c9, %v11_4040aa
  store i8 %v7_4040c9, i8* %v26_4040af, align 1
  %v3_4040cb = load i32, i32* @ebx, align 4
  %v4_4040cb = udiv i32 %v3_4040cb, 256
  %v5_4040cb = trunc i32 %v4_4040cb to i8
  %v6_4040cb = add i8 %v5_4040cb, %v11_4040aa
  %v11_4040cb = icmp ult i8 %v6_4040cb, %v11_4040aa
  store i1 %v11_4040cb, i1* %cf.global-to-local, align 1
  %v16_4040cb = icmp eq i8 %v6_4040cb, 0
  store i1 %v16_4040cb, i1* %zf.global-to-local, align 1
  %v21_4040cb = zext i8 %v6_4040cb to i32
  %v24_4040cb = and i32 %v25_4040af, -256
  %v25_4040cb = or i32 %v24_4040cb, %v21_4040cb
  store i32 %v25_4040cb, i32* @eax, align 4
  %v1_4040cd = inttoptr i32 %v25_4040cb to i8*
  %v2_4040cd = load i8, i8* %v1_4040cd, align 1
  %v6_4040cd = zext i1 %v11_4040cb to i8
  %v7_4040cd = add i8 %v2_4040cd, %v6_4040cb
  %v8_4040cd = add i8 %v7_4040cd, %v6_4040cd
  %v19_4040cd = icmp eq i8 %v8_4040cd, 0
  store i1 %v19_4040cd, i1* %zf.global-to-local, align 1
  %v26_4040cd = icmp ule i8 %v8_4040cd, %v2_4040cd
  %v27_4040cd = icmp ult i8 %v7_4040cd, %v2_4040cd
  %v28_4040cd = select i1 %v11_4040cb, i1 %v26_4040cd, i1 %v27_4040cd
  store i1 %v28_4040cd, i1* %cf.global-to-local, align 1
  store i8 %v8_4040cd, i8* %v1_4040cd, align 1
  %v31_4040cd = load i32, i32* @eax, align 4
  ret i32 %v31_4040cd

dec_label_pc_4040d0:                              ; preds = %dec_label_pc_404044
  store i1 false, i1* %cf.global-to-local, align 1
  store i1 %v15_40407a, i1* %zf.global-to-local, align 1
  store i32 %v23_40407a, i32* @eax, align 4
  %v2_4040d2 = load i8, i8* %v24_40407a, align 1
  %v5_4040d2 = add i8 %v2_4040d2, %v5_40407a
  store i8 %v5_4040d2, i8* %v24_40407a, align 1
  %v0_4040d4 = load i32, i32* @edx, align 4
  %v1_4040d4 = load i32, i32* @ebp, align 4
  %v2_4040d4 = add i32 %v1_4040d4, 83
  %v3_4040d4 = inttoptr i32 %v2_4040d4 to i32*
  %v4_4040d4 = load i32, i32* %v3_4040d4, align 4
  %v5_4040d4 = and i32 %v4_4040d4, %v0_4040d4
  %v8_4040d4 = trunc i32 %v5_4040d4 to i8
  %v12_4040d4 = trunc i32 %v5_4040d4 to i16
  store i32 %v5_4040d4, i32* @edx, align 4
  %v0_4040d7 = load i32, i32* @eax, align 4
  %v1_4040d7 = udiv i32 %v0_4040d7, 256
  %v2_4040d7 = trunc i32 %v1_4040d7 to i8
  %v3_4040d7 = load i32, i32* @ebx, align 4
  %v4_4040d7 = udiv i32 %v3_4040d7, 256
  %v5_4040d7 = trunc i32 %v4_4040d7 to i8
  %v6_4040d7 = add i8 %v5_4040d7, %v2_4040d7
  %v11_4040d7 = icmp ult i8 %v6_4040d7, %v2_4040d7
  store i1 %v11_4040d7, i1* %cf.global-to-local, align 1
  %v16_4040d7 = icmp eq i8 %v6_4040d7, 0
  store i1 %v16_4040d7, i1* %zf.global-to-local, align 1
  %v21_4040d7 = zext i8 %v6_4040d7 to i32
  %v23_4040d7 = mul i32 %v21_4040d7, 256
  %v24_4040d7 = and i32 %v0_4040d7, -65281
  %v25_4040d7 = or i32 %v23_4040d7, %v24_4040d7
  store i32 %v25_4040d7, i32* @eax, align 4
  %v1_4040d9 = inttoptr i32 %v25_4040d7 to i8*
  %v2_4040d9 = load i8, i8* %v1_4040d9, align 1
  %v4_4040d9 = trunc i32 %v0_4040d7 to i8
  %v6_4040d9 = zext i1 %v11_4040d7 to i8
  %v7_4040d9 = add i8 %v2_4040d9, %v4_4040d9
  %v8_4040d9 = add i8 %v7_4040d9, %v6_4040d9
  %v19_4040d9 = icmp eq i8 %v8_4040d9, 0
  store i1 %v19_4040d9, i1* %zf.global-to-local, align 1
  %v26_4040d9 = icmp ule i8 %v8_4040d9, %v2_4040d9
  %v27_4040d9 = icmp ult i8 %v7_4040d9, %v2_4040d9
  %v28_4040d9 = select i1 %v11_4040d7, i1 %v26_4040d9, i1 %v27_4040d9
  store i1 %v28_4040d9, i1* %cf.global-to-local, align 1
  store i8 %v8_4040d9, i8* %v1_4040d9, align 1
  %v0_4040db = load i32, i32* @eax, align 4
  %v1_4040db = inttoptr i32 %v0_4040db to i8*
  %v2_4040db = load i8, i8* %v1_4040db, align 1
  %v6_4040db = add i8 %v2_4040db, %v8_4040d4
  %v11_4040db = icmp ult i8 %v6_4040db, %v2_4040db
  store i1 %v11_4040db, i1* %cf.global-to-local, align 1
  %v16_4040db = icmp eq i8 %v6_4040db, 0
  store i1 %v16_4040db, i1* %zf.global-to-local, align 1
  store i8 %v6_4040db, i8* %v1_4040db, align 1
  %v0_4040dd = load i32, i32* @eax, align 4
  %v1_4040dd = inttoptr i32 %v0_4040dd to i8*
  %v2_4040dd = load i8, i8* %v1_4040dd, align 1
  %v4_4040dd = trunc i32 %v0_4040dd to i8
  %v5_4040dd = add i8 %v2_4040dd, %v4_4040dd
  store i8 %v5_4040dd, i8* %v1_4040dd, align 1
  %v2_4040df = load i8, i8* bitcast (i32* @ebx to i8*), align 4
  %v3_4040df = load i32, i32* @eax, align 4
  %v4_4040df = udiv i32 %v3_4040df, 256
  %v5_4040df = trunc i32 %v4_4040df to i8
  %v6_4040df = add i8 %v2_4040df, %v5_4040df
  %v11_4040df = icmp ult i8 %v6_4040df, %v2_4040df
  store i1 %v11_4040df, i1* %cf.global-to-local, align 1
  %v21_4040df = load i32, i32* @ebx, align 4
  %v22_4040df = inttoptr i32 %v21_4040df to i8*
  store i8 %v6_4040df, i8* %v22_4040df, align 1
  %v0_4040e1 = load i32, i32* @edi, align 4
  %v1_4040e1 = add i32 %v0_4040e1, 1
  store i32 %v1_4040e1, i32* @edi, align 4
  %v0_4040e3 = load i32, i32* @ecx, align 4
  %v1_4040e3 = add i32 %v0_4040e3, -1
  store i32 %v1_4040e3, i32* @ecx, align 4
  store i1 false, i1* %zf.global-to-local, align 1
  %v0_4040e5 = load i32, i32* @eax, align 4
  %v1_4040e5 = inttoptr i32 %v0_4040e5 to i8*
  %v2_4040e5 = load i8, i8* %v1_4040e5, align 1
  %v4_4040e5 = trunc i32 %v0_4040e5 to i8
  %v5_4040e5 = add i8 %v2_4040e5, %v4_4040e5
  %v10_4040e5 = icmp ult i8 %v5_4040e5, %v2_4040e5
  store i1 %v10_4040e5, i1* %cf.global-to-local, align 1
  %v15_4040e5 = icmp eq i8 %v5_4040e5, 0
  store i1 %v15_4040e5, i1* %zf.global-to-local, align 1
  store i8 %v5_4040e5, i8* %v1_4040e5, align 1
  %v0_4040e7 = load i32, i32* @ecx, align 4
  %sext30 = mul i32 %v5_4040d4, 65536
  %v2_4040e7 = sdiv i32 %sext30, 65536
  %v4_4040e7 = add i32 %v0_4040e7, %v2_4040e7
  %v5_4040e7 = inttoptr i32 %v4_4040e7 to i8*
  %v6_4040e7 = load i8, i8* %v5_4040e7, align 1
  %v8_4040e7 = trunc i32 %v0_4040e7 to i8
  %v9_4040e7 = add i8 %v6_4040e7, %v8_4040e7
  %v14_4040e7 = icmp ult i8 %v9_4040e7, %v6_4040e7
  store i1 %v14_4040e7, i1* %cf.global-to-local, align 1
  %v19_4040e7 = icmp eq i8 %v9_4040e7, 0
  store i1 %v19_4040e7, i1* %zf.global-to-local, align 1
  store i8 %v9_4040e7, i8* %v5_4040e7, align 1
  %v0_4040ea = load i32, i32* @eax, align 4
  %v1_4040ea = inttoptr i32 %v0_4040ea to i8*
  %v2_4040ea = load i8, i8* %v1_4040ea, align 1
  %v4_4040ea = trunc i32 %v0_4040ea to i8
  %v5_4040ea = add i8 %v2_4040ea, %v4_4040ea
  store i8 %v5_4040ea, i8* %v1_4040ea, align 1
  %v2_4040ec = load i8, i8* bitcast (i32* @ebx to i8*), align 4
  %v3_4040ec = load i32, i32* @eax, align 4
  %v4_4040ec = trunc i32 %v3_4040ec to i8
  %v10_4040ec = icmp ult i8 %v2_4040ec, %v4_4040ec
  store i1 %v10_4040ec, i1* %cf.global-to-local, align 1
  %v15_4040ec = icmp eq i8 %v2_4040ec, %v4_4040ec
  store i1 %v15_4040ec, i1* %zf.global-to-local, align 1
  %v1_4040ee = inttoptr i32 %v3_4040ec to i8*
  %v2_4040ee = load i8, i8* %v1_4040ee, align 1
  %v5_4040ee = add i8 %v2_4040ee, %v4_4040ec
  store i8 %v5_4040ee, i8* %v1_4040ee, align 1
  %v0_4040f0 = load i32, i32* @eax, align 4
  %v3_4040f0 = add nsw i32 %v2_4040e7, 108
  %v4_4040f0 = inttoptr i32 %v3_4040f0 to i32*
  %v5_4040f0 = load i32, i32* %v4_4040f0, align 4
  %v6_4040f0 = and i32 %v5_4040f0, %v0_4040f0
  store i1 false, i1* %cf.global-to-local, align 1
  %v7_4040f0 = icmp eq i32 %v6_4040f0, 0
  store i1 %v7_4040f0, i1* %zf.global-to-local, align 1
  store i32 %v6_4040f0, i32* @eax, align 4
  %v5_4040f3 = load i32, i32* @esi, align 4
  call void @__asm_outsd(i16 %v12_4040d4, i32 %v5_4040f3)
  %v0_4040f4 = load i32, i32* @eax, align 4
  %v1_4040f4 = inttoptr i32 %v0_4040f4 to i64*
  %v2_4040f4 = load i64, i64* %v1_4040f4, align 4
  %v3_4040f4 = call i32 @__asm_bound(i64 %v2_4040f4)
  store i32 %v3_4040f4, i32* @eax, align 4
  %v1_4040f6 = inttoptr i32 %v3_4040f4 to i8*
  %v2_4040f6 = load i8, i8* %v1_4040f6, align 1
  %v4_4040f6 = trunc i32 %v3_4040f4 to i8
  %v5_4040f6 = add i8 %v2_4040f6, %v4_4040f6
  %v10_4040f6 = icmp ult i8 %v5_4040f6, %v2_4040f6
  store i1 %v10_4040f6, i1* %cf.global-to-local, align 1
  %v15_4040f6 = icmp eq i8 %v5_4040f6, 0
  store i1 %v15_4040f6, i1* %zf.global-to-local, align 1
  store i8 %v5_4040f6, i8* %v1_4040f6, align 1
  %v0_4040f8 = load i32, i32* @eax, align 4
  %v1_4040f8 = inttoptr i32 %v0_4040f8 to i8*
  %v2_4040f8 = load i8, i8* %v1_4040f8, align 1
  %v4_4040f8 = trunc i32 %v0_4040f8 to i8
  %v5_4040f8 = add i8 %v2_4040f8, %v4_4040f8
  %v10_4040f8 = icmp ult i8 %v5_4040f8, %v2_4040f8
  store i1 %v10_4040f8, i1* %cf.global-to-local, align 1
  %v15_4040f8 = icmp eq i8 %v5_4040f8, 0
  store i1 %v15_4040f8, i1* %zf.global-to-local, align 1
  store i8 %v5_4040f8, i8* %v1_4040f8, align 1
  %v0_4040fa = load i32, i32* @eax, align 4
  %v1_4040fa = inttoptr i32 %v0_4040fa to i8*
  %v2_4040fa = load i8, i8* %v1_4040fa, align 1
  %v4_4040fa = trunc i32 %v0_4040fa to i8
  %v5_4040fa = add i8 %v2_4040fa, %v4_4040fa
  %v10_4040fa = icmp ult i8 %v5_4040fa, %v2_4040fa
  store i1 %v10_4040fa, i1* %cf.global-to-local, align 1
  %v15_4040fa = icmp eq i8 %v5_4040fa, 0
  store i1 %v15_4040fa, i1* %zf.global-to-local, align 1
  store i8 %v5_4040fa, i8* %v1_4040fa, align 1
  %v0_4040fc = load i32, i32* @eax, align 4
  %v1_4040fc = trunc i32 %v0_4040fc to i8
  %v3_4040fc = inttoptr i32 %v0_4040fc to i8*
  %v4_4040fc = load i8, i8* %v3_4040fc, align 1
  %v5_4040fc = add i8 %v4_4040fc, %v1_4040fc
  %v10_4040fc = icmp ult i8 %v5_4040fc, %v1_4040fc
  store i1 %v10_4040fc, i1* %cf.global-to-local, align 1
  %v15_4040fc = icmp eq i8 %v5_4040fc, 0
  store i1 %v15_4040fc, i1* %zf.global-to-local, align 1
  %v20_4040fc = zext i8 %v5_4040fc to i32
  %v22_4040fc = and i32 %v0_4040fc, -256
  %v23_4040fc = or i32 %v22_4040fc, %v20_4040fc
  store i32 %v23_4040fc, i32* @eax, align 4
  %v0_4040fe = load i32, i32* @ecx, align 4
  %v1_4040fe = inttoptr i32 %v0_4040fe to i8*
  %v2_4040fe = load i8, i8* %v1_4040fe, align 1
  %v5_4040fe = add i8 %v2_4040fe, %v5_4040fc
  store i8 %v5_4040fe, i8* %v1_4040fe, align 1
  %v0_404101 = load i32, i32* @eax, align 4
  %v1_404101 = load i32, i32* @ebp, align 4
  store i32 %v1_404101, i32* @eax, align 4
  store i32 %v0_404101, i32* @ebp, align 4
  %v1_404102 = trunc i32 %v1_404101 to i8
  store i8 %v1_404102, i8* inttoptr (i32 985405 to i8*), align 1
  %v2_404107 = load i8, i8* bitcast (i32* @ebp to i8*), align 4
  %v3_404107 = load i32, i32* @eax, align 4
  %v4_404107 = trunc i32 %v3_404107 to i8
  %v5_404107 = add i8 %v2_404107, %v4_404107
  %v21_404107 = inttoptr i32 %v3_404107 to i8*
  store i8 %v5_404107, i8* %v21_404107, align 1
  %v2_404109 = load i8, i8* bitcast (i32* @ebp to i8*), align 4
  %v3_404109 = load i32, i32* @eax, align 4
  %v4_404109 = trunc i32 %v3_404109 to i8
  %v5_404109 = add i8 %v2_404109, %v4_404109
  %v21_404109 = inttoptr i32 %v3_404109 to i8*
  store i8 %v5_404109, i8* %v21_404109, align 1
  %v2_40410b = load i8, i8* bitcast (i32* @ebp to i8*), align 4
  %v3_40410b = load i32, i32* @eax, align 4
  %v4_40410b = trunc i32 %v3_40410b to i8
  %v5_40410b = add i8 %v2_40410b, %v4_40410b
  %v21_40410b = inttoptr i32 %v3_40410b to i8*
  store i8 %v5_40410b, i8* %v21_40410b, align 1
  %v2_40410d = load i8, i8* bitcast (i32* @ebp to i8*), align 4
  %v3_40410d = load i32, i32* @eax, align 4
  %v4_40410d = trunc i32 %v3_40410d to i8
  %v5_40410d = add i8 %v2_40410d, %v4_40410d
  %v10_40410d = icmp ult i8 %v5_40410d, %v2_40410d
  store i1 %v10_40410d, i1* %cf.global-to-local, align 1
  %v15_40410d = icmp eq i8 %v5_40410d, 0
  store i1 %v15_40410d, i1* %zf.global-to-local, align 1
  %v21_40410d = inttoptr i32 %v3_40410d to i8*
  store i8 %v5_40410d, i8* %v21_40410d, align 1
  %v0_40410f = load i32, i32* @ecx, align 4
  %v1_40410f = inttoptr i32 %v0_40410f to i8*
  %v2_40410f = load i8, i8* %v1_40410f, align 1
  %v3_40410f = load i32, i32* @eax, align 4
  %v4_40410f = trunc i32 %v3_40410f to i8
  %v5_40410f = add i8 %v2_40410f, %v4_40410f
  store i8 %v5_40410f, i8* %v1_40410f, align 1
  %v2_404111 = load i8, i8* bitcast (i32* @ebp to i8*), align 4
  %v3_404111 = load i32, i32* @eax, align 4
  %v4_404111 = trunc i32 %v3_404111 to i8
  %v5_404111 = add i8 %v2_404111, %v4_404111
  %v21_404111 = inttoptr i32 %v3_404111 to i8*
  store i8 %v5_404111, i8* %v21_404111, align 1
  %v2_404113 = load i8, i8* bitcast (i32* @ebx to i8*), align 4
  %v3_404113 = load i32, i32* @eax, align 4
  %v4_404113 = udiv i32 %v3_404113, 256
  %v5_404113 = trunc i32 %v4_404113 to i8
  %v6_404113 = add i8 %v2_404113, %v5_404113
  %v21_404113 = load i32, i32* @ebx, align 4
  %v22_404113 = inttoptr i32 %v21_404113 to i8*
  store i8 %v6_404113, i8* %v22_404113, align 1
  %v2_404115 = load i8, i8* bitcast (i32* @ebp to i8*), align 4
  %v3_404115 = load i32, i32* @eax, align 4
  %v4_404115 = trunc i32 %v3_404115 to i8
  %v5_404115 = add i8 %v2_404115, %v4_404115
  %v10_404115 = icmp ult i8 %v5_404115, %v2_404115
  store i1 %v10_404115, i1* %cf.global-to-local, align 1
  %v15_404115 = icmp eq i8 %v5_404115, 0
  store i1 %v15_404115, i1* %zf.global-to-local, align 1
  %v21_404115 = inttoptr i32 %v3_404115 to i8*
  store i8 %v5_404115, i8* %v21_404115, align 1
  %v0_404117 = load i8, i8* inttoptr (i32 150994944 to i8*), align 16777216
  %v1_404117 = load i32, i32* @ecx, align 4
  %v2_404117 = trunc i32 %v1_404117 to i8
  %v3_404117 = add i8 %v0_404117, %v2_404117
  store i8 %v3_404117, i8* inttoptr (i32 150994944 to i8*), align 16777216
  %v2_40411d = load i8, i8* bitcast (i32* @ebp to i8*), align 4
  %v3_40411d = load i32, i32* @eax, align 4
  %v4_40411d = trunc i32 %v3_40411d to i8
  %v5_40411d = add i8 %v2_40411d, %v4_40411d
  %v21_40411d = inttoptr i32 %v3_40411d to i8*
  store i8 %v5_40411d, i8* %v21_40411d, align 1
  %v2_40411f = load i8, i8* bitcast (i32* @ebp to i8*), align 4
  %v3_40411f = load i32, i32* @ecx, align 4
  %v4_40411f = trunc i32 %v3_40411f to i8
  %v5_40411f = add i8 %v2_40411f, %v4_40411f
  %v20_40411f = load i32, i32* @eax, align 4
  %v21_40411f = inttoptr i32 %v20_40411f to i8*
  store i8 %v5_40411f, i8* %v21_40411f, align 1
  %v2_404122 = load i8, i8* bitcast (i32* @ebp to i8*), align 4
  %v3_404122 = load i32, i32* @eax, align 4
  %v4_404122 = trunc i32 %v3_404122 to i8
  %v5_404122 = add i8 %v2_404122, %v4_404122
  %v21_404122 = inttoptr i32 %v3_404122 to i8*
  store i8 %v5_404122, i8* %v21_404122, align 1
  %v0_404124 = load i32, i32* @eax, align 4
  %v1_404124 = add i32 %v0_404124, 671088640
  %v4_404124 = icmp ugt i32 %v0_404124, -671088641
  store i1 %v4_404124, i1* %cf.global-to-local, align 1
  %v9_404124 = icmp eq i32 %v1_404124, 0
  store i1 %v9_404124, i1* %zf.global-to-local, align 1
  %v11_404124 = trunc i32 %v1_404124 to i8
  store i32 %v1_404124, i32* @eax, align 4
  %v1_404129 = inttoptr i32 %v1_404124 to i8*
  %v2_404129 = load i8, i8* %v1_404129, align 1
  %v5_404129 = add i8 %v2_404129, %v11_404124
  %v10_404129 = icmp ult i8 %v5_404129, %v2_404129
  store i1 %v10_404129, i1* %cf.global-to-local, align 1
  %v15_404129 = icmp eq i8 %v5_404129, 0
  store i1 %v15_404129, i1* %zf.global-to-local, align 1
  store i8 %v5_404129, i8* %v1_404129, align 1
  %v0_40412b = load i32, i32* @edi, align 4
  %v1_40412b = inttoptr i32 %v0_40412b to i8*
  %v2_40412b = load i8, i8* %v1_40412b, align 1
  %v3_40412b = load i32, i32* @ebx, align 4
  %v4_40412b = trunc i32 %v3_40412b to i8
  %v5_40412b = add i8 %v2_40412b, %v4_40412b
  %v10_40412b = icmp ult i8 %v5_40412b, %v2_40412b
  store i1 %v10_40412b, i1* %cf.global-to-local, align 1
  %v15_40412b = icmp eq i8 %v5_40412b, 0
  store i1 %v15_40412b, i1* %zf.global-to-local, align 1
  store i8 %v5_40412b, i8* %v1_40412b, align 1
  %v22_40412b = load i32, i32* @eax, align 4
  ret i32 %v22_40412b
}

define i32 @function_40480d(i32 %arg1, i32 %arg2, i32 %arg3, i32 %arg4, i32 %arg5, i32 %arg6, i32 %arg7, i32 %arg8, i32 %arg9, i32 %arg10, i32 %arg11, i32 %arg12, i32 %arg13, i32 %arg14, i32 %arg15, i32 %arg16, i32 %arg17, i32 %arg18, i32 %arg19, i32 %arg20, i32 %arg21, i32 %arg22, i32 %arg23, i32 %arg24, i32 %arg25, i32 %arg26, i32 %arg27, i32 %arg28, i32 %arg29, i32 %arg30, i32 %arg31) local_unnamed_addr {
dec_label_pc_40480d:
  %az.global-to-local = alloca i1, align 1
  %cf.global-to-local = alloca i1, align 1
  %df.global-to-local = alloca i1, align 1
  %of.global-to-local = alloca i1, align 1
  %sf.global-to-local = alloca i1, align 1
  %zf.global-to-local = alloca i1, align 1
  %stack_var_136 = alloca i32, align 4
  %stack_var_104 = alloca i32, align 4
  store i32 %arg25, i32* %stack_var_104, align 4
  %stack_var_72 = alloca i32, align 4
  store i32 %arg18, i32* %stack_var_72, align 4
  %stack_var_68 = alloca i32, align 4
  store i32 %arg17, i32* %stack_var_68, align 4
  %stack_var_40 = alloca i32, align 4
  store i32 %arg11, i32* %stack_var_40, align 4
  %stack_var_36 = alloca i32, align 4
  store i32 %arg10, i32* %stack_var_36, align 4
  %stack_var_8 = alloca i32, align 4
  store i32 %arg3, i32* %stack_var_8, align 4
  %stack_var_4 = alloca i32, align 4
  store i32 %arg2, i32* %stack_var_4, align 4
  %v0_40480d = load i32, i32* @ecx, align 4
  %v2_40480d = load i32, i32* @edx, align 4
  %v3_40480d = udiv i32 %v2_40480d, 256
  %v5_40480d = add i32 %v3_40480d, %v0_40480d
  %v20_40480d = urem i32 %v5_40480d, 256
  %v22_40480d = and i32 %v0_40480d, -256
  %v23_40480d = or i32 %v20_40480d, %v22_40480d
  store i32 %v23_40480d, i32* @ecx, align 4
  %v2_40480f = load i8, i8* bitcast (i32* @ebx to i8*), align 4
  %v3_40480f = load i32, i32* @ebx, align 4
  %v4_40480f = trunc i32 %v3_40480f to i8
  %v5_40480f = add i8 %v2_40480f, %v4_40480f
  %v21_40480f = inttoptr i32 %v3_40480f to i8*
  store i8 %v5_40480f, i8* %v21_40480f, align 1
  %v19_404812 = load i32, i32* @eax, align 4
  %v20_404812 = inttoptr i32 %v19_404812 to i32*
  store i32 0, i32* %v20_404812, align 4
  %v2_404815 = load i8, i8* bitcast (i32* @ebx to i8*), align 4
  %v3_404815 = load i32, i32* @ebx, align 4
  %v4_404815 = trunc i32 %v3_404815 to i8
  %v5_404815 = add i8 %v2_404815, %v4_404815
  %v21_404815 = inttoptr i32 %v3_404815 to i8*
  store i8 %v5_404815, i8* %v21_404815, align 1
  %v2_404818 = load i8, i8* bitcast (i32* @edx to i8*), align 4
  %v3_404818 = load i32, i32* @eax, align 4
  %v4_404818 = trunc i32 %v3_404818 to i8
  %v5_404818 = load i32, i32* @edx, align 4
  %v6_404818 = inttoptr i32 %v5_404818 to i8*
  store i8 %v4_404818, i8* %v6_404818, align 1
  %v7_404818 = zext i8 %v2_404818 to i32
  %v8_404818 = load i32, i32* @eax, align 4
  %v9_404818 = and i32 %v8_404818, -256
  %v10_404818 = or i32 %v9_404818, %v7_404818
  store i32 %v10_404818, i32* @eax, align 4
  %v0_40481a = load i32, i32* @ecx, align 4
  %v1_40481a = inttoptr i32 %v0_40481a to i32*
  %v2_40481a = load i32, i32* %v1_40481a, align 4
  %v4_40481a = add i32 %v2_40481a, %v10_404818
  store i32 %v4_40481a, i32* %v1_40481a, align 4
  store i32 %arg1, i32* @ebx, align 4
  %v2_40481d = load i8, i8* bitcast (i32* @edx to i8*), align 4
  %v3_40481d = load i32, i32* @eax, align 4
  %v4_40481d = udiv i32 %v3_40481d, 256
  %v5_40481d = trunc i32 %v4_40481d to i8
  %v6_40481d = add i8 %v2_40481d, %v5_40481d
  %v21_40481d = load i32, i32* @edx, align 4
  %v22_40481d = inttoptr i32 %v21_40481d to i8*
  store i8 %v6_40481d, i8* %v22_40481d, align 1
  %v0_40481f = load i32, i32* @ecx, align 4
  %v2_40481f = inttoptr i32 %v0_40481f to i32*
  %v3_40481f = load i32, i32* %v2_40481f, align 4
  %v4_40481f = add i32 %v3_40481f, %v0_40481f
  store i32 %v4_40481f, i32* @ecx, align 4
  %v0_404821 = load i32, i32* @ebx, align 4
  %v1_404821 = inttoptr i32 %v0_404821 to i32*
  %v2_404821 = load i32, i32* %v1_404821, align 4
  %v4_404821 = add i32 %v2_404821, %v0_404821
  %v5_404821 = urem i32 %v2_404821, 16
  %v6_404821 = urem i32 %v0_404821, 16
  %v7_404821 = add nuw nsw i32 %v5_404821, %v6_404821
  %v8_404821 = icmp ugt i32 %v7_404821, 15
  %v9_404821 = icmp ult i32 %v4_404821, %v2_404821
  %v10_404821 = xor i32 %v4_404821, %v2_404821
  %v11_404821 = xor i32 %v4_404821, %v0_404821
  %v12_404821 = and i32 %v10_404821, %v11_404821
  %v13_404821 = icmp slt i32 %v12_404821, 0
  store i1 %v8_404821, i1* %az.global-to-local, align 1
  store i1 %v9_404821, i1* %cf.global-to-local, align 1
  store i1 %v13_404821, i1* %of.global-to-local, align 1
  %v14_404821 = icmp eq i32 %v4_404821, 0
  store i1 %v14_404821, i1* %zf.global-to-local, align 1
  %v15_404821 = icmp slt i32 %v4_404821, 0
  store i1 %v15_404821, i1* %sf.global-to-local, align 1
  store i32 %v4_404821, i32* %v1_404821, align 4
  %v0_404824 = load i32, i32* @ebx, align 4
  %v1_404824 = inttoptr i32 %v0_404824 to i8*
  %v2_404824 = load i8, i8* %v1_404824, align 1
  %v3_404824 = load i32, i32* @eax, align 4
  %v4_404824 = trunc i32 %v3_404824 to i8
  %v5_404824 = xor i8 %v2_404824, %v4_404824
  store i1 false, i1* %cf.global-to-local, align 1
  store i8 %v5_404824, i8* %v1_404824, align 1
  %v0_404826 = load i32, i32* @ecx, align 4
  %v1_404826 = inttoptr i32 %v0_404826 to i32*
  %v2_404826 = load i32, i32* %v1_404826, align 4
  %v3_404826 = load i32, i32* @eax, align 4
  %v4_404826 = load i1, i1* %cf.global-to-local, align 1
  %v5_404826 = zext i1 %v4_404826 to i32
  %v6_404826 = add i32 %v3_404826, %v2_404826
  %v7_404826 = add i32 %v6_404826, %v5_404826
  %v8_404826 = urem i32 %v2_404826, 16
  %v9_404826 = urem i32 %v3_404826, 16
  %v10_404826 = add nuw nsw i32 %v9_404826, %v8_404826
  %v11_404826 = add nuw nsw i32 %v10_404826, %v5_404826
  %v12_404826 = icmp ugt i32 %v11_404826, 15
  %v13_404826 = add i32 %v7_404826, %v5_404826
  %v14_404826 = xor i32 %v13_404826, %v2_404826
  %v15_404826 = xor i32 %v13_404826, %v3_404826
  %v16_404826 = and i32 %v14_404826, %v15_404826
  %v17_404826 = icmp slt i32 %v16_404826, 0
  store i1 %v12_404826, i1* %az.global-to-local, align 1
  store i1 %v17_404826, i1* %of.global-to-local, align 1
  %v18_404826 = icmp eq i32 %v7_404826, 0
  store i1 %v18_404826, i1* %zf.global-to-local, align 1
  %v19_404826 = icmp slt i32 %v7_404826, 0
  store i1 %v19_404826, i1* %sf.global-to-local, align 1
  %v26_404826 = icmp ule i32 %v7_404826, %v2_404826
  %v27_404826 = icmp ult i32 %v6_404826, %v2_404826
  %v28_404826 = select i1 %v4_404826, i1 %v26_404826, i1 %v27_404826
  store i1 %v28_404826, i1* %cf.global-to-local, align 1
  store i32 %v7_404826, i32* %v1_404826, align 4
  %v2_404828 = load i32, i32* %stack_var_4, align 4
  store i32 %v2_404828, i32* @ebx, align 4
  %v0_404829 = load i32, i32* @ecx, align 4
  %v1_404829 = inttoptr i32 %v0_404829 to i8*
  %v2_404829 = load i8, i8* %v1_404829, align 1
  %v4_404829 = udiv i32 %v0_404829, 256
  %v5_404829 = trunc i32 %v4_404829 to i8
  %v6_404829 = add i8 %v2_404829, %v5_404829
  %v7_404829 = urem i8 %v2_404829, 16
  %v8_404829 = urem i8 %v5_404829, 16
  %v9_404829 = add nuw nsw i8 %v8_404829, %v7_404829
  %v10_404829 = icmp ugt i8 %v9_404829, 15
  %v11_404829 = icmp ult i8 %v6_404829, %v2_404829
  %v12_404829 = xor i8 %v6_404829, %v2_404829
  %v13_404829 = xor i8 %v6_404829, %v5_404829
  %v14_404829 = and i8 %v12_404829, %v13_404829
  %v15_404829 = icmp slt i8 %v14_404829, 0
  store i1 %v10_404829, i1* %az.global-to-local, align 1
  store i1 %v11_404829, i1* %cf.global-to-local, align 1
  store i1 %v15_404829, i1* %of.global-to-local, align 1
  %v16_404829 = icmp eq i8 %v6_404829, 0
  store i1 %v16_404829, i1* %zf.global-to-local, align 1
  %v17_404829 = icmp slt i8 %v6_404829, 0
  store i1 %v17_404829, i1* %sf.global-to-local, align 1
  store i8 %v6_404829, i8* %v1_404829, align 1
  %v0_40482b = load i32, i32* @ecx, align 4
  %v1_40482b = inttoptr i32 %v0_40482b to i8*
  %v2_40482b = load i8, i8* %v1_40482b, align 1
  %v3_40482b = load i32, i32* @ebx, align 4
  %v4_40482b = trunc i32 %v3_40482b to i8
  %v5_40482b = add i8 %v2_40482b, %v4_40482b
  store i8 %v5_40482b, i8* %v1_40482b, align 1
  %v0_40482d = load i32, i32* @ebx, align 4
  %v1_40482d = inttoptr i32 %v0_40482d to i32*
  %v2_40482d = load i32, i32* %v1_40482d, align 4
  %v4_40482d = add i32 %v2_40482d, %v0_40482d
  store i32 %v4_40482d, i32* %v1_40482d, align 4
  %v0_404830 = load i32, i32* @eax, align 4
  %v1_404830 = inttoptr i32 %v0_404830 to i32*
  %v2_404830 = load i32, i32* %v1_404830, align 4
  %v4_404830 = sub i32 %v2_404830, %v0_404830
  store i32 %v4_404830, i32* %v1_404830, align 4
  %v0_404832 = load i32, i32* @eax, align 4
  %v1_404832 = inttoptr i32 %v0_404832 to i32*
  %v2_404832 = load i32, i32* %v1_404832, align 4
  %v4_404832 = sub i32 %v2_404832, %v0_404832
  store i32 %v4_404832, i32* %v1_404832, align 4
  %v0_404834 = load i32, i32* @eax, align 4
  %v1_404834 = load i32, i32* @ecx, align 4
  %v2_404834 = inttoptr i32 %v1_404834 to i32*
  %v3_404834 = load i32, i32* %v2_404834, align 4
  %v4_404834 = and i32 %v3_404834, %v0_404834
  store i1 false, i1* %az.global-to-local, align 1
  store i1 false, i1* %cf.global-to-local, align 1
  store i1 false, i1* %of.global-to-local, align 1
  %v5_404834 = icmp eq i32 %v4_404834, 0
  store i1 %v5_404834, i1* %zf.global-to-local, align 1
  %v6_404834 = icmp slt i32 %v4_404834, 0
  store i1 %v6_404834, i1* %sf.global-to-local, align 1
  store i32 %v4_404834, i32* @eax, align 4
  %v1_404836 = icmp eq i1 %v5_404834, false
  br i1 %v1_404836, label %dec_label_pc_40483a, label %dec_label_pc_404838

dec_label_pc_404838:                              ; preds = %dec_label_pc_404a7e, %dec_label_pc_4050e7, %dec_label_pc_404dc7, %dec_label_pc_405152, %dec_label_pc_4048b8, %dec_label_pc_40480d
  %merge = phi i32 [ %v4_404834, %dec_label_pc_40480d ], [ %v8_4048b8, %dec_label_pc_4048b8 ], [ %v0_4051ca, %dec_label_pc_405152 ], [ %v3_404e05, %dec_label_pc_404dc7 ], [ %v0_4051ca50, %dec_label_pc_4050e7 ], [ %arg31, %dec_label_pc_404a7e ]
  ret i32 %merge

dec_label_pc_40483a:                              ; preds = %dec_label_pc_40480d
  %v2_40483a = inttoptr i32 %v4_404834 to i32*
  %v3_40483a = load i32, i32* %v2_40483a, align 4
  %v4_40483a = or i32 %v3_40483a, %v4_404834
  store i1 false, i1* %az.global-to-local, align 1
  store i1 false, i1* %cf.global-to-local, align 1
  store i1 false, i1* %of.global-to-local, align 1
  %v5_40483a = icmp eq i32 %v4_40483a, 0
  store i1 %v5_40483a, i1* %zf.global-to-local, align 1
  %v6_40483a = icmp slt i32 %v4_40483a, 0
  store i1 %v6_40483a, i1* %sf.global-to-local, align 1
  %v7_40483a = trunc i32 %v4_40483a to i8
  store i32 %v4_40483a, i32* @eax, align 4
  %v3_40483c = inttoptr i32 %v4_40483a to i8*
  %v4_40483c = load i8, i8* %v3_40483c, align 1
  %v5_40483c = or i8 %v4_40483c, %v7_40483a
  store i1 false, i1* %az.global-to-local, align 1
  store i1 false, i1* %cf.global-to-local, align 1
  store i1 false, i1* %of.global-to-local, align 1
  %v6_40483c = icmp eq i8 %v5_40483c, 0
  store i1 %v6_40483c, i1* %zf.global-to-local, align 1
  %v7_40483c = icmp slt i8 %v5_40483c, 0
  store i1 %v7_40483c, i1* %sf.global-to-local, align 1
  %v11_40483c = zext i8 %v5_40483c to i32
  %v13_40483c = and i32 %v4_40483a, -256
  %v14_40483c = or i32 %v13_40483c, %v11_40483c
  %v15_40483c = inttoptr i32 %v14_40483c to i16*
  store i32 %v14_40483c, i32* @eax, align 4
  %v0_40483e = load i32, i32* @ebx, align 4
  %v1_40483e = inttoptr i32 %v0_40483e to i8*
  %v2_40483e = load i8, i8* %v1_40483e, align 1
  %v3_40483e = load i32, i32* @edx, align 4
  %v4_40483e = trunc i32 %v3_40483e to i8
  %v5_40483e = add i8 %v2_40483e, %v4_40483e
  %v6_40483e = urem i8 %v2_40483e, 16
  %v7_40483e = urem i8 %v4_40483e, 16
  %v8_40483e = add nuw nsw i8 %v7_40483e, %v6_40483e
  %v9_40483e = icmp ugt i8 %v8_40483e, 15
  %v10_40483e = icmp ult i8 %v5_40483e, %v2_40483e
  %v11_40483e = xor i8 %v5_40483e, %v2_40483e
  %v12_40483e = xor i8 %v5_40483e, %v4_40483e
  %v13_40483e = and i8 %v11_40483e, %v12_40483e
  %v14_40483e = icmp slt i8 %v13_40483e, 0
  store i1 %v9_40483e, i1* %az.global-to-local, align 1
  store i1 %v10_40483e, i1* %cf.global-to-local, align 1
  store i1 %v14_40483e, i1* %of.global-to-local, align 1
  %v15_40483e = icmp eq i8 %v5_40483e, 0
  store i1 %v15_40483e, i1* %zf.global-to-local, align 1
  %v16_40483e = icmp slt i8 %v5_40483e, 0
  store i1 %v16_40483e, i1* %sf.global-to-local, align 1
  store i8 %v5_40483e, i8* %v1_40483e, align 1
  %v0_404841 = load i8, i8* inttoptr (i32 587221248 to i8*), align 256
  %v1_404841 = load i32, i32* @ecx, align 4
  %v2_404841 = udiv i32 %v1_404841, 256
  %v3_404841 = trunc i32 %v2_404841 to i8
  %v4_404841 = add i8 %v0_404841, %v3_404841
  store i8 %v4_404841, i8* inttoptr (i32 587221248 to i8*), align 256
  %v0_404847 = load i32, i32* @ebx, align 4
  %v1_404847 = add i32 %v0_404847, 50356994
  %v2_404847 = inttoptr i32 %v1_404847 to i32*
  %v3_404847 = load i32, i32* %v2_404847, align 4
  %v4_404847 = load i32, i32* @ecx, align 4
  %v5_404847 = add i32 %v4_404847, %v3_404847
  store i32 %v5_404847, i32* %v2_404847, align 4
  %v0_40484d = load i32, i32* @ecx, align 4
  %v1_40484d = add i32 %v0_40484d, 2
  %v2_40484d = inttoptr i32 %v1_40484d to i32*
  %v3_40484d = load i32, i32* %v2_40484d, align 4
  %v5_40484d = add i32 %v3_40484d, %v0_40484d
  %v6_40484d = urem i32 %v3_40484d, 16
  %v7_40484d = urem i32 %v0_40484d, 16
  %v8_40484d = add nuw nsw i32 %v6_40484d, %v7_40484d
  %v9_40484d = icmp ugt i32 %v8_40484d, 15
  %v10_40484d = icmp ult i32 %v5_40484d, %v3_40484d
  %v11_40484d = xor i32 %v5_40484d, %v3_40484d
  %v12_40484d = xor i32 %v5_40484d, %v0_40484d
  %v13_40484d = and i32 %v11_40484d, %v12_40484d
  %v14_40484d = icmp slt i32 %v13_40484d, 0
  store i1 %v9_40484d, i1* %az.global-to-local, align 1
  store i1 %v10_40484d, i1* %cf.global-to-local, align 1
  store i1 %v14_40484d, i1* %of.global-to-local, align 1
  %v15_40484d = icmp eq i32 %v5_40484d, 0
  store i1 %v15_40484d, i1* %zf.global-to-local, align 1
  %v16_40484d = icmp slt i32 %v5_40484d, 0
  store i1 %v16_40484d, i1* %sf.global-to-local, align 1
  store i32 %v5_40484d, i32* %v2_40484d, align 4
  %v3_404850 = load i16, i16* %v15_40483c, align 2
  %v6_404850 = trunc i32 %v14_40483c to i16
  call void @__asm_arpl(i16 %v3_404850, i16 %v6_404850)
  %v2_404852 = load i32, i32* @ecx, align 4
  %v3_404852 = inttoptr i32 %v2_404852 to i32*
  %v4_404852 = load i32, i32* %v3_404852, align 4
  %v5_404852 = or i32 %v4_404852, %v14_40483c
  store i1 false, i1* %az.global-to-local, align 1
  store i1 false, i1* %cf.global-to-local, align 1
  store i1 false, i1* %of.global-to-local, align 1
  %v6_404852 = icmp eq i32 %v5_404852, 0
  store i1 %v6_404852, i1* %zf.global-to-local, align 1
  %v7_404852 = icmp slt i32 %v5_404852, 0
  store i1 %v7_404852, i1* %sf.global-to-local, align 1
  %v9_404854 = load i32, i32* %stack_var_8, align 4
  %v21_404854 = load i32, i32* %stack_var_36, align 4
  store i32 %v9_404854, i32* @edi, align 4
  store i32 %arg4, i32* @esi, align 4
  store i32 %arg5, i32* @ebp, align 4
  store i32 %arg7, i32* @ebx, align 4
  store i32 %arg8, i32* @edx, align 4
  store i32 %arg9, i32* @ecx, align 4
  store i32 %v21_404854, i32* @eax, align 4
  %v22_404854 = ptrtoint i32* %stack_var_40 to i32
  %v1_404855 = udiv i32 %arg9, 256
  %v2_404855 = trunc i32 %v1_404855 to i8
  %v4_404855 = inttoptr i32 %arg9 to i8*
  %v5_404855 = load i8, i8* %v4_404855, align 1
  %v6_404855 = add i8 %v5_404855, %v2_404855
  %v21_404855 = zext i8 %v6_404855 to i32
  %v23_404855 = mul i32 %v21_404855, 256
  %v24_404855 = and i32 %arg9, -65281
  %v25_404855 = or i32 %v23_404855, %v24_404855
  store i32 %v25_404855, i32* @ecx, align 4
  %v2_404858 = inttoptr i32 %v25_404855 to i32*
  %v3_404858 = load i32, i32* %v2_404858, align 4
  %v4_404858 = and i32 %v3_404858, %v21_404854
  store i1 false, i1* %az.global-to-local, align 1
  store i1 false, i1* %cf.global-to-local, align 1
  store i1 false, i1* %of.global-to-local, align 1
  %v5_404858 = icmp eq i32 %v4_404858, 0
  store i1 %v5_404858, i1* %zf.global-to-local, align 1
  %v6_404858 = icmp slt i32 %v4_404858, 0
  store i1 %v6_404858, i1* %sf.global-to-local, align 1
  %v7_404858 = trunc i32 %v4_404858 to i8
  %v8_404858 = call i8 @llvm.ctpop.i8(i8 %v7_404858), !range !0
  %v9_404858 = urem i8 %v8_404858, 2
  %v10_404858 = icmp eq i8 %v9_404858, 0
  %v3_40485a = zext i1 %v10_404858 to i32
  %v7_40485a = zext i1 %v5_404858 to i32
  %v9_40485a = zext i1 %v6_404858 to i32
  %v12_40485a = mul i32 %v3_40485a, 1024
  %v16_40485a = mul i32 %v7_40485a, 16384
  %v18_40485a = mul i32 %v9_40485a, 32768
  %v23_40485a = and i32 %v4_404858, -65281
  %v15_40485a = or i32 %v23_40485a, %v16_40485a
  %v19_40485a = or i32 %v15_40485a, %v18_40485a
  %v22_40485a = or i32 %v19_40485a, %v12_40485a
  %v24_40485a = or i32 %v22_40485a, 512
  store i32 %v24_40485a, i32* @eax, align 4
  %v3_40485b = add i32 %arg7, 1627458304
  %v4_40485b = inttoptr i32 %v3_40485b to i8*
  %v5_40485b = load i8, i8* %v4_40485b, align 1
  %v6_40485b = add i8 %v5_40485b, %v7_404858
  %v7_40485b = urem i8 %v7_404858, 16
  %v8_40485b = urem i8 %v5_40485b, 16
  %v9_40485b = add nuw nsw i8 %v8_40485b, %v7_40485b
  %v10_40485b = icmp ugt i8 %v9_40485b, 15
  %v11_40485b = icmp ult i8 %v6_40485b, %v7_404858
  %v12_40485b = xor i8 %v6_40485b, %v7_404858
  %v13_40485b = xor i8 %v6_40485b, %v5_40485b
  %v14_40485b = and i8 %v12_40485b, %v13_40485b
  %v15_40485b = icmp slt i8 %v14_40485b, 0
  store i1 %v10_40485b, i1* %az.global-to-local, align 1
  store i1 %v11_40485b, i1* %cf.global-to-local, align 1
  store i1 %v15_40485b, i1* %of.global-to-local, align 1
  %v16_40485b = icmp eq i8 %v6_40485b, 0
  store i1 %v16_40485b, i1* %zf.global-to-local, align 1
  %v17_40485b = icmp slt i8 %v6_40485b, 0
  store i1 %v17_40485b, i1* %sf.global-to-local, align 1
  %v21_40485b = zext i8 %v6_40485b to i32
  %v23_40485b = and i32 %v24_40485a, -14848
  %v24_40485b = or i32 %v23_40485b, %v21_40485b
  store i32 %v24_40485b, i32* @eax, align 4
  %v3_404861 = add i32 %arg7, 1224803072
  %v4_404861 = inttoptr i32 %v3_404861 to i8*
  %v5_404861 = load i8, i8* %v4_404861, align 1
  %v6_404861 = add i8 %v5_404861, %v6_40485b
  %v7_404861 = urem i8 %v6_40485b, 16
  %v8_404861 = urem i8 %v5_404861, 16
  %v9_404861 = add nuw nsw i8 %v8_404861, %v7_404861
  %v10_404861 = icmp ugt i8 %v9_404861, 15
  %v11_404861 = icmp ult i8 %v6_404861, %v6_40485b
  %v12_404861 = xor i8 %v6_404861, %v6_40485b
  %v13_404861 = xor i8 %v6_404861, %v5_404861
  %v14_404861 = and i8 %v12_404861, %v13_404861
  %v15_404861 = icmp slt i8 %v14_404861, 0
  store i1 %v10_404861, i1* %az.global-to-local, align 1
  store i1 %v11_404861, i1* %cf.global-to-local, align 1
  store i1 %v15_404861, i1* %of.global-to-local, align 1
  %v16_404861 = icmp eq i8 %v6_404861, 0
  store i1 %v16_404861, i1* %zf.global-to-local, align 1
  %v17_404861 = icmp slt i8 %v6_404861, 0
  store i1 %v17_404861, i1* %sf.global-to-local, align 1
  %v21_404861 = zext i8 %v6_404861 to i32
  %v24_404861 = or i32 %v23_40485b, %v21_404861
  store i32 %v24_404861, i32* @eax, align 4
  %v1_404867 = trunc i32 %arg9 to i8
  %v3_404867 = add i32 %v25_404855, -1409211648
  %v4_404867 = inttoptr i32 %v3_404867 to i8*
  %v5_404867 = load i8, i8* %v4_404867, align 1
  %v6_404867 = add i8 %v5_404867, %v1_404867
  %v7_404867 = urem i8 %v1_404867, 16
  %v8_404867 = urem i8 %v5_404867, 16
  %v9_404867 = add nuw nsw i8 %v8_404867, %v7_404867
  %v10_404867 = icmp ugt i8 %v9_404867, 15
  %v11_404867 = icmp ult i8 %v6_404867, %v1_404867
  %v12_404867 = xor i8 %v6_404867, %v1_404867
  %v13_404867 = xor i8 %v6_404867, %v5_404867
  %v14_404867 = and i8 %v12_404867, %v13_404867
  %v15_404867 = icmp slt i8 %v14_404867, 0
  store i1 %v10_404867, i1* %az.global-to-local, align 1
  store i1 %v11_404867, i1* %cf.global-to-local, align 1
  store i1 %v15_404867, i1* %of.global-to-local, align 1
  %v16_404867 = icmp eq i8 %v6_404867, 0
  store i1 %v16_404867, i1* %zf.global-to-local, align 1
  %v17_404867 = icmp slt i8 %v6_404867, 0
  store i1 %v17_404867, i1* %sf.global-to-local, align 1
  %v21_404867 = zext i8 %v6_404867 to i32
  %v23_404867 = and i32 %v25_404855, -256
  %v24_404867 = or i32 %v23_404867, %v21_404867
  store i32 %v24_404867, i32* @ecx, align 4
  %v1_40486d = udiv i32 %v23_40485b, 256
  %v2_40486d = trunc i32 %v1_40486d to i8
  %v4_40486d = add i32 %v24_404867, 1879128832
  %v5_40486d = inttoptr i32 %v4_40486d to i8*
  %v6_40486d = load i8, i8* %v5_40486d, align 1
  %v7_40486d = add i8 %v6_40486d, %v2_40486d
  %v8_40486d = and i8 %v2_40486d, 6
  %v9_40486d = urem i8 %v6_40486d, 16
  %v10_40486d = add nuw nsw i8 %v9_40486d, %v8_40486d
  %v11_40486d = icmp ugt i8 %v10_40486d, 15
  %v12_40486d = icmp ult i8 %v7_40486d, %v2_40486d
  %v13_40486d = xor i8 %v7_40486d, %v2_40486d
  %v14_40486d = xor i8 %v7_40486d, %v6_40486d
  %v15_40486d = and i8 %v13_40486d, %v14_40486d
  %v16_40486d = icmp slt i8 %v15_40486d, 0
  store i1 %v11_40486d, i1* %az.global-to-local, align 1
  store i1 %v12_40486d, i1* %cf.global-to-local, align 1
  store i1 %v16_40486d, i1* %of.global-to-local, align 1
  %v17_40486d = icmp eq i8 %v7_40486d, 0
  store i1 %v17_40486d, i1* %zf.global-to-local, align 1
  %v18_40486d = icmp slt i8 %v7_40486d, 0
  store i1 %v18_40486d, i1* %sf.global-to-local, align 1
  %v22_40486d = zext i8 %v7_40486d to i32
  %v24_40486d = mul i32 %v22_40486d, 256
  %v25_40486d = and i32 %v24_404861, -65281
  %v26_40486d = or i32 %v24_40486d, %v25_40486d
  store i32 %v26_40486d, i32* @eax, align 4
  %v3_404873 = load i32, i32* @ebx, align 4
  %v4_404873 = add i32 %v3_404873, 1879118592
  %v5_404873 = inttoptr i32 %v4_404873 to i8*
  %v6_404873 = load i8, i8* %v5_404873, align 1
  %v7_404873 = add i8 %v6_404873, %v7_40486d
  %v8_404873 = urem i8 %v7_40486d, 16
  %v9_404873 = urem i8 %v6_404873, 16
  %v10_404873 = add nuw nsw i8 %v9_404873, %v8_404873
  %v11_404873 = icmp ugt i8 %v10_404873, 15
  %v12_404873 = icmp ult i8 %v7_404873, %v7_40486d
  %v13_404873 = xor i8 %v7_404873, %v7_40486d
  %v14_404873 = xor i8 %v7_404873, %v6_404873
  %v15_404873 = and i8 %v13_404873, %v14_404873
  %v16_404873 = icmp slt i8 %v15_404873, 0
  store i1 %v11_404873, i1* %az.global-to-local, align 1
  store i1 %v12_404873, i1* %cf.global-to-local, align 1
  store i1 %v16_404873, i1* %of.global-to-local, align 1
  %v17_404873 = icmp eq i8 %v7_404873, 0
  store i1 %v17_404873, i1* %zf.global-to-local, align 1
  %v18_404873 = icmp slt i8 %v7_404873, 0
  store i1 %v18_404873, i1* %sf.global-to-local, align 1
  %v22_404873 = zext i8 %v7_404873 to i32
  %v24_404873 = mul i32 %v22_404873, 256
  %v26_404873 = or i32 %v24_404873, %v25_40486d
  store i32 %v26_404873, i32* @eax, align 4
  %v4_404879 = add i32 %v3_404873, 1879120640
  %v5_404879 = inttoptr i32 %v4_404879 to i8*
  %v6_404879 = load i8, i8* %v5_404879, align 1
  %v7_404879 = add i8 %v6_404879, %v7_404873
  %v8_404879 = urem i8 %v7_404873, 16
  %v9_404879 = urem i8 %v6_404879, 16
  %v10_404879 = add nuw nsw i8 %v9_404879, %v8_404879
  %v11_404879 = icmp ugt i8 %v10_404879, 15
  %v12_404879 = icmp ult i8 %v7_404879, %v7_404873
  %v13_404879 = xor i8 %v7_404879, %v7_404873
  %v14_404879 = xor i8 %v7_404879, %v6_404879
  %v15_404879 = and i8 %v13_404879, %v14_404879
  %v16_404879 = icmp slt i8 %v15_404879, 0
  store i1 %v11_404879, i1* %az.global-to-local, align 1
  store i1 %v12_404879, i1* %cf.global-to-local, align 1
  store i1 %v16_404879, i1* %of.global-to-local, align 1
  %v17_404879 = icmp eq i8 %v7_404879, 0
  store i1 %v17_404879, i1* %zf.global-to-local, align 1
  %v18_404879 = icmp slt i8 %v7_404879, 0
  store i1 %v18_404879, i1* %sf.global-to-local, align 1
  %v22_404879 = zext i8 %v7_404879 to i32
  %v24_404879 = mul i32 %v22_404879, 256
  %v26_404879 = or i32 %v24_404879, %v25_40486d
  store i32 %v26_404879, i32* @eax, align 4
  %v4_40487f = add i32 %v3_404873, 1224803072
  %v5_40487f = inttoptr i32 %v4_40487f to i8*
  %v6_40487f = load i8, i8* %v5_40487f, align 1
  %v7_40487f = add i8 %v6_40487f, %v7_404879
  %v22_40487f = zext i8 %v7_40487f to i32
  %v24_40487f = mul i32 %v22_40487f, 256
  %v3_404885 = trunc i32 %v3_404873 to i8
  %v4_404885 = add i8 %v6_404861, %v3_404885
  %v5_404885 = urem i8 %v6_404861, 16
  %v6_404885 = urem i8 %v3_404885, 16
  %v7_404885 = add nuw nsw i8 %v6_404885, %v5_404885
  %v8_404885 = icmp ugt i8 %v7_404885, 15
  %v9_404885 = icmp ult i8 %v4_404885, %v6_404861
  %v10_404885 = xor i8 %v4_404885, %v6_404861
  %v11_404885 = xor i8 %v4_404885, %v3_404885
  %v12_404885 = and i8 %v10_404885, %v11_404885
  %v13_404885 = icmp slt i8 %v12_404885, 0
  store i1 %v8_404885, i1* %az.global-to-local, align 1
  store i1 %v9_404885, i1* %cf.global-to-local, align 1
  store i1 %v13_404885, i1* %of.global-to-local, align 1
  %v14_404885 = icmp eq i8 %v4_404885, 0
  store i1 %v14_404885, i1* %zf.global-to-local, align 1
  %v15_404885 = icmp slt i8 %v4_404885, 0
  store i1 %v15_404885, i1* %sf.global-to-local, align 1
  %v19_404885 = zext i8 %v4_404885 to i32
  %v25_40486d.masked = and i32 %v4_404858, -65536
  %v21_404885 = or i32 %v25_40486d.masked, %v19_404885
  %v22_404885 = or i32 %v21_404885, %v24_40487f
  store i32 %v22_404885, i32* @eax, align 4
  %v1_404887 = inttoptr i32 %v3_404873 to i8*
  %v2_404887 = load i8, i8* %v1_404887, align 1
  %v3_404887 = load i32, i32* @ecx, align 4
  %v4_404887 = udiv i32 %v3_404887, 256
  %v5_404887 = trunc i32 %v4_404887 to i8
  %v6_404887 = add i8 %v2_404887, %v5_404887
  store i8 %v6_404887, i8* %v1_404887, align 1
  %v0_404889 = load i32, i32* @eax, align 4
  %v2_404889 = mul i32 %v0_404889, 2
  %v14_404889 = trunc i32 %v2_404889 to i8
  %v2_40488b = load i32, i32* @ebx, align 4
  %v3_40488b = trunc i32 %v2_40488b to i8
  %v4_40488b = add i8 %v3_40488b, %v14_404889
  %v5_40488b = and i8 %v14_404889, 14
  %v6_40488b = urem i8 %v3_40488b, 16
  %v7_40488b = add nuw nsw i8 %v6_40488b, %v5_40488b
  %v8_40488b = icmp ugt i8 %v7_40488b, 15
  %v9_40488b = icmp ult i8 %v4_40488b, %v14_404889
  %v10_40488b = xor i8 %v4_40488b, %v14_404889
  %v11_40488b = xor i8 %v4_40488b, %v3_40488b
  %v12_40488b = and i8 %v10_40488b, %v11_40488b
  %v13_40488b = icmp slt i8 %v12_40488b, 0
  store i1 %v8_40488b, i1* %az.global-to-local, align 1
  store i1 %v9_40488b, i1* %cf.global-to-local, align 1
  store i1 %v13_40488b, i1* %of.global-to-local, align 1
  %v14_40488b = icmp eq i8 %v4_40488b, 0
  store i1 %v14_40488b, i1* %zf.global-to-local, align 1
  %v15_40488b = icmp slt i8 %v4_40488b, 0
  store i1 %v15_40488b, i1* %sf.global-to-local, align 1
  %v19_40488b = zext i8 %v4_40488b to i32
  %v21_40488b = and i32 %v2_404889, -256
  %v22_40488b = or i32 %v21_40488b, %v19_40488b
  store i32 %v22_40488b, i32* @eax, align 4
  %v1_40488d = inttoptr i32 %v2_40488b to i8*
  %v2_40488d = load i8, i8* %v1_40488d, align 1
  %v3_40488d = load i32, i32* @ecx, align 4
  %v4_40488d = trunc i32 %v3_40488d to i8
  %v5_40488d = add i8 %v2_40488d, %v4_40488d
  store i8 %v5_40488d, i8* %v1_40488d, align 1
  %v0_40488f = load i32, i32* @ecx, align 4
  %v1_40488f = add i32 %v0_40488f, 2
  %v2_40488f = inttoptr i32 %v1_40488f to i32*
  %v3_40488f = load i32, i32* %v2_40488f, align 4
  %v4_40488f = load i32, i32* @esp, align 4
  %v5_40488f = add i32 %v4_40488f, %v3_40488f
  store i32 %v5_40488f, i32* %v2_40488f, align 4
  %v9_404896 = load i32, i32* %stack_var_40, align 4
  %v21_404896 = load i32, i32* %stack_var_68, align 4
  store i32 %v9_404896, i32* @edi, align 4
  store i32 %arg12, i32* @esi, align 4
  store i32 %arg13, i32* @ebp, align 4
  store i32 %arg14, i32* @ebx, align 4
  store i32 %arg15, i32* @edx, align 4
  store i32 %arg16, i32* @ecx, align 4
  %v22_404896 = ptrtoint i32* %stack_var_72 to i32
  %v1_404897 = udiv i32 %v21_404896, 256
  %v2_404897 = trunc i32 %v1_404897 to i8
  %v4_404897 = trunc i32 %arg14 to i8
  %v5_404897 = add i8 %v2_404897, %v4_404897
  %v6_404897 = urem i8 %v2_404897, 16
  %v7_404897 = urem i8 %v4_404897, 16
  %v8_404897 = add nuw nsw i8 %v6_404897, %v7_404897
  %v9_404897 = icmp ugt i8 %v8_404897, 15
  %v10_404897 = icmp ult i8 %v5_404897, %v2_404897
  %v11_404897 = xor i8 %v5_404897, %v2_404897
  %v12_404897 = xor i8 %v5_404897, %v4_404897
  %v13_404897 = and i8 %v11_404897, %v12_404897
  %v14_404897 = icmp slt i8 %v13_404897, 0
  store i1 %v9_404897, i1* %az.global-to-local, align 1
  store i1 %v10_404897, i1* %cf.global-to-local, align 1
  store i1 %v14_404897, i1* %of.global-to-local, align 1
  %v15_404897 = icmp eq i8 %v5_404897, 0
  store i1 %v15_404897, i1* %zf.global-to-local, align 1
  %v16_404897 = icmp slt i8 %v5_404897, 0
  store i1 %v16_404897, i1* %sf.global-to-local, align 1
  %v20_404897 = zext i8 %v5_404897 to i32
  %v22_404897 = mul i32 %v20_404897, 256
  %v23_404897 = and i32 %v21_404896, -65281
  %v24_404897 = or i32 %v22_404897, %v23_404897
  store i32 %v24_404897, i32* @eax, align 4
  %v1_404899 = inttoptr i32 %arg14 to i8*
  %v2_404899 = load i8, i8* %v1_404899, align 1
  %v4_404899 = trunc i32 %arg16 to i8
  %v5_404899 = add i8 %v2_404899, %v4_404899
  store i8 %v5_404899, i8* %v1_404899, align 1
  %v0_40489b = load i32, i32* @ecx, align 4
  %v1_40489b = add i32 %v0_40489b, 2
  %v2_40489b = inttoptr i32 %v1_40489b to i32*
  %v3_40489b = load i32, i32* %v2_40489b, align 4
  %v4_40489b = load i32, i32* @esp, align 4
  %v5_40489b = add i32 %v4_40489b, %v3_40489b
  store i32 %v5_40489b, i32* %v2_40489b, align 4
  %v0_4048a0 = load i32, i32* @eax, align 4
  %v1_4048a0 = load i32, i32* @ecx, align 4
  %v2_4048a0 = inttoptr i32 %v1_4048a0 to i32*
  %v3_4048a0 = load i32, i32* %v2_4048a0, align 4
  %v4_4048a0 = xor i32 %v3_4048a0, %v0_4048a0
  store i1 false, i1* %az.global-to-local, align 1
  store i1 false, i1* %cf.global-to-local, align 1
  store i1 false, i1* %of.global-to-local, align 1
  %v5_4048a0 = icmp eq i32 %v4_4048a0, 0
  store i1 %v5_4048a0, i1* %zf.global-to-local, align 1
  %v6_4048a0 = icmp slt i32 %v4_4048a0, 0
  store i1 %v6_4048a0, i1* %sf.global-to-local, align 1
  %v7_4048a0 = trunc i32 %v4_4048a0 to i8
  store i32 %v4_4048a0, i32* @eax, align 4
  %v2_4048a2 = load i32, i32* @ebx, align 4
  %v3_4048a2 = inttoptr i32 %v2_4048a2 to i8*
  %v4_4048a2 = load i8, i8* %v3_4048a2, align 1
  %v5_4048a2 = sub i8 %v7_4048a0, %v4_4048a2
  %v20_4048a2 = zext i8 %v5_4048a2 to i32
  %v22_4048a2 = and i32 %v4_4048a0, -256
  %v23_4048a2 = or i32 %v22_4048a2, %v20_4048a2
  store i32 %v23_4048a2, i32* @eax, align 4
  %v3_4048a4 = load i32, i32* %v2_4048a0, align 4
  %v4_4048a4 = add i32 %v23_4048a2, %v3_4048a4
  %v9_4048a4 = icmp ult i32 %v4_4048a4, %v23_4048a2
  store i32 %v4_4048a4, i32* @eax, align 4
  %v3_4048a6 = load i32, i32* %v2_4048a0, align 4
  %v5_4048a6 = zext i1 %v9_4048a4 to i32
  %v6_4048a6 = add i32 %v3_4048a6, %v5_4048a6
  %v7_4048a6 = sub i32 %v4_4048a4, %v6_4048a6
  %v8_4048a6 = urem i32 %v4_4048a4, 16
  %v9_4048a6 = urem i32 %v6_4048a6, 16
  %v10_4048a6 = add nuw nsw i32 %v8_4048a6, %v5_4048a6
  %v13_4048a6 = sub nsw i32 %v10_4048a6, %v9_4048a6
  %v14_4048a6 = icmp ugt i32 %v13_4048a6, 15
  %v17_4048a6 = sub i32 %v7_4048a6, %v5_4048a6
  %v18_4048a6 = icmp ult i32 %v4_4048a4, %v17_4048a6
  %v19_4048a6 = icmp ne i32 %v6_4048a6, -1
  %v20_4048a6 = or i1 %v19_4048a6, %v18_4048a6
  %v21_4048a6 = icmp ult i32 %v4_4048a4, %v6_4048a6
  %v22_4048a6 = select i1 %v9_4048a4, i1 %v20_4048a6, i1 %v21_4048a6
  %v26_4048a6 = xor i32 %v6_4048a6, %v4_4048a4
  %v27_4048a6 = xor i32 %v17_4048a6, %v4_4048a4
  %v28_4048a6 = and i32 %v27_4048a6, %v26_4048a6
  %v29_4048a6 = icmp slt i32 %v28_4048a6, 0
  store i1 %v14_4048a6, i1* %az.global-to-local, align 1
  store i1 %v22_4048a6, i1* %cf.global-to-local, align 1
  store i1 %v29_4048a6, i1* %of.global-to-local, align 1
  %v30_4048a6 = icmp eq i32 %v7_4048a6, 0
  store i1 %v30_4048a6, i1* %zf.global-to-local, align 1
  %v31_4048a6 = icmp slt i32 %v7_4048a6, 0
  store i1 %v31_4048a6, i1* %sf.global-to-local, align 1
  store i32 %v7_4048a6, i32* @eax, align 4
  br i1 %v29_4048a6, label %dec_label_pc_4048ac, label %dec_label_pc_4048aa

dec_label_pc_4048aa:                              ; preds = %dec_label_pc_40483a
  %v3_4048aa = load i32, i32* %v2_4048a0, align 4
  %v4_4048aa = and i32 %v3_4048aa, %v7_4048a6
  store i1 false, i1* %az.global-to-local, align 1
  store i1 false, i1* %cf.global-to-local, align 1
  store i1 false, i1* %of.global-to-local, align 1
  %v5_4048aa = icmp eq i32 %v4_4048aa, 0
  store i1 %v5_4048aa, i1* %zf.global-to-local, align 1
  %v6_4048aa = icmp slt i32 %v4_4048aa, 0
  store i1 %v6_4048aa, i1* %sf.global-to-local, align 1
  store i32 %v4_4048aa, i32* @eax, align 4
  br label %dec_label_pc_4048ac

dec_label_pc_4048ac:                              ; preds = %dec_label_pc_40483a, %dec_label_pc_4048aa
  %v4_4048ac = phi i1 [ false, %dec_label_pc_4048aa ], [ %v22_4048a6, %dec_label_pc_40483a ]
  %v0_4048ac = phi i32 [ %v4_4048aa, %dec_label_pc_4048aa ], [ %v7_4048a6, %dec_label_pc_40483a ]
  %v3_4048ac = load i32, i32* %v2_4048a0, align 4
  %v5_4048ac = zext i1 %v4_4048ac to i32
  %v6_4048ac = add i32 %v3_4048ac, %v5_4048ac
  %v7_4048ac = sub i32 %v0_4048ac, %v6_4048ac
  %v8_4048ac = urem i32 %v0_4048ac, 16
  %v9_4048ac = urem i32 %v6_4048ac, 16
  %v10_4048ac = add nuw nsw i32 %v8_4048ac, %v5_4048ac
  %v13_4048ac = sub nsw i32 %v10_4048ac, %v9_4048ac
  %v14_4048ac = icmp ugt i32 %v13_4048ac, 15
  %v17_4048ac = sub i32 %v7_4048ac, %v5_4048ac
  %v18_4048ac = icmp ult i32 %v0_4048ac, %v17_4048ac
  %v19_4048ac = icmp ne i32 %v6_4048ac, -1
  %v20_4048ac = or i1 %v19_4048ac, %v18_4048ac
  %v21_4048ac = icmp ult i32 %v0_4048ac, %v6_4048ac
  %spec.select = select i1 %v4_4048ac, i1 %v20_4048ac, i1 %v21_4048ac
  %v26_4048ac = xor i32 %v6_4048ac, %v0_4048ac
  %v27_4048ac = xor i32 %v17_4048ac, %v0_4048ac
  %v28_4048ac = and i32 %v27_4048ac, %v26_4048ac
  %v29_4048ac = icmp slt i32 %v28_4048ac, 0
  store i1 %v14_4048ac, i1* %az.global-to-local, align 1
  store i1 %spec.select, i1* %cf.global-to-local, align 1
  store i1 %v29_4048ac, i1* %of.global-to-local, align 1
  %v30_4048ac = icmp eq i32 %v7_4048ac, 0
  store i1 %v30_4048ac, i1* %zf.global-to-local, align 1
  %v31_4048ac = icmp slt i32 %v7_4048ac, 0
  store i1 %v31_4048ac, i1* %sf.global-to-local, align 1
  store i32 %v7_4048ac, i32* @eax, align 4
  br i1 %v29_4048ac, label %dec_label_pc_4048b2, label %dec_label_pc_4048b0

dec_label_pc_4048b0:                              ; preds = %dec_label_pc_4048ac
  %v1_4048b0 = add i32 %v2_4048a2, 1
  %v2_4048b0 = urem i32 %v2_4048a2, 16
  %v4_4048b0 = icmp eq i32 %v2_4048b0, 15
  %tmp474 = xor i32 %v2_4048a2, -2147483648
  %v7_4048b0 = and i32 %v1_4048b0, %tmp474
  %v8_4048b0 = icmp slt i32 %v7_4048b0, 0
  store i1 %v4_4048b0, i1* %az.global-to-local, align 1
  store i1 %v8_4048b0, i1* %of.global-to-local, align 1
  %v9_4048b0 = icmp eq i32 %v1_4048b0, 0
  store i1 %v9_4048b0, i1* %zf.global-to-local, align 1
  %v10_4048b0 = icmp slt i32 %v1_4048b0, 0
  store i1 %v10_4048b0, i1* %sf.global-to-local, align 1
  store i32 %v1_4048b0, i32* @ebx, align 4
  ret i32 %v7_4048ac

dec_label_pc_4048b2:                              ; preds = %dec_label_pc_4048ac
  %v3_4048b2 = load i32, i32* %v2_4048a0, align 4
  %v5_4048b2 = zext i1 %spec.select to i32
  %v6_4048b2 = add i32 %v3_4048b2, %v5_4048b2
  %v7_4048b2 = sub i32 %v7_4048ac, %v6_4048b2
  %v8_4048b2 = urem i32 %v7_4048ac, 16
  %v9_4048b2 = urem i32 %v6_4048b2, 16
  %v10_4048b2 = add nuw nsw i32 %v8_4048b2, %v5_4048b2
  %v13_4048b2 = sub nsw i32 %v10_4048b2, %v9_4048b2
  %v14_4048b2 = icmp ugt i32 %v13_4048b2, 15
  %v17_4048b2 = sub i32 %v7_4048b2, %v5_4048b2
  %v18_4048b2 = icmp ult i32 %v7_4048ac, %v17_4048b2
  %v19_4048b2 = icmp ne i32 %v6_4048b2, -1
  %v20_4048b2 = or i1 %v19_4048b2, %v18_4048b2
  %v21_4048b2 = icmp ult i32 %v7_4048ac, %v6_4048b2
  %v22_4048b2 = select i1 %spec.select, i1 %v20_4048b2, i1 %v21_4048b2
  %v26_4048b2 = xor i32 %v6_4048b2, %v7_4048ac
  %v27_4048b2 = xor i32 %v17_4048b2, %v7_4048ac
  %v28_4048b2 = and i32 %v27_4048b2, %v26_4048b2
  %v29_4048b2 = icmp slt i32 %v28_4048b2, 0
  store i1 %v14_4048b2, i1* %az.global-to-local, align 1
  store i1 %v22_4048b2, i1* %cf.global-to-local, align 1
  store i1 %v29_4048b2, i1* %of.global-to-local, align 1
  %v30_4048b2 = icmp eq i32 %v7_4048b2, 0
  store i1 %v30_4048b2, i1* %zf.global-to-local, align 1
  %v31_4048b2 = icmp slt i32 %v7_4048b2, 0
  store i1 %v31_4048b2, i1* %sf.global-to-local, align 1
  store i32 %v7_4048b2, i32* @eax, align 4
  br i1 %v29_4048b2, label %dec_label_pc_4048b8, label %dec_label_pc_4048b6

dec_label_pc_4048b6:                              ; preds = %dec_label_pc_4048b2
  %v36_4048b2 = trunc i32 %v7_4048b2 to i16
  %v1_4048b6 = inttoptr i32 %v1_4048a0 to i16*
  %v2_4048b6 = load i16, i16* %v1_4048b6, align 2
  call void @__asm_arpl(i16 %v2_4048b6, i16 %v36_4048b2)
  %v2_4048b8.pre = load i32, i32* @ecx, align 4
  %v5_4048b8.pre = load i1, i1* %cf.global-to-local, align 1
  %.pre131 = inttoptr i32 %v2_4048b8.pre to i32*
  br label %dec_label_pc_4048b8

dec_label_pc_4048b8:                              ; preds = %dec_label_pc_4048b6, %dec_label_pc_4048b2
  %v3_4048b8.pre-phi = phi i32* [ %.pre131, %dec_label_pc_4048b6 ], [ %v2_4048a0, %dec_label_pc_4048b2 ]
  %v5_4048b8 = phi i1 [ %v5_4048b8.pre, %dec_label_pc_4048b6 ], [ %v22_4048b2, %dec_label_pc_4048b2 ]
  %sext = mul i32 %v7_4048b2, 65536
  %v1_4048b8 = sdiv i32 %sext, 65536
  %v4_4048b8 = load i32, i32* %v3_4048b8.pre-phi, align 4
  %v6_4048b8 = zext i1 %v5_4048b8 to i32
  %v7_4048b8 = add i32 %v4_4048b8, %v6_4048b8
  %v8_4048b8 = sub i32 %v1_4048b8, %v7_4048b8
  %v9_4048b8 = urem i32 %v1_4048b8, 16
  %v10_4048b8 = urem i32 %v7_4048b8, 16
  %v11_4048b8 = add nuw nsw i32 %v9_4048b8, %v6_4048b8
  %v14_4048b8 = sub nsw i32 %v11_4048b8, %v10_4048b8
  %v15_4048b8 = icmp ugt i32 %v14_4048b8, 15
  %v18_4048b8 = sub i32 %v8_4048b8, %v6_4048b8
  %v19_4048b8 = icmp ugt i32 %v18_4048b8, %v1_4048b8
  %v20_4048b8 = icmp ne i32 %v7_4048b8, -1
  %v21_4048b8 = or i1 %v20_4048b8, %v19_4048b8
  %v22_4048b8 = icmp ult i32 %v1_4048b8, %v7_4048b8
  %v23_4048b8 = select i1 %v5_4048b8, i1 %v21_4048b8, i1 %v22_4048b8
  %v27_4048b8 = xor i32 %v7_4048b8, %v1_4048b8
  %v28_4048b8 = xor i32 %v18_4048b8, %v1_4048b8
  %v29_4048b8 = and i32 %v28_4048b8, %v27_4048b8
  %v30_4048b8 = icmp slt i32 %v29_4048b8, 0
  store i1 %v15_4048b8, i1* %az.global-to-local, align 1
  store i1 %v23_4048b8, i1* %cf.global-to-local, align 1
  store i1 %v30_4048b8, i1* %of.global-to-local, align 1
  %v31_4048b8 = icmp eq i32 %v8_4048b8, 0
  store i1 %v31_4048b8, i1* %zf.global-to-local, align 1
  %v32_4048b8 = icmp slt i32 %v8_4048b8, 0
  store i1 %v32_4048b8, i1* %sf.global-to-local, align 1
  store i32 %v8_4048b8, i32* @eax, align 4
  br i1 %v30_4048b8, label %dec_label_pc_4048be, label %dec_label_pc_404838

dec_label_pc_4048be:                              ; preds = %dec_label_pc_4048b8
  %v3_4048be = load i32, i32* %v3_4048b8.pre-phi, align 4
  %v5_4048be = zext i1 %v23_4048b8 to i32
  %v6_4048be = add i32 %v3_4048be, %v5_4048be
  %v7_4048be = sub i32 %v8_4048b8, %v6_4048be
  %v8_4048be = urem i32 %v8_4048b8, 16
  %v9_4048be = urem i32 %v6_4048be, 16
  %v10_4048be = add nuw nsw i32 %v8_4048be, %v5_4048be
  %v13_4048be = sub nsw i32 %v10_4048be, %v9_4048be
  %v14_4048be = icmp ugt i32 %v13_4048be, 15
  %v17_4048be = sub i32 %v7_4048be, %v5_4048be
  %v18_4048be = icmp ult i32 %v8_4048b8, %v17_4048be
  %v19_4048be = icmp ne i32 %v6_4048be, -1
  %v20_4048be = or i1 %v19_4048be, %v18_4048be
  %v21_4048be = icmp ult i32 %v8_4048b8, %v6_4048be
  %v22_4048be = select i1 %v23_4048b8, i1 %v20_4048be, i1 %v21_4048be
  %v26_4048be = xor i32 %v6_4048be, %v8_4048b8
  %v27_4048be = xor i32 %v17_4048be, %v8_4048b8
  %v28_4048be = and i32 %v27_4048be, %v26_4048be
  %v29_4048be = icmp slt i32 %v28_4048be, 0
  store i1 %v14_4048be, i1* %az.global-to-local, align 1
  store i1 %v22_4048be, i1* %cf.global-to-local, align 1
  store i1 %v29_4048be, i1* %of.global-to-local, align 1
  %v30_4048be = icmp eq i32 %v7_4048be, 0
  store i1 %v30_4048be, i1* %zf.global-to-local, align 1
  %v31_4048be = icmp slt i32 %v7_4048be, 0
  store i1 %v31_4048be, i1* %sf.global-to-local, align 1
  %v32_4048be = trunc i32 %v7_4048be to i8
  store i32 %v7_4048be, i32* @eax, align 4
  br i1 %v29_4048be, label %dec_label_pc_4048c4, label %dec_label_pc_4048c2

dec_label_pc_4048c2:                              ; preds = %dec_label_pc_4048be
  %v0_4048c2 = load i32, i32* @ebx, align 4
  %v1_4048c2 = inttoptr i32 %v0_4048c2 to i8*
  %v2_4048c2 = load i8, i8* %v1_4048c2, align 1
  %v5_4048c2 = add i8 %v2_4048c2, %v32_4048be
  %v6_4048c2 = urem i8 %v2_4048c2, 16
  %v7_4048c2 = urem i8 %v32_4048be, 16
  %v8_4048c2 = add nuw nsw i8 %v6_4048c2, %v7_4048c2
  %v9_4048c2 = icmp ugt i8 %v8_4048c2, 15
  %v10_4048c2 = icmp ult i8 %v5_4048c2, %v2_4048c2
  %v11_4048c2 = xor i8 %v5_4048c2, %v2_4048c2
  %v12_4048c2 = xor i8 %v5_4048c2, %v32_4048be
  %v13_4048c2 = and i8 %v11_4048c2, %v12_4048c2
  %v14_4048c2 = icmp slt i8 %v13_4048c2, 0
  store i1 %v9_4048c2, i1* %az.global-to-local, align 1
  store i1 %v10_4048c2, i1* %cf.global-to-local, align 1
  store i1 %v14_4048c2, i1* %of.global-to-local, align 1
  %v15_4048c2 = icmp eq i8 %v5_4048c2, 0
  store i1 %v15_4048c2, i1* %zf.global-to-local, align 1
  %v16_4048c2 = icmp slt i8 %v5_4048c2, 0
  store i1 %v16_4048c2, i1* %sf.global-to-local, align 1
  store i8 %v5_4048c2, i8* %v1_4048c2, align 1
  %v0_4048c4.pre = load i32, i32* @eax, align 4
  %v1_4048c4.pre = load i32, i32* @ecx, align 4
  %.pre132 = inttoptr i32 %v1_4048c4.pre to i32*
  br label %dec_label_pc_4048c4

dec_label_pc_4048c4:                              ; preds = %dec_label_pc_4048c2, %dec_label_pc_4048be
  %v2_4048c4.pre-phi = phi i32* [ %.pre132, %dec_label_pc_4048c2 ], [ %v3_4048b8.pre-phi, %dec_label_pc_4048be ]
  %v0_4048c4 = phi i32 [ %v0_4048c4.pre, %dec_label_pc_4048c2 ], [ %v7_4048be, %dec_label_pc_4048be ]
  %v3_4048c4 = load i32, i32* %v2_4048c4.pre-phi, align 4
  %v4_4048c4 = or i32 %v3_4048c4, %v0_4048c4
  store i1 false, i1* %az.global-to-local, align 1
  store i1 false, i1* %cf.global-to-local, align 1
  store i1 false, i1* %of.global-to-local, align 1
  %v5_4048c4 = icmp eq i32 %v4_4048c4, 0
  store i1 %v5_4048c4, i1* %zf.global-to-local, align 1
  %v6_4048c4 = icmp slt i32 %v4_4048c4, 0
  store i1 %v6_4048c4, i1* %sf.global-to-local, align 1
  %v9_4048c6 = load i32, i32* %stack_var_72, align 4
  store i32 %v9_4048c6, i32* @edi, align 4
  store i32 %arg19, i32* @esi, align 4
  store i32 %arg20, i32* @ebp, align 4
  store i32 %arg21, i32* @ebx, align 4
  store i32 %arg22, i32* @edx, align 4
  store i32 %arg23, i32* @ecx, align 4
  store i32 %arg24, i32* @eax, align 4
  %v22_4048c6 = ptrtoint i32* %stack_var_104 to i32
  %v1_4048c7 = udiv i32 %arg24, 256
  %v2_4048c7 = trunc i32 %v1_4048c7 to i8
  %v4_4048c7 = inttoptr i32 %arg24 to i8*
  %v5_4048c7 = load i8, i8* %v4_4048c7, align 1
  %v6_4048c7 = add i8 %v5_4048c7, %v2_4048c7
  %v21_4048c7 = zext i8 %v6_4048c7 to i32
  %v23_4048c7 = mul i32 %v21_4048c7, 256
  %v24_4048c7 = and i32 %arg24, -65281
  %v25_4048c7 = or i32 %v23_4048c7, %v24_4048c7
  store i32 %v25_4048c7, i32* @eax, align 4
  %v2_4048c9 = inttoptr i32 %arg21 to i32*
  %v3_4048c9 = load i32, i32* %v2_4048c9, align 4
  %v4_4048c9 = add i32 %v3_4048c9, %arg23
  store i32 %v4_4048c9, i32* @ecx, align 4
  %v1_4048cb = add i32 %v4_4048c9, 2
  %v2_4048cb = inttoptr i32 %v1_4048cb to i32*
  %v3_4048cb = load i32, i32* %v2_4048cb, align 4
  %v5_4048cb = add i32 %v3_4048cb, %v22_4048c6
  store i32 %v5_4048cb, i32* %v2_4048cb, align 4
  %v0_4048ce = load i32, i32* @eax, align 4
  %v1_4048ce = add i32 %v0_4048ce, 1
  store i32 %v1_4048ce, i32* @eax, align 4
  %v0_4048cf = load i32, i32* @ecx, align 4
  %v1_4048cf = load i32, i32* @ebx, align 4
  %v2_4048cf = inttoptr i32 %v1_4048cf to i32*
  %v3_4048cf = load i32, i32* %v2_4048cf, align 4
  %v4_4048cf = add i32 %v3_4048cf, %v0_4048cf
  store i32 %v4_4048cf, i32* @ecx, align 4
  %v1_4048d1 = add i32 %v4_4048cf, 2
  %v2_4048d1 = inttoptr i32 %v1_4048d1 to i32*
  %v3_4048d1 = load i32, i32* %v2_4048d1, align 4
  %v4_4048d1 = load i32, i32* @esp, align 4
  %v5_4048d1 = add i32 %v4_4048d1, %v3_4048d1
  store i32 %v5_4048d1, i32* %v2_4048d1, align 4
  %v10_4048d4 = load i32, i32* @eax, align 4
  %v12_4048d4 = load i32, i32* @ecx, align 4
  %v14_4048d4 = load i32, i32* @edx, align 4
  %v16_4048d4 = load i32, i32* @ebx, align 4
  %v20_4048d4 = load i32, i32* @ebp, align 4
  %v22_4048d4 = load i32, i32* @esi, align 4
  %v24_4048d4 = load i32, i32* @edi, align 4
  store i32 %v24_4048d4, i32* %stack_var_72, align 4
  %v2_4048d5 = inttoptr i32 %v16_4048d4 to i32*
  %v3_4048d5 = load i32, i32* %v2_4048d5, align 4
  %v4_4048d5 = add i32 %v3_4048d5, %v12_4048d4
  store i32 %v4_4048d5, i32* @ecx, align 4
  %v1_4048d7 = add i32 %v4_4048d5, 2
  %v2_4048d7 = inttoptr i32 %v1_4048d7 to i32*
  %v3_4048d7 = load i32, i32* %v2_4048d7, align 4
  %v5_4048d7 = add i32 %v3_4048d7, %v22_404896
  store i32 %v5_4048d7, i32* %v2_4048d7, align 4
  %v0_4048dd = load i32, i32* @ecx, align 4
  %v1_4048dd = add i32 %v0_4048dd, 2
  %v2_4048dd = inttoptr i32 %v1_4048dd to i32*
  %v3_4048dd = load i32, i32* %v2_4048dd, align 4
  %v4_4048dd = load i32, i32* @esp, align 4
  %v5_4048dd = add i32 %v4_4048dd, %v3_4048dd
  %v6_4048dd = urem i32 %v3_4048dd, 16
  %v7_4048dd = urem i32 %v4_4048dd, 16
  %v8_4048dd = add nuw nsw i32 %v7_4048dd, %v6_4048dd
  %v9_4048dd = icmp ugt i32 %v8_4048dd, 15
  %v10_4048dd = icmp ult i32 %v5_4048dd, %v3_4048dd
  %v11_4048dd = xor i32 %v5_4048dd, %v3_4048dd
  %v12_4048dd = xor i32 %v5_4048dd, %v4_4048dd
  %v13_4048dd = and i32 %v11_4048dd, %v12_4048dd
  %v14_4048dd = icmp slt i32 %v13_4048dd, 0
  store i1 %v9_4048dd, i1* %az.global-to-local, align 1
  store i1 %v10_4048dd, i1* %cf.global-to-local, align 1
  store i1 %v14_4048dd, i1* %of.global-to-local, align 1
  %v15_4048dd = icmp eq i32 %v5_4048dd, 0
  store i1 %v15_4048dd, i1* %zf.global-to-local, align 1
  %v16_4048dd = icmp slt i32 %v5_4048dd, 0
  store i1 %v16_4048dd, i1* %sf.global-to-local, align 1
  store i32 %v5_4048dd, i32* %v2_4048dd, align 4
  %v0_4048e0 = load i8, i8* inttoptr (i32 39911691 to i8*), align 1
  %v1_4048e0 = load i32, i32* @eax, align 4
  %v2_4048e0 = trunc i32 %v1_4048e0 to i8
  %v3_4048e0 = add i8 %v0_4048e0, %v2_4048e0
  %v4_4048e0 = urem i8 %v0_4048e0, 16
  %v5_4048e0 = urem i8 %v2_4048e0, 16
  %v6_4048e0 = add nuw nsw i8 %v5_4048e0, %v4_4048e0
  %v7_4048e0 = icmp ugt i8 %v6_4048e0, 15
  %v8_4048e0 = icmp ult i8 %v3_4048e0, %v0_4048e0
  %v9_4048e0 = xor i8 %v3_4048e0, %v0_4048e0
  %v10_4048e0 = xor i8 %v3_4048e0, %v2_4048e0
  %v11_4048e0 = and i8 %v9_4048e0, %v10_4048e0
  %v12_4048e0 = icmp slt i8 %v11_4048e0, 0
  store i1 %v7_4048e0, i1* %az.global-to-local, align 1
  store i1 %v8_4048e0, i1* %cf.global-to-local, align 1
  store i1 %v12_4048e0, i1* %of.global-to-local, align 1
  %v13_4048e0 = icmp eq i8 %v3_4048e0, 0
  store i1 %v13_4048e0, i1* %zf.global-to-local, align 1
  %v14_4048e0 = icmp slt i8 %v3_4048e0, 0
  store i1 %v14_4048e0, i1* %sf.global-to-local, align 1
  store i8 %v3_4048e0, i8* inttoptr (i32 39911691 to i8*), align 1
  %v0_4048e6 = load i8, i8* inttoptr (i32 1879130885 to i8*), align 1
  %v1_4048e6 = zext i8 %v0_4048e6 to i32
  %v2_4048e6 = load i32, i32* @eax, align 4
  %v3_4048e6 = and i32 %v2_4048e6, -256
  %v4_4048e6 = or i32 %v3_4048e6, %v1_4048e6
  store i32 %v4_4048e6, i32* @eax, align 4
  %v2_4048eb = load i32, i32* @ecx, align 4
  %v3_4048eb = inttoptr i32 %v2_4048eb to i8*
  %v4_4048eb = load i8, i8* %v3_4048eb, align 1
  %v5_4048eb = add i8 %v4_4048eb, %v0_4048e6
  %v6_4048eb = urem i8 %v0_4048e6, 16
  %v7_4048eb = urem i8 %v4_4048eb, 16
  %v8_4048eb = add nuw nsw i8 %v7_4048eb, %v6_4048eb
  %v9_4048eb = icmp ugt i8 %v8_4048eb, 15
  %v10_4048eb = icmp ult i8 %v5_4048eb, %v0_4048e6
  %v11_4048eb = xor i8 %v5_4048eb, %v0_4048e6
  %v12_4048eb = xor i8 %v5_4048eb, %v4_4048eb
  %v13_4048eb = and i8 %v11_4048eb, %v12_4048eb
  %v14_4048eb = icmp slt i8 %v13_4048eb, 0
  store i1 %v9_4048eb, i1* %az.global-to-local, align 1
  store i1 %v10_4048eb, i1* %cf.global-to-local, align 1
  store i1 %v14_4048eb, i1* %of.global-to-local, align 1
  %v15_4048eb = icmp eq i8 %v5_4048eb, 0
  store i1 %v15_4048eb, i1* %zf.global-to-local, align 1
  %v16_4048eb = icmp slt i8 %v5_4048eb, 0
  store i1 %v16_4048eb, i1* %sf.global-to-local, align 1
  %v20_4048eb = zext i8 %v5_4048eb to i32
  %v23_4048eb = or i32 %v3_4048e6, %v20_4048eb
  store i32 %v23_4048eb, i32* @eax, align 4
  %v0_4048ed = load i32, i32* @edx, align 4
  %v1_4048ed = inttoptr i32 %v0_4048ed to i8*
  %v2_4048ed = load i8, i8* %v1_4048ed, align 1
  %v4_4048ed = trunc i32 %v0_4048ed to i8
  %v5_4048ed = add i8 %v2_4048ed, %v4_4048ed
  store i8 %v5_4048ed, i8* %v1_4048ed, align 1
  %v0_4048ef = load i32, i32* @ecx, align 4
  %v1_4048ef = inttoptr i32 %v0_4048ef to i32*
  %v2_4048ef = load i32, i32* %v1_4048ef, align 4
  %v3_4048ef = load i32, i32* @eax, align 4
  %v4_4048ef = and i32 %v3_4048ef, %v2_4048ef
  store i1 false, i1* %az.global-to-local, align 1
  store i1 false, i1* %cf.global-to-local, align 1
  store i1 false, i1* %of.global-to-local, align 1
  %v5_4048ef = icmp eq i32 %v4_4048ef, 0
  store i1 %v5_4048ef, i1* %zf.global-to-local, align 1
  %v6_4048ef = icmp slt i32 %v4_4048ef, 0
  store i1 %v6_4048ef, i1* %sf.global-to-local, align 1
  store i32 %v4_4048ef, i32* %v1_4048ef, align 4
  %v0_4048f1 = load i8, i8* inttoptr (i32 -1325380864 to i8*), align 256
  %v1_4048f1 = load i32, i32* @ecx, align 4
  %v2_4048f1 = trunc i32 %v1_4048f1 to i8
  %v3_4048f1 = add i8 %v0_4048f1, %v2_4048f1
  store i8 %v3_4048f1, i8* inttoptr (i32 -1325380864 to i8*), align 256
  %v0_4048f7 = load i32, i32* @edx, align 4
  %v1_4048f7 = udiv i32 %v0_4048f7, 256
  %v2_4048f7 = trunc i32 %v1_4048f7 to i8
  %v4_4048f7 = trunc i32 %v0_4048f7 to i8
  %v5_4048f7 = add i8 %v2_4048f7, %v4_4048f7
  %v20_4048f7 = zext i8 %v5_4048f7 to i32
  %v22_4048f7 = mul i32 %v20_4048f7, 256
  %v23_4048f7 = and i32 %v0_4048f7, -65281
  %v24_4048f7 = or i32 %v22_4048f7, %v23_4048f7
  store i32 %v24_4048f7, i32* @edx, align 4
  %v0_4048f9 = load i32, i32* @ebx, align 4
  %v1_4048f9 = udiv i32 %v0_4048f9, 256
  %v3_4048f9 = load i32, i32* @eax, align 4
  %v4_4048f9 = udiv i32 %v3_4048f9, 256
  %v6_4048f9 = add nuw nsw i32 %v4_4048f9, %v1_4048f9
  %v21_4048f9 = mul i32 %v6_4048f9, 256
  %v23_4048f9 = and i32 %v21_4048f9, 65280
  %v24_4048f9 = and i32 %v0_4048f9, -65536
  %v25_4048f9 = or i32 %v23_4048f9, %v24_4048f9
  %v1_4048fb = trunc i32 %v0_4048f9 to i8
  %v5_4048fb = add i8 %v5_4048f7, %v1_4048fb
  %v20_4048fb = zext i8 %v5_4048fb to i32
  %v23_4048fb = or i32 %v24_4048f9, %v20_4048fb
  %v1_4048fd = udiv i32 %v25_4048f9, 256
  %v2_4048fd = trunc i32 %v1_4048fd to i8
  %v6_4048fd25 = udiv i32 %v25_4048f9, 128
  %v6_4048fd = trunc i32 %v6_4048fd25 to i8
  %v9_4048fd29 = and i32 %v6_4048f9, 8
  %v10_4048fd = icmp ne i32 %v9_4048fd29, 0
  %v11_4048fd = icmp ult i8 %v6_4048fd, %v2_4048fd
  %v12_4048fd30 = xor i32 %v6_4048fd25, %v1_4048fd
  %v12_4048fd = trunc i32 %v12_4048fd30 to i8
  %v15_4048fd = icmp slt i8 %v12_4048fd, 0
  store i1 %v10_4048fd, i1* %az.global-to-local, align 1
  store i1 %v11_4048fd, i1* %cf.global-to-local, align 1
  store i1 %v15_4048fd, i1* %of.global-to-local, align 1
  %v16_4048fd = icmp eq i8 %v6_4048fd, 0
  store i1 %v16_4048fd, i1* %zf.global-to-local, align 1
  %v17_4048fd = icmp slt i8 %v6_4048fd, 0
  store i1 %v17_4048fd, i1* %sf.global-to-local, align 1
  %v21_4048fd = mul i32 %v6_4048f9, 512
  %v23_4048fd = and i32 %v21_4048fd, 65024
  %v25_4048fd = or i32 %v23_4048fb, %v23_4048fd
  store i32 %v25_4048fd, i32* @ebx, align 4
  %v0_4048ff = load i32, i32* @edi, align 4
  %v1_4048ff = inttoptr i32 %v0_4048ff to i8*
  %v2_4048ff = load i8, i8* %v1_4048ff, align 1
  %v5_4048ff = add i8 %v2_4048ff, %v5_4048fb
  store i8 %v5_4048ff, i8* %v1_4048ff, align 1
  %v0_404901 = load i32, i32* @ecx, align 4
  %v1_404901 = inttoptr i32 %v0_404901 to i32*
  %v2_404901 = load i32, i32* %v1_404901, align 4
  %v3_404901 = load i32, i32* @ebp, align 4
  %v4_404901 = add i32 %v3_404901, %v2_404901
  store i32 %v4_404901, i32* %v1_404901, align 4
  %v0_404903 = load i32, i32* @ecx, align 4
  %v1_404903 = load i32, i32* @eax, align 4
  %v3_404903 = add i32 %v1_404903, %v0_404903
  %v4_404903 = inttoptr i32 %v3_404903 to i32*
  %v5_404903 = load i32, i32* %v4_404903, align 4
  %v6_404903 = load i32, i32* @esi, align 4
  %v7_404903 = add i32 %v6_404903, %v5_404903
  %v8_404903 = urem i32 %v5_404903, 16
  %v9_404903 = urem i32 %v6_404903, 16
  %v10_404903 = add nuw nsw i32 %v9_404903, %v8_404903
  %v11_404903 = icmp ugt i32 %v10_404903, 15
  store i32 %v7_404903, i32* %v4_404903, align 4
  %v0_404906 = load i32, i32* @eax, align 4
  %v6_40490617 = and i32 %v0_404906, 14
  %v7_404906 = icmp ugt i32 %v6_40490617, 9
  %v8_404906 = or i1 %v11_404903, %v7_404906
  %v9_404906 = add i32 %v0_404906, 10
  %v11_404906 = select i1 %v8_404906, i32 %v9_404906, i32 %v0_404906
  %v10_404906 = sext i1 %v8_404906 to i32
  %v15_404906 = urem i32 %v11_404906, 16
  %v18_404906 = and i32 %v0_404906, -65536
  %v19_404906 = or i32 %v15_404906, %v18_404906
  %v12_4049061 = mul i32 %v10_404906, 256
  %v3_4049062 = add i32 %v12_4049061, %v0_404906
  %v22_404906 = and i32 %v3_4049062, 65280
  %v24_404906 = or i32 %v19_404906, %v22_404906
  store i32 %v24_404906, i32* @eax, align 4
  %v0_404907 = load i32, i32* @ecx, align 4
  %v3_404907 = add i32 %v0_404907, 104
  %v4_404907 = add i32 %v3_404907, %v24_404906
  %v5_404907 = inttoptr i32 %v4_404907 to i32*
  %v6_404907 = load i32, i32* %v5_404907, align 4
  %v8_404907 = add i32 %v6_404907, %v0_404907
  store i32 %v8_404907, i32* %v5_404907, align 4
  %v0_40490b = load i32, i32* @ecx, align 4
  %v1_40490b = load i32, i32* @eax, align 4
  %v3_40490b = add i32 %v0_40490b, -106
  %v4_40490b = add i32 %v3_40490b, %v1_40490b
  %v5_40490b = inttoptr i32 %v4_40490b to i32*
  %v6_40490b = load i32, i32* %v5_40490b, align 4
  %v7_40490b = load i32, i32* @edi, align 4
  %v8_40490b = add i32 %v7_40490b, %v6_40490b
  store i32 %v8_40490b, i32* %v5_40490b, align 4
  %v0_40490f = load i32, i32* @ecx, align 4
  %v1_40490f = add i32 %v0_40490f, 369232129
  %v2_40490f = inttoptr i32 %v1_40490f to i32*
  %v3_40490f = load i32, i32* %v2_40490f, align 4
  %v4_40490f = load i32, i32* @ebp, align 4
  %v5_40490f = add i32 %v4_40490f, %v3_40490f
  %v6_40490f = urem i32 %v3_40490f, 16
  %v7_40490f = urem i32 %v4_40490f, 16
  %v8_40490f = add nuw nsw i32 %v7_40490f, %v6_40490f
  %v9_40490f = icmp ugt i32 %v8_40490f, 15
  %v10_40490f = icmp ult i32 %v5_40490f, %v3_40490f
  %v11_40490f = xor i32 %v5_40490f, %v3_40490f
  %v12_40490f = xor i32 %v5_40490f, %v4_40490f
  %v13_40490f = and i32 %v11_40490f, %v12_40490f
  %v14_40490f = icmp slt i32 %v13_40490f, 0
  store i1 %v9_40490f, i1* %az.global-to-local, align 1
  store i1 %v10_40490f, i1* %cf.global-to-local, align 1
  store i1 %v14_40490f, i1* %of.global-to-local, align 1
  %v15_40490f = icmp eq i32 %v5_40490f, 0
  store i1 %v15_40490f, i1* %zf.global-to-local, align 1
  %v16_40490f = icmp slt i32 %v5_40490f, 0
  store i1 %v16_40490f, i1* %sf.global-to-local, align 1
  store i32 %v5_40490f, i32* %v2_40490f, align 4
  %v0_404915 = load i32, i32* @eax, align 4
  %v1_404915 = trunc i32 %v0_404915 to i8
  %v2_404915 = load i8, i8* inttoptr (i32 256 to i8*), align 256
  %v3_404915 = add i8 %v2_404915, %v1_404915
  %v4_404915 = urem i8 %v1_404915, 16
  %v5_404915 = urem i8 %v2_404915, 16
  %v6_404915 = add nuw nsw i8 %v4_404915, %v5_404915
  %v7_404915 = icmp ugt i8 %v6_404915, 15
  %v8_404915 = icmp ult i8 %v3_404915, %v1_404915
  %v9_404915 = xor i8 %v3_404915, %v1_404915
  %v10_404915 = xor i8 %v3_404915, %v2_404915
  %v11_404915 = and i8 %v9_404915, %v10_404915
  %v12_404915 = icmp slt i8 %v11_404915, 0
  store i1 %v7_404915, i1* %az.global-to-local, align 1
  store i1 %v8_404915, i1* %cf.global-to-local, align 1
  store i1 %v12_404915, i1* %of.global-to-local, align 1
  %v13_404915 = icmp eq i8 %v3_404915, 0
  store i1 %v13_404915, i1* %zf.global-to-local, align 1
  %v14_404915 = icmp slt i8 %v3_404915, 0
  store i1 %v14_404915, i1* %sf.global-to-local, align 1
  %v18_404915 = zext i8 %v3_404915 to i32
  %v20_404915 = and i32 %v0_404915, -256
  %v21_404915 = or i32 %v20_404915, %v18_404915
  store i32 %v21_404915, i32* @eax, align 4
  %v0_40491b = load i32, i32* @edx, align 4
  %v1_40491b = add i32 %v0_40491b, 67074
  %v2_40491b = inttoptr i32 %v1_40491b to i8*
  %v3_40491b = load i8, i8* %v2_40491b, align 1
  %v4_40491b = load i32, i32* @ebx, align 4
  %v5_40491b = udiv i32 %v4_40491b, 256
  %v6_40491b = trunc i32 %v5_40491b to i8
  %v7_40491b = add i8 %v3_40491b, %v6_40491b
  %v8_40491b = urem i8 %v3_40491b, 16
  %v9_40491b = urem i8 %v6_40491b, 16
  %v10_40491b = add nuw nsw i8 %v9_40491b, %v8_40491b
  %v11_40491b = icmp ugt i8 %v10_40491b, 15
  %v12_40491b = icmp ult i8 %v7_40491b, %v3_40491b
  %v13_40491b = xor i8 %v7_40491b, %v3_40491b
  %v14_40491b = xor i8 %v7_40491b, %v6_40491b
  %v15_40491b = and i8 %v13_40491b, %v14_40491b
  %v16_40491b = icmp slt i8 %v15_40491b, 0
  store i1 %v11_40491b, i1* %az.global-to-local, align 1
  store i1 %v12_40491b, i1* %cf.global-to-local, align 1
  store i1 %v16_40491b, i1* %of.global-to-local, align 1
  %v17_40491b = icmp eq i8 %v7_40491b, 0
  store i1 %v17_40491b, i1* %zf.global-to-local, align 1
  %v18_40491b = icmp slt i8 %v7_40491b, 0
  store i1 %v18_40491b, i1* %sf.global-to-local, align 1
  store i8 %v7_40491b, i8* %v2_40491b, align 1
  %v0_404921 = load i32, i32* @ecx, align 4
  %v1_404921 = load i32, i32* @eax, align 4
  %v3_404921 = add i32 %v0_404921, 11
  %v4_404921 = add i32 %v3_404921, %v1_404921
  %v5_404921 = inttoptr i32 %v4_404921 to i8*
  %v6_404921 = load i8, i8* %v5_404921, align 1
  %v7_404921 = load i32, i32* @edx, align 4
  %v8_404921 = trunc i32 %v7_404921 to i8
  %v9_404921 = add i8 %v6_404921, %v8_404921
  store i8 %v9_404921, i8* %v5_404921, align 1
  %v0_404925 = load i32, i32* @eax, align 4
  %v1_404925 = inttoptr i32 %v0_404925 to i32*
  %v2_404925 = load i32, i32* %v1_404925, align 4
  %v4_404925 = add i32 %v2_404925, %v0_404925
  %v5_404925 = urem i32 %v2_404925, 16
  %v6_404925 = urem i32 %v0_404925, 16
  %v7_404925 = add nuw nsw i32 %v5_404925, %v6_404925
  %v8_404925 = icmp ugt i32 %v7_404925, 15
  %v9_404925 = icmp ult i32 %v4_404925, %v2_404925
  %v10_404925 = xor i32 %v4_404925, %v2_404925
  %v11_404925 = xor i32 %v4_404925, %v0_404925
  %v12_404925 = and i32 %v10_404925, %v11_404925
  %v13_404925 = icmp slt i32 %v12_404925, 0
  store i1 %v8_404925, i1* %az.global-to-local, align 1
  store i1 %v9_404925, i1* %cf.global-to-local, align 1
  store i1 %v13_404925, i1* %of.global-to-local, align 1
  %v14_404925 = icmp eq i32 %v4_404925, 0
  store i1 %v14_404925, i1* %zf.global-to-local, align 1
  %v15_404925 = icmp slt i32 %v4_404925, 0
  store i1 %v15_404925, i1* %sf.global-to-local, align 1
  store i32 %v4_404925, i32* %v1_404925, align 4
  %v0_404927 = load i32, i32* @esi, align 4
  %v1_404927 = add i32 %v0_404927, 1
  %v2_404927 = inttoptr i32 %v1_404927 to i8*
  %v3_404927 = load i8, i8* %v2_404927, align 1
  %v4_404927 = load i32, i32* @ebx, align 4
  %v5_404927 = trunc i32 %v4_404927 to i8
  %v6_404927 = add i8 %v3_404927, %v5_404927
  %v7_404927 = urem i8 %v3_404927, 16
  %v8_404927 = urem i8 %v5_404927, 16
  %v9_404927 = add nuw nsw i8 %v8_404927, %v7_404927
  %v10_404927 = icmp ugt i8 %v9_404927, 15
  %v11_404927 = icmp ult i8 %v6_404927, %v3_404927
  %v12_404927 = xor i8 %v6_404927, %v3_404927
  %v13_404927 = xor i8 %v6_404927, %v5_404927
  %v14_404927 = and i8 %v12_404927, %v13_404927
  %v15_404927 = icmp slt i8 %v14_404927, 0
  store i1 %v10_404927, i1* %az.global-to-local, align 1
  store i1 %v11_404927, i1* %cf.global-to-local, align 1
  store i1 %v15_404927, i1* %of.global-to-local, align 1
  %v16_404927 = icmp eq i8 %v6_404927, 0
  store i1 %v16_404927, i1* %zf.global-to-local, align 1
  %v17_404927 = icmp slt i8 %v6_404927, 0
  store i1 %v17_404927, i1* %sf.global-to-local, align 1
  store i8 %v6_404927, i8* %v2_404927, align 1
  %v0_40492a = load i32, i32* @ecx, align 4
  %v1_40492a = inttoptr i32 %v0_40492a to i8*
  %v2_40492a = load i8, i8* %v1_40492a, align 1
  %v3_40492a = load i32, i32* @eax, align 4
  %v4_40492a = trunc i32 %v3_40492a to i8
  %v5_40492a = load i1, i1* %cf.global-to-local, align 1
  %v6_40492a = zext i1 %v5_40492a to i8
  %v7_40492a = add i8 %v2_40492a, %v4_40492a
  %v8_40492a = add i8 %v7_40492a, %v6_40492a
  %v9_40492a = urem i8 %v2_40492a, 16
  %v10_40492a = urem i8 %v4_40492a, 16
  %v11_40492a = add nuw nsw i8 %v10_40492a, %v9_40492a
  %v12_40492a = add nuw nsw i8 %v11_40492a, %v6_40492a
  %v13_40492a = icmp ugt i8 %v12_40492a, 15
  %v14_40492a = add i8 %v8_40492a, %v6_40492a
  %v15_40492a = xor i8 %v14_40492a, %v2_40492a
  %v16_40492a = xor i8 %v14_40492a, %v4_40492a
  %v17_40492a = and i8 %v15_40492a, %v16_40492a
  %v18_40492a = icmp slt i8 %v17_40492a, 0
  store i1 %v13_40492a, i1* %az.global-to-local, align 1
  store i1 %v18_40492a, i1* %of.global-to-local, align 1
  %v19_40492a = icmp eq i8 %v8_40492a, 0
  store i1 %v19_40492a, i1* %zf.global-to-local, align 1
  %v20_40492a = icmp slt i8 %v8_40492a, 0
  store i1 %v20_40492a, i1* %sf.global-to-local, align 1
  %v26_40492a = icmp ule i8 %v8_40492a, %v2_40492a
  %v27_40492a = icmp ult i8 %v7_40492a, %v2_40492a
  %v28_40492a = select i1 %v5_40492a, i1 %v26_40492a, i1 %v27_40492a
  store i1 %v28_40492a, i1* %cf.global-to-local, align 1
  store i8 %v8_40492a, i8* %v1_40492a, align 1
  %v0_40492c = load i32, i32* @eax, align 4
  %v1_40492c = inttoptr i32 %v0_40492c to i8*
  %v2_40492c = load i8, i8* %v1_40492c, align 1
  %v4_40492c = trunc i32 %v0_40492c to i8
  %v5_40492c = add i8 %v2_40492c, %v4_40492c
  store i8 %v5_40492c, i8* %v1_40492c, align 1
  store i32 %v22_404896, i32* %stack_var_68, align 4
  %v0_40492f = load i32, i32* inttoptr (i32 301990401 to i32*), align 4
  %v1_40492f = load i32, i32* @edx, align 4
  %v2_40492f = add i32 %v1_40492f, %v0_40492f
  %v3_40492f = urem i32 %v0_40492f, 16
  %v4_40492f = urem i32 %v1_40492f, 16
  %v5_40492f = add nuw nsw i32 %v4_40492f, %v3_40492f
  %v6_40492f = icmp ugt i32 %v5_40492f, 15
  %v7_40492f = icmp ult i32 %v2_40492f, %v0_40492f
  %v8_40492f = xor i32 %v2_40492f, %v0_40492f
  %v9_40492f = xor i32 %v2_40492f, %v1_40492f
  %v10_40492f = and i32 %v8_40492f, %v9_40492f
  %v11_40492f = icmp slt i32 %v10_40492f, 0
  store i1 %v6_40492f, i1* %az.global-to-local, align 1
  store i1 %v7_40492f, i1* %cf.global-to-local, align 1
  store i1 %v11_40492f, i1* %of.global-to-local, align 1
  %v12_40492f = icmp eq i32 %v2_40492f, 0
  store i1 %v12_40492f, i1* %zf.global-to-local, align 1
  %v13_40492f = icmp slt i32 %v2_40492f, 0
  store i1 %v13_40492f, i1* %sf.global-to-local, align 1
  store i32 %v2_40492f, i32* inttoptr (i32 301990401 to i32*), align 4
  %v0_404935 = load i32, i32* @ebx, align 4
  %v1_404935 = inttoptr i32 %v0_404935 to i8*
  %v2_404935 = load i8, i8* %v1_404935, align 1
  %v3_404935 = load i32, i32* @eax, align 4
  %v4_404935 = trunc i32 %v3_404935 to i8
  %v5_404935 = add i8 %v2_404935, %v4_404935
  %v6_404935 = urem i8 %v2_404935, 16
  %v7_404935 = urem i8 %v4_404935, 16
  %v8_404935 = add nuw nsw i8 %v7_404935, %v6_404935
  %v9_404935 = icmp ugt i8 %v8_404935, 15
  %v10_404935 = icmp ult i8 %v5_404935, %v2_404935
  %v11_404935 = xor i8 %v5_404935, %v2_404935
  %v12_404935 = xor i8 %v5_404935, %v4_404935
  %v13_404935 = and i8 %v11_404935, %v12_404935
  %v14_404935 = icmp slt i8 %v13_404935, 0
  store i1 %v9_404935, i1* %az.global-to-local, align 1
  store i1 %v10_404935, i1* %cf.global-to-local, align 1
  store i1 %v14_404935, i1* %of.global-to-local, align 1
  %v15_404935 = icmp eq i8 %v5_404935, 0
  store i1 %v15_404935, i1* %zf.global-to-local, align 1
  %v16_404935 = icmp slt i8 %v5_404935, 0
  store i1 %v16_404935, i1* %sf.global-to-local, align 1
  store i8 %v5_404935, i8* %v1_404935, align 1
  %v0_404937 = load i32, i32* @edx, align 4
  %v1_404937 = inttoptr i32 %v0_404937 to i8*
  %v2_404937 = load i8, i8* %v1_404937, align 1
  %v3_404937 = load i32, i32* @eax, align 4
  %v4_404937 = trunc i32 %v3_404937 to i8
  %v5_404937 = add i8 %v2_404937, %v4_404937
  %v6_404937 = urem i8 %v2_404937, 16
  %v7_404937 = urem i8 %v4_404937, 16
  %v8_404937 = add nuw nsw i8 %v7_404937, %v6_404937
  %v9_404937 = icmp ugt i8 %v8_404937, 15
  %v10_404937 = icmp ult i8 %v5_404937, %v2_404937
  %v11_404937 = xor i8 %v5_404937, %v2_404937
  %v12_404937 = xor i8 %v5_404937, %v4_404937
  %v13_404937 = and i8 %v11_404937, %v12_404937
  %v14_404937 = icmp slt i8 %v13_404937, 0
  store i1 %v9_404937, i1* %az.global-to-local, align 1
  store i1 %v10_404937, i1* %cf.global-to-local, align 1
  store i1 %v14_404937, i1* %of.global-to-local, align 1
  %v15_404937 = icmp eq i8 %v5_404937, 0
  store i1 %v15_404937, i1* %zf.global-to-local, align 1
  %v16_404937 = icmp slt i8 %v5_404937, 0
  store i1 %v16_404937, i1* %sf.global-to-local, align 1
  store i8 %v5_404937, i8* %v1_404937, align 1
  %v0_404939 = load i32, i32* @ebx, align 4
  %v1_404939 = inttoptr i32 %v0_404939 to i8*
  %v2_404939 = load i8, i8* %v1_404939, align 1
  %v3_404939 = load i32, i32* @edx, align 4
  %v4_404939 = trunc i32 %v3_404939 to i8
  %v5_404939 = add i8 %v2_404939, %v4_404939
  %v6_404939 = urem i8 %v2_404939, 16
  %v7_404939 = urem i8 %v4_404939, 16
  %v8_404939 = add nuw nsw i8 %v7_404939, %v6_404939
  %v9_404939 = icmp ugt i8 %v8_404939, 15
  %v10_404939 = icmp ult i8 %v5_404939, %v2_404939
  %v11_404939 = xor i8 %v5_404939, %v2_404939
  %v12_404939 = xor i8 %v5_404939, %v4_404939
  %v13_404939 = and i8 %v11_404939, %v12_404939
  %v14_404939 = icmp slt i8 %v13_404939, 0
  store i1 %v9_404939, i1* %az.global-to-local, align 1
  store i1 %v10_404939, i1* %cf.global-to-local, align 1
  store i1 %v14_404939, i1* %of.global-to-local, align 1
  %v15_404939 = icmp eq i8 %v5_404939, 0
  store i1 %v15_404939, i1* %zf.global-to-local, align 1
  %v16_404939 = icmp slt i8 %v5_404939, 0
  store i1 %v16_404939, i1* %sf.global-to-local, align 1
  store i8 %v5_404939, i8* %v1_404939, align 1
  %v0_40493b = load i8, i8* inttoptr (i32 335544832 to i8*), align 512
  %v1_40493b = load i32, i32* @eax, align 4
  %v2_40493b = trunc i32 %v1_40493b to i8
  %v3_40493b = add i8 %v0_40493b, %v2_40493b
  %v4_40493b = urem i8 %v0_40493b, 16
  %v5_40493b = urem i8 %v2_40493b, 16
  %v6_40493b = add nuw nsw i8 %v5_40493b, %v4_40493b
  %v7_40493b = icmp ugt i8 %v6_40493b, 15
  %v8_40493b = icmp ult i8 %v3_40493b, %v0_40493b
  %v9_40493b = xor i8 %v3_40493b, %v0_40493b
  %v10_40493b = xor i8 %v3_40493b, %v2_40493b
  %v11_40493b = and i8 %v9_40493b, %v10_40493b
  %v12_40493b = icmp slt i8 %v11_40493b, 0
  store i1 %v7_40493b, i1* %az.global-to-local, align 1
  store i1 %v8_40493b, i1* %cf.global-to-local, align 1
  store i1 %v12_40493b, i1* %of.global-to-local, align 1
  %v13_40493b = icmp eq i8 %v3_40493b, 0
  store i1 %v13_40493b, i1* %zf.global-to-local, align 1
  %v14_40493b = icmp slt i8 %v3_40493b, 0
  store i1 %v14_40493b, i1* %sf.global-to-local, align 1
  store i8 %v3_40493b, i8* inttoptr (i32 335544832 to i8*), align 512
  %v0_404941 = load i32, i32* @edi, align 4
  %v1_404941 = inttoptr i32 %v0_404941 to i8*
  %v2_404941 = load i8, i8* %v1_404941, align 1
  %v3_404941 = load i32, i32* @eax, align 4
  %v4_404941 = trunc i32 %v3_404941 to i8
  %v5_404941 = add i8 %v2_404941, %v4_404941
  %v6_404941 = urem i8 %v2_404941, 16
  %v7_404941 = urem i8 %v4_404941, 16
  %v8_404941 = add nuw nsw i8 %v7_404941, %v6_404941
  %v9_404941 = icmp ugt i8 %v8_404941, 15
  %v10_404941 = icmp ult i8 %v5_404941, %v2_404941
  %v11_404941 = xor i8 %v5_404941, %v2_404941
  %v12_404941 = xor i8 %v5_404941, %v4_404941
  %v13_404941 = and i8 %v11_404941, %v12_404941
  %v14_404941 = icmp slt i8 %v13_404941, 0
  store i1 %v9_404941, i1* %az.global-to-local, align 1
  store i1 %v10_404941, i1* %cf.global-to-local, align 1
  store i1 %v14_404941, i1* %of.global-to-local, align 1
  %v15_404941 = icmp eq i8 %v5_404941, 0
  store i1 %v15_404941, i1* %zf.global-to-local, align 1
  %v16_404941 = icmp slt i8 %v5_404941, 0
  store i1 %v16_404941, i1* %sf.global-to-local, align 1
  store i8 %v5_404941, i8* %v1_404941, align 1
  %v0_404943 = load i32, i32* @edx, align 4
  %v1_404943 = inttoptr i32 %v0_404943 to i8*
  %v2_404943 = load i8, i8* %v1_404943, align 1
  %v3_404943 = load i32, i32* @eax, align 4
  %v4_404943 = trunc i32 %v3_404943 to i8
  %v5_404943 = add i8 %v2_404943, %v4_404943
  %v6_404943 = urem i8 %v2_404943, 16
  %v7_404943 = urem i8 %v4_404943, 16
  %v8_404943 = add nuw nsw i8 %v7_404943, %v6_404943
  %v9_404943 = icmp ugt i8 %v8_404943, 15
  %v10_404943 = icmp ult i8 %v5_404943, %v2_404943
  %v11_404943 = xor i8 %v5_404943, %v2_404943
  %v12_404943 = xor i8 %v5_404943, %v4_404943
  %v13_404943 = and i8 %v11_404943, %v12_404943
  %v14_404943 = icmp slt i8 %v13_404943, 0
  store i1 %v9_404943, i1* %az.global-to-local, align 1
  store i1 %v10_404943, i1* %cf.global-to-local, align 1
  store i1 %v14_404943, i1* %of.global-to-local, align 1
  %v15_404943 = icmp eq i8 %v5_404943, 0
  store i1 %v15_404943, i1* %zf.global-to-local, align 1
  %v16_404943 = icmp slt i8 %v5_404943, 0
  store i1 %v16_404943, i1* %sf.global-to-local, align 1
  store i8 %v5_404943, i8* %v1_404943, align 1
  %v0_404945 = load i8, i8* inttoptr (i32 -452982528 to i8*), align 256
  %v1_404945 = load i32, i32* @edx, align 4
  %v2_404945 = trunc i32 %v1_404945 to i8
  %v3_404945 = add i8 %v0_404945, %v2_404945
  store i8 %v3_404945, i8* inttoptr (i32 -452982528 to i8*), align 256
  %v0_40494b = load i32, i32* @esi, align 4
  %v1_40494b = load i32, i32* @ebp, align 4
  %v2_40494b = add i32 %v1_40494b, -1023362048
  %v3_40494b = inttoptr i32 %v2_40494b to i32*
  %v4_40494b = load i32, i32* %v3_40494b, align 4
  %v5_40494b = add i32 %v4_40494b, %v0_40494b
  store i32 %v5_40494b, i32* @esi, align 4
  %v0_404951 = load i32, i32* @edx, align 4
  %v1_404951 = trunc i32 %v0_404951 to i8
  %v2_404951 = load i32, i32* @ecx, align 4
  %v3_404951 = trunc i32 %v2_404951 to i8
  %v4_404951 = add i8 %v3_404951, %v1_404951
  %v5_404951 = urem i8 %v1_404951, 16
  %v6_404951 = urem i8 %v3_404951, 16
  %v7_404951 = add nuw nsw i8 %v6_404951, %v5_404951
  %v8_404951 = icmp ugt i8 %v7_404951, 15
  %v9_404951 = icmp ult i8 %v4_404951, %v1_404951
  %v10_404951 = xor i8 %v4_404951, %v1_404951
  %v11_404951 = xor i8 %v4_404951, %v3_404951
  %v12_404951 = and i8 %v10_404951, %v11_404951
  %v13_404951 = icmp slt i8 %v12_404951, 0
  store i1 %v8_404951, i1* %az.global-to-local, align 1
  store i1 %v9_404951, i1* %cf.global-to-local, align 1
  store i1 %v13_404951, i1* %of.global-to-local, align 1
  %v14_404951 = icmp eq i8 %v4_404951, 0
  store i1 %v14_404951, i1* %zf.global-to-local, align 1
  %v15_404951 = icmp slt i8 %v4_404951, 0
  store i1 %v15_404951, i1* %sf.global-to-local, align 1
  %v19_404951 = zext i8 %v4_404951 to i32
  %v21_404951 = and i32 %v0_404951, -256
  %v22_404951 = or i32 %v21_404951, %v19_404951
  store i32 %v22_404951, i32* @edx, align 4
  %v1_404953 = add i32 %v22_404951, 1
  %v2_404953 = inttoptr i32 %v1_404953 to i8*
  %v3_404953 = load i8, i8* %v2_404953, align 1
  %v6_404953 = add i8 %v3_404953, %v4_404951
  %v7_404953 = urem i8 %v3_404953, 16
  %v8_404953 = urem i8 %v4_404951, 16
  %v9_404953 = add nuw nsw i8 %v7_404953, %v8_404953
  %v10_404953 = icmp ugt i8 %v9_404953, 15
  %v11_404953 = icmp ult i8 %v6_404953, %v3_404953
  %v12_404953 = xor i8 %v6_404953, %v3_404953
  %v13_404953 = xor i8 %v6_404953, %v4_404951
  %v14_404953 = and i8 %v12_404953, %v13_404953
  %v15_404953 = icmp slt i8 %v14_404953, 0
  store i1 %v10_404953, i1* %az.global-to-local, align 1
  store i1 %v11_404953, i1* %cf.global-to-local, align 1
  store i1 %v15_404953, i1* %of.global-to-local, align 1
  %v16_404953 = icmp eq i8 %v6_404953, 0
  store i1 %v16_404953, i1* %zf.global-to-local, align 1
  %v17_404953 = icmp slt i8 %v6_404953, 0
  store i1 %v17_404953, i1* %sf.global-to-local, align 1
  store i8 %v6_404953, i8* %v2_404953, align 1
  %v0_404956 = load i32, i32* @ecx, align 4
  %v1_404956 = inttoptr i32 %v0_404956 to i8*
  %v2_404956 = load i8, i8* %v1_404956, align 1
  %v3_404956 = add i8 %v2_404956, -119
  store i8 %v3_404956, i8* %v1_404956, align 1
  %v0_404959 = load i32, i32* @ebx, align 4
  %v1_404959 = add i32 %v0_404959, 1
  %v2_404959 = inttoptr i32 %v1_404959 to i32*
  %v3_404959 = load i32, i32* %v2_404959, align 4
  %v4_404959 = load i32, i32* @eax, align 4
  %v5_404959 = add i32 %v4_404959, %v3_404959
  store i32 %v5_404959, i32* %v2_404959, align 4
  %v2_40495c = load i32, i32* %stack_var_68, align 4
  store i32 %v2_40495c, i32* @ecx, align 4
  %v0_40495d = load i32, i32* @ebx, align 4
  %v1_40495d137 = mul i32 %v0_40495d, 257
  %v22_40495d = and i32 %v1_40495d137, 65280
  %v23_40495d = and i32 %v0_40495d, -65281
  %v24_40495d = or i32 %v22_40495d, %v23_40495d
  store i32 %v24_40495d, i32* @ebx, align 4
  %v0_40495f = load i32, i32* @eax, align 4
  %v2_40495f = inttoptr i32 %v2_40495c to i32*
  %v3_40495f = load i32, i32* %v2_40495f, align 4
  %v4_40495f = add i32 %v3_40495f, %v0_40495f
  %v5_40495f = urem i32 %v0_40495f, 16
  %v6_40495f = urem i32 %v3_40495f, 16
  %v7_40495f = add nuw nsw i32 %v6_40495f, %v5_40495f
  %v8_40495f = icmp ugt i32 %v7_40495f, 15
  %v9_40495f = icmp ult i32 %v4_40495f, %v0_40495f
  %v10_40495f = xor i32 %v4_40495f, %v0_40495f
  %v11_40495f = xor i32 %v4_40495f, %v3_40495f
  %v12_40495f = and i32 %v10_40495f, %v11_40495f
  %v13_40495f = icmp slt i32 %v12_40495f, 0
  store i1 %v8_40495f, i1* %az.global-to-local, align 1
  store i1 %v9_40495f, i1* %cf.global-to-local, align 1
  store i1 %v13_40495f, i1* %of.global-to-local, align 1
  %v14_40495f = icmp eq i32 %v4_40495f, 0
  store i1 %v14_40495f, i1* %zf.global-to-local, align 1
  %v15_40495f = icmp slt i32 %v4_40495f, 0
  store i1 %v15_40495f, i1* %sf.global-to-local, align 1
  %v16_40495f = trunc i32 %v4_40495f to i8
  store i32 %v4_40495f, i32* @eax, align 4
  %v1_404961 = inttoptr i32 %v4_40495f to i8*
  %v2_404961 = load i8, i8* %v1_404961, align 1
  %v5_404961 = add i8 %v2_404961, %v16_40495f
  %v6_404961 = urem i8 %v2_404961, 16
  %v7_404961 = urem i8 %v16_40495f, 16
  %v8_404961 = add nuw nsw i8 %v6_404961, %v7_404961
  %v9_404961 = icmp ugt i8 %v8_404961, 15
  %v10_404961 = icmp ult i8 %v5_404961, %v2_404961
  %v11_404961 = xor i8 %v5_404961, %v2_404961
  %v12_404961 = xor i8 %v5_404961, %v16_40495f
  %v13_404961 = and i8 %v11_404961, %v12_404961
  %v14_404961 = icmp slt i8 %v13_404961, 0
  store i1 %v9_404961, i1* %az.global-to-local, align 1
  store i1 %v10_404961, i1* %cf.global-to-local, align 1
  store i1 %v14_404961, i1* %of.global-to-local, align 1
  %v15_404961 = icmp eq i8 %v5_404961, 0
  store i1 %v15_404961, i1* %zf.global-to-local, align 1
  %v16_404961 = icmp slt i8 %v5_404961, 0
  store i1 %v16_404961, i1* %sf.global-to-local, align 1
  store i8 %v5_404961, i8* %v1_404961, align 1
  %v10_404963 = load i32, i32* @eax, align 4
  store i32 %v10_404963, i32* %stack_var_68, align 4
  %v12_404963 = load i32, i32* @ecx, align 4
  %v14_404963 = load i32, i32* @edx, align 4
  %v16_404963 = load i32, i32* @ebx, align 4
  %v20_404963 = load i32, i32* @ebp, align 4
  %v22_404963 = load i32, i32* @esi, align 4
  %v24_404963 = load i32, i32* @edi, align 4
  store i32 %v24_404963, i32* %stack_var_40, align 4
  %v1_404964 = inttoptr i32 %v10_404963 to i8*
  %v2_404964 = load i8, i8* %v1_404964, align 1
  %v4_404964 = trunc i32 %v10_404963 to i8
  %v5_404964 = add i8 %v2_404964, %v4_404964
  store i8 %v5_404964, i8* %v1_404964, align 1
  %v0_404966 = load i32, i32* @eax, align 4
  %v1_404966 = inttoptr i32 %v0_404966 to i32*
  %v2_404966 = load i32, i32* %v1_404966, align 4
  %v4_404966 = or i32 %v2_404966, %v0_404966
  store i32 %v4_404966, i32* %v1_404966, align 4
  %v0_404968 = load i32, i32* @eax, align 4
  %v1_404968 = trunc i32 %v0_404968 to i8
  %v2_404968 = xor i8 %v1_404968, -128
  %v5_404968 = icmp ult i8 %v2_404968, %v1_404968
  %v9_404968 = icmp slt i8 %v1_404968, 0
  store i1 false, i1* %az.global-to-local, align 1
  store i1 %v5_404968, i1* %cf.global-to-local, align 1
  store i1 %v9_404968, i1* %of.global-to-local, align 1
  %v10_404968 = icmp eq i8 %v2_404968, 0
  store i1 %v10_404968, i1* %zf.global-to-local, align 1
  %v11_404968 = icmp slt i8 %v2_404968, 0
  store i1 %v11_404968, i1* %sf.global-to-local, align 1
  %v15_404968 = zext i8 %v2_404968 to i32
  %v17_404968 = and i32 %v0_404968, -256
  %v18_404968 = or i32 %v17_404968, %v15_404968
  store i32 %v18_404968, i32* @eax, align 4
  %v1_40496a = inttoptr i32 %v18_404968 to i8*
  %v2_40496a = load i8, i8* %v1_40496a, align 1
  %v5_40496a = add i8 %v2_40496a, %v2_404968
  %v6_40496a = urem i8 %v2_40496a, 16
  %v7_40496a = urem i8 %v1_404968, 16
  %v8_40496a = add nuw nsw i8 %v6_40496a, %v7_40496a
  %v9_40496a = icmp ugt i8 %v8_40496a, 15
  %v10_40496a = icmp ult i8 %v5_40496a, %v2_40496a
  %v11_40496a = xor i8 %v5_40496a, %v2_40496a
  %v12_40496a = xor i8 %v5_40496a, %v2_404968
  %v13_40496a = and i8 %v11_40496a, %v12_40496a
  %v14_40496a = icmp slt i8 %v13_40496a, 0
  store i1 %v9_40496a, i1* %az.global-to-local, align 1
  store i1 %v10_40496a, i1* %cf.global-to-local, align 1
  store i1 %v14_40496a, i1* %of.global-to-local, align 1
  %v15_40496a = icmp eq i8 %v5_40496a, 0
  store i1 %v15_40496a, i1* %zf.global-to-local, align 1
  %v16_40496a = icmp slt i8 %v5_40496a, 0
  store i1 %v16_40496a, i1* %sf.global-to-local, align 1
  store i8 %v5_40496a, i8* %v1_40496a, align 1
  %v0_40496c = load i32, i32* @eax, align 4
  %v1_40496c = inttoptr i32 %v0_40496c to i8*
  %v2_40496c = load i8, i8* %v1_40496c, align 1
  %v4_40496c = trunc i32 %v0_40496c to i8
  %v5_40496c = add i8 %v2_40496c, %v4_40496c
  %v6_40496c = urem i8 %v2_40496c, 16
  %v7_40496c = urem i8 %v4_40496c, 16
  %v8_40496c = add nuw nsw i8 %v6_40496c, %v7_40496c
  %v9_40496c = icmp ugt i8 %v8_40496c, 15
  %v10_40496c = icmp ult i8 %v5_40496c, %v2_40496c
  %v11_40496c = xor i8 %v5_40496c, %v2_40496c
  %v12_40496c = xor i8 %v5_40496c, %v4_40496c
  %v13_40496c = and i8 %v11_40496c, %v12_40496c
  %v14_40496c = icmp slt i8 %v13_40496c, 0
  store i1 %v9_40496c, i1* %az.global-to-local, align 1
  store i1 %v10_40496c, i1* %cf.global-to-local, align 1
  store i1 %v14_40496c, i1* %of.global-to-local, align 1
  %v15_40496c = icmp eq i8 %v5_40496c, 0
  store i1 %v15_40496c, i1* %zf.global-to-local, align 1
  %v16_40496c = icmp slt i8 %v5_40496c, 0
  store i1 %v16_40496c, i1* %sf.global-to-local, align 1
  store i8 %v5_40496c, i8* %v1_40496c, align 1
  %v0_40496e = load i32, i32* @eax, align 4
  %v1_40496e = inttoptr i32 %v0_40496e to i8*
  %v2_40496e = load i8, i8* %v1_40496e, align 1
  %v4_40496e = trunc i32 %v0_40496e to i8
  %v5_40496e = add i8 %v2_40496e, %v4_40496e
  %v6_40496e = urem i8 %v2_40496e, 16
  %v7_40496e = urem i8 %v4_40496e, 16
  %v8_40496e = add nuw nsw i8 %v6_40496e, %v7_40496e
  %v9_40496e = icmp ugt i8 %v8_40496e, 15
  %v10_40496e = icmp ult i8 %v5_40496e, %v2_40496e
  %v11_40496e = xor i8 %v5_40496e, %v2_40496e
  %v12_40496e = xor i8 %v5_40496e, %v4_40496e
  %v13_40496e = and i8 %v11_40496e, %v12_40496e
  %v14_40496e = icmp slt i8 %v13_40496e, 0
  store i1 %v9_40496e, i1* %az.global-to-local, align 1
  store i1 %v10_40496e, i1* %cf.global-to-local, align 1
  store i1 %v14_40496e, i1* %of.global-to-local, align 1
  %v15_40496e = icmp eq i8 %v5_40496e, 0
  store i1 %v15_40496e, i1* %zf.global-to-local, align 1
  %v16_40496e = icmp slt i8 %v5_40496e, 0
  store i1 %v16_40496e, i1* %sf.global-to-local, align 1
  store i8 %v5_40496e, i8* %v1_40496e, align 1
  %v0_404970 = load i32, i32* @eax, align 4
  %v1_404970 = inttoptr i32 %v0_404970 to i8*
  %v2_404970 = load i8, i8* %v1_404970, align 1
  %v4_404970 = trunc i32 %v0_404970 to i8
  %v5_404970 = add i8 %v2_404970, %v4_404970
  %v6_404970 = urem i8 %v2_404970, 16
  %v7_404970 = urem i8 %v4_404970, 16
  %v8_404970 = add nuw nsw i8 %v6_404970, %v7_404970
  %v9_404970 = icmp ugt i8 %v8_404970, 15
  %v10_404970 = icmp ult i8 %v5_404970, %v2_404970
  %v11_404970 = xor i8 %v5_404970, %v2_404970
  %v12_404970 = xor i8 %v5_404970, %v4_404970
  %v13_404970 = and i8 %v11_404970, %v12_404970
  %v14_404970 = icmp slt i8 %v13_404970, 0
  store i1 %v9_404970, i1* %az.global-to-local, align 1
  store i1 %v10_404970, i1* %cf.global-to-local, align 1
  store i1 %v14_404970, i1* %of.global-to-local, align 1
  %v15_404970 = icmp eq i8 %v5_404970, 0
  store i1 %v15_404970, i1* %zf.global-to-local, align 1
  %v16_404970 = icmp slt i8 %v5_404970, 0
  store i1 %v16_404970, i1* %sf.global-to-local, align 1
  store i8 %v5_404970, i8* %v1_404970, align 1
  %v0_404972 = load i32, i32* @eax, align 4
  %v1_404972 = inttoptr i32 %v0_404972 to i8*
  %v2_404972 = load i8, i8* %v1_404972, align 1
  %v4_404972 = trunc i32 %v0_404972 to i8
  %v5_404972 = add i8 %v2_404972, %v4_404972
  %v6_404972 = urem i8 %v2_404972, 16
  %v7_404972 = urem i8 %v4_404972, 16
  %v8_404972 = add nuw nsw i8 %v6_404972, %v7_404972
  %v9_404972 = icmp ugt i8 %v8_404972, 15
  %v10_404972 = icmp ult i8 %v5_404972, %v2_404972
  %v11_404972 = xor i8 %v5_404972, %v2_404972
  %v12_404972 = xor i8 %v5_404972, %v4_404972
  %v13_404972 = and i8 %v11_404972, %v12_404972
  %v14_404972 = icmp slt i8 %v13_404972, 0
  store i1 %v9_404972, i1* %az.global-to-local, align 1
  store i1 %v10_404972, i1* %cf.global-to-local, align 1
  store i1 %v14_404972, i1* %of.global-to-local, align 1
  %v15_404972 = icmp eq i8 %v5_404972, 0
  store i1 %v15_404972, i1* %zf.global-to-local, align 1
  %v16_404972 = icmp slt i8 %v5_404972, 0
  store i1 %v16_404972, i1* %sf.global-to-local, align 1
  store i8 %v5_404972, i8* %v1_404972, align 1
  %v0_404974 = load i32, i32* @eax, align 4
  %v1_404974 = inttoptr i32 %v0_404974 to i8*
  %v2_404974 = load i8, i8* %v1_404974, align 1
  %v4_404974 = trunc i32 %v0_404974 to i8
  %v5_404974 = add i8 %v2_404974, %v4_404974
  %v6_404974 = urem i8 %v2_404974, 16
  %v7_404974 = urem i8 %v4_404974, 16
  %v8_404974 = add nuw nsw i8 %v6_404974, %v7_404974
  %v9_404974 = icmp ugt i8 %v8_404974, 15
  %v10_404974 = icmp ult i8 %v5_404974, %v2_404974
  %v11_404974 = xor i8 %v5_404974, %v2_404974
  %v12_404974 = xor i8 %v5_404974, %v4_404974
  %v13_404974 = and i8 %v11_404974, %v12_404974
  %v14_404974 = icmp slt i8 %v13_404974, 0
  store i1 %v9_404974, i1* %az.global-to-local, align 1
  store i1 %v10_404974, i1* %cf.global-to-local, align 1
  store i1 %v14_404974, i1* %of.global-to-local, align 1
  %v15_404974 = icmp eq i8 %v5_404974, 0
  store i1 %v15_404974, i1* %zf.global-to-local, align 1
  %v16_404974 = icmp slt i8 %v5_404974, 0
  store i1 %v16_404974, i1* %sf.global-to-local, align 1
  store i8 %v5_404974, i8* %v1_404974, align 1
  %v0_404976 = load i32, i32* @eax, align 4
  %v1_404976 = inttoptr i32 %v0_404976 to i8*
  %v2_404976 = load i8, i8* %v1_404976, align 1
  %v4_404976 = trunc i32 %v0_404976 to i8
  %v5_404976 = add i8 %v2_404976, %v4_404976
  %v6_404976 = urem i8 %v2_404976, 16
  %v7_404976 = urem i8 %v4_404976, 16
  %v8_404976 = add nuw nsw i8 %v6_404976, %v7_404976
  %v9_404976 = icmp ugt i8 %v8_404976, 15
  %v10_404976 = icmp ult i8 %v5_404976, %v2_404976
  %v11_404976 = xor i8 %v5_404976, %v2_404976
  %v12_404976 = xor i8 %v5_404976, %v4_404976
  %v13_404976 = and i8 %v11_404976, %v12_404976
  %v14_404976 = icmp slt i8 %v13_404976, 0
  store i1 %v9_404976, i1* %az.global-to-local, align 1
  store i1 %v10_404976, i1* %cf.global-to-local, align 1
  store i1 %v14_404976, i1* %of.global-to-local, align 1
  %v15_404976 = icmp eq i8 %v5_404976, 0
  store i1 %v15_404976, i1* %zf.global-to-local, align 1
  %v16_404976 = icmp slt i8 %v5_404976, 0
  store i1 %v16_404976, i1* %sf.global-to-local, align 1
  store i8 %v5_404976, i8* %v1_404976, align 1
  %v0_404978 = load i32, i32* @eax, align 4
  %v1_404978 = inttoptr i32 %v0_404978 to i8*
  %v2_404978 = load i8, i8* %v1_404978, align 1
  %v4_404978 = trunc i32 %v0_404978 to i8
  %v5_404978 = add i8 %v2_404978, %v4_404978
  store i8 %v5_404978, i8* %v1_404978, align 1
  %v0_40497a = load i32, i32* @eax, align 4
  %v1_40497a = inttoptr i32 %v0_40497a to i32*
  %v2_40497a = load i32, i32* %v1_40497a, align 4
  %v4_40497a = add i32 %v2_40497a, %v0_40497a
  %v5_40497a = urem i32 %v2_40497a, 16
  %v6_40497a = urem i32 %v0_40497a, 16
  %v7_40497a = add nuw nsw i32 %v5_40497a, %v6_40497a
  %v8_40497a = icmp ugt i32 %v7_40497a, 15
  %v9_40497a = icmp ult i32 %v4_40497a, %v2_40497a
  %v10_40497a = xor i32 %v4_40497a, %v2_40497a
  %v11_40497a = xor i32 %v4_40497a, %v0_40497a
  %v12_40497a = and i32 %v10_40497a, %v11_40497a
  %v13_40497a = icmp slt i32 %v12_40497a, 0
  store i1 %v8_40497a, i1* %az.global-to-local, align 1
  store i1 %v9_40497a, i1* %cf.global-to-local, align 1
  store i1 %v13_40497a, i1* %of.global-to-local, align 1
  %v14_40497a = icmp eq i32 %v4_40497a, 0
  store i1 %v14_40497a, i1* %zf.global-to-local, align 1
  %v15_40497a = icmp slt i32 %v4_40497a, 0
  store i1 %v15_40497a, i1* %sf.global-to-local, align 1
  store i32 %v4_40497a, i32* %v1_40497a, align 4
  %v0_40497c = load i32, i32* @eax, align 4
  %v1_40497c = inttoptr i32 %v0_40497c to i8*
  %v2_40497c = load i8, i8* %v1_40497c, align 1
  %v4_40497c = trunc i32 %v0_40497c to i8
  %v5_40497c = add i8 %v2_40497c, %v4_40497c
  %v6_40497c = urem i8 %v2_40497c, 16
  %v7_40497c = urem i8 %v4_40497c, 16
  %v8_40497c = add nuw nsw i8 %v6_40497c, %v7_40497c
  %v9_40497c = icmp ugt i8 %v8_40497c, 15
  %v10_40497c = icmp ult i8 %v5_40497c, %v2_40497c
  %v11_40497c = xor i8 %v5_40497c, %v2_40497c
  %v12_40497c = xor i8 %v5_40497c, %v4_40497c
  %v13_40497c = and i8 %v11_40497c, %v12_40497c
  %v14_40497c = icmp slt i8 %v13_40497c, 0
  store i1 %v9_40497c, i1* %az.global-to-local, align 1
  store i1 %v10_40497c, i1* %cf.global-to-local, align 1
  store i1 %v14_40497c, i1* %of.global-to-local, align 1
  %v15_40497c = icmp eq i8 %v5_40497c, 0
  store i1 %v15_40497c, i1* %zf.global-to-local, align 1
  %v16_40497c = icmp slt i8 %v5_40497c, 0
  store i1 %v16_40497c, i1* %sf.global-to-local, align 1
  store i8 %v5_40497c, i8* %v1_40497c, align 1
  %v0_40497e = load i32, i32* @eax, align 4
  %v1_40497e = trunc i32 %v0_40497e to i8
  %v3_40497e = inttoptr i32 %v0_40497e to i8*
  %v4_40497e = load i8, i8* %v3_40497e, align 1
  %v5_40497e = add i8 %v4_40497e, %v1_40497e
  %v6_40497e = urem i8 %v1_40497e, 16
  %v7_40497e = urem i8 %v4_40497e, 16
  %v8_40497e = add nuw nsw i8 %v6_40497e, %v7_40497e
  %v9_40497e = icmp ugt i8 %v8_40497e, 15
  %v10_40497e = icmp ult i8 %v5_40497e, %v1_40497e
  %v11_40497e = xor i8 %v5_40497e, %v1_40497e
  %v12_40497e = xor i8 %v5_40497e, %v4_40497e
  %v13_40497e = and i8 %v11_40497e, %v12_40497e
  %v14_40497e = icmp slt i8 %v13_40497e, 0
  store i1 %v9_40497e, i1* %az.global-to-local, align 1
  store i1 %v10_40497e, i1* %cf.global-to-local, align 1
  store i1 %v14_40497e, i1* %of.global-to-local, align 1
  %v15_40497e = icmp eq i8 %v5_40497e, 0
  store i1 %v15_40497e, i1* %zf.global-to-local, align 1
  %v16_40497e = icmp slt i8 %v5_40497e, 0
  store i1 %v16_40497e, i1* %sf.global-to-local, align 1
  %v20_40497e = zext i8 %v5_40497e to i32
  %v22_40497e = and i32 %v0_40497e, -256
  %v23_40497e = or i32 %v22_40497e, %v20_40497e
  store i32 %v23_40497e, i32* @eax, align 4
  %v1_404980 = inttoptr i32 %v23_40497e to i8*
  %v2_404980 = load i8, i8* %v1_404980, align 1
  %v5_404980 = add i8 %v2_404980, %v5_40497e
  %v6_404980 = urem i8 %v2_404980, 16
  %v7_404980 = urem i8 %v5_40497e, 16
  %v8_404980 = add nuw nsw i8 %v6_404980, %v7_404980
  %v9_404980 = icmp ugt i8 %v8_404980, 15
  %v10_404980 = icmp ult i8 %v5_404980, %v2_404980
  %v11_404980 = xor i8 %v5_404980, %v2_404980
  %v12_404980 = xor i8 %v5_404980, %v5_40497e
  %v13_404980 = and i8 %v11_404980, %v12_404980
  %v14_404980 = icmp slt i8 %v13_404980, 0
  store i1 %v9_404980, i1* %az.global-to-local, align 1
  store i1 %v10_404980, i1* %cf.global-to-local, align 1
  store i1 %v14_404980, i1* %of.global-to-local, align 1
  %v15_404980 = icmp eq i8 %v5_404980, 0
  store i1 %v15_404980, i1* %zf.global-to-local, align 1
  %v16_404980 = icmp slt i8 %v5_404980, 0
  store i1 %v16_404980, i1* %sf.global-to-local, align 1
  store i8 %v5_404980, i8* %v1_404980, align 1
  %v0_404982 = load i32, i32* @eax, align 4
  %v1_404982 = inttoptr i32 %v0_404982 to i8*
  %v2_404982 = load i8, i8* %v1_404982, align 1
  %v4_404982 = trunc i32 %v0_404982 to i8
  %v5_404982 = add i8 %v2_404982, %v4_404982
  %v6_404982 = urem i8 %v2_404982, 16
  %v7_404982 = urem i8 %v4_404982, 16
  %v8_404982 = add nuw nsw i8 %v6_404982, %v7_404982
  %v9_404982 = icmp ugt i8 %v8_404982, 15
  %v10_404982 = icmp ult i8 %v5_404982, %v2_404982
  %v11_404982 = xor i8 %v5_404982, %v2_404982
  %v12_404982 = xor i8 %v5_404982, %v4_404982
  %v13_404982 = and i8 %v11_404982, %v12_404982
  %v14_404982 = icmp slt i8 %v13_404982, 0
  store i1 %v9_404982, i1* %az.global-to-local, align 1
  store i1 %v10_404982, i1* %cf.global-to-local, align 1
  store i1 %v14_404982, i1* %of.global-to-local, align 1
  %v15_404982 = icmp eq i8 %v5_404982, 0
  store i1 %v15_404982, i1* %zf.global-to-local, align 1
  %v16_404982 = icmp slt i8 %v5_404982, 0
  store i1 %v16_404982, i1* %sf.global-to-local, align 1
  store i8 %v5_404982, i8* %v1_404982, align 1
  %v0_404984 = load i32, i32* @eax, align 4
  %v1_404984 = inttoptr i32 %v0_404984 to i8*
  %v2_404984 = load i8, i8* %v1_404984, align 1
  %v4_404984 = trunc i32 %v0_404984 to i8
  %v5_404984 = add i8 %v2_404984, %v4_404984
  %v6_404984 = urem i8 %v2_404984, 16
  %v7_404984 = urem i8 %v4_404984, 16
  %v8_404984 = add nuw nsw i8 %v6_404984, %v7_404984
  %v9_404984 = icmp ugt i8 %v8_404984, 15
  %v10_404984 = icmp ult i8 %v5_404984, %v2_404984
  %v11_404984 = xor i8 %v5_404984, %v2_404984
  %v12_404984 = xor i8 %v5_404984, %v4_404984
  %v13_404984 = and i8 %v11_404984, %v12_404984
  %v14_404984 = icmp slt i8 %v13_404984, 0
  store i1 %v9_404984, i1* %az.global-to-local, align 1
  store i1 %v10_404984, i1* %cf.global-to-local, align 1
  store i1 %v14_404984, i1* %of.global-to-local, align 1
  %v15_404984 = icmp eq i8 %v5_404984, 0
  store i1 %v15_404984, i1* %zf.global-to-local, align 1
  %v16_404984 = icmp slt i8 %v5_404984, 0
  store i1 %v16_404984, i1* %sf.global-to-local, align 1
  store i8 %v5_404984, i8* %v1_404984, align 1
  %v0_404986 = load i32, i32* @eax, align 4
  %v1_404986 = inttoptr i32 %v0_404986 to i8*
  %v2_404986 = load i8, i8* %v1_404986, align 1
  %v4_404986 = trunc i32 %v0_404986 to i8
  %v5_404986 = add i8 %v2_404986, %v4_404986
  %v6_404986 = urem i8 %v2_404986, 16
  %v7_404986 = urem i8 %v4_404986, 16
  %v8_404986 = add nuw nsw i8 %v6_404986, %v7_404986
  %v9_404986 = icmp ugt i8 %v8_404986, 15
  %v10_404986 = icmp ult i8 %v5_404986, %v2_404986
  %v11_404986 = xor i8 %v5_404986, %v2_404986
  %v12_404986 = xor i8 %v5_404986, %v4_404986
  %v13_404986 = and i8 %v11_404986, %v12_404986
  %v14_404986 = icmp slt i8 %v13_404986, 0
  store i1 %v9_404986, i1* %az.global-to-local, align 1
  store i1 %v10_404986, i1* %cf.global-to-local, align 1
  store i1 %v14_404986, i1* %of.global-to-local, align 1
  %v15_404986 = icmp eq i8 %v5_404986, 0
  store i1 %v15_404986, i1* %zf.global-to-local, align 1
  %v16_404986 = icmp slt i8 %v5_404986, 0
  store i1 %v16_404986, i1* %sf.global-to-local, align 1
  store i8 %v5_404986, i8* %v1_404986, align 1
  %v0_404988 = load i32, i32* @eax, align 4
  %v1_404988 = inttoptr i32 %v0_404988 to i8*
  %v2_404988 = load i8, i8* %v1_404988, align 1
  %v4_404988 = trunc i32 %v0_404988 to i8
  %v5_404988 = add i8 %v2_404988, %v4_404988
  store i8 %v5_404988, i8* %v1_404988, align 1
  %v0_40498a = load i32, i32* @eax, align 4
  %v1_40498a = inttoptr i32 %v0_40498a to i32*
  %v2_40498a = load i32, i32* %v1_40498a, align 4
  %v4_40498a = add i32 %v2_40498a, %v0_40498a
  %v9_40498a = icmp ult i32 %v4_40498a, %v2_40498a
  store i32 %v4_40498a, i32* %v1_40498a, align 4
  %v0_40498c = load i32, i32* @eax, align 4
  %v1_40498c = trunc i32 %v0_40498c to i8
  %v3_40498c = zext i1 %v9_40498a to i8
  %v4_40498c = add i8 %v3_40498c, %v1_40498c
  %v5_40498c = urem i8 %v1_40498c, 16
  %v6_40498c = add nuw nsw i8 %v5_40498c, %v3_40498c
  %v7_40498c = icmp ugt i8 %v6_40498c, 15
  %v8_40498c = add i8 %v4_40498c, %v3_40498c
  %tmp475 = xor i8 %v1_40498c, -128
  %v11_40498c = and i8 %v8_40498c, %tmp475
  %v12_40498c = icmp slt i8 %v11_40498c, 0
  store i1 %v7_40498c, i1* %az.global-to-local, align 1
  store i1 %v12_40498c, i1* %of.global-to-local, align 1
  %v13_40498c = icmp eq i8 %v4_40498c, 0
  store i1 %v13_40498c, i1* %zf.global-to-local, align 1
  %v14_40498c = icmp slt i8 %v4_40498c, 0
  store i1 %v14_40498c, i1* %sf.global-to-local, align 1
  %v19_40498c = icmp ule i8 %v4_40498c, %v1_40498c
  %v21_40498c = icmp eq i1 %v9_40498a, %v19_40498c
  store i1 %v21_40498c, i1* %cf.global-to-local, align 1
  %v22_40498c = zext i8 %v4_40498c to i32
  %v24_40498c = and i32 %v0_40498c, -256
  %v25_40498c = or i32 %v24_40498c, %v22_40498c
  store i32 %v25_40498c, i32* @eax, align 4
  %v1_40498e = inttoptr i32 %v25_40498c to i8*
  %v2_40498e = load i8, i8* %v1_40498e, align 1
  %v5_40498e = add i8 %v2_40498e, %v4_40498c
  %v6_40498e = urem i8 %v2_40498e, 16
  %v7_40498e = urem i8 %v4_40498c, 16
  %v8_40498e = add nuw nsw i8 %v6_40498e, %v7_40498e
  %v9_40498e = icmp ugt i8 %v8_40498e, 15
  %v10_40498e = icmp ult i8 %v5_40498e, %v2_40498e
  %v11_40498e = xor i8 %v5_40498e, %v2_40498e
  %v12_40498e = xor i8 %v5_40498e, %v4_40498c
  %v13_40498e = and i8 %v11_40498e, %v12_40498e
  %v14_40498e = icmp slt i8 %v13_40498e, 0
  store i1 %v9_40498e, i1* %az.global-to-local, align 1
  store i1 %v10_40498e, i1* %cf.global-to-local, align 1
  store i1 %v14_40498e, i1* %of.global-to-local, align 1
  %v15_40498e = icmp eq i8 %v5_40498e, 0
  store i1 %v15_40498e, i1* %zf.global-to-local, align 1
  %v16_40498e = icmp slt i8 %v5_40498e, 0
  store i1 %v16_40498e, i1* %sf.global-to-local, align 1
  store i8 %v5_40498e, i8* %v1_40498e, align 1
  %v0_404990 = load i32, i32* @eax, align 4
  %v1_404990 = inttoptr i32 %v0_404990 to i8*
  %v2_404990 = load i8, i8* %v1_404990, align 1
  %v4_404990 = trunc i32 %v0_404990 to i8
  %v5_404990 = add i8 %v2_404990, %v4_404990
  %v6_404990 = urem i8 %v2_404990, 16
  %v7_404990 = urem i8 %v4_404990, 16
  %v8_404990 = add nuw nsw i8 %v6_404990, %v7_404990
  %v9_404990 = icmp ugt i8 %v8_404990, 15
  %v10_404990 = icmp ult i8 %v5_404990, %v2_404990
  %v11_404990 = xor i8 %v5_404990, %v2_404990
  %v12_404990 = xor i8 %v5_404990, %v4_404990
  %v13_404990 = and i8 %v11_404990, %v12_404990
  %v14_404990 = icmp slt i8 %v13_404990, 0
  store i1 %v9_404990, i1* %az.global-to-local, align 1
  store i1 %v10_404990, i1* %cf.global-to-local, align 1
  store i1 %v14_404990, i1* %of.global-to-local, align 1
  %v15_404990 = icmp eq i8 %v5_404990, 0
  store i1 %v15_404990, i1* %zf.global-to-local, align 1
  %v16_404990 = icmp slt i8 %v5_404990, 0
  store i1 %v16_404990, i1* %sf.global-to-local, align 1
  store i8 %v5_404990, i8* %v1_404990, align 1
  %v0_404992 = load i32, i32* @eax, align 4
  %v1_404992 = inttoptr i32 %v0_404992 to i8*
  %v2_404992 = load i8, i8* %v1_404992, align 1
  %v4_404992 = trunc i32 %v0_404992 to i8
  %v5_404992 = or i8 %v2_404992, %v4_404992
  store i1 false, i1* %az.global-to-local, align 1
  store i1 false, i1* %cf.global-to-local, align 1
  store i1 false, i1* %of.global-to-local, align 1
  %v6_404992 = icmp eq i8 %v5_404992, 0
  store i1 %v6_404992, i1* %zf.global-to-local, align 1
  %v7_404992 = icmp slt i8 %v5_404992, 0
  store i1 %v7_404992, i1* %sf.global-to-local, align 1
  store i8 %v5_404992, i8* %v1_404992, align 1
  %v0_404994 = load i32, i32* @eax, align 4
  %v1_404994 = inttoptr i32 %v0_404994 to i8*
  %v2_404994 = load i8, i8* %v1_404994, align 1
  %v4_404994 = trunc i32 %v0_404994 to i8
  %v5_404994 = add i8 %v2_404994, %v4_404994
  %v6_404994 = urem i8 %v2_404994, 16
  %v7_404994 = urem i8 %v4_404994, 16
  %v8_404994 = add nuw nsw i8 %v6_404994, %v7_404994
  %v9_404994 = icmp ugt i8 %v8_404994, 15
  %v10_404994 = icmp ult i8 %v5_404994, %v2_404994
  %v11_404994 = xor i8 %v5_404994, %v2_404994
  %v12_404994 = xor i8 %v5_404994, %v4_404994
  %v13_404994 = and i8 %v11_404994, %v12_404994
  %v14_404994 = icmp slt i8 %v13_404994, 0
  store i1 %v9_404994, i1* %az.global-to-local, align 1
  store i1 %v10_404994, i1* %cf.global-to-local, align 1
  store i1 %v14_404994, i1* %of.global-to-local, align 1
  %v15_404994 = icmp eq i8 %v5_404994, 0
  store i1 %v15_404994, i1* %zf.global-to-local, align 1
  %v16_404994 = icmp slt i8 %v5_404994, 0
  store i1 %v16_404994, i1* %sf.global-to-local, align 1
  store i8 %v5_404994, i8* %v1_404994, align 1
  %v0_404996 = load i32, i32* @eax, align 4
  %v1_404996 = inttoptr i32 %v0_404996 to i8*
  %v2_404996 = load i8, i8* %v1_404996, align 1
  %v4_404996 = trunc i32 %v0_404996 to i8
  %v5_404996 = add i8 %v2_404996, %v4_404996
  %v6_404996 = urem i8 %v2_404996, 16
  %v7_404996 = urem i8 %v4_404996, 16
  %v8_404996 = add nuw nsw i8 %v6_404996, %v7_404996
  %v9_404996 = icmp ugt i8 %v8_404996, 15
  %v10_404996 = icmp ult i8 %v5_404996, %v2_404996
  %v11_404996 = xor i8 %v5_404996, %v2_404996
  %v12_404996 = xor i8 %v5_404996, %v4_404996
  %v13_404996 = and i8 %v11_404996, %v12_404996
  %v14_404996 = icmp slt i8 %v13_404996, 0
  store i1 %v9_404996, i1* %az.global-to-local, align 1
  store i1 %v10_404996, i1* %cf.global-to-local, align 1
  store i1 %v14_404996, i1* %of.global-to-local, align 1
  %v15_404996 = icmp eq i8 %v5_404996, 0
  store i1 %v15_404996, i1* %zf.global-to-local, align 1
  %v16_404996 = icmp slt i8 %v5_404996, 0
  store i1 %v16_404996, i1* %sf.global-to-local, align 1
  store i8 %v5_404996, i8* %v1_404996, align 1
  %v0_404998 = load i32, i32* @eax, align 4
  %v1_404998 = inttoptr i32 %v0_404998 to i8*
  %v2_404998 = load i8, i8* %v1_404998, align 1
  %v4_404998 = trunc i32 %v0_404998 to i8
  %v5_404998 = add i8 %v2_404998, %v4_404998
  %v6_404998 = urem i8 %v2_404998, 16
  %v7_404998 = urem i8 %v4_404998, 16
  %v8_404998 = add nuw nsw i8 %v6_404998, %v7_404998
  %v9_404998 = icmp ugt i8 %v8_404998, 15
  %v10_404998 = icmp ult i8 %v5_404998, %v2_404998
  %v11_404998 = xor i8 %v5_404998, %v2_404998
  %v12_404998 = xor i8 %v5_404998, %v4_404998
  %v13_404998 = and i8 %v11_404998, %v12_404998
  %v14_404998 = icmp slt i8 %v13_404998, 0
  store i1 %v9_404998, i1* %az.global-to-local, align 1
  store i1 %v10_404998, i1* %cf.global-to-local, align 1
  store i1 %v14_404998, i1* %of.global-to-local, align 1
  %v15_404998 = icmp eq i8 %v5_404998, 0
  store i1 %v15_404998, i1* %zf.global-to-local, align 1
  %v16_404998 = icmp slt i8 %v5_404998, 0
  store i1 %v16_404998, i1* %sf.global-to-local, align 1
  store i8 %v5_404998, i8* %v1_404998, align 1
  %v0_40499a = load i32, i32* @eax, align 4
  %v1_40499a = inttoptr i32 %v0_40499a to i8*
  %v2_40499a = load i8, i8* %v1_40499a, align 1
  %v4_40499a = trunc i32 %v0_40499a to i8
  %v5_40499a = add i8 %v2_40499a, %v4_40499a
  %v6_40499a = urem i8 %v2_40499a, 16
  %v7_40499a = urem i8 %v4_40499a, 16
  %v8_40499a = add nuw nsw i8 %v6_40499a, %v7_40499a
  %v9_40499a = icmp ugt i8 %v8_40499a, 15
  %v10_40499a = icmp ult i8 %v5_40499a, %v2_40499a
  %v11_40499a = xor i8 %v5_40499a, %v2_40499a
  %v12_40499a = xor i8 %v5_40499a, %v4_40499a
  %v13_40499a = and i8 %v11_40499a, %v12_40499a
  %v14_40499a = icmp slt i8 %v13_40499a, 0
  store i1 %v9_40499a, i1* %az.global-to-local, align 1
  store i1 %v10_40499a, i1* %cf.global-to-local, align 1
  store i1 %v14_40499a, i1* %of.global-to-local, align 1
  %v15_40499a = icmp eq i8 %v5_40499a, 0
  store i1 %v15_40499a, i1* %zf.global-to-local, align 1
  %v16_40499a = icmp slt i8 %v5_40499a, 0
  store i1 %v16_40499a, i1* %sf.global-to-local, align 1
  store i8 %v5_40499a, i8* %v1_40499a, align 1
  %v0_40499c = load i32, i32* @eax, align 4
  %v1_40499c = inttoptr i32 %v0_40499c to i8*
  %v2_40499c = load i8, i8* %v1_40499c, align 1
  %v4_40499c = trunc i32 %v0_40499c to i8
  %v5_40499c = add i8 %v2_40499c, %v4_40499c
  store i8 %v5_40499c, i8* %v1_40499c, align 1
  %v0_40499e = load i32, i32* @eax, align 4
  %v2_40499e = inttoptr i32 %v0_40499e to i32*
  %v3_40499e = load i32, i32* %v2_40499e, align 4
  %v4_40499e = sub i32 %v0_40499e, %v3_40499e
  %v5_40499e = urem i32 %v0_40499e, 16
  %v6_40499e = urem i32 %v3_40499e, 16
  %v7_40499e = sub nsw i32 %v5_40499e, %v6_40499e
  %v8_40499e = icmp ugt i32 %v7_40499e, 15
  %v9_40499e = icmp ult i32 %v0_40499e, %v3_40499e
  %v10_40499e = xor i32 %v3_40499e, %v0_40499e
  %v11_40499e = xor i32 %v4_40499e, %v0_40499e
  %v12_40499e = and i32 %v11_40499e, %v10_40499e
  %v13_40499e = icmp slt i32 %v12_40499e, 0
  store i1 %v8_40499e, i1* %az.global-to-local, align 1
  store i1 %v9_40499e, i1* %cf.global-to-local, align 1
  store i1 %v13_40499e, i1* %of.global-to-local, align 1
  %v14_40499e = icmp eq i32 %v4_40499e, 0
  store i1 %v14_40499e, i1* %zf.global-to-local, align 1
  %v15_40499e = icmp slt i32 %v4_40499e, 0
  store i1 %v15_40499e, i1* %sf.global-to-local, align 1
  %v1_4049a0 = trunc i32 %v0_40499e to i8
  %v2_4049a0 = load i32, i32* @ecx, align 4
  %v3_4049a0 = inttoptr i32 %v2_4049a0 to i8*
  %v4_4049a0 = load i8, i8* %v3_4049a0, align 1
  %v5_4049a0 = add i8 %v4_4049a0, %v1_4049a0
  %v6_4049a0 = urem i8 %v1_4049a0, 16
  %v7_4049a0 = urem i8 %v4_4049a0, 16
  %v8_4049a0 = add nuw nsw i8 %v7_4049a0, %v6_4049a0
  %v9_4049a0 = icmp ugt i8 %v8_4049a0, 15
  %v10_4049a0 = icmp ult i8 %v5_4049a0, %v1_4049a0
  %v11_4049a0 = xor i8 %v5_4049a0, %v1_4049a0
  %v12_4049a0 = xor i8 %v5_4049a0, %v4_4049a0
  %v13_4049a0 = and i8 %v11_4049a0, %v12_4049a0
  %v14_4049a0 = icmp slt i8 %v13_4049a0, 0
  store i1 %v9_4049a0, i1* %az.global-to-local, align 1
  store i1 %v10_4049a0, i1* %cf.global-to-local, align 1
  store i1 %v14_4049a0, i1* %of.global-to-local, align 1
  %v15_4049a0 = icmp eq i8 %v5_4049a0, 0
  store i1 %v15_4049a0, i1* %zf.global-to-local, align 1
  %v16_4049a0 = icmp slt i8 %v5_4049a0, 0
  store i1 %v16_4049a0, i1* %sf.global-to-local, align 1
  %v20_4049a0 = zext i8 %v5_4049a0 to i32
  %v22_4049a0 = and i32 %v0_40499e, -256
  %v23_4049a0 = or i32 %v22_4049a0, %v20_4049a0
  store i32 %v23_4049a0, i32* @eax, align 4
  %v1_4049a2 = inttoptr i32 %v23_4049a0 to i8*
  %v2_4049a2 = load i8, i8* %v1_4049a2, align 1
  %v5_4049a2 = add i8 %v2_4049a2, %v5_4049a0
  %v6_4049a2 = urem i8 %v2_4049a2, 16
  %v7_4049a2 = urem i8 %v5_4049a0, 16
  %v8_4049a2 = add nuw nsw i8 %v6_4049a2, %v7_4049a2
  %v9_4049a2 = icmp ugt i8 %v8_4049a2, 15
  %v10_4049a2 = icmp ult i8 %v5_4049a2, %v2_4049a2
  %v11_4049a2 = xor i8 %v5_4049a2, %v2_4049a2
  %v12_4049a2 = xor i8 %v5_4049a2, %v5_4049a0
  %v13_4049a2 = and i8 %v11_4049a2, %v12_4049a2
  %v14_4049a2 = icmp slt i8 %v13_4049a2, 0
  store i1 %v9_4049a2, i1* %az.global-to-local, align 1
  store i1 %v10_4049a2, i1* %cf.global-to-local, align 1
  store i1 %v14_4049a2, i1* %of.global-to-local, align 1
  %v15_4049a2 = icmp eq i8 %v5_4049a2, 0
  store i1 %v15_4049a2, i1* %zf.global-to-local, align 1
  %v16_4049a2 = icmp slt i8 %v5_4049a2, 0
  store i1 %v16_4049a2, i1* %sf.global-to-local, align 1
  store i8 %v5_4049a2, i8* %v1_4049a2, align 1
  %v0_4049a4 = load i32, i32* @eax, align 4
  %v1_4049a4 = inttoptr i32 %v0_4049a4 to i8*
  %v2_4049a4 = load i8, i8* %v1_4049a4, align 1
  %v4_4049a4 = trunc i32 %v0_4049a4 to i8
  %v5_4049a4 = add i8 %v2_4049a4, %v4_4049a4
  %v6_4049a4 = urem i8 %v2_4049a4, 16
  %v7_4049a4 = urem i8 %v4_4049a4, 16
  %v8_4049a4 = add nuw nsw i8 %v6_4049a4, %v7_4049a4
  %v9_4049a4 = icmp ugt i8 %v8_4049a4, 15
  %v10_4049a4 = icmp ult i8 %v5_4049a4, %v2_4049a4
  %v11_4049a4 = xor i8 %v5_4049a4, %v2_4049a4
  %v12_4049a4 = xor i8 %v5_4049a4, %v4_4049a4
  %v13_4049a4 = and i8 %v11_4049a4, %v12_4049a4
  %v14_4049a4 = icmp slt i8 %v13_4049a4, 0
  store i1 %v9_4049a4, i1* %az.global-to-local, align 1
  store i1 %v10_4049a4, i1* %cf.global-to-local, align 1
  store i1 %v14_4049a4, i1* %of.global-to-local, align 1
  %v15_4049a4 = icmp eq i8 %v5_4049a4, 0
  store i1 %v15_4049a4, i1* %zf.global-to-local, align 1
  %v16_4049a4 = icmp slt i8 %v5_4049a4, 0
  store i1 %v16_4049a4, i1* %sf.global-to-local, align 1
  store i8 %v5_4049a4, i8* %v1_4049a4, align 1
  %v0_4049a6 = load i32, i32* @eax, align 4
  %v1_4049a6 = trunc i32 %v0_4049a6 to i8
  %v3_4049a6 = inttoptr i32 %v0_4049a6 to i8*
  %v4_4049a6 = load i8, i8* %v3_4049a6, align 1
  %v5_4049a6 = add i8 %v4_4049a6, %v1_4049a6
  %v6_4049a6 = urem i8 %v1_4049a6, 16
  %v7_4049a6 = urem i8 %v4_4049a6, 16
  %v8_4049a6 = add nuw nsw i8 %v6_4049a6, %v7_4049a6
  %v9_4049a6 = icmp ugt i8 %v8_4049a6, 15
  %v10_4049a6 = icmp ult i8 %v5_4049a6, %v1_4049a6
  %v11_4049a6 = xor i8 %v5_4049a6, %v1_4049a6
  %v12_4049a6 = xor i8 %v5_4049a6, %v4_4049a6
  %v13_4049a6 = and i8 %v11_4049a6, %v12_4049a6
  %v14_4049a6 = icmp slt i8 %v13_4049a6, 0
  store i1 %v9_4049a6, i1* %az.global-to-local, align 1
  store i1 %v10_4049a6, i1* %cf.global-to-local, align 1
  store i1 %v14_4049a6, i1* %of.global-to-local, align 1
  %v15_4049a6 = icmp eq i8 %v5_4049a6, 0
  store i1 %v15_4049a6, i1* %zf.global-to-local, align 1
  %v16_4049a6 = icmp slt i8 %v5_4049a6, 0
  store i1 %v16_4049a6, i1* %sf.global-to-local, align 1
  %v20_4049a6 = zext i8 %v5_4049a6 to i32
  %v22_4049a6 = and i32 %v0_4049a6, -256
  %v23_4049a6 = or i32 %v22_4049a6, %v20_4049a6
  %v24_4049a6 = inttoptr i32 %v23_4049a6 to i8*
  store i32 %v23_4049a6, i32* @eax, align 4
  %v3_4049a8 = load i8, i8* %v24_4049a6, align 1
  %v9_4049a8 = urem i8 %v5_4049a6, 16
  %factor543 = mul i8 %v5_4049a6, 5
  %v7_4049b0 = add i8 %v3_4049a8, %factor543
  store i8 %v7_4049b0, i8* %v24_4049a6, align 1
  %tmp476 = inttoptr i32 %v23_4049a6 to i32*
  %v3_4049b2 = load i32, i32* %tmp476, align 4
  %v6_4049b2 = add i32 %v3_4049b2, %v23_4049a6
  store i32 %v6_4049b2, i32* %tmp476, align 4
  store i1 false, i1* %az.global-to-local, align 1
  store i1 false, i1* %cf.global-to-local, align 1
  store i1 false, i1* %of.global-to-local, align 1
  %v2_4049b4 = icmp eq i32 %v23_4049a6, 0
  store i1 %v2_4049b4, i1* %zf.global-to-local, align 1
  %v3_4049b4 = icmp slt i32 %v23_4049a6, 0
  store i1 %v3_4049b4, i1* %sf.global-to-local, align 1
  store i32 %v23_4049a6, i32* @eax, align 4
  %v2_4049b9 = load i8, i8* %v24_4049a6, align 1
  %v5_4049b9 = add i8 %v2_4049b9, %v5_4049a6
  %v6_4049b9 = urem i8 %v2_4049b9, 16
  %v8_4049b9 = add nuw nsw i8 %v6_4049b9, %v9_4049a8
  %v9_4049b9 = icmp ugt i8 %v8_4049b9, 15
  %v10_4049b9 = icmp ult i8 %v5_4049b9, %v2_4049b9
  %v11_4049b9 = xor i8 %v5_4049b9, %v2_4049b9
  %v12_4049b9 = xor i8 %v5_4049b9, %v5_4049a6
  %v13_4049b9 = and i8 %v11_4049b9, %v12_4049b9
  %v14_4049b9 = icmp slt i8 %v13_4049b9, 0
  store i1 %v9_4049b9, i1* %az.global-to-local, align 1
  store i1 %v10_4049b9, i1* %cf.global-to-local, align 1
  store i1 %v14_4049b9, i1* %of.global-to-local, align 1
  %v15_4049b9 = icmp eq i8 %v5_4049b9, 0
  store i1 %v15_4049b9, i1* %zf.global-to-local, align 1
  %v16_4049b9 = icmp slt i8 %v5_4049b9, 0
  store i1 %v16_4049b9, i1* %sf.global-to-local, align 1
  store i8 %v5_4049b9, i8* %v24_4049a6, align 1
  %v0_4049bb = load i32, i32* @eax, align 4
  %v1_4049bb = inttoptr i32 %v0_4049bb to i8*
  %v2_4049bb = load i8, i8* %v1_4049bb, align 1
  %v4_4049bb = trunc i32 %v0_4049bb to i8
  %v5_4049bb = add i8 %v2_4049bb, %v4_4049bb
  %v6_4049bb = urem i8 %v2_4049bb, 16
  %v7_4049bb = urem i8 %v4_4049bb, 16
  %v8_4049bb = add nuw nsw i8 %v6_4049bb, %v7_4049bb
  %v9_4049bb = icmp ugt i8 %v8_4049bb, 15
  %v10_4049bb = icmp ult i8 %v5_4049bb, %v2_4049bb
  %v11_4049bb = xor i8 %v5_4049bb, %v2_4049bb
  %v12_4049bb = xor i8 %v5_4049bb, %v4_4049bb
  %v13_4049bb = and i8 %v11_4049bb, %v12_4049bb
  %v14_4049bb = icmp slt i8 %v13_4049bb, 0
  store i1 %v9_4049bb, i1* %az.global-to-local, align 1
  store i1 %v10_4049bb, i1* %cf.global-to-local, align 1
  store i1 %v14_4049bb, i1* %of.global-to-local, align 1
  %v15_4049bb = icmp eq i8 %v5_4049bb, 0
  store i1 %v15_4049bb, i1* %zf.global-to-local, align 1
  %v16_4049bb = icmp slt i8 %v5_4049bb, 0
  store i1 %v16_4049bb, i1* %sf.global-to-local, align 1
  store i8 %v5_4049bb, i8* %v1_4049bb, align 1
  %v0_4049bd = load i32, i32* @ecx, align 4
  %v1_4049bd = inttoptr i32 %v0_4049bd to i8*
  %v2_4049bd = load i8, i8* %v1_4049bd, align 1
  %v3_4049bd = load i32, i32* @eax, align 4
  %v4_4049bd = trunc i32 %v3_4049bd to i8
  %v5_4049bd = add i8 %v2_4049bd, %v4_4049bd
  %v6_4049bd = urem i8 %v2_4049bd, 16
  %v7_4049bd = urem i8 %v4_4049bd, 16
  %v8_4049bd = add nuw nsw i8 %v7_4049bd, %v6_4049bd
  %v9_4049bd = icmp ugt i8 %v8_4049bd, 15
  %v10_4049bd = icmp ult i8 %v5_4049bd, %v2_4049bd
  %v11_4049bd = xor i8 %v5_4049bd, %v2_4049bd
  %v12_4049bd = xor i8 %v5_4049bd, %v4_4049bd
  %v13_4049bd = and i8 %v11_4049bd, %v12_4049bd
  %v14_4049bd = icmp slt i8 %v13_4049bd, 0
  store i1 %v9_4049bd, i1* %az.global-to-local, align 1
  store i1 %v10_4049bd, i1* %cf.global-to-local, align 1
  store i1 %v14_4049bd, i1* %of.global-to-local, align 1
  %v15_4049bd = icmp eq i8 %v5_4049bd, 0
  store i1 %v15_4049bd, i1* %zf.global-to-local, align 1
  %v16_4049bd = icmp slt i8 %v5_4049bd, 0
  store i1 %v16_4049bd, i1* %sf.global-to-local, align 1
  store i8 %v5_4049bd, i8* %v1_4049bd, align 1
  %v0_4049bf = load i32, i32* @eax, align 4
  %v1_4049bf = inttoptr i32 %v0_4049bf to i8*
  %v2_4049bf = load i8, i8* %v1_4049bf, align 1
  %v4_4049bf = trunc i32 %v0_4049bf to i8
  %v5_4049bf = add i8 %v2_4049bf, %v4_4049bf
  %v6_4049bf = urem i8 %v2_4049bf, 16
  %v7_4049bf = urem i8 %v4_4049bf, 16
  %v8_4049bf = add nuw nsw i8 %v6_4049bf, %v7_4049bf
  %v9_4049bf = icmp ugt i8 %v8_4049bf, 15
  %v10_4049bf = icmp ult i8 %v5_4049bf, %v2_4049bf
  %v11_4049bf = xor i8 %v5_4049bf, %v2_4049bf
  %v12_4049bf = xor i8 %v5_4049bf, %v4_4049bf
  %v13_4049bf = and i8 %v11_4049bf, %v12_4049bf
  %v14_4049bf = icmp slt i8 %v13_4049bf, 0
  store i1 %v9_4049bf, i1* %az.global-to-local, align 1
  store i1 %v10_4049bf, i1* %cf.global-to-local, align 1
  store i1 %v14_4049bf, i1* %of.global-to-local, align 1
  %v15_4049bf = icmp eq i8 %v5_4049bf, 0
  store i1 %v15_4049bf, i1* %zf.global-to-local, align 1
  %v16_4049bf = icmp slt i8 %v5_4049bf, 0
  store i1 %v16_4049bf, i1* %sf.global-to-local, align 1
  store i8 %v5_4049bf, i8* %v1_4049bf, align 1
  %v0_4049c1 = load i32, i32* @edx, align 4
  %v1_4049c1 = inttoptr i32 %v0_4049c1 to i8*
  %v2_4049c1 = load i8, i8* %v1_4049c1, align 1
  %v4_4049c1 = udiv i32 %v0_4049c1, 256
  %v5_4049c1 = trunc i32 %v4_4049c1 to i8
  %v6_4049c1 = add i8 %v2_4049c1, %v5_4049c1
  %v7_4049c1 = urem i8 %v2_4049c1, 16
  %v8_4049c1 = urem i8 %v5_4049c1, 16
  %v9_4049c1 = add nuw nsw i8 %v8_4049c1, %v7_4049c1
  %v10_4049c1 = icmp ugt i8 %v9_4049c1, 15
  %v11_4049c1 = icmp ult i8 %v6_4049c1, %v2_4049c1
  %v12_4049c1 = xor i8 %v6_4049c1, %v2_4049c1
  %v13_4049c1 = xor i8 %v6_4049c1, %v5_4049c1
  %v14_4049c1 = and i8 %v12_4049c1, %v13_4049c1
  %v15_4049c1 = icmp slt i8 %v14_4049c1, 0
  store i1 %v10_4049c1, i1* %az.global-to-local, align 1
  store i1 %v11_4049c1, i1* %cf.global-to-local, align 1
  store i1 %v15_4049c1, i1* %of.global-to-local, align 1
  %v16_4049c1 = icmp eq i8 %v6_4049c1, 0
  store i1 %v16_4049c1, i1* %zf.global-to-local, align 1
  %v17_4049c1 = icmp slt i8 %v6_4049c1, 0
  store i1 %v17_4049c1, i1* %sf.global-to-local, align 1
  store i8 %v6_4049c1, i8* %v1_4049c1, align 1
  %v0_4049c4 = load i32, i32* @eax, align 4
  %v1_4049c4 = inttoptr i32 %v0_4049c4 to i8*
  %v2_4049c4 = load i8, i8* %v1_4049c4, align 1
  %v4_4049c4 = trunc i32 %v0_4049c4 to i8
  %v5_4049c4 = add i8 %v2_4049c4, %v4_4049c4
  %v6_4049c4 = urem i8 %v2_4049c4, 16
  %v7_4049c4 = urem i8 %v4_4049c4, 16
  %v8_4049c4 = add nuw nsw i8 %v6_4049c4, %v7_4049c4
  %v9_4049c4 = icmp ugt i8 %v8_4049c4, 15
  %v10_4049c4 = icmp ult i8 %v5_4049c4, %v2_4049c4
  %v11_4049c4 = xor i8 %v5_4049c4, %v2_4049c4
  %v12_4049c4 = xor i8 %v5_4049c4, %v4_4049c4
  %v13_4049c4 = and i8 %v11_4049c4, %v12_4049c4
  %v14_4049c4 = icmp slt i8 %v13_4049c4, 0
  store i1 %v9_4049c4, i1* %az.global-to-local, align 1
  store i1 %v10_4049c4, i1* %cf.global-to-local, align 1
  store i1 %v14_4049c4, i1* %of.global-to-local, align 1
  %v15_4049c4 = icmp eq i8 %v5_4049c4, 0
  store i1 %v15_4049c4, i1* %zf.global-to-local, align 1
  %v16_4049c4 = icmp slt i8 %v5_4049c4, 0
  store i1 %v16_4049c4, i1* %sf.global-to-local, align 1
  store i8 %v5_4049c4, i8* %v1_4049c4, align 1
  %v0_4049c6 = load i16, i16* @es, align 4
  %v4_4049c6 = sext i16 %v0_4049c6 to i32
  store i32 %v4_4049c6, i32* %stack_var_36, align 4
  %v0_4049c7 = load i8, i8* inttoptr (i32 83887872 to i8*), align 256
  %v1_4049c7 = load i32, i32* @eax, align 4
  %v2_4049c7 = trunc i32 %v1_4049c7 to i8
  %v3_4049c7 = add i8 %v0_4049c7, %v2_4049c7
  %v4_4049c7 = urem i8 %v0_4049c7, 16
  %v5_4049c7 = urem i8 %v2_4049c7, 16
  %v6_4049c7 = add nuw nsw i8 %v5_4049c7, %v4_4049c7
  %v7_4049c7 = icmp ugt i8 %v6_4049c7, 15
  %v8_4049c7 = icmp ult i8 %v3_4049c7, %v0_4049c7
  %v9_4049c7 = xor i8 %v3_4049c7, %v0_4049c7
  %v10_4049c7 = xor i8 %v3_4049c7, %v2_4049c7
  %v11_4049c7 = and i8 %v9_4049c7, %v10_4049c7
  %v12_4049c7 = icmp slt i8 %v11_4049c7, 0
  store i1 %v7_4049c7, i1* %az.global-to-local, align 1
  store i1 %v8_4049c7, i1* %cf.global-to-local, align 1
  store i1 %v12_4049c7, i1* %of.global-to-local, align 1
  %v13_4049c7 = icmp eq i8 %v3_4049c7, 0
  store i1 %v13_4049c7, i1* %zf.global-to-local, align 1
  %v14_4049c7 = icmp slt i8 %v3_4049c7, 0
  store i1 %v14_4049c7, i1* %sf.global-to-local, align 1
  store i8 %v3_4049c7, i8* inttoptr (i32 83887872 to i8*), align 256
  %v0_4049cd = load i8, i8* inttoptr (i32 3072 to i8*), align 1024
  %v1_4049cd = load i32, i32* @ecx, align 4
  %v2_4049cd = trunc i32 %v1_4049cd to i8
  %v3_4049cd = add i8 %v0_4049cd, %v2_4049cd
  %v4_4049cd = urem i8 %v0_4049cd, 16
  %v5_4049cd = urem i8 %v2_4049cd, 16
  %v6_4049cd = add nuw nsw i8 %v5_4049cd, %v4_4049cd
  %v7_4049cd = icmp ugt i8 %v6_4049cd, 15
  %v8_4049cd = icmp ult i8 %v3_4049cd, %v0_4049cd
  %v9_4049cd = xor i8 %v3_4049cd, %v0_4049cd
  %v10_4049cd = xor i8 %v3_4049cd, %v2_4049cd
  %v11_4049cd = and i8 %v9_4049cd, %v10_4049cd
  %v12_4049cd = icmp slt i8 %v11_4049cd, 0
  store i1 %v7_4049cd, i1* %az.global-to-local, align 1
  store i1 %v8_4049cd, i1* %cf.global-to-local, align 1
  store i1 %v12_4049cd, i1* %of.global-to-local, align 1
  %v13_4049cd = icmp eq i8 %v3_4049cd, 0
  store i1 %v13_4049cd, i1* %zf.global-to-local, align 1
  %v14_4049cd = icmp slt i8 %v3_4049cd, 0
  store i1 %v14_4049cd, i1* %sf.global-to-local, align 1
  store i8 %v3_4049cd, i8* inttoptr (i32 3072 to i8*), align 1024
  %v0_4049d3 = load i32, i32* @eax, align 4
  %v1_4049d3 = inttoptr i32 %v0_4049d3 to i8*
  %v2_4049d3 = load i8, i8* %v1_4049d3, align 1
  %v3_4049d3 = load i32, i32* @edx, align 4
  %v4_4049d3 = trunc i32 %v3_4049d3 to i8
  %v5_4049d3 = add i8 %v2_4049d3, %v4_4049d3
  %v6_4049d3 = urem i8 %v2_4049d3, 16
  %v7_4049d3 = urem i8 %v4_4049d3, 16
  %v8_4049d3 = add nuw nsw i8 %v7_4049d3, %v6_4049d3
  %v9_4049d3 = icmp ugt i8 %v8_4049d3, 15
  %v10_4049d3 = icmp ult i8 %v5_4049d3, %v2_4049d3
  %v11_4049d3 = xor i8 %v5_4049d3, %v2_4049d3
  %v12_4049d3 = xor i8 %v5_4049d3, %v4_4049d3
  %v13_4049d3 = and i8 %v11_4049d3, %v12_4049d3
  %v14_4049d3 = icmp slt i8 %v13_4049d3, 0
  store i1 %v9_4049d3, i1* %az.global-to-local, align 1
  store i1 %v10_4049d3, i1* %cf.global-to-local, align 1
  store i1 %v14_4049d3, i1* %of.global-to-local, align 1
  %v15_4049d3 = icmp eq i8 %v5_4049d3, 0
  store i1 %v15_4049d3, i1* %zf.global-to-local, align 1
  %v16_4049d3 = icmp slt i8 %v5_4049d3, 0
  store i1 %v16_4049d3, i1* %sf.global-to-local, align 1
  store i8 %v5_4049d3, i8* %v1_4049d3, align 1
  %v0_4049d5 = load i32, i32* @esi, align 4
  %v1_4049d5 = inttoptr i32 %v0_4049d5 to i8*
  %v2_4049d5 = load i8, i8* %v1_4049d5, align 1
  %v3_4049d5 = load i32, i32* @ecx, align 4
  %v4_4049d5 = trunc i32 %v3_4049d5 to i8
  %v5_4049d5 = add i8 %v2_4049d5, %v4_4049d5
  %v6_4049d5 = urem i8 %v2_4049d5, 16
  %v7_4049d5 = urem i8 %v4_4049d5, 16
  %v8_4049d5 = add nuw nsw i8 %v7_4049d5, %v6_4049d5
  %v9_4049d5 = icmp ugt i8 %v8_4049d5, 15
  %v10_4049d5 = icmp ult i8 %v5_4049d5, %v2_4049d5
  %v11_4049d5 = xor i8 %v5_4049d5, %v2_4049d5
  %v12_4049d5 = xor i8 %v5_4049d5, %v4_4049d5
  %v13_4049d5 = and i8 %v11_4049d5, %v12_4049d5
  %v14_4049d5 = icmp slt i8 %v13_4049d5, 0
  store i1 %v9_4049d5, i1* %az.global-to-local, align 1
  store i1 %v10_4049d5, i1* %cf.global-to-local, align 1
  store i1 %v14_4049d5, i1* %of.global-to-local, align 1
  %v15_4049d5 = icmp eq i8 %v5_4049d5, 0
  store i1 %v15_4049d5, i1* %zf.global-to-local, align 1
  %v16_4049d5 = icmp slt i8 %v5_4049d5, 0
  store i1 %v16_4049d5, i1* %sf.global-to-local, align 1
  store i8 %v5_4049d5, i8* %v1_4049d5, align 1
  %v0_4049d7 = load i32, i32* @esi, align 4
  %v1_4049d7 = add i32 %v0_4049d7, 1
  %v2_4049d7 = inttoptr i32 %v1_4049d7 to i8*
  %v3_4049d7 = load i8, i8* %v2_4049d7, align 1
  %v4_4049d7 = load i32, i32* @eax, align 4
  %v5_4049d7 = udiv i32 %v4_4049d7, 256
  %v6_4049d7 = trunc i32 %v5_4049d7 to i8
  %v7_4049d7 = add i8 %v3_4049d7, %v6_4049d7
  %v8_4049d7 = urem i8 %v3_4049d7, 16
  %v9_4049d7 = urem i8 %v6_4049d7, 16
  %v10_4049d7 = add nuw nsw i8 %v9_4049d7, %v8_4049d7
  %v11_4049d7 = icmp ugt i8 %v10_4049d7, 15
  %v12_4049d7 = icmp ult i8 %v7_4049d7, %v3_4049d7
  %v13_4049d7 = xor i8 %v7_4049d7, %v3_4049d7
  %v14_4049d7 = xor i8 %v7_4049d7, %v6_4049d7
  %v15_4049d7 = and i8 %v13_4049d7, %v14_4049d7
  %v16_4049d7 = icmp slt i8 %v15_4049d7, 0
  store i1 %v11_4049d7, i1* %az.global-to-local, align 1
  store i1 %v12_4049d7, i1* %cf.global-to-local, align 1
  store i1 %v16_4049d7, i1* %of.global-to-local, align 1
  %v17_4049d7 = icmp eq i8 %v7_4049d7, 0
  store i1 %v17_4049d7, i1* %zf.global-to-local, align 1
  %v18_4049d7 = icmp slt i8 %v7_4049d7, 0
  store i1 %v18_4049d7, i1* %sf.global-to-local, align 1
  store i8 %v7_4049d7, i8* %v2_4049d7, align 1
  %v0_4049da = load i32, i32* @eax, align 4
  %v1_4049da = inttoptr i32 %v0_4049da to i8*
  %v2_4049da = load i8, i8* %v1_4049da, align 1
  %v4_4049da = trunc i32 %v0_4049da to i8
  %v5_4049da = add i8 %v2_4049da, %v4_4049da
  %v6_4049da = urem i8 %v2_4049da, 16
  %v7_4049da = urem i8 %v4_4049da, 16
  %v8_4049da = add nuw nsw i8 %v6_4049da, %v7_4049da
  %v9_4049da = icmp ugt i8 %v8_4049da, 15
  %v10_4049da = icmp ult i8 %v5_4049da, %v2_4049da
  %v11_4049da = xor i8 %v5_4049da, %v2_4049da
  %v12_4049da = xor i8 %v5_4049da, %v4_4049da
  %v13_4049da = and i8 %v11_4049da, %v12_4049da
  %v14_4049da = icmp slt i8 %v13_4049da, 0
  store i1 %v9_4049da, i1* %az.global-to-local, align 1
  store i1 %v10_4049da, i1* %cf.global-to-local, align 1
  store i1 %v14_4049da, i1* %of.global-to-local, align 1
  %v15_4049da = icmp eq i8 %v5_4049da, 0
  store i1 %v15_4049da, i1* %zf.global-to-local, align 1
  %v16_4049da = icmp slt i8 %v5_4049da, 0
  store i1 %v16_4049da, i1* %sf.global-to-local, align 1
  store i8 %v5_4049da, i8* %v1_4049da, align 1
  %v0_4049dc = load i32, i32* @eax, align 4
  %v1_4049dc = inttoptr i32 %v0_4049dc to i8*
  %v2_4049dc = load i8, i8* %v1_4049dc, align 1
  %v4_4049dc = trunc i32 %v0_4049dc to i8
  %v5_4049dc = load i1, i1* %cf.global-to-local, align 1
  %v6_4049dc = zext i1 %v5_4049dc to i8
  %v7_4049dc = add i8 %v2_4049dc, %v4_4049dc
  %v8_4049dc = add i8 %v7_4049dc, %v6_4049dc
  store i8 %v8_4049dc, i8* %v1_4049dc, align 1
  %v0_4049de = load i32, i32* @eax, align 4
  %v1_4049de = inttoptr i32 %v0_4049de to i32*
  %v2_4049de = load i32, i32* %v1_4049de, align 4
  %v4_4049de = sub i32 %v2_4049de, %v0_4049de
  %v5_4049de = urem i32 %v2_4049de, 16
  %v6_4049de = urem i32 %v0_4049de, 16
  %v7_4049de = sub nsw i32 %v5_4049de, %v6_4049de
  %v8_4049de = icmp ugt i32 %v7_4049de, 15
  %v9_4049de = icmp ult i32 %v2_4049de, %v0_4049de
  %v10_4049de = xor i32 %v2_4049de, %v0_4049de
  %v11_4049de = xor i32 %v4_4049de, %v2_4049de
  %v12_4049de = and i32 %v11_4049de, %v10_4049de
  %v13_4049de = icmp slt i32 %v12_4049de, 0
  store i1 %v8_4049de, i1* %az.global-to-local, align 1
  store i1 %v9_4049de, i1* %cf.global-to-local, align 1
  store i1 %v13_4049de, i1* %of.global-to-local, align 1
  %v14_4049de = icmp eq i32 %v4_4049de, 0
  store i1 %v14_4049de, i1* %zf.global-to-local, align 1
  %v15_4049de = icmp slt i32 %v4_4049de, 0
  store i1 %v15_4049de, i1* %sf.global-to-local, align 1
  %v0_4049e0 = load i32, i32* @ebx, align 4
  %v1_4049e0 = inttoptr i32 %v0_4049e0 to i8*
  %v2_4049e0 = load i8, i8* %v1_4049e0, align 1
  %v4_4049e0 = trunc i32 %v0_4049de to i8
  %v5_4049e0 = add i8 %v2_4049e0, %v4_4049e0
  %v6_4049e0 = urem i8 %v2_4049e0, 16
  %v7_4049e0 = urem i8 %v4_4049e0, 16
  %v8_4049e0 = add nuw nsw i8 %v6_4049e0, %v7_4049e0
  %v9_4049e0 = icmp ugt i8 %v8_4049e0, 15
  %v10_4049e0 = icmp ult i8 %v5_4049e0, %v2_4049e0
  %v11_4049e0 = xor i8 %v5_4049e0, %v2_4049e0
  %v12_4049e0 = xor i8 %v5_4049e0, %v4_4049e0
  %v13_4049e0 = and i8 %v11_4049e0, %v12_4049e0
  %v14_4049e0 = icmp slt i8 %v13_4049e0, 0
  store i1 %v9_4049e0, i1* %az.global-to-local, align 1
  store i1 %v10_4049e0, i1* %cf.global-to-local, align 1
  store i1 %v14_4049e0, i1* %of.global-to-local, align 1
  %v15_4049e0 = icmp eq i8 %v5_4049e0, 0
  store i1 %v15_4049e0, i1* %zf.global-to-local, align 1
  %v16_4049e0 = icmp slt i8 %v5_4049e0, 0
  store i1 %v16_4049e0, i1* %sf.global-to-local, align 1
  store i8 %v5_4049e0, i8* %v1_4049e0, align 1
  %v0_4049e2 = load i32, i32* @eax, align 4
  %v1_4049e2 = inttoptr i32 %v0_4049e2 to i8*
  %v2_4049e2 = load i8, i8* %v1_4049e2, align 1
  %v4_4049e2 = trunc i32 %v0_4049e2 to i8
  %v5_4049e2 = add i8 %v2_4049e2, %v4_4049e2
  %v6_4049e2 = urem i8 %v2_4049e2, 16
  %v7_4049e2 = urem i8 %v4_4049e2, 16
  %v8_4049e2 = add nuw nsw i8 %v6_4049e2, %v7_4049e2
  %v9_4049e2 = icmp ugt i8 %v8_4049e2, 15
  %v10_4049e2 = icmp ult i8 %v5_4049e2, %v2_4049e2
  %v11_4049e2 = xor i8 %v5_4049e2, %v2_4049e2
  %v12_4049e2 = xor i8 %v5_4049e2, %v4_4049e2
  %v13_4049e2 = and i8 %v11_4049e2, %v12_4049e2
  %v14_4049e2 = icmp slt i8 %v13_4049e2, 0
  store i1 %v9_4049e2, i1* %az.global-to-local, align 1
  store i1 %v10_4049e2, i1* %cf.global-to-local, align 1
  store i1 %v14_4049e2, i1* %of.global-to-local, align 1
  %v15_4049e2 = icmp eq i8 %v5_4049e2, 0
  store i1 %v15_4049e2, i1* %zf.global-to-local, align 1
  %v16_4049e2 = icmp slt i8 %v5_4049e2, 0
  store i1 %v16_4049e2, i1* %sf.global-to-local, align 1
  store i8 %v5_4049e2, i8* %v1_4049e2, align 1
  %v0_4049e4 = load i32, i32* @eax, align 4
  %v1_4049e4 = inttoptr i32 %v0_4049e4 to i8*
  %v2_4049e4 = load i8, i8* %v1_4049e4, align 1
  %v4_4049e4 = trunc i32 %v0_4049e4 to i8
  %v5_4049e4 = add i8 %v2_4049e4, %v4_4049e4
  store i8 %v5_4049e4, i8* %v1_4049e4, align 1
  %v0_4049e6 = load i32, i32* @eax, align 4
  %v2_4049e6 = inttoptr i32 %v0_4049e6 to i32*
  %v3_4049e6 = load i32, i32* %v2_4049e6, align 4
  %v4_4049e6 = sub i32 %v0_4049e6, %v3_4049e6
  %v5_4049e6 = urem i32 %v0_4049e6, 16
  %v6_4049e6 = urem i32 %v3_4049e6, 16
  %v7_4049e6 = sub nsw i32 %v5_4049e6, %v6_4049e6
  %v8_4049e6 = icmp ugt i32 %v7_4049e6, 15
  %v9_4049e6 = icmp ult i32 %v0_4049e6, %v3_4049e6
  %v10_4049e6 = xor i32 %v3_4049e6, %v0_4049e6
  %v11_4049e6 = xor i32 %v4_4049e6, %v0_4049e6
  %v12_4049e6 = and i32 %v11_4049e6, %v10_4049e6
  %v13_4049e6 = icmp slt i32 %v12_4049e6, 0
  store i1 %v8_4049e6, i1* %az.global-to-local, align 1
  store i1 %v9_4049e6, i1* %cf.global-to-local, align 1
  store i1 %v13_4049e6, i1* %of.global-to-local, align 1
  %v14_4049e6 = icmp eq i32 %v4_4049e6, 0
  store i1 %v14_4049e6, i1* %zf.global-to-local, align 1
  %v15_4049e6 = icmp slt i32 %v4_4049e6, 0
  store i1 %v15_4049e6, i1* %sf.global-to-local, align 1
  %v0_4049e8 = load i32, i32* @ebx, align 4
  %v1_4049e8 = inttoptr i32 %v0_4049e8 to i8*
  %v2_4049e8 = load i8, i8* %v1_4049e8, align 1
  %v4_4049e8 = trunc i32 %v0_4049e6 to i8
  %v5_4049e8 = add i8 %v2_4049e8, %v4_4049e8
  store i8 %v5_4049e8, i8* %v1_4049e8, align 1
  %v0_4049ea = load i32, i32* @eax, align 4
  %v2_4049ea = inttoptr i32 %v0_4049ea to i32*
  %v3_4049ea = load i32, i32* %v2_4049ea, align 4
  %v4_4049ea = and i32 %v3_4049ea, %v0_4049ea
  store i32 %v4_4049ea, i32* @eax, align 4
  %v2_4049ec = load i32, i32* %stack_var_36, align 4
  store i32 %v2_4049ec, i32* @ebx, align 4
  %v2_4049ed = load i32, i32* bitcast (i16* @es to i32*), align 4
  %v4_4049ed = add i32 %v2_4049ed, %v22_404854
  %v5_4049ed = urem i32 %v2_4049ed, 16
  %v6_4049ed = and i32 %v22_404854, 12
  %v7_4049ed = add nuw nsw i32 %v5_4049ed, %v6_4049ed
  %v8_4049ed = icmp ugt i32 %v7_4049ed, 15
  %v9_4049ed = icmp ult i32 %v4_4049ed, %v2_4049ed
  %v10_4049ed = xor i32 %v4_4049ed, %v2_4049ed
  %v11_4049ed = xor i32 %v4_4049ed, %v22_404854
  %v12_4049ed = and i32 %v10_4049ed, %v11_4049ed
  %v13_4049ed = icmp slt i32 %v12_4049ed, 0
  store i1 %v8_4049ed, i1* %az.global-to-local, align 1
  store i1 %v9_4049ed, i1* %cf.global-to-local, align 1
  store i1 %v13_4049ed, i1* %of.global-to-local, align 1
  %v14_4049ed = icmp eq i32 %v4_4049ed, 0
  store i1 %v14_4049ed, i1* %zf.global-to-local, align 1
  %v15_4049ed = icmp slt i32 %v4_4049ed, 0
  store i1 %v15_4049ed, i1* %sf.global-to-local, align 1
  %v21_4049ed = inttoptr i32 %v2_4049ec to i32*
  store i32 %v4_4049ed, i32* %v21_4049ed, align 4
  %v0_4049ef = load i32, i32* @ecx, align 4
  %v1_4049ef = load i32, i32* @eax, align 4
  %v3_4049ef = add i32 %v0_4049ef, 1828716544
  %v4_4049ef = add i32 %v3_4049ef, %v1_4049ef
  %v5_4049ef = inttoptr i32 %v4_4049ef to i8*
  %v6_4049ef = load i8, i8* %v5_4049ef, align 1
  %v8_4049ef = trunc i32 %v0_4049ef to i8
  %v9_4049ef = add i8 %v6_4049ef, %v8_4049ef
  %v14_4049ef = icmp ult i8 %v9_4049ef, %v6_4049ef
  store i1 %v14_4049ef, i1* %cf.global-to-local, align 1
  store i8 %v9_4049ef, i8* %v5_4049ef, align 1
  %v9_4049f6 = load i32, i32* %stack_var_40, align 4
  %v21_4049f6 = load i32, i32* %stack_var_68, align 4
  store i32 %v9_4049f6, i32* @edi, align 4
  store i32 %v22_404963, i32* @esi, align 4
  store i32 %v20_404963, i32* @ebp, align 4
  store i32 %v16_404963, i32* @ebx, align 4
  store i32 %v12_404963, i32* @ecx, align 4
  store i32 %v21_4049f6, i32* @eax, align 4
  %v1_4049f9 = add i32 %v14_404963, 1
  %v2_4049f9 = urem i32 %v14_404963, 16
  %v4_4049f9 = icmp eq i32 %v2_4049f9, 15
  %tmp477 = xor i32 %v14_404963, -2147483648
  %v7_4049f9 = and i32 %v1_4049f9, %tmp477
  %v8_4049f9 = icmp slt i32 %v7_4049f9, 0
  store i1 %v4_4049f9, i1* %az.global-to-local, align 1
  store i1 %v8_4049f9, i1* %of.global-to-local, align 1
  %v9_4049f9 = icmp eq i32 %v1_4049f9, 0
  store i1 %v9_4049f9, i1* %zf.global-to-local, align 1
  %v10_4049f9 = icmp slt i32 %v1_4049f9, 0
  store i1 %v10_4049f9, i1* %sf.global-to-local, align 1
  store i32 %v1_4049f9, i32* @edx, align 4
  %v1_4049fa = trunc i32 %v1_4049f9 to i16
  %v3_4049fa = inttoptr i32 %v22_404963 to i32*
  %v4_4049fa = load i32, i32* %v3_4049fa, align 4
  call void @__asm_outsd(i16 %v1_4049fa, i32 %v4_4049fa)
  %v2_4049fb = load i32, i32* @esi, align 4
  %v3_4049fb = inttoptr i32 %v2_4049fb to i32*
  %v4_4049fb = load i32, i32* %v3_4049fb, align 4
  call void @__asm_outsd(i16 %v1_4049fa, i32 %v4_4049fb)
  %v2_4049fc = call i8 @__asm_insb(i16 %v1_4049fa)
  %v3_4049fc = load i32, i32* @edi, align 4
  %v4_4049fc = inttoptr i32 %v3_4049fc to i8*
  store i8 %v2_4049fc, i8* %v4_4049fc, align 1
  %v9_4049fd = load i32, i32* %stack_var_72, align 4
  store i32 %v9_4049fd, i32* @edi, align 4
  store i32 %v22_4048d4, i32* @esi, align 4
  store i32 %v20_4048d4, i32* @ebp, align 4
  store i32 %v16_4048d4, i32* @ebx, align 4
  store i32 %v14_4048d4, i32* @edx, align 4
  store i32 %v12_4048d4, i32* @ecx, align 4
  store i32 %v10_4048d4, i32* @eax, align 4
  %v1_4049ff = trunc i32 %v14_4048d4 to i16
  %v3_4049ff = inttoptr i32 %v22_4048d4 to i8*
  %v4_4049ff = load i8, i8* %v3_4049ff, align 1
  call void @__asm_outsb(i16 %v1_4049ff, i8 %v4_4049ff)
  %v0_404a00 = load i32, i32* @ebx, align 4
  %v1_404a00 = add i32 %v0_404a00, 121
  %v2_404a00 = inttoptr i32 %v1_404a00 to i8*
  %v3_404a00 = load i8, i8* %v2_404a00, align 1
  %v5_404a00 = trunc i32 %v14_4048d4 to i8
  %v6_404a00 = add i8 %v3_404a00, %v5_404a00
  %v7_404a00 = urem i8 %v3_404a00, 16
  %v8_404a00 = urem i8 %v5_404a00, 16
  %v9_404a00 = add nuw nsw i8 %v7_404a00, %v8_404a00
  %v10_404a00 = icmp ugt i8 %v9_404a00, 15
  %v11_404a00 = icmp ult i8 %v6_404a00, %v3_404a00
  %v12_404a00 = xor i8 %v6_404a00, %v3_404a00
  %v13_404a00 = xor i8 %v6_404a00, %v5_404a00
  %v14_404a00 = and i8 %v12_404a00, %v13_404a00
  %v15_404a00 = icmp slt i8 %v14_404a00, 0
  store i1 %v10_404a00, i1* %az.global-to-local, align 1
  store i1 %v11_404a00, i1* %cf.global-to-local, align 1
  store i1 %v15_404a00, i1* %of.global-to-local, align 1
  %v16_404a00 = icmp eq i8 %v6_404a00, 0
  store i1 %v16_404a00, i1* %zf.global-to-local, align 1
  %v17_404a00 = icmp slt i8 %v6_404a00, 0
  store i1 %v17_404a00, i1* %sf.global-to-local, align 1
  store i8 %v6_404a00, i8* %v2_404a00, align 1
  %v0_404a03 = load i1, i1* %cf.global-to-local, align 1
  %v1_404a03 = icmp eq i1 %v0_404a03, false
  br i1 %v1_404a03, label %dec_label_pc_404a79, label %dec_label_pc_404a05

dec_label_pc_404a05:                              ; preds = %dec_label_pc_4048c4
  %v2_404a05 = call i32 @__asm_insd(i16 %v1_4049ff)
  %v3_404a05 = load i32, i32* @edi, align 4
  %v4_404a05 = inttoptr i32 %v3_404a05 to i32*
  store i32 %v2_404a05, i32* %v4_404a05, align 4
  %v1_404a07 = add i32 %v20_4048d4, 115
  %v2_404a07 = inttoptr i32 %v1_404a07 to i8*
  %v3_404a07 = load i8, i8* %v2_404a07, align 1
  %v4_404a07 = load i32, i32* @ecx, align 4
  %v5_404a07 = udiv i32 %v4_404a07, 256
  %v6_404a07 = trunc i32 %v5_404a07 to i8
  %v7_404a07 = add i8 %v3_404a07, %v6_404a07
  %v8_404a07 = urem i8 %v3_404a07, 16
  %v9_404a07 = urem i8 %v6_404a07, 16
  %v10_404a07 = add nuw nsw i8 %v9_404a07, %v8_404a07
  %v11_404a07 = icmp ugt i8 %v10_404a07, 15
  %v12_404a07 = icmp ult i8 %v7_404a07, %v3_404a07
  %v13_404a07 = xor i8 %v7_404a07, %v3_404a07
  %v14_404a07 = xor i8 %v7_404a07, %v6_404a07
  %v15_404a07 = and i8 %v13_404a07, %v14_404a07
  %v16_404a07 = icmp slt i8 %v15_404a07, 0
  store i1 %v11_404a07, i1* %az.global-to-local, align 1
  store i1 %v12_404a07, i1* %cf.global-to-local, align 1
  store i1 %v16_404a07, i1* %of.global-to-local, align 1
  %v17_404a07 = icmp eq i8 %v7_404a07, 0
  store i1 %v17_404a07, i1* %zf.global-to-local, align 1
  %v18_404a07 = icmp slt i8 %v7_404a07, 0
  store i1 %v18_404a07, i1* %sf.global-to-local, align 1
  store i8 %v7_404a07, i8* %v2_404a07, align 1
  %v0_404a0a = load i32, i32* @edi, align 4
  %v1_404a0a = add i32 %v0_404a0a, 114
  %v2_404a0a = inttoptr i32 %v1_404a0a to i16*
  %v3_404a0a = load i16, i16* %v2_404a0a, align 2
  %v5_404a0a = trunc i32 %v20_4048d4 to i16
  call void @__asm_arpl(i16 %v3_404a0a, i16 %v5_404a0a)
  %v2_404a0d = call i8 @__asm_insb(i16 %v1_4049ff)
  %v3_404a0d = load i32, i32* @edi, align 4
  %v4_404a0d = inttoptr i32 %v3_404a0d to i8*
  store i8 %v2_404a0d, i8* %v4_404a0d, align 1
  %v2_404a0e = inttoptr i32 %v14_4048d4 to i32*
  %v3_404a0e = load i32, i32* %v2_404a0e, align 4
  %v5_404a0e = mul i32 %v3_404a0e, 1953396050
  %v2_404a15 = add i32 %v20_4048d4, 101
  %v3_404a15 = inttoptr i32 %v2_404a15 to i32*
  %v4_404a15 = load i32, i32* %v3_404a15, align 4
  %v6_404a15 = mul i32 %v4_404a15, 1886220099
  store i32 %v6_404a15, i32* @ebp, align 4
  store i1 false, i1* %of.global-to-local, align 1
  store i1 false, i1* %cf.global-to-local, align 1
  %v1_404a1c = add i32 %v5_404a0e, 4
  %v2_404a1c = add i32 %v5_404a0e, 8
  %v3_404a1c = add i32 %v5_404a0e, 16
  %v4_404a1c = add i32 %v5_404a0e, 20
  %v5_404a1c = add i32 %v5_404a0e, 24
  %v6_404a1c = add i32 %v5_404a0e, 28
  %v8_404a1c = inttoptr i32 %v5_404a0e to i32*
  %v9_404a1c = load i32, i32* %v8_404a1c, align 4
  %v10_404a1c = inttoptr i32 %v1_404a1c to i32*
  %v11_404a1c = load i32, i32* %v10_404a1c, align 4
  %v12_404a1c = inttoptr i32 %v2_404a1c to i32*
  %v13_404a1c = load i32, i32* %v12_404a1c, align 4
  %v14_404a1c = inttoptr i32 %v3_404a1c to i32*
  %v15_404a1c = load i32, i32* %v14_404a1c, align 4
  %v16_404a1c = inttoptr i32 %v4_404a1c to i32*
  %v17_404a1c = load i32, i32* %v16_404a1c, align 4
  %v18_404a1c = inttoptr i32 %v5_404a1c to i32*
  %v19_404a1c = load i32, i32* %v18_404a1c, align 4
  %v20_404a1c = inttoptr i32 %v6_404a1c to i32*
  %v21_404a1c = load i32, i32* %v20_404a1c, align 4
  store i32 %v9_404a1c, i32* @edi, align 4
  store i32 %v11_404a1c, i32* @esi, align 4
  store i32 %v13_404a1c, i32* @ebp, align 4
  store i32 %v15_404a1c, i32* @ebx, align 4
  %v22_404a1c = trunc i32 %v17_404a1c to i16
  store i32 %v17_404a1c, i32* @edx, align 4
  store i32 %v19_404a1c, i32* @ecx, align 4
  store i32 %v21_404a1c, i32* @eax, align 4
  %v0_404a1d = load i1, i1* %zf.global-to-local, align 1
  br i1 %v0_404a1d, label %dec_label_pc_404a88, label %dec_label_pc_404a1f

dec_label_pc_404a1f:                              ; preds = %dec_label_pc_404a05
  %v1_404a1f = add i32 %v19_404a1c, 108
  %v2_404a1f = inttoptr i32 %v1_404a1f to i64*
  %v3_404a1f = load i64, i64* %v2_404a1f, align 4
  %v4_404a1f = call i32 @__asm_bound(i64 %v3_404a1f)
  store i32 %v4_404a1f, i32* @ebp, align 4
  %v1_404a22 = load i32, i32* @ecx, align 4
  %v2_404a22 = load i32, i32* @edi, align 4
  %v3_404a22 = mul i32 %v2_404a22, 2
  %v4_404a22 = add i32 %v1_404a22, 65
  %v5_404a22 = add i32 %v4_404a22, %v3_404a22
  %v6_404a22 = inttoptr i32 %v5_404a22 to i32*
  %v7_404a22 = load i32, i32* %v6_404a22, align 4
  %v9_404a22 = mul i32 %v7_404a22, 1769108596
  store i32 %v9_404a22, i32* @esi, align 4
  store i1 false, i1* %of.global-to-local, align 1
  store i1 false, i1* %cf.global-to-local, align 1
  %v1_404a2a = add i32 %v4_404a1f, 116
  %v2_404a2a = inttoptr i32 %v1_404a2a to i64*
  %v3_404a2a = load i64, i64* %v2_404a2a, align 4
  %v4_404a2a = call i32 @__asm_bound(i64 %v3_404a2a)
  store i32 %v4_404a2a, i32* @esi, align 4
  %v0_404a2d = load i32, i32* @ebx, align 4
  %v1_404a2d = add i32 %v0_404a2d, 121
  %v3_404a2d = call i8 @__readgsbyte(i32 %v1_404a2d)
  %tmp478 = trunc i32 %v17_404a1c to i8
  %v7_404a2d = add i8 %v3_404a2d, %tmp478
  %v8_404a2d = urem i8 %v3_404a2d, 16
  %v9_404a2d = urem i8 %tmp478, 16
  %v10_404a2d = add nuw nsw i8 %v8_404a2d, %v9_404a2d
  %v11_404a2d = icmp ugt i8 %v10_404a2d, 15
  %v12_404a2d = icmp ult i8 %v7_404a2d, %v3_404a2d
  %v13_404a2d = xor i8 %v7_404a2d, %v3_404a2d
  %v14_404a2d = xor i8 %v7_404a2d, %tmp478
  %v15_404a2d = and i8 %v13_404a2d, %v14_404a2d
  %v16_404a2d = icmp slt i8 %v15_404a2d, 0
  store i1 %v11_404a2d, i1* %az.global-to-local, align 1
  store i1 %v12_404a2d, i1* %cf.global-to-local, align 1
  store i1 %v16_404a2d, i1* %of.global-to-local, align 1
  %v17_404a2d = icmp eq i8 %v7_404a2d, 0
  store i1 %v17_404a2d, i1* %zf.global-to-local, align 1
  %v18_404a2d = icmp slt i8 %v7_404a2d, 0
  store i1 %v18_404a2d, i1* %sf.global-to-local, align 1
  %v22_404a2d = load i32, i32* @ebx, align 4
  %v23_404a2d = add i32 %v22_404a2d, 121
  call void @__writegsbyte(i32 %v23_404a2d, i8 %v7_404a2d)
  %v0_404a31 = load i1, i1* %cf.global-to-local, align 1
  %v1_404a31 = icmp eq i1 %v0_404a31, false
  br i1 %v1_404a31, label %dec_label_pc_404aac, label %dec_label_pc_404a33

dec_label_pc_404a33:                              ; preds = %dec_label_pc_404a1f
  %v3_404a33 = call i32 @__asm_insd(i16 %v22_404a1c)
  %v4_404a33 = load i32, i32* @edi, align 4
  %v5_404a33 = inttoptr i32 %v4_404a33 to i32*
  store i32 %v3_404a33, i32* %v5_404a33, align 4
  %sext31 = mul i32 %v17_404a1c, 65536
  %v1_404a35 = sdiv i32 %sext31, 65536
  %v2_404a35 = load i32, i32* @esp, align 4
  %v3_404a35 = add i32 %v2_404a35, -4
  %v4_404a35 = inttoptr i32 %v3_404a35 to i32*
  store i32 %v1_404a35, i32* %v4_404a35, align 4
  %v0_404a37 = load i1, i1* %zf.global-to-local, align 1
  %v1_404a37 = icmp eq i1 %v0_404a37, false
  br i1 %v1_404a37, label %dec_label_pc_404aac, label %dec_label_pc_404aa4

dec_label_pc_404a79:                              ; preds = %dec_label_pc_4048c4
  %v0_404a79 = load i32, i32* @ecx, align 4
  %v1_404a79 = add i32 %v0_404a79, 110
  %v2_404a79 = inttoptr i32 %v1_404a79 to i8*
  %v3_404a79 = load i8, i8* %v2_404a79, align 1
  %v5_404a79 = trunc i32 %v0_404a79 to i8
  %v6_404a79 = add i8 %v3_404a79, %v5_404a79
  %v7_404a79 = urem i8 %v3_404a79, 16
  %v8_404a79 = urem i8 %v5_404a79, 16
  %v9_404a79 = add nuw nsw i8 %v7_404a79, %v8_404a79
  %v10_404a79 = icmp ugt i8 %v9_404a79, 15
  %v11_404a79 = icmp ult i8 %v6_404a79, %v3_404a79
  %v12_404a79 = xor i8 %v6_404a79, %v3_404a79
  %v13_404a79 = xor i8 %v6_404a79, %v5_404a79
  %v14_404a79 = and i8 %v12_404a79, %v13_404a79
  %v15_404a79 = icmp slt i8 %v14_404a79, 0
  store i1 %v10_404a79, i1* %az.global-to-local, align 1
  store i1 %v11_404a79, i1* %cf.global-to-local, align 1
  store i1 %v15_404a79, i1* %of.global-to-local, align 1
  %v16_404a79 = icmp eq i8 %v6_404a79, 0
  store i1 %v16_404a79, i1* %zf.global-to-local, align 1
  %v17_404a79 = icmp slt i8 %v6_404a79, 0
  store i1 %v17_404a79, i1* %sf.global-to-local, align 1
  store i8 %v6_404a79, i8* %v2_404a79, align 1
  %v0_404a7c = load i1, i1* %zf.global-to-local, align 1
  br i1 %v0_404a7c, label %dec_label_pc_404ab1, label %dec_label_pc_404a7e

dec_label_pc_404a7e:                              ; preds = %dec_label_pc_404a79
  %v0_404a7e = load i32, i32* @eax, align 4
  %v1_404a7e = trunc i32 %v0_404a7e to i8
  %v3_404a7e = inttoptr i32 %v0_404a7e to i8*
  %v4_404a7e = load i8, i8* %v3_404a7e, align 1
  %v5_404a7e = xor i8 %v4_404a7e, %v1_404a7e
  store i1 false, i1* %az.global-to-local, align 1
  store i1 false, i1* %cf.global-to-local, align 1
  store i1 false, i1* %of.global-to-local, align 1
  %v6_404a7e = icmp eq i8 %v5_404a7e, 0
  store i1 %v6_404a7e, i1* %zf.global-to-local, align 1
  %v7_404a7e = icmp slt i8 %v5_404a7e, 0
  store i1 %v7_404a7e, i1* %sf.global-to-local, align 1
  %v11_404a7e = zext i8 %v5_404a7e to i32
  %v13_404a7e = and i32 %v0_404a7e, -256
  %v14_404a7e = or i32 %v13_404a7e, %v11_404a7e
  store i32 %v14_404a7e, i32* @eax, align 4
  %v2_404a80 = call i32 @__asm_insd(i16 %v1_4049ff)
  %v3_404a80 = load i32, i32* @edi, align 4
  %v4_404a80 = inttoptr i32 %v3_404a80 to i32*
  store i32 %v2_404a80, i32* %v4_404a80, align 4
  %v9_404a81 = load i32, i32* %stack_var_104, align 4
  store i32 %v9_404a81, i32* @edi, align 4
  store i32 %arg26, i32* @esi, align 4
  store i32 %arg27, i32* @ebp, align 4
  store i32 %arg28, i32* @ebx, align 4
  store i32 %arg29, i32* @edx, align 4
  store i32 %arg30, i32* @ecx, align 4
  store i32 %arg31, i32* @eax, align 4
  %v0_404a82 = load i1, i1* %sf.global-to-local, align 1
  br i1 %v0_404a82, label %dec_label_pc_404ab2, label %dec_label_pc_404838

dec_label_pc_404a88:                              ; preds = %dec_label_pc_404a05
  %v1_404a88 = trunc i32 %v21_404a1c to i8
  %v2_404a88 = add i8 %v1_404a88, -77
  %v3_404a88 = urem i8 %v1_404a88, 16
  %v4_404a88 = add nsw i8 %v3_404a88, -13
  %v5_404a88 = icmp ugt i8 %v4_404a88, 15
  %v6_404a88 = icmp ult i8 %v1_404a88, 77
  %tmp479 = sub i8 76, %v1_404a88
  %v9_404a88 = and i8 %tmp479, %v1_404a88
  %v10_404a88 = icmp slt i8 %v9_404a88, 0
  store i1 %v5_404a88, i1* %az.global-to-local, align 1
  store i1 %v6_404a88, i1* %cf.global-to-local, align 1
  store i1 %v10_404a88, i1* %of.global-to-local, align 1
  %v11_404a88 = icmp eq i8 %v2_404a88, 0
  store i1 %v11_404a88, i1* %zf.global-to-local, align 1
  %v12_404a88 = icmp slt i8 %v2_404a88, 0
  store i1 %v12_404a88, i1* %sf.global-to-local, align 1
  %v4_404a8a = inttoptr i32 %v11_404a1c to i32*
  %v5_404a8a = load i32, i32* %v4_404a8a, align 4
  call void @__asm_outsd(i16 %v22_404a1c, i32 %v5_404a8a)
  %v0_404a8b = load i1, i1* %zf.global-to-local, align 1
  %v1_404a8b = icmp eq i1 %v0_404a8b, false
  br i1 %v1_404a8b, label %dec_label_pc_404afa, label %dec_label_pc_404a9a

dec_label_pc_404a9a:                              ; preds = %dec_label_pc_404a88
  %v0_404a8e = load i32, i32* @ecx, align 4
  %v1_404a8e = add i32 %v0_404a8e, 110
  %v2_404a8e = inttoptr i32 %v1_404a8e to i8*
  %v3_404a8e = load i8, i8* %v2_404a8e, align 1
  %tmp480 = trunc i32 %v17_404a1c to i8
  %v7_404a8e = add i8 %v3_404a8e, %tmp480
  %v8_404a8e = urem i8 %v3_404a8e, 16
  %v9_404a8e = urem i8 %tmp480, 16
  %v10_404a8e = add nuw nsw i8 %v8_404a8e, %v9_404a8e
  %v11_404a8e = icmp ugt i8 %v10_404a8e, 15
  %v12_404a8e = icmp ult i8 %v7_404a8e, %v3_404a8e
  %v13_404a8e = xor i8 %v7_404a8e, %v3_404a8e
  %v14_404a8e = xor i8 %v7_404a8e, %tmp480
  %v15_404a8e = and i8 %v13_404a8e, %v14_404a8e
  %v16_404a8e = icmp slt i8 %v15_404a8e, 0
  store i1 %v11_404a8e, i1* %az.global-to-local, align 1
  store i1 %v12_404a8e, i1* %cf.global-to-local, align 1
  store i1 %v16_404a8e, i1* %of.global-to-local, align 1
  %v17_404a8e = icmp eq i8 %v7_404a8e, 0
  store i1 %v17_404a8e, i1* %zf.global-to-local, align 1
  %v18_404a8e = icmp slt i8 %v7_404a8e, 0
  store i1 %v18_404a8e, i1* %sf.global-to-local, align 1
  store i8 %v7_404a8e, i8* %v2_404a8e, align 1
  %v0_404a93 = load i32, i32* @esi, align 4
  %v1_404a93 = load i32, i32* @esp, align 4
  %v2_404a93 = add i32 %v1_404a93, -4
  %v3_404a93 = inttoptr i32 %v2_404a93 to i32*
  store i32 %v0_404a93, i32* %v3_404a93, align 4
  %sext46 = mul i32 %v17_404a1c, 65536
  %v1_404a94 = sdiv i32 %sext46, 65536
  %v3_404a94 = add i32 %v1_404a93, -8
  %v4_404a94 = inttoptr i32 %v3_404a94 to i32*
  store i32 %v1_404a94, i32* %v4_404a94, align 4
  %v3_404a95 = load i32, i32* @esi, align 4
  %v5_404a95 = call i8 @__readgsbyte(i32 %v3_404a95)
  call void @__asm_outsb(i16 %v22_404a1c, i8 %v5_404a95)
  %v0_404a97 = load i32, i32* @eax, align 4
  store i1 false, i1* %cf.global-to-local, align 1
  %v11_404a97 = xor i32 %v0_404a97, 110
  store i32 %v11_404a97, i32* @eax, align 4
  %v0_404a99 = load i32, i32* @ebp, align 4
  %v1_404a99 = add i32 %v0_404a99, -1
  %v2_404a99 = urem i32 %v0_404a99, 16
  %v3_404a99 = add nsw i32 %v2_404a99, -1
  %v4_404a99 = icmp ugt i32 %v3_404a99, 15
  %tmp481 = sub i32 0, %v0_404a99
  %v7_404a99 = and i32 %v0_404a99, %tmp481
  %v8_404a99 = icmp slt i32 %v7_404a99, 0
  store i1 %v4_404a99, i1* %az.global-to-local, align 1
  store i1 %v8_404a99, i1* %of.global-to-local, align 1
  %v9_404a99 = icmp eq i32 %v1_404a99, 0
  store i1 %v9_404a99, i1* %zf.global-to-local, align 1
  %v10_404a99 = icmp slt i32 %v1_404a99, 0
  store i1 %v10_404a99, i1* %sf.global-to-local, align 1
  store i32 %v1_404a99, i32* @ebp, align 4
  %v0_404a9a = load i32, i32* @esp, align 4
  %v1_404a9a = add i32 %v0_404a9a, 4
  %v2_404a9a = add i32 %v0_404a9a, 8
  %v3_404a9a = add i32 %v0_404a9a, 16
  %v4_404a9a = add i32 %v0_404a9a, 20
  %v5_404a9a = add i32 %v0_404a9a, 24
  %v6_404a9a = add i32 %v0_404a9a, 28
  %v7_404a9a = add i32 %v0_404a9a, 32
  %v8_404a9a = inttoptr i32 %v0_404a9a to i32*
  %v9_404a9a = load i32, i32* %v8_404a9a, align 4
  %v10_404a9a = inttoptr i32 %v1_404a9a to i32*
  %v11_404a9a = load i32, i32* %v10_404a9a, align 4
  %v12_404a9a = inttoptr i32 %v2_404a9a to i32*
  %v13_404a9a = load i32, i32* %v12_404a9a, align 4
  %v14_404a9a = inttoptr i32 %v3_404a9a to i32*
  %v15_404a9a = load i32, i32* %v14_404a9a, align 4
  %v16_404a9a = inttoptr i32 %v4_404a9a to i32*
  %v17_404a9a = load i32, i32* %v16_404a9a, align 4
  %v18_404a9a = inttoptr i32 %v5_404a9a to i32*
  %v19_404a9a = load i32, i32* %v18_404a9a, align 4
  %v20_404a9a = inttoptr i32 %v6_404a9a to i32*
  %v21_404a9a = load i32, i32* %v20_404a9a, align 4
  store i32 %v9_404a9a, i32* @edi, align 4
  store i32 %v11_404a9a, i32* @esi, align 4
  store i32 %v13_404a9a, i32* @ebp, align 4
  store i32 %v15_404a9a, i32* @ebx, align 4
  store i32 %v17_404a9a, i32* @edx, align 4
  store i32 %v19_404a9a, i32* @ecx, align 4
  store i32 %v21_404a9a, i32* @eax, align 4
  br i1 %v9_404a99, label %dec_label_pc_404af2, label %dec_label_pc_404a9d

dec_label_pc_404a9d:                              ; preds = %dec_label_pc_404a9a
  %v1_404a9d = add i32 %v11_404a9a, 65
  %v2_404a9d = inttoptr i32 %v1_404a9d to i32*
  %v3_404a9d = load i32, i32* %v2_404a9d, align 4
  %v5_404a9d = xor i32 %v3_404a9d, %v13_404a9a
  store i1 false, i1* %az.global-to-local, align 1
  store i1 false, i1* %cf.global-to-local, align 1
  store i1 false, i1* %of.global-to-local, align 1
  %v6_404a9d = icmp eq i32 %v5_404a9d, 0
  store i1 %v6_404a9d, i1* %zf.global-to-local, align 1
  %v7_404a9d = icmp slt i32 %v5_404a9d, 0
  store i1 %v7_404a9d, i1* %sf.global-to-local, align 1
  store i32 %v5_404a9d, i32* %v2_404a9d, align 4
  %sext45 = mul i32 %v7_404a9a, 65536
  %v1_404aa0 = sdiv i32 %sext45, 65536
  %v2_404aa0 = add nsw i32 %v1_404aa0, -2
  %v3_404aa0 = inttoptr i32 %v2_404aa0 to i16*
  store i16 97, i16* %v3_404aa0, align 2
  br label %dec_label_pc_404aa4

dec_label_pc_404aa4:                              ; preds = %dec_label_pc_404a33, %dec_label_pc_404a9d
  %v0_404aa4 = load i32, i32* @edi, align 4
  %v1_404aa4 = add i32 %v0_404aa4, -1
  %v2_404aa4 = urem i32 %v0_404aa4, 16
  %v3_404aa4 = add nsw i32 %v2_404aa4, -1
  %v4_404aa4 = icmp ugt i32 %v3_404aa4, 15
  %tmp482 = sub i32 0, %v0_404aa4
  %v7_404aa4 = and i32 %v0_404aa4, %tmp482
  %v8_404aa4 = icmp slt i32 %v7_404aa4, 0
  store i1 %v4_404aa4, i1* %az.global-to-local, align 1
  store i1 %v8_404aa4, i1* %of.global-to-local, align 1
  %v9_404aa4 = icmp eq i32 %v1_404aa4, 0
  store i1 %v9_404aa4, i1* %zf.global-to-local, align 1
  %v10_404aa4 = icmp slt i32 %v1_404aa4, 0
  store i1 %v10_404aa4, i1* %sf.global-to-local, align 1
  store i32 %v1_404aa4, i32* @edi, align 4
  %v15_404aa4 = load i32, i32* @eax, align 4
  ret i32 %v15_404aa4

dec_label_pc_404aac:                              ; preds = %dec_label_pc_404a1f, %dec_label_pc_404a33
  %v0_404aa7 = load i32, i32* @eax, align 4
  %factor = mul i32 %v0_404aa7, 2
  %v4_404aa7 = add i32 %factor, 77
  %v6_404aa7 = call i16 @__readgsword(i32 %v4_404aa7)
  %v7_404aa7 = load i32, i32* @esi, align 4
  %v8_404aa7 = trunc i32 %v7_404aa7 to i16
  call void @__asm_arpl(i16 %v6_404aa7, i16 %v8_404aa7)
  %v0_404aac = load i1, i1* %sf.global-to-local, align 1
  %v1_404aac = icmp eq i1 %v0_404aac, false
  br i1 %v1_404aac, label %dec_label_pc_404aef, label %dec_label_pc_404aae

dec_label_pc_404aae:                              ; preds = %dec_label_pc_404aac
  %v0_404aae = load i1, i1* %of.global-to-local, align 1
  br i1 %v0_404aae, label %dec_label_pc_404b20, label %dec_label_pc_404ab0

dec_label_pc_404ab0:                              ; preds = %dec_label_pc_404aae
  %v3_404ab0 = call i8 @__asm_insb(i16 %v22_404a1c)
  %v4_404ab0 = load i32, i32* @edi, align 4
  %v5_404ab0 = inttoptr i32 %v4_404ab0 to i8*
  store i8 %v3_404ab0, i8* %v5_404ab0, align 1
  br label %dec_label_pc_404ab1

dec_label_pc_404ab1:                              ; preds = %dec_label_pc_404ab0, %dec_label_pc_404a79
  %v6_404ab0 = load i32, i32* @eax, align 4
  ret i32 %v6_404ab0

dec_label_pc_404ab2:                              ; preds = %dec_label_pc_404a7e
  %v22_404a81 = ptrtoint i32* %stack_var_136 to i32
  %v1_404ab2 = add i32 %arg30, 116
  %v2_404ab2 = inttoptr i32 %v1_404ab2 to i16*
  %v3_404ab2 = load i16, i16* %v2_404ab2, align 2
  %v5_404ab2 = trunc i32 %v22_404a81 to i16
  call void @__asm_arpl(i16 %v3_404ab2, i16 %v5_404ab2)
  %v1_404ab5 = load i32, i32* @edi, align 4
  %v2_404ab5 = add i32 %v1_404ab5, 110
  %v3_404ab5 = inttoptr i32 %v2_404ab5 to i32*
  %v4_404ab5 = load i32, i32* %v3_404ab5, align 4
  %v6_404ab5 = mul i32 %v4_404ab5, 7949568
  store i32 %v6_404ab5, i32* @ebp, align 4
  store i1 false, i1* %cf.global-to-local, align 1
  %v0_404abc = load i32, i32* @ecx, align 4
  %v1_404abc = add i32 %v0_404abc, 1
  %v2_404abc = urem i32 %v0_404abc, 16
  %v4_404abc = icmp eq i32 %v2_404abc, 15
  %tmp483 = xor i32 %v0_404abc, -2147483648
  %v7_404abc = and i32 %v1_404abc, %tmp483
  %v8_404abc = icmp slt i32 %v7_404abc, 0
  store i1 %v4_404abc, i1* %az.global-to-local, align 1
  store i1 %v8_404abc, i1* %of.global-to-local, align 1
  %v9_404abc = icmp eq i32 %v1_404abc, 0
  store i1 %v9_404abc, i1* %zf.global-to-local, align 1
  %v10_404abc = icmp slt i32 %v1_404abc, 0
  store i1 %v10_404abc, i1* %sf.global-to-local, align 1
  store i32 %v1_404abc, i32* @ecx, align 4
  %v0_404b2f = load i32, i32* @edx, align 4
  %v1_404b2f = trunc i32 %v0_404b2f to i16
  %v2_404b2f = call i8 @__asm_insb(i16 %v1_404b2f)
  %v3_404b2f = load i32, i32* @edi, align 4
  %v4_404b2f = inttoptr i32 %v3_404b2f to i8*
  store i8 %v2_404b2f, i8* %v4_404b2f, align 1
  br i1 %v8_404abc, label %dec_label_pc_404b30, label %dec_label_pc_404abf

dec_label_pc_404abf:                              ; preds = %dec_label_pc_404ab2
  %v5_404abf = load i32, i32* @eax, align 4
  ret i32 %v5_404abf

dec_label_pc_404ade:                              ; preds = %dec_label_pc_404b52
  %v1_404ad6 = load i32, i32* @esp, align 4
  %v2_404ad6 = add i32 %v1_404ad6, -4
  %v3_404ad6 = inttoptr i32 %v2_404ad6 to i32*
  store i32 %v0_404ad6, i32* %v3_404ad6, align 4
  %v1_404ad7 = load i32, i32* @ebx, align 4
  %v2_404ad7 = add i32 %v1_404ad7, 117
  %v3_404ad7 = inttoptr i32 %v2_404ad7 to i32*
  %v4_404ad7 = load i32, i32* %v3_404ad7, align 4
  %v6_404ad7 = mul i32 %v4_404ad7, 1631743073
  store i32 %v6_404ad7, i32* @esi, align 4
  store i1 false, i1* %of.global-to-local, align 1
  store i1 false, i1* %cf.global-to-local, align 1
  br label %dec_label_pc_404b52

dec_label_pc_404aef:                              ; preds = %dec_label_pc_404aac
  %v0_404aef = load i1, i1* %cf.global-to-local, align 1
  br i1 %v0_404aef, label %dec_label_pc_404aef.dec_label_pc_404b67_crit_edge, label %dec_label_pc_404af1

dec_label_pc_404aef.dec_label_pc_404b67_crit_edge: ; preds = %dec_label_pc_404aef
  %v0_404b67.pre = load i32, i32* @esp, align 4
  %.pre135 = inttoptr i32 %v0_404b67.pre to i32*
  br label %dec_label_pc_404b67

dec_label_pc_404af1:                              ; preds = %dec_label_pc_404aef
  %v1_404aef = load i32, i32* @eax, align 4
  ret i32 %v1_404aef

dec_label_pc_404af2:                              ; preds = %dec_label_pc_404a9a
  %v22_404a9a = trunc i32 %v7_404a9a to i16
  %v1_404af2 = add i32 %v13_404a9a, 115
  %v2_404af2 = inttoptr i32 %v1_404af2 to i16*
  %v3_404af2 = load i16, i16* %v2_404af2, align 2
  call void @__asm_arpl(i16 %v3_404af2, i16 %v22_404a9a)
  %v0_404af5 = load i32, i32* @ebp, align 4
  %v1_404af5 = add i32 %v0_404af5, 105
  %v2_404af5 = inttoptr i32 %v1_404af5 to i8*
  %v3_404af5 = load i8, i8* %v2_404af5, align 1
  %v4_404af5 = load i32, i32* @ecx, align 4
  %v5_404af5 = trunc i32 %v4_404af5 to i8
  %v6_404af5 = add i8 %v3_404af5, %v5_404af5
  %v7_404af5 = urem i8 %v3_404af5, 16
  %v8_404af5 = urem i8 %v5_404af5, 16
  %v9_404af5 = add nuw nsw i8 %v8_404af5, %v7_404af5
  %v10_404af5 = icmp ugt i8 %v9_404af5, 15
  %v11_404af5 = icmp ult i8 %v6_404af5, %v3_404af5
  %v12_404af5 = xor i8 %v6_404af5, %v3_404af5
  %v13_404af5 = xor i8 %v6_404af5, %v5_404af5
  %v14_404af5 = and i8 %v12_404af5, %v13_404af5
  %v15_404af5 = icmp slt i8 %v14_404af5, 0
  store i1 %v10_404af5, i1* %az.global-to-local, align 1
  store i1 %v11_404af5, i1* %cf.global-to-local, align 1
  store i1 %v15_404af5, i1* %of.global-to-local, align 1
  %v16_404af5 = icmp eq i8 %v6_404af5, 0
  store i1 %v16_404af5, i1* %zf.global-to-local, align 1
  %v17_404af5 = icmp slt i8 %v6_404af5, 0
  store i1 %v17_404af5, i1* %sf.global-to-local, align 1
  store i8 %v6_404af5, i8* %v2_404af5, align 1
  %v24_404af5 = load i32, i32* @eax, align 4
  ret i32 %v24_404af5

dec_label_pc_404afa:                              ; preds = %dec_label_pc_404a88
  %v3_404afa = load i32, i32* @esi, align 4
  %v4_404afa = inttoptr i32 %v3_404afa to i32*
  %v5_404afa = load i32, i32* %v4_404afa, align 4
  call void @__asm_outsd(i16 %v22_404a1c, i32 %v5_404afa)
  %v0_404afb = load i1, i1* %cf.global-to-local, align 1
  %v1_404afb = icmp eq i1 %v0_404afb, false
  br i1 %v1_404afb, label %dec_label_pc_404b6c, label %dec_label_pc_404afd

dec_label_pc_404afd:                              ; preds = %dec_label_pc_404afa
  %v0_404afd = load i1, i1* %zf.global-to-local, align 1
  br i1 %v0_404afd, label %bb, label %dec_label_pc_404b00

bb:                                               ; preds = %dec_label_pc_404afd
  %v1_404afd = call i32 @unknown_4b2e()
  store i32 %v1_404afd, i32* @eax, align 4
  br label %dec_label_pc_404b00

dec_label_pc_404b00:                              ; preds = %bb, %dec_label_pc_404afd
  %v0_404b00 = load i32, i32* @esi, align 4
  %v1_404b00 = load i32, i32* @esp, align 4
  %v2_404b00 = add i32 %v1_404b00, -4
  %v3_404b00 = inttoptr i32 %v2_404b00 to i32*
  store i32 %v0_404b00, i32* %v3_404b00, align 4
  %v4_404b00 = load i32, i32* @eax, align 4
  ret i32 %v4_404b00

dec_label_pc_404b20:                              ; preds = %dec_label_pc_404aae
  %v0_404b20 = load i32, i32* @ebp, align 4
  %v1_404b20 = add i32 %v0_404b20, -1
  %v2_404b20 = urem i32 %v0_404b20, 16
  %v3_404b20 = add nsw i32 %v2_404b20, -1
  %v4_404b20 = icmp ugt i32 %v3_404b20, 15
  store i1 %v4_404b20, i1* %az.global-to-local, align 1
  %v9_404b20 = icmp eq i32 %v1_404b20, 0
  store i1 %v9_404b20, i1* %zf.global-to-local, align 1
  %v10_404b20 = icmp slt i32 %v1_404b20, 0
  store i1 %v10_404b20, i1* %sf.global-to-local, align 1
  store i32 %v1_404b20, i32* @ebp, align 4
  %v1_404b21 = load i32, i32* @ebx, align 4
  %v2_404b21 = add i32 %v1_404b21, 114
  %v3_404b21 = inttoptr i32 %v2_404b21 to i32*
  %v4_404b21 = load i32, i32* %v3_404b21, align 4
  store i1 false, i1* %of.global-to-local, align 1
  store i1 false, i1* %cf.global-to-local, align 1
  br i1 %v9_404b20, label %dec_label_pc_404b58, label %dec_label_pc_404b2a

dec_label_pc_404b2a:                              ; preds = %dec_label_pc_404b20
  %v6_404b21 = mul i32 %v4_404b21, 1718580079
  %v0_404b2a = load i32, i32* @esi, align 4
  %v2_404b2a = add i32 %v6_404b21, -4
  %v3_404b2a = inttoptr i32 %v2_404b2a to i32*
  store i32 %v0_404b2a, i32* %v3_404b2a, align 4
  %v4_404b2a = load i32, i32* @eax, align 4
  ret i32 %v4_404b2a

dec_label_pc_404b30:                              ; preds = %dec_label_pc_404ab2
  %v0_404b30 = load i32, i32* @edx, align 4
  %v1_404b30 = add i32 %v0_404b30, 1
  %v2_404b30 = urem i32 %v0_404b30, 16
  %v4_404b30 = icmp eq i32 %v2_404b30, 15
  %tmp484 = xor i32 %v0_404b30, -2147483648
  %v7_404b30 = and i32 %v1_404b30, %tmp484
  %v8_404b30 = icmp slt i32 %v7_404b30, 0
  store i1 %v4_404b30, i1* %az.global-to-local, align 1
  store i1 %v8_404b30, i1* %of.global-to-local, align 1
  %v9_404b30 = icmp eq i32 %v1_404b30, 0
  store i1 %v9_404b30, i1* %zf.global-to-local, align 1
  %v10_404b30 = icmp slt i32 %v1_404b30, 0
  store i1 %v10_404b30, i1* %sf.global-to-local, align 1
  store i32 %v1_404b30, i32* @edx, align 4
  %v0_404b31 = load i32, i32* @esp, align 4
  %v1_404b31 = add i32 %v0_404b31, 4
  %v2_404b31 = add i32 %v0_404b31, 8
  %v3_404b31 = add i32 %v0_404b31, 16
  %v4_404b31 = add i32 %v0_404b31, 20
  %v5_404b31 = add i32 %v0_404b31, 24
  %v6_404b31 = add i32 %v0_404b31, 28
  %v7_404b31 = add i32 %v0_404b31, 32
  %v8_404b31 = inttoptr i32 %v0_404b31 to i32*
  %v9_404b31 = load i32, i32* %v8_404b31, align 4
  %v10_404b31 = inttoptr i32 %v1_404b31 to i32*
  %v11_404b31 = load i32, i32* %v10_404b31, align 4
  %v12_404b31 = inttoptr i32 %v2_404b31 to i32*
  %v13_404b31 = load i32, i32* %v12_404b31, align 4
  %v14_404b31 = inttoptr i32 %v3_404b31 to i32*
  %v15_404b31 = load i32, i32* %v14_404b31, align 4
  %v16_404b31 = inttoptr i32 %v4_404b31 to i32*
  %v17_404b31 = load i32, i32* %v16_404b31, align 4
  %v18_404b31 = inttoptr i32 %v5_404b31 to i32*
  %v19_404b31 = load i32, i32* %v18_404b31, align 4
  %v20_404b31 = inttoptr i32 %v6_404b31 to i32*
  %v21_404b31 = load i32, i32* %v20_404b31, align 4
  store i32 %v9_404b31, i32* @edi, align 4
  store i32 %v11_404b31, i32* @esi, align 4
  store i32 %v13_404b31, i32* @ebp, align 4
  store i32 %v15_404b31, i32* @ebx, align 4
  store i32 %v17_404b31, i32* @edx, align 4
  store i32 %v19_404b31, i32* @ecx, align 4
  store i32 %v21_404b31, i32* @eax, align 4
  %v0_404b32 = load i1, i1* %cf.global-to-local, align 1
  %v1_404b32 = icmp eq i1 %v0_404b32, false
  br i1 %v1_404b32, label %dec_label_pc_404b9d, label %dec_label_pc_404b34

dec_label_pc_404b34:                              ; preds = %dec_label_pc_404b30
  %v1_404b34 = inttoptr i32 %v11_404b31 to i16*
  %v2_404b34 = load i16, i16* %v1_404b34, align 2
  %v4_404b34 = trunc i32 %v13_404b31 to i16
  call void @__asm_arpl(i16 %v2_404b34, i16 %v4_404b34)
  %v0_404b36 = load i32, i32* @esp, align 4
  %v1_404b36 = add i32 %v0_404b36, 1
  %v2_404b36 = urem i32 %v0_404b36, 16
  %v4_404b36 = icmp eq i32 %v2_404b36, 15
  %tmp485 = xor i32 %v0_404b36, -2147483648
  %v7_404b36 = and i32 %v1_404b36, %tmp485
  %v8_404b36 = icmp slt i32 %v7_404b36, 0
  store i1 %v4_404b36, i1* %az.global-to-local, align 1
  store i1 %v8_404b36, i1* %of.global-to-local, align 1
  %v9_404b36 = icmp eq i32 %v1_404b36, 0
  store i1 %v9_404b36, i1* %zf.global-to-local, align 1
  %v10_404b36 = icmp slt i32 %v1_404b36, 0
  store i1 %v10_404b36, i1* %sf.global-to-local, align 1
  %v0_404b37 = load i1, i1* %cf.global-to-local, align 1
  %v2_404b37 = or i1 %v9_404b36, %v0_404b37
  br i1 %v2_404b37, label %dec_label_pc_404ba3, label %dec_label_pc_404b3a

dec_label_pc_404b3a:                              ; preds = %dec_label_pc_404b34
  %v0_404b3a = load i32, i32* @ebp, align 4
  %v1_404b3a = add i32 %v0_404b3a, 115
  %v2_404b3a = inttoptr i32 %v1_404b3a to i16*
  %v3_404b3a = load i16, i16* %v2_404b3a, align 2
  %v5_404b3a = trunc i32 %v1_404b36 to i16
  call void @__asm_arpl(i16 %v3_404b3a, i16 %v5_404b3a)
  %v0_404b3d = load i32, i32* @ebp, align 4
  %v1_404b3d = add i32 %v0_404b3d, 121
  %v2_404b3d = inttoptr i32 %v1_404b3d to i8*
  %v3_404b3d = load i8, i8* %v2_404b3d, align 1
  %v4_404b3d = load i32, i32* @ecx, align 4
  %v5_404b3d = trunc i32 %v4_404b3d to i8
  %v6_404b3d = add i8 %v3_404b3d, %v5_404b3d
  %v7_404b3d = urem i8 %v3_404b3d, 16
  %v8_404b3d = urem i8 %v5_404b3d, 16
  %v9_404b3d = add nuw nsw i8 %v8_404b3d, %v7_404b3d
  %v10_404b3d = icmp ugt i8 %v9_404b3d, 15
  %v11_404b3d = icmp ult i8 %v6_404b3d, %v3_404b3d
  %v12_404b3d = xor i8 %v6_404b3d, %v3_404b3d
  %v13_404b3d = xor i8 %v6_404b3d, %v5_404b3d
  %v14_404b3d = and i8 %v12_404b3d, %v13_404b3d
  %v15_404b3d = icmp slt i8 %v14_404b3d, 0
  store i1 %v10_404b3d, i1* %az.global-to-local, align 1
  store i1 %v11_404b3d, i1* %cf.global-to-local, align 1
  store i1 %v15_404b3d, i1* %of.global-to-local, align 1
  %v16_404b3d = icmp eq i8 %v6_404b3d, 0
  store i1 %v16_404b3d, i1* %zf.global-to-local, align 1
  %v17_404b3d = icmp slt i8 %v6_404b3d, 0
  store i1 %v17_404b3d, i1* %sf.global-to-local, align 1
  store i8 %v6_404b3d, i8* %v2_404b3d, align 1
  %v0_404b40 = load i32, i32* @eax, align 4
  %v1_404b40 = load i32, i32* @esp, align 4
  %v2_404b40 = add i32 %v1_404b40, -4
  %v3_404b40 = inttoptr i32 %v2_404b40 to i32*
  store i32 %v0_404b40, i32* %v3_404b40, align 4
  %v0_404b41 = load i1, i1* %cf.global-to-local, align 1
  br i1 %v0_404b41, label %dec_label_pc_404bb2, label %dec_label_pc_404b43

dec_label_pc_404b43:                              ; preds = %dec_label_pc_404b3a
  %v1_404b43 = add i32 %v1_404b40, -8
  %v2_404b43 = inttoptr i32 %v1_404b43 to i32*
  store i32 101, i32* %v2_404b43, align 4
  %v0_404b45 = load i32, i32* @eax, align 4
  %factor54 = mul i32 %v0_404b45, 2
  %v4_404b45 = add i32 %factor54, -31
  %v5_404b45 = inttoptr i32 %v4_404b45 to i16*
  %v6_404b45 = load i16, i16* %v5_404b45, align 2
  %v7_404b45 = load i32, i32* @esi, align 4
  %v8_404b45 = trunc i32 %v7_404b45 to i16
  call void @__asm_arpl(i16 %v6_404b45, i16 %v8_404b45)
  br label %dec_label_pc_404b52

dec_label_pc_404b52:                              ; preds = %dec_label_pc_404ade, %dec_label_pc_404b43
  %v0_404b49 = load i32, i32* @eax, align 4
  %v1_404b49 = add i32 %v0_404b49, -1383931648
  %v2_404b49 = inttoptr i32 %v1_404b49 to i8*
  %v3_404b49 = load i8, i8* %v2_404b49, align 1
  %v4_404b49 = xor i8 %v3_404b49, 96
  store i8 %v4_404b49, i8* %v2_404b49, align 1
  %v0_404b50 = load i32, i32* @eax, align 4
  %v1_404b50 = inttoptr i32 %v0_404b50 to i32*
  %v2_404b50 = load i32, i32* %v1_404b50, align 4
  %v4_404b50 = xor i32 %v2_404b50, %v0_404b50
  store i1 false, i1* %az.global-to-local, align 1
  store i1 false, i1* %cf.global-to-local, align 1
  store i1 false, i1* %of.global-to-local, align 1
  %v5_404b50 = icmp eq i32 %v4_404b50, 0
  store i1 %v5_404b50, i1* %zf.global-to-local, align 1
  %v6_404b50 = icmp slt i32 %v4_404b50, 0
  store i1 %v6_404b50, i1* %sf.global-to-local, align 1
  store i32 %v4_404b50, i32* %v1_404b50, align 4
  %v0_404b52 = load i32, i32* @ecx, align 4
  %v1_404b52 = add i32 %v0_404b52, -1
  store i32 %v1_404b52, i32* @ecx, align 4
  %v2_404b52 = icmp ne i32 %v1_404b52, 0
  %v4_404b52 = icmp eq i1 %v2_404b52, %v5_404b50
  %v0_404ad6 = load i32, i32* @esi, align 4
  br i1 %v4_404b52, label %dec_label_pc_404ade, label %dec_label_pc_404b54

dec_label_pc_404b54:                              ; preds = %dec_label_pc_404b52
  %v1_404b54 = inttoptr i32 %v0_404ad6 to i32*
  %v2_404b54 = load i32, i32* %v1_404b54, align 4
  store i32 %v2_404b54, i32* @eax, align 4
  %v4_404b54 = load i1, i1* %df.global-to-local, align 1
  %v5_404b54 = select i1 %v4_404b54, i32 -4, i32 4
  %v6_404b54 = add i32 %v5_404b54, %v0_404ad6
  store i32 %v6_404b54, i32* @esi, align 4
  %v1_404b55 = trunc i32 %v1_404b52 to i8
  %v3_404b55 = udiv i32 %v2_404b54, 256
  %v4_404b55 = trunc i32 %v3_404b55 to i8
  %v5_404b55 = add i8 %v4_404b55, %v1_404b55
  %v6_404b55 = urem i8 %v1_404b55, 16
  %v7_404b55 = urem i8 %v4_404b55, 16
  %v8_404b55 = add nuw nsw i8 %v7_404b55, %v6_404b55
  %v9_404b55 = icmp ugt i8 %v8_404b55, 15
  %v10_404b55 = icmp ult i8 %v5_404b55, %v1_404b55
  %v11_404b55 = xor i8 %v5_404b55, %v1_404b55
  %v12_404b55 = xor i8 %v5_404b55, %v4_404b55
  %v13_404b55 = and i8 %v11_404b55, %v12_404b55
  %v14_404b55 = icmp slt i8 %v13_404b55, 0
  store i1 %v9_404b55, i1* %az.global-to-local, align 1
  store i1 %v10_404b55, i1* %cf.global-to-local, align 1
  store i1 %v14_404b55, i1* %of.global-to-local, align 1
  %v15_404b55 = icmp eq i8 %v5_404b55, 0
  store i1 %v15_404b55, i1* %zf.global-to-local, align 1
  %v16_404b55 = icmp slt i8 %v5_404b55, 0
  store i1 %v16_404b55, i1* %sf.global-to-local, align 1
  %v20_404b55 = zext i8 %v5_404b55 to i32
  %v22_404b55 = and i32 %v1_404b52, -256
  %v23_404b55 = or i32 %v22_404b55, %v20_404b55
  store i32 %v23_404b55, i32* @ecx, align 4
  ret i32 %v2_404b54

dec_label_pc_404b58:                              ; preds = %dec_label_pc_404b20
  %v0_404b58 = load i32, i32* @eax, align 4
  store i32 %v0_404b58, i32* inttoptr (i32 -1534926592 to i32*), align 256
  %v0_404b5d = load i32, i32* @ecx, align 4
  %v1_404b5d = trunc i32 %v0_404b5d to i8
  %v2_404b5d = load i32, i32* @eax, align 4
  %v3_404b5d = udiv i32 %v2_404b5d, 256
  %v4_404b5d = trunc i32 %v3_404b5d to i8
  %v5_404b5d = add i8 %v4_404b5d, %v1_404b5d
  %v6_404b5d = urem i8 %v1_404b5d, 16
  %v7_404b5d = urem i8 %v4_404b5d, 16
  %v8_404b5d = add nuw nsw i8 %v7_404b5d, %v6_404b5d
  %v9_404b5d = icmp ugt i8 %v8_404b5d, 15
  %v10_404b5d = icmp ult i8 %v5_404b5d, %v1_404b5d
  %v11_404b5d = xor i8 %v5_404b5d, %v1_404b5d
  %v12_404b5d = xor i8 %v5_404b5d, %v4_404b5d
  %v13_404b5d = and i8 %v11_404b5d, %v12_404b5d
  %v14_404b5d = icmp slt i8 %v13_404b5d, 0
  store i1 %v9_404b5d, i1* %az.global-to-local, align 1
  store i1 %v10_404b5d, i1* %cf.global-to-local, align 1
  store i1 %v14_404b5d, i1* %of.global-to-local, align 1
  %v15_404b5d = icmp eq i8 %v5_404b5d, 0
  store i1 %v15_404b5d, i1* %zf.global-to-local, align 1
  %v16_404b5d = icmp slt i8 %v5_404b5d, 0
  store i1 %v16_404b5d, i1* %sf.global-to-local, align 1
  %v20_404b5d = zext i8 %v5_404b5d to i32
  %v22_404b5d = and i32 %v0_404b5d, -256
  %v23_404b5d = or i32 %v22_404b5d, %v20_404b5d
  store i32 %v23_404b5d, i32* @ecx, align 4
  %v0_404b5f = load i32, i32* @edi, align 4
  %v1_404b5f = add i32 %v0_404b5f, -1467817728
  %v2_404b5f = inttoptr i32 %v1_404b5f to i8*
  %v3_404b5f = load i8, i8* %v2_404b5f, align 1
  store i1 false, i1* %az.global-to-local, align 1
  store i1 false, i1* %cf.global-to-local, align 1
  store i1 false, i1* %of.global-to-local, align 1
  %v5_404b5f = icmp eq i8 %v3_404b5f, 0
  store i1 %v5_404b5f, i1* %zf.global-to-local, align 1
  %v6_404b5f = icmp slt i8 %v3_404b5f, 0
  store i1 %v6_404b5f, i1* %sf.global-to-local, align 1
  store i8 %v3_404b5f, i8* %v2_404b5f, align 1
  %v0_404b66 = load i32, i32* @esi, align 4
  %v1_404b66 = load i32, i32* @esp, align 4
  %v2_404b66 = add i32 %v1_404b66, -4
  %v3_404b66 = inttoptr i32 %v2_404b66 to i32*
  store i32 %v0_404b66, i32* %v3_404b66, align 4
  br label %dec_label_pc_404b67

dec_label_pc_404b67:                              ; preds = %dec_label_pc_404aef.dec_label_pc_404b67_crit_edge, %dec_label_pc_404b58
  %v8_404b67.pre-phi = phi i32* [ %.pre135, %dec_label_pc_404aef.dec_label_pc_404b67_crit_edge ], [ %v3_404b66, %dec_label_pc_404b58 ]
  %v0_404b67 = phi i32 [ %v0_404b67.pre, %dec_label_pc_404aef.dec_label_pc_404b67_crit_edge ], [ %v2_404b66, %dec_label_pc_404b58 ]
  %v1_404b67 = add i32 %v0_404b67, 4
  %v2_404b67 = add i32 %v0_404b67, 8
  %v3_404b67 = add i32 %v0_404b67, 16
  %v4_404b67 = add i32 %v0_404b67, 20
  %v5_404b67 = add i32 %v0_404b67, 24
  %v6_404b67 = add i32 %v0_404b67, 28
  %v9_404b67 = load i32, i32* %v8_404b67.pre-phi, align 4
  %v10_404b67 = inttoptr i32 %v1_404b67 to i32*
  %v11_404b67 = load i32, i32* %v10_404b67, align 4
  %v12_404b67 = inttoptr i32 %v2_404b67 to i32*
  %v13_404b67 = load i32, i32* %v12_404b67, align 4
  %v14_404b67 = inttoptr i32 %v3_404b67 to i32*
  %v15_404b67 = load i32, i32* %v14_404b67, align 4
  %v16_404b67 = inttoptr i32 %v4_404b67 to i32*
  %v17_404b67 = load i32, i32* %v16_404b67, align 4
  %v18_404b67 = inttoptr i32 %v5_404b67 to i32*
  %v19_404b67 = load i32, i32* %v18_404b67, align 4
  %v20_404b67 = inttoptr i32 %v6_404b67 to i32*
  %v21_404b67 = load i32, i32* %v20_404b67, align 4
  store i32 %v9_404b67, i32* @edi, align 4
  store i32 %v11_404b67, i32* @esi, align 4
  store i32 %v13_404b67, i32* @ebp, align 4
  store i32 %v15_404b67, i32* @ebx, align 4
  %v22_404b67 = trunc i32 %v17_404b67 to i16
  store i32 %v17_404b67, i32* @edx, align 4
  store i32 %v19_404b67, i32* @ecx, align 4
  store i32 %v21_404b67, i32* @eax, align 4
  %v3_404b68 = call i8 @__asm_insb(i16 %v22_404b67)
  %v4_404b68 = load i32, i32* @edi, align 4
  %v5_404b68 = inttoptr i32 %v4_404b68 to i8*
  store i8 %v3_404b68, i8* %v5_404b68, align 1
  %v0_404b69 = load i1, i1* %zf.global-to-local, align 1
  %v1_404b69 = icmp eq i1 %v0_404b69, false
  br i1 %v1_404b69, label %dec_label_pc_404bd0, label %dec_label_pc_404b6b

dec_label_pc_404b6b:                              ; preds = %dec_label_pc_404b67
  %v0_404b6b = load i32, i32* @esp, align 4
  %v2_404b6b = add i32 %v0_404b6b, -4
  %v3_404b6b = inttoptr i32 %v2_404b6b to i32*
  store i32 %v0_404b6b, i32* %v3_404b6b, align 4
  br label %dec_label_pc_404b6c

dec_label_pc_404b6c:                              ; preds = %dec_label_pc_404b6b, %dec_label_pc_404afa
  %v0_404b6c = load i1, i1* %sf.global-to-local, align 1
  %v1_404b6c = icmp eq i1 %v0_404b6c, false
  br i1 %v1_404b6c, label %dec_label_pc_404bde, label %dec_label_pc_404b6e

dec_label_pc_404b6e:                              ; preds = %dec_label_pc_404b6c
  %v0_404b6e = load i32, i32* @ebp, align 4
  %v1_404b6e = add i32 %v0_404b6e, 73
  %v3_404b6e = call i8 @__readgsbyte(i32 %v1_404b6e)
  %v4_404b6e = load i32, i32* @eax, align 4
  %v5_404b6e = trunc i32 %v4_404b6e to i8
  %v6_404b6e = add i8 %v3_404b6e, %v5_404b6e
  %v7_404b6e = urem i8 %v3_404b6e, 16
  %v8_404b6e = urem i8 %v5_404b6e, 16
  %v9_404b6e = add nuw nsw i8 %v8_404b6e, %v7_404b6e
  %v10_404b6e = icmp ugt i8 %v9_404b6e, 15
  %v11_404b6e = icmp ult i8 %v6_404b6e, %v3_404b6e
  %v12_404b6e = xor i8 %v6_404b6e, %v3_404b6e
  %v13_404b6e = xor i8 %v6_404b6e, %v5_404b6e
  %v14_404b6e = and i8 %v12_404b6e, %v13_404b6e
  %v15_404b6e = icmp slt i8 %v14_404b6e, 0
  store i1 %v10_404b6e, i1* %az.global-to-local, align 1
  store i1 %v11_404b6e, i1* %cf.global-to-local, align 1
  store i1 %v15_404b6e, i1* %of.global-to-local, align 1
  %v16_404b6e = icmp eq i8 %v6_404b6e, 0
  store i1 %v16_404b6e, i1* %zf.global-to-local, align 1
  %v17_404b6e = icmp slt i8 %v6_404b6e, 0
  store i1 %v17_404b6e, i1* %sf.global-to-local, align 1
  %v21_404b6e = load i32, i32* @ebp, align 4
  %v22_404b6e = add i32 %v21_404b6e, 73
  call void @__writegsbyte(i32 %v22_404b6e, i8 %v6_404b6e)
  %v24_404b6e = load i32, i32* @eax, align 4
  ret i32 %v24_404b6e

dec_label_pc_404b9d:                              ; preds = %dec_label_pc_404b30
  br i1 %v8_404b30, label %dec_label_pc_404bd7, label %dec_label_pc_404b9f

dec_label_pc_404b9f:                              ; preds = %dec_label_pc_404b9d
  store i32 %v21_404b31, i32* %v20_404b31, align 4
  %v0_404ba0 = load i32, i32* @esi, align 4
  store i32 %v0_404ba0, i32* %v18_404b31, align 4
  %v1_404ba1 = icmp eq i1 %v9_404b30, false
  br i1 %v1_404ba1, label %dec_label_pc_404c0f, label %dec_label_pc_404ba3

dec_label_pc_404ba3:                              ; preds = %dec_label_pc_404b9f, %dec_label_pc_404b34
  %v0_404ba3 = load i32, i32* @esi, align 4
  %v1_404ba3 = add i32 %v0_404ba3, 114
  %v2_404ba3 = inttoptr i32 %v1_404ba3 to i16*
  %v3_404ba3 = load i16, i16* %v2_404ba3, align 2
  %v4_404ba3 = load i32, i32* @ebp, align 4
  %v5_404ba3 = trunc i32 %v4_404ba3 to i16
  call void @__asm_arpl(i16 %v3_404ba3, i16 %v5_404ba3)
  %v0_404ba6 = load i32, i32* @edi, align 4
  %v1_404ba6 = add i32 %v0_404ba6, -1
  %v2_404ba6 = urem i32 %v0_404ba6, 16
  %v3_404ba6 = add nsw i32 %v2_404ba6, -1
  %v4_404ba6 = icmp ugt i32 %v3_404ba6, 15
  %tmp486 = sub i32 0, %v0_404ba6
  %v7_404ba6 = and i32 %v0_404ba6, %tmp486
  %v8_404ba6 = icmp slt i32 %v7_404ba6, 0
  store i1 %v4_404ba6, i1* %az.global-to-local, align 1
  store i1 %v8_404ba6, i1* %of.global-to-local, align 1
  %v9_404ba6 = icmp eq i32 %v1_404ba6, 0
  store i1 %v9_404ba6, i1* %zf.global-to-local, align 1
  %v10_404ba6 = icmp slt i32 %v1_404ba6, 0
  store i1 %v10_404ba6, i1* %sf.global-to-local, align 1
  store i32 %v1_404ba6, i32* @edi, align 4
  %v0_404ba7 = load i32, i32* @edx, align 4
  %v1_404ba7 = trunc i32 %v0_404ba7 to i16
  %v2_404ba7 = call i32 @__asm_insd(i16 %v1_404ba7)
  %v3_404ba7 = load i32, i32* @edi, align 4
  %v4_404ba7 = inttoptr i32 %v3_404ba7 to i32*
  store i32 %v2_404ba7, i32* %v4_404ba7, align 4
  %v0_404ba8 = load i32, i32* @esi, align 4
  %v1_404ba8 = add i32 %v0_404ba8, 77
  %v2_404ba8 = inttoptr i32 %v1_404ba8 to i8*
  %v3_404ba8 = load i8, i8* %v2_404ba8, align 1
  %v4_404ba8 = load i32, i32* @edx, align 4
  %v5_404ba8 = trunc i32 %v4_404ba8 to i8
  %v6_404ba8 = add i8 %v3_404ba8, %v5_404ba8
  store i8 %v6_404ba8, i8* %v2_404ba8, align 1
  %v0_404bab = load i32, i32* @esi, align 4
  %v1_404bab = add i32 %v0_404bab, 1
  store i32 %v1_404bab, i32* @esi, align 4
  %v0_404bac = load i32, i32* @esp, align 4
  %v1_404bac = add i32 %v0_404bac, 2
  %v2_404bac = add i32 %v0_404bac, 4
  %v3_404bac = add i32 %v0_404bac, 8
  %v4_404bac = add i32 %v0_404bac, 10
  %v5_404bac = add i32 %v0_404bac, 12
  %v6_404bac = add i32 %v0_404bac, 14
  %v8_404bac = inttoptr i32 %v0_404bac to i16*
  %v9_404bac = load i16, i16* %v8_404bac, align 2
  %v10_404bac = inttoptr i32 %v1_404bac to i16*
  %v11_404bac = load i16, i16* %v10_404bac, align 2
  %v12_404bac = inttoptr i32 %v2_404bac to i16*
  %v13_404bac = load i16, i16* %v12_404bac, align 2
  %v14_404bac = inttoptr i32 %v3_404bac to i16*
  %v15_404bac = load i16, i16* %v14_404bac, align 2
  %v16_404bac = inttoptr i32 %v4_404bac to i16*
  %v17_404bac = load i16, i16* %v16_404bac, align 2
  %v18_404bac = inttoptr i32 %v5_404bac to i16*
  %v19_404bac = load i16, i16* %v18_404bac, align 2
  %v20_404bac = inttoptr i32 %v6_404bac to i16*
  %v21_404bac = load i16, i16* %v20_404bac, align 2
  %v22_404bac = zext i16 %v9_404bac to i32
  %v23_404bac = load i32, i32* @edi, align 4
  %v24_404bac = and i32 %v23_404bac, -65536
  %v25_404bac = or i32 %v24_404bac, %v22_404bac
  store i32 %v25_404bac, i32* @edi, align 4
  %v26_404bac = zext i16 %v11_404bac to i32
  %v28_404bac = and i32 %v1_404bab, -65536
  %v29_404bac = or i32 %v28_404bac, %v26_404bac
  store i32 %v29_404bac, i32* @esi, align 4
  %v30_404bac = zext i16 %v13_404bac to i32
  %v31_404bac = load i32, i32* @ebp, align 4
  %v32_404bac = and i32 %v31_404bac, -65536
  %v33_404bac = or i32 %v32_404bac, %v30_404bac
  store i32 %v33_404bac, i32* @ebp, align 4
  %v34_404bac = zext i16 %v15_404bac to i32
  %v35_404bac = load i32, i32* @ebx, align 4
  %v36_404bac = and i32 %v35_404bac, -65536
  %v37_404bac = or i32 %v36_404bac, %v34_404bac
  store i32 %v37_404bac, i32* @ebx, align 4
  %v38_404bac = zext i16 %v17_404bac to i32
  %v39_404bac = load i32, i32* @edx, align 4
  %v40_404bac = and i32 %v39_404bac, -65536
  %v41_404bac = or i32 %v40_404bac, %v38_404bac
  store i32 %v41_404bac, i32* @edx, align 4
  %v42_404bac = zext i16 %v19_404bac to i32
  %v43_404bac = load i32, i32* @ecx, align 4
  %v44_404bac = and i32 %v43_404bac, -65536
  %v45_404bac = or i32 %v44_404bac, %v42_404bac
  store i32 %v45_404bac, i32* @ecx, align 4
  %v46_404bac = zext i16 %v21_404bac to i32
  %v47_404bac = load i32, i32* @eax, align 4
  %v48_404bac = and i32 %v47_404bac, -65536
  %v49_404bac = or i32 %v48_404bac, %v46_404bac
  store i32 %v49_404bac, i32* @eax, align 4
  %v1_404bae = add i32 %v45_404bac, 53
  %v2_404bae = inttoptr i32 %v1_404bae to i32*
  %v3_404bae = load i32, i32* %v2_404bae, align 4
  %v5_404bae = xor i32 %v3_404bae, %v41_404bac
  store i1 false, i1* %cf.global-to-local, align 1
  store i32 %v5_404bae, i32* %v2_404bae, align 4
  %v0_404bb1 = load i32, i32* @esi, align 4
  %v1_404bb1 = add i32 %v0_404bb1, -1
  %v2_404bb1 = urem i32 %v0_404bb1, 16
  %v3_404bb1 = add nsw i32 %v2_404bb1, -1
  %v4_404bb1 = icmp ugt i32 %v3_404bb1, 15
  %tmp487 = sub i32 0, %v0_404bb1
  %v7_404bb1 = and i32 %v0_404bb1, %tmp487
  %v8_404bb1 = icmp slt i32 %v7_404bb1, 0
  store i1 %v4_404bb1, i1* %az.global-to-local, align 1
  store i1 %v8_404bb1, i1* %of.global-to-local, align 1
  %v9_404bb1 = icmp eq i32 %v1_404bb1, 0
  store i1 %v9_404bb1, i1* %zf.global-to-local, align 1
  %v10_404bb1 = icmp slt i32 %v1_404bb1, 0
  store i1 %v10_404bb1, i1* %sf.global-to-local, align 1
  store i32 %v1_404bb1, i32* @esi, align 4
  %v0_404bb2.pre = load i32, i32* @esp, align 4
  br label %dec_label_pc_404bb2

dec_label_pc_404bb2:                              ; preds = %dec_label_pc_404ba3, %dec_label_pc_404b3a
  %v0_404bb2 = phi i32 [ %v0_404bb2.pre, %dec_label_pc_404ba3 ], [ %v2_404b40, %dec_label_pc_404b3a ]
  %v1_404bb2 = add i32 %v0_404bb2, -1
  %v1_404bb3 = inttoptr i32 %v1_404bb2 to i32*
  %v2_404bb3 = load i32, i32* %v1_404bb3, align 4
  store i32 %v2_404bb3, i32* @edx, align 4
  %v0_404bb4 = load i32, i32* @ecx, align 4
  %v1_404bb4 = add i32 %v0_404bb4, 1
  store i32 %v1_404bb4, i32* @ecx, align 4
  %v0_404bb5 = load i32, i32* @ebx, align 4
  %v1_404bb5 = add i32 %v0_404bb5, 1
  %v2_404bb5 = urem i32 %v0_404bb5, 16
  %v4_404bb5 = icmp eq i32 %v2_404bb5, 15
  %tmp488 = xor i32 %v0_404bb5, -2147483648
  %v7_404bb5 = and i32 %v1_404bb5, %tmp488
  %v8_404bb5 = icmp slt i32 %v7_404bb5, 0
  store i1 %v4_404bb5, i1* %az.global-to-local, align 1
  store i1 %v8_404bb5, i1* %of.global-to-local, align 1
  %v9_404bb5 = icmp eq i32 %v1_404bb5, 0
  store i1 %v9_404bb5, i1* %zf.global-to-local, align 1
  %v10_404bb5 = icmp slt i32 %v1_404bb5, 0
  store i1 %v10_404bb5, i1* %sf.global-to-local, align 1
  store i32 %v1_404bb5, i32* @ebx, align 4
  %v1_404bb6 = icmp eq i1 %v10_404bb5, false
  br i1 %v1_404bb6, label %dec_label_pc_404bea, label %dec_label_pc_404bb8

dec_label_pc_404bb8:                              ; preds = %dec_label_pc_404bb2
  store i32 %v1_404bb5, i32* %v1_404bb3, align 4
  %v2_404bb9 = urem i32 %v1_404bb2, 16
  %v4_404bb9 = icmp eq i32 %v2_404bb9, 15
  %tmp489 = sub i32 0, %v0_404bb2
  %v7_404bb9 = and i32 %v0_404bb2, %tmp489
  %v8_404bb9 = icmp slt i32 %v7_404bb9, 0
  store i1 %v4_404bb9, i1* %az.global-to-local, align 1
  store i1 %v8_404bb9, i1* %of.global-to-local, align 1
  %v9_404bb9 = icmp eq i32 %v0_404bb2, 0
  store i1 %v9_404bb9, i1* %zf.global-to-local, align 1
  %v10_404bb9 = icmp slt i32 %v0_404bb2, 0
  store i1 %v10_404bb9, i1* %sf.global-to-local, align 1
  %v0_404bba = load i32, i32* @esi, align 4
  %v2_404bba = add i32 %v0_404bb2, -4
  %v3_404bba = inttoptr i32 %v2_404bba to i32*
  store i32 %v0_404bba, i32* %v3_404bba, align 4
  %v0_404bbb = load i32, i32* @edx, align 4
  %v1_404bbb = add i32 %v0_404bbb, 117
  %v2_404bbb = inttoptr i32 %v1_404bbb to i8*
  %v3_404bbb = load i8, i8* %v2_404bbb, align 1
  %v5_404bbb = trunc i32 %v0_404bbb to i8
  %v6_404bbb = add i8 %v3_404bbb, %v5_404bbb
  %v7_404bbb = urem i8 %v3_404bbb, 16
  %v8_404bbb = urem i8 %v5_404bbb, 16
  %v9_404bbb = add nuw nsw i8 %v7_404bbb, %v8_404bbb
  %v10_404bbb = icmp ugt i8 %v9_404bbb, 15
  %v11_404bbb = icmp ult i8 %v6_404bbb, %v3_404bbb
  %v12_404bbb = xor i8 %v6_404bbb, %v3_404bbb
  %v13_404bbb = xor i8 %v6_404bbb, %v5_404bbb
  %v14_404bbb = and i8 %v12_404bbb, %v13_404bbb
  %v15_404bbb = icmp slt i8 %v14_404bbb, 0
  store i1 %v10_404bbb, i1* %az.global-to-local, align 1
  store i1 %v11_404bbb, i1* %cf.global-to-local, align 1
  store i1 %v15_404bbb, i1* %of.global-to-local, align 1
  %v16_404bbb = icmp eq i8 %v6_404bbb, 0
  store i1 %v16_404bbb, i1* %zf.global-to-local, align 1
  %v17_404bbb = icmp slt i8 %v6_404bbb, 0
  store i1 %v17_404bbb, i1* %sf.global-to-local, align 1
  store i8 %v6_404bbb, i8* %v2_404bbb, align 1
  %v0_404bbe = load i32, i32* @edx, align 4
  %v1_404bbe = trunc i32 %v0_404bbe to i16
  %v2_404bbe = load i32, i32* @esi, align 4
  %v3_404bbe = inttoptr i32 %v2_404bbe to i8*
  %v4_404bbe = load i8, i8* %v3_404bbe, align 1
  call void @__asm_outsb(i16 %v1_404bbe, i8 %v4_404bbe)
  %v0_404bbf = load i1, i1* %zf.global-to-local, align 1
  br i1 %v0_404bbf, label %dec_label_pc_404c2a, label %dec_label_pc_404bc1

dec_label_pc_404bc1:                              ; preds = %dec_label_pc_404bb8
  %v0_404bc1 = load i32, i32* @edx, align 4
  %v1_404bc1 = trunc i32 %v0_404bc1 to i16
  %v2_404bc1 = call i32 @__asm_insd(i16 %v1_404bc1)
  %v3_404bc1 = load i32, i32* @edi, align 4
  %v4_404bc1 = inttoptr i32 %v3_404bc1 to i32*
  store i32 %v2_404bc1, i32* %v4_404bc1, align 4
  %v0_404bc2 = load i32, i32* @eax, align 4
  %v1_404bc2 = add i32 %v0_404bc2, -1
  %v2_404bc2 = urem i32 %v0_404bc2, 16
  %v3_404bc2 = add nsw i32 %v2_404bc2, -1
  %v4_404bc2 = icmp ugt i32 %v3_404bc2, 15
  %tmp490 = sub i32 0, %v0_404bc2
  %v7_404bc2 = and i32 %v0_404bc2, %tmp490
  %v8_404bc2 = icmp slt i32 %v7_404bc2, 0
  store i1 %v4_404bc2, i1* %az.global-to-local, align 1
  store i1 %v8_404bc2, i1* %of.global-to-local, align 1
  %v9_404bc2 = icmp eq i32 %v1_404bc2, 0
  store i1 %v9_404bc2, i1* %zf.global-to-local, align 1
  %v10_404bc2 = icmp slt i32 %v1_404bc2, 0
  store i1 %v10_404bc2, i1* %sf.global-to-local, align 1
  store i32 %v1_404bc2, i32* @eax, align 4
  %v0_404bc4 = load i32, i32* @edx, align 4
  %v1_404bc4 = trunc i32 %v0_404bc4 to i16
  %v2_404bc4 = call i8 @__asm_insb(i16 %v1_404bc4)
  %v3_404bc4 = load i32, i32* @edi, align 4
  %v4_404bc4 = inttoptr i32 %v3_404bc4 to i8*
  store i8 %v2_404bc4, i8* %v4_404bc4, align 1
  %v0_404bc6 = load i1, i1* %of.global-to-local, align 1
  br i1 %v0_404bc6, label %dec_label_pc_404bc1.dec_label_pc_404c2d_crit_edge, label %dec_label_pc_404bc8

dec_label_pc_404bc1.dec_label_pc_404c2d_crit_edge: ; preds = %dec_label_pc_404bc1
  %v0_404c2d.pre = load i1, i1* %sf.global-to-local, align 1
  br label %dec_label_pc_404c2d

dec_label_pc_404bc8:                              ; preds = %dec_label_pc_404bc1
  %v0_404bc8 = load i1, i1* %cf.global-to-local, align 1
  br i1 %v0_404bc8, label %dec_label_pc_404bc8.dec_label_pc_404c3d_crit_edge, label %dec_label_pc_404bca

dec_label_pc_404bc8.dec_label_pc_404c3d_crit_edge: ; preds = %dec_label_pc_404bc8
  %v0_404c3d.pre = load i32, i32* @ebx, align 4
  %v1_404c3d.pre = load i32, i32* @esp, align 4
  br label %dec_label_pc_404c3d

dec_label_pc_404bca:                              ; preds = %dec_label_pc_404bc8
  %v0_404bca = load i32, i32* @edi, align 4
  %v1_404bca = add i32 %v0_404bca, 101
  %v2_404bca = inttoptr i32 %v1_404bca to i8*
  %v3_404bca = load i8, i8* %v2_404bca, align 1
  %v4_404bca = load i32, i32* @eax, align 4
  %v5_404bca = trunc i32 %v4_404bca to i8
  %v6_404bca = add i8 %v3_404bca, %v5_404bca
  %v7_404bca = urem i8 %v3_404bca, 16
  %v8_404bca = urem i8 %v5_404bca, 16
  %v9_404bca = add nuw nsw i8 %v8_404bca, %v7_404bca
  %v10_404bca = icmp ugt i8 %v9_404bca, 15
  %v11_404bca = icmp ult i8 %v6_404bca, %v3_404bca
  %v12_404bca = xor i8 %v6_404bca, %v3_404bca
  %v13_404bca = xor i8 %v6_404bca, %v5_404bca
  %v14_404bca = and i8 %v12_404bca, %v13_404bca
  %v15_404bca = icmp slt i8 %v14_404bca, 0
  store i1 %v10_404bca, i1* %az.global-to-local, align 1
  store i1 %v11_404bca, i1* %cf.global-to-local, align 1
  store i1 %v15_404bca, i1* %of.global-to-local, align 1
  %v16_404bca = icmp eq i8 %v6_404bca, 0
  store i1 %v16_404bca, i1* %zf.global-to-local, align 1
  %v17_404bca = icmp slt i8 %v6_404bca, 0
  store i1 %v17_404bca, i1* %sf.global-to-local, align 1
  store i8 %v6_404bca, i8* %v2_404bca, align 1
  %v0_404bcd = load i1, i1* %zf.global-to-local, align 1
  br i1 %v0_404bcd, label %dec_label_pc_404c20, label %dec_label_pc_404bcf

dec_label_pc_404bcf:                              ; preds = %dec_label_pc_404bca
  %v1_404bcd = load i32, i32* @eax, align 4
  ret i32 %v1_404bcd

dec_label_pc_404bd0:                              ; preds = %dec_label_pc_404b67
  %v2_404bcd = load i32, i32* @eax, align 4
  ret i32 %v2_404bcd

dec_label_pc_404bd7:                              ; preds = %dec_label_pc_404b9d
  %v1_404bd7 = icmp eq i1 %v9_404b30, false
  br i1 %v1_404bd7, label %dec_label_pc_404c3e, label %dec_label_pc_404bd9

dec_label_pc_404bd9:                              ; preds = %dec_label_pc_404bd7
  %v1_404bd9 = add i32 %v15_404b31, 104
  %v2_404bd9 = inttoptr i32 %v1_404bd9 to i8*
  %v3_404bd9 = load i8, i8* %v2_404bd9, align 1
  %v5_404bd9 = trunc i32 %v21_404b31 to i8
  %v6_404bd9 = add i8 %v3_404bd9, %v5_404bd9
  %v7_404bd9 = urem i8 %v3_404bd9, 16
  %v8_404bd9 = urem i8 %v5_404bd9, 16
  %v9_404bd9 = add nuw nsw i8 %v7_404bd9, %v8_404bd9
  %v10_404bd9 = icmp ugt i8 %v9_404bd9, 15
  %v11_404bd9 = icmp ult i8 %v6_404bd9, %v3_404bd9
  %v12_404bd9 = xor i8 %v6_404bd9, %v3_404bd9
  %v13_404bd9 = xor i8 %v6_404bd9, %v5_404bd9
  %v14_404bd9 = and i8 %v12_404bd9, %v13_404bd9
  %v15_404bd9 = icmp slt i8 %v14_404bd9, 0
  store i1 %v10_404bd9, i1* %az.global-to-local, align 1
  store i1 %v11_404bd9, i1* %cf.global-to-local, align 1
  store i1 %v15_404bd9, i1* %of.global-to-local, align 1
  %v16_404bd9 = icmp eq i8 %v6_404bd9, 0
  store i1 %v16_404bd9, i1* %zf.global-to-local, align 1
  %v17_404bd9 = icmp slt i8 %v6_404bd9, 0
  store i1 %v17_404bd9, i1* %sf.global-to-local, align 1
  store i8 %v6_404bd9, i8* %v2_404bd9, align 1
  %v0_404bdc = load i32, i32* @esp, align 4
  %v1_404bdc = add i32 %v0_404bdc, 4
  %v2_404bdc = add i32 %v0_404bdc, 8
  %v3_404bdc = add i32 %v0_404bdc, 16
  %v4_404bdc = add i32 %v0_404bdc, 20
  %v5_404bdc = add i32 %v0_404bdc, 24
  %v6_404bdc = add i32 %v0_404bdc, 28
  %v8_404bdc = inttoptr i32 %v0_404bdc to i32*
  %v9_404bdc = load i32, i32* %v8_404bdc, align 4
  %v10_404bdc = inttoptr i32 %v1_404bdc to i32*
  %v11_404bdc = load i32, i32* %v10_404bdc, align 4
  %v12_404bdc = inttoptr i32 %v2_404bdc to i32*
  %v13_404bdc = load i32, i32* %v12_404bdc, align 4
  %v14_404bdc = inttoptr i32 %v3_404bdc to i32*
  %v15_404bdc = load i32, i32* %v14_404bdc, align 4
  %v16_404bdc = inttoptr i32 %v4_404bdc to i32*
  %v17_404bdc = load i32, i32* %v16_404bdc, align 4
  %v18_404bdc = inttoptr i32 %v5_404bdc to i32*
  %v19_404bdc = load i32, i32* %v18_404bdc, align 4
  %v20_404bdc = inttoptr i32 %v6_404bdc to i32*
  %v21_404bdc = load i32, i32* %v20_404bdc, align 4
  store i32 %v9_404bdc, i32* @edi, align 4
  store i32 %v11_404bdc, i32* @esi, align 4
  store i32 %v13_404bdc, i32* @ebp, align 4
  store i32 %v15_404bdc, i32* @ebx, align 4
  store i32 %v17_404bdc, i32* @edx, align 4
  store i32 %v19_404bdc, i32* @ecx, align 4
  store i32 %v21_404bdc, i32* @eax, align 4
  ret i32 %v21_404bdc

dec_label_pc_404bde:                              ; preds = %dec_label_pc_404b6c
  %v0_404bde = load i32, i32* @ecx, align 4
  %v1_404bde = trunc i32 %v0_404bde to i8
  %v2_404bde = load i32, i32* @eax, align 4
  %v3_404bde = udiv i32 %v2_404bde, 256
  %v4_404bde = trunc i32 %v3_404bde to i8
  %v5_404bde = add i8 %v4_404bde, %v1_404bde
  %v6_404bde = urem i8 %v1_404bde, 16
  %v7_404bde = urem i8 %v4_404bde, 16
  %v8_404bde = add nuw nsw i8 %v7_404bde, %v6_404bde
  %v9_404bde = icmp ugt i8 %v8_404bde, 15
  %v10_404bde = icmp ult i8 %v5_404bde, %v1_404bde
  %v11_404bde = xor i8 %v5_404bde, %v1_404bde
  %v12_404bde = xor i8 %v5_404bde, %v4_404bde
  %v13_404bde = and i8 %v11_404bde, %v12_404bde
  %v14_404bde = icmp slt i8 %v13_404bde, 0
  store i1 %v9_404bde, i1* %az.global-to-local, align 1
  store i1 %v10_404bde, i1* %cf.global-to-local, align 1
  store i1 %v14_404bde, i1* %of.global-to-local, align 1
  %v15_404bde = icmp eq i8 %v5_404bde, 0
  store i1 %v15_404bde, i1* %zf.global-to-local, align 1
  %v16_404bde = icmp slt i8 %v5_404bde, 0
  store i1 %v16_404bde, i1* %sf.global-to-local, align 1
  %v20_404bde = zext i8 %v5_404bde to i32
  %v22_404bde = and i32 %v0_404bde, -256
  %v23_404bde = or i32 %v22_404bde, %v20_404bde
  store i32 %v23_404bde, i32* @ecx, align 4
  ret i32 %v2_404bde

dec_label_pc_404bea:                              ; preds = %dec_label_pc_404bb2
  %v1_404bea = trunc i32 %v2_404bb3 to i16
  %v2_404bea = load i32, i32* @esi, align 4
  %v3_404bea = inttoptr i32 %v2_404bea to i8*
  %v4_404bea = load i8, i8* %v3_404bea, align 1
  call void @__asm_outsb(i16 %v1_404bea, i8 %v4_404bea)
  %v0_404beb = load i1, i1* %zf.global-to-local, align 1
  br i1 %v0_404beb, label %dec_label_pc_404c56, label %dec_label_pc_404bed

dec_label_pc_404bed:                              ; preds = %dec_label_pc_404bea
  %v0_404bed = load i32, i32* @edx, align 4
  %v1_404bed = trunc i32 %v0_404bed to i16
  %v2_404bed = call i32 @__asm_insd(i16 %v1_404bed)
  %v3_404bed = load i32, i32* @edi, align 4
  %v4_404bed = inttoptr i32 %v3_404bed to i32*
  store i32 %v2_404bed, i32* %v4_404bed, align 4
  %v0_404bee = load i32, i32* @esp, align 4
  %v2_404bee = add i32 %v0_404bee, -4
  %v3_404bee = inttoptr i32 %v2_404bee to i32*
  store i32 %v0_404bee, i32* %v3_404bee, align 4
  %v0_404bf0 = load i1, i1* %sf.global-to-local, align 1
  %v1_404bf0 = icmp eq i1 %v0_404bf0, false
  br i1 %v1_404bf0, label %dec_label_pc_404c62, label %dec_label_pc_404bf6

dec_label_pc_404bf6:                              ; preds = %dec_label_pc_404bed
  %v0_404bf2 = load i32, i32* @eax, align 4
  %v1_404bf2 = add i32 %v0_404bf2, -1
  %v2_404bf2 = urem i32 %v0_404bf2, 16
  %v3_404bf2 = add nsw i32 %v2_404bf2, -1
  %v4_404bf2 = icmp ugt i32 %v3_404bf2, 15
  %tmp491 = sub i32 0, %v0_404bf2
  %v7_404bf2 = and i32 %v0_404bf2, %tmp491
  %v8_404bf2 = icmp slt i32 %v7_404bf2, 0
  store i1 %v4_404bf2, i1* %az.global-to-local, align 1
  store i1 %v8_404bf2, i1* %of.global-to-local, align 1
  %v9_404bf2 = icmp eq i32 %v1_404bf2, 0
  store i1 %v9_404bf2, i1* %zf.global-to-local, align 1
  %v10_404bf2 = icmp slt i32 %v1_404bf2, 0
  store i1 %v10_404bf2, i1* %sf.global-to-local, align 1
  store i32 %v1_404bf2, i32* @eax, align 4
  %v2_404bf4 = add i32 %v0_404bee, 4
  %v3_404bf4 = add i32 %v0_404bee, 12
  %v4_404bf4 = add i32 %v0_404bee, 16
  %v5_404bf4 = add i32 %v0_404bee, 20
  %v6_404bf4 = add i32 %v0_404bee, 24
  %v9_404bf4 = load i32, i32* %v3_404bee, align 4
  %v10_404bf4 = inttoptr i32 %v0_404bee to i32*
  %v11_404bf4 = load i32, i32* %v10_404bf4, align 4
  %v12_404bf4 = inttoptr i32 %v2_404bf4 to i32*
  %v13_404bf4 = load i32, i32* %v12_404bf4, align 4
  %v14_404bf4 = inttoptr i32 %v3_404bf4 to i32*
  %v15_404bf4 = load i32, i32* %v14_404bf4, align 4
  %v16_404bf4 = inttoptr i32 %v4_404bf4 to i32*
  %v17_404bf4 = load i32, i32* %v16_404bf4, align 4
  %v18_404bf4 = inttoptr i32 %v5_404bf4 to i32*
  %v19_404bf4 = load i32, i32* %v18_404bf4, align 4
  %v20_404bf4 = inttoptr i32 %v6_404bf4 to i32*
  %v21_404bf4 = load i32, i32* %v20_404bf4, align 4
  store i32 %v9_404bf4, i32* @edi, align 4
  store i32 %v11_404bf4, i32* @esi, align 4
  store i32 %v13_404bf4, i32* @ebp, align 4
  store i32 %v15_404bf4, i32* @ebx, align 4
  store i32 %v17_404bf4, i32* @edx, align 4
  store i32 %v19_404bf4, i32* @ecx, align 4
  store i32 %v21_404bf4, i32* @eax, align 4
  %v1_404bf5 = trunc i32 %v17_404bf4 to i16
  %v3_404bf5 = inttoptr i32 %v11_404bf4 to i8*
  %v4_404bf5 = load i8, i8* %v3_404bf5, align 1
  call void @__asm_outsb(i16 %v1_404bf5, i8 %v4_404bf5)
  %v0_404bf6 = load i32, i32* @edx, align 4
  %v1_404bf6 = trunc i32 %v0_404bf6 to i16
  %v2_404bf6 = call i8 @__asm_insb(i16 %v1_404bf6)
  %v3_404bf6 = load i32, i32* @edi, align 4
  %v4_404bf6 = inttoptr i32 %v3_404bf6 to i8*
  store i8 %v2_404bf6, i8* %v4_404bf6, align 1
  %v0_404bf8 = load i32, i32* @ebp, align 4
  %v1_404bf8 = load i32, i32* @esi, align 4
  %v3_404bf8 = add i32 %v0_404bf8, 111
  %v4_404bf8 = add i32 %v3_404bf8, %v1_404bf8
  %v6_404bf8 = call i8 @__readgsbyte(i32 %v4_404bf8)
  %v7_404bf8 = load i32, i32* @eax, align 4
  %v8_404bf8 = udiv i32 %v7_404bf8, 256
  %v9_404bf8 = trunc i32 %v8_404bf8 to i8
  %v10_404bf8 = add i8 %v6_404bf8, %v9_404bf8
  %v11_404bf8 = urem i8 %v6_404bf8, 16
  %v12_404bf8 = urem i8 %v9_404bf8, 16
  %v13_404bf8 = add nuw nsw i8 %v12_404bf8, %v11_404bf8
  %v14_404bf8 = icmp ugt i8 %v13_404bf8, 15
  %v15_404bf8 = icmp ult i8 %v10_404bf8, %v6_404bf8
  %v16_404bf8 = xor i8 %v10_404bf8, %v6_404bf8
  %v17_404bf8 = xor i8 %v10_404bf8, %v9_404bf8
  %v18_404bf8 = and i8 %v16_404bf8, %v17_404bf8
  %v19_404bf8 = icmp slt i8 %v18_404bf8, 0
  store i1 %v14_404bf8, i1* %az.global-to-local, align 1
  store i1 %v15_404bf8, i1* %cf.global-to-local, align 1
  store i1 %v19_404bf8, i1* %of.global-to-local, align 1
  %v20_404bf8 = icmp eq i8 %v10_404bf8, 0
  store i1 %v20_404bf8, i1* %zf.global-to-local, align 1
  %v21_404bf8 = icmp slt i8 %v10_404bf8, 0
  store i1 %v21_404bf8, i1* %sf.global-to-local, align 1
  %v25_404bf8 = load i32, i32* @ebp, align 4
  %v26_404bf8 = load i32, i32* @esi, align 4
  %v28_404bf8 = add i32 %v25_404bf8, 111
  %v29_404bf8 = add i32 %v28_404bf8, %v26_404bf8
  call void @__writegsbyte(i32 %v29_404bf8, i8 %v10_404bf8)
  %v0_404bfd = load i32, i32* @esi, align 4
  %v1_404bfd = load i32, i32* @ebp, align 4
  %v2_404bfd = mul i32 %v1_404bfd, 2
  %v3_404bfd = add i32 %v0_404bfd, 101
  %v4_404bfd = add i32 %v3_404bfd, %v2_404bfd
  %v5_404bfd = inttoptr i32 %v4_404bfd to i64*
  %v6_404bfd = load i64, i64* %v5_404bfd, align 4
  %v7_404bfd = call i32 @__asm_bound(i64 %v6_404bfd)
  store i32 %v7_404bfd, i32* @ebp, align 4
  %v0_404c01 = load i32, i32* @edx, align 4
  %v1_404c01 = trunc i32 %v0_404c01 to i16
  %v2_404c01 = load i32, i32* @esi, align 4
  %v3_404c01 = inttoptr i32 %v2_404c01 to i32*
  %v4_404c01 = load i32, i32* %v3_404c01, align 4
  call void @__asm_outsd(i16 %v1_404c01, i32 %v4_404c01)
  %v0_404c02 = load i32, i32* @esp, align 4
  %v2_404c02 = add i32 %v0_404c02, -4
  %v3_404c02 = inttoptr i32 %v2_404c02 to i32*
  store i32 %v0_404c02, i32* %v3_404c02, align 4
  %v0_404c04 = load i32, i32* @ebx, align 4
  %v1_404c04 = trunc i32 %v0_404c04 to i16
  %v2_404c04 = add i16 %v1_404c04, 1
  %v3_404c04 = urem i16 %v1_404c04, 16
  %v5_404c04 = icmp eq i16 %v3_404c04, 15
  %tmp492 = xor i16 %v1_404c04, -32768
  %v8_404c04 = and i16 %v2_404c04, %tmp492
  %v9_404c04 = icmp slt i16 %v8_404c04, 0
  store i1 %v5_404c04, i1* %az.global-to-local, align 1
  store i1 %v9_404c04, i1* %of.global-to-local, align 1
  %v10_404c04 = icmp eq i16 %v2_404c04, 0
  store i1 %v10_404c04, i1* %zf.global-to-local, align 1
  %v11_404c04 = icmp slt i16 %v2_404c04, 0
  store i1 %v11_404c04, i1* %sf.global-to-local, align 1
  %v16_404c04 = zext i16 %v2_404c04 to i32
  %v18_404c04 = and i32 %v0_404c04, -65536
  %v19_404c04 = or i32 %v18_404c04, %v16_404c04
  store i32 %v19_404c04, i32* @ebx, align 4
  %v0_404c06 = load i1, i1* %cf.global-to-local, align 1
  %v1_404c06 = icmp eq i1 %v0_404c06, false
  br i1 %v1_404c06, label %dec_label_pc_404c49, label %dec_label_pc_404c08

dec_label_pc_404c08:                              ; preds = %dec_label_pc_404bf6
  %v2_404c08 = load i32, i32* %v3_404c02, align 4
  store i32 %v2_404c08, i32* @ecx, align 4
  %v0_404c09 = load i32, i32* @edi, align 4
  %v1_404c09 = add i32 %v0_404c09, -1
  %v1_404c0a = add i32 %v0_404c09, -2
  %v2_404c0a = urem i32 %v1_404c09, 16
  %v3_404c0a = add nsw i32 %v2_404c0a, -1
  %v4_404c0a = icmp ugt i32 %v3_404c0a, 15
  %tmp493 = sub i32 1, %v0_404c09
  %v7_404c0a = and i32 %v1_404c09, %tmp493
  %v8_404c0a = icmp slt i32 %v7_404c0a, 0
  store i1 %v4_404c0a, i1* %az.global-to-local, align 1
  store i1 %v8_404c0a, i1* %of.global-to-local, align 1
  %v9_404c0a = icmp eq i32 %v1_404c0a, 0
  store i1 %v9_404c0a, i1* %zf.global-to-local, align 1
  %v10_404c0a = icmp slt i32 %v1_404c0a, 0
  store i1 %v10_404c0a, i1* %sf.global-to-local, align 1
  store i32 %v1_404c0a, i32* @edi, align 4
  %v0_404c0b = load i32, i32* @eax, align 4
  %v1_404c0b = inttoptr i32 %v0_404c0b to i8*
  %v2_404c0b = load i8, i8* %v1_404c0b, align 1
  %v4_404c0b = trunc i32 %v0_404c0b to i8
  %v5_404c0b = xor i8 %v2_404c0b, %v4_404c0b
  store i1 false, i1* %cf.global-to-local, align 1
  store i8 %v5_404c0b, i8* %v1_404c0b, align 1
  %v0_404c0d = load i32, i32* @esp, align 4
  %v1_404c0d = add i32 %v0_404c0d, 1
  %v2_404c0d = urem i32 %v0_404c0d, 16
  %v4_404c0d = icmp eq i32 %v2_404c0d, 15
  %tmp494 = xor i32 %v0_404c0d, -2147483648
  %v7_404c0d = and i32 %v1_404c0d, %tmp494
  %v8_404c0d = icmp slt i32 %v7_404c0d, 0
  store i1 %v4_404c0d, i1* %az.global-to-local, align 1
  store i1 %v8_404c0d, i1* %of.global-to-local, align 1
  %v9_404c0d = icmp eq i32 %v1_404c0d, 0
  store i1 %v9_404c0d, i1* %zf.global-to-local, align 1
  %v10_404c0d = icmp slt i32 %v1_404c0d, 0
  store i1 %v10_404c0d, i1* %sf.global-to-local, align 1
  %v0_404c0e = load i32, i32* @ebp, align 4
  %v2_404c0e = add i32 %v0_404c0d, -3
  %v3_404c0e = inttoptr i32 %v2_404c0e to i32*
  store i32 %v0_404c0e, i32* %v3_404c0e, align 4
  br label %dec_label_pc_404c0f

dec_label_pc_404c0f:                              ; preds = %dec_label_pc_404c08, %dec_label_pc_404b9f
  %v0_404c10 = load i32, i32* @edi, align 4
  %v1_404c10 = add i32 %v0_404c10, -1
  store i32 %v1_404c10, i32* @edi, align 4
  %v1_404c11 = inttoptr i32 %v1_404c10 to i32*
  %v2_404c11 = load i32, i32* %v1_404c11, align 4
  %v3_404c11 = load i32, i32* @esi, align 4
  %v4_404c11 = xor i32 %v3_404c11, %v2_404c11
  store i1 false, i1* %cf.global-to-local, align 1
  store i32 %v4_404c11, i32* %v1_404c11, align 4
  %v0_404c13 = load i32, i32* @edx, align 4
  %v1_404c13 = add i32 %v0_404c13, 1
  %v2_404c13 = urem i32 %v0_404c13, 16
  %v4_404c13 = icmp eq i32 %v2_404c13, 15
  %tmp495 = xor i32 %v0_404c13, -2147483648
  %v7_404c13 = and i32 %v1_404c13, %tmp495
  %v8_404c13 = icmp slt i32 %v7_404c13, 0
  store i1 %v4_404c13, i1* %az.global-to-local, align 1
  store i1 %v8_404c13, i1* %of.global-to-local, align 1
  %v9_404c13 = icmp eq i32 %v1_404c13, 0
  store i1 %v9_404c13, i1* %zf.global-to-local, align 1
  %v10_404c13 = icmp slt i32 %v1_404c13, 0
  store i1 %v10_404c13, i1* %sf.global-to-local, align 1
  store i32 %v1_404c13, i32* @edx, align 4
  %v0_404c14 = load i32, i32* @edi, align 4
  %v1_404c14 = load i32, i32* @esp, align 4
  %v2_404c14 = add i32 %v1_404c14, -4
  %v3_404c14 = inttoptr i32 %v2_404c14 to i32*
  store i32 %v0_404c14, i32* %v3_404c14, align 4
  %v0_404c15 = load i32, i32* @ebx, align 4
  %v2_404c15 = add i32 %v1_404c14, -8
  %v3_404c15 = inttoptr i32 %v2_404c15 to i32*
  store i32 %v0_404c15, i32* %v3_404c15, align 4
  br i1 %v8_404c13, label %dec_label_pc_404c4f, label %dec_label_pc_404c18

dec_label_pc_404c18:                              ; preds = %dec_label_pc_404c0f
  %v0_404c18 = load i32, i32* @ebp, align 4
  %v1_404c18 = add i32 %v0_404c18, 1
  store i32 %v1_404c18, i32* @ebp, align 4
  %v0_404c19 = load i32, i32* @ecx, align 4
  %v2_404c19 = add i32 %v1_404c14, -12
  %v3_404c19 = inttoptr i32 %v2_404c19 to i32*
  store i32 %v0_404c19, i32* %v3_404c19, align 4
  %v0_404c1a = load i32, i32* @esi, align 4
  %v1_404c1a = add i32 %v0_404c1a, 1
  %v2_404c1a = urem i32 %v0_404c1a, 16
  %v4_404c1a = icmp eq i32 %v2_404c1a, 15
  %tmp496 = xor i32 %v0_404c1a, -2147483648
  %v7_404c1a = and i32 %v1_404c1a, %tmp496
  %v8_404c1a = icmp slt i32 %v7_404c1a, 0
  store i1 %v4_404c1a, i1* %az.global-to-local, align 1
  store i1 %v8_404c1a, i1* %of.global-to-local, align 1
  %v9_404c1a = icmp eq i32 %v1_404c1a, 0
  store i1 %v9_404c1a, i1* %zf.global-to-local, align 1
  %v10_404c1a = icmp slt i32 %v1_404c1a, 0
  store i1 %v10_404c1a, i1* %sf.global-to-local, align 1
  store i32 %v1_404c1a, i32* @esi, align 4
  %v0_404c1b = load i32, i32* @ebp, align 4
  %v2_404c1b = add i32 %v1_404c14, -16
  %v3_404c1b = inttoptr i32 %v2_404c1b to i32*
  store i32 %v0_404c1b, i32* %v3_404c1b, align 4
  %v0_404c1c = load i32, i32* @ebp, align 4
  %v2_404c1c = add i32 %v1_404c14, -20
  %v3_404c1c = inttoptr i32 %v2_404c1c to i32*
  store i32 %v0_404c1c, i32* %v3_404c1c, align 4
  %v0_404c1d = load i32, i32* @edx, align 4
  %v2_404c1d = add i32 %v1_404c14, -24
  %v3_404c1d = inttoptr i32 %v2_404c1d to i32*
  store i32 %v0_404c1d, i32* %v3_404c1d, align 4
  br label %dec_label_pc_404c20

dec_label_pc_404c20:                              ; preds = %dec_label_pc_404bca, %dec_label_pc_404c18
  %v0_404c20 = load i32, i32* @edx, align 4
  %v1_404c20 = trunc i32 %v0_404c20 to i16
  %v2_404c20 = call i32 @__asm_insd(i16 %v1_404c20)
  %v3_404c20 = load i32, i32* @edi, align 4
  %v4_404c20 = inttoptr i32 %v3_404c20 to i32*
  store i32 %v2_404c20, i32* %v4_404c20, align 4
  %v0_404c21 = load i32, i32* @esi, align 4
  %v1_404c21 = add i32 %v0_404c21, -1
  %v2_404c21 = urem i32 %v0_404c21, 16
  %v3_404c21 = add nsw i32 %v2_404c21, -1
  %v4_404c21 = icmp ugt i32 %v3_404c21, 15
  %v0_404c22 = load i32, i32* @eax, align 4
  %v6_404c2218 = and i32 %v0_404c22, 14
  %v7_404c22 = icmp ugt i32 %v6_404c2218, 9
  %v8_404c22 = or i1 %v4_404c21, %v7_404c22
  %v9_404c22 = add i32 %v0_404c22, 6
  %v11_404c22 = select i1 %v8_404c22, i32 %v9_404c22, i32 %v0_404c22
  %v10_404c22 = zext i1 %v8_404c22 to i32
  %v15_404c22 = urem i32 %v11_404c22, 16
  %v18_404c22 = and i32 %v0_404c22, -65536
  %v19_404c22 = or i32 %v15_404c22, %v18_404c22
  %v12_404c223 = mul i32 %v10_404c22, 256
  %v3_404c224 = add i32 %v12_404c223, %v0_404c22
  %v22_404c22 = and i32 %v3_404c224, 65280
  %v24_404c22 = or i32 %v19_404c22, %v22_404c22
  store i32 %v24_404c22, i32* @eax, align 4
  store i1 %v8_404c22, i1* %cf.global-to-local, align 1
  %v1_404c23 = add i32 %v0_404c21, -2
  %v2_404c23 = urem i32 %v1_404c21, 16
  %v3_404c23 = add nsw i32 %v2_404c23, -1
  %v4_404c23 = icmp ugt i32 %v3_404c23, 15
  %tmp497 = sub i32 1, %v0_404c21
  %v7_404c23 = and i32 %v1_404c21, %tmp497
  %v8_404c23 = icmp slt i32 %v7_404c23, 0
  store i1 %v4_404c23, i1* %az.global-to-local, align 1
  store i1 %v8_404c23, i1* %of.global-to-local, align 1
  %v9_404c23 = icmp eq i32 %v1_404c23, 0
  store i1 %v9_404c23, i1* %zf.global-to-local, align 1
  %v10_404c23 = icmp slt i32 %v1_404c23, 0
  store i1 %v10_404c23, i1* %sf.global-to-local, align 1
  store i32 %v1_404c23, i32* @esi, align 4
  %v2_404c24 = or i1 %v9_404c23, %v8_404c22
  br i1 %v2_404c24, label %dec_label_pc_404c9a, label %dec_label_pc_404c26

dec_label_pc_404c26:                              ; preds = %dec_label_pc_404c20
  %v1_404c26 = add i32 %v24_404c22, -1
  %v3_404c26 = add nsw i32 %v15_404c22, -1
  %v4_404c26 = icmp ugt i32 %v3_404c26, 15
  %tmp498 = sub i32 0, %v24_404c22
  %v7_404c26 = and i32 %v0_404c22, %tmp498
  %v8_404c26 = icmp slt i32 %v7_404c26, 0
  store i1 %v4_404c26, i1* %az.global-to-local, align 1
  store i1 %v8_404c26, i1* %of.global-to-local, align 1
  %v9_404c26 = icmp eq i32 %v1_404c26, 0
  store i1 %v9_404c26, i1* %zf.global-to-local, align 1
  %v10_404c26 = icmp slt i32 %v1_404c26, 0
  store i1 %v10_404c26, i1* %sf.global-to-local, align 1
  store i32 %v1_404c26, i32* @eax, align 4
  br label %dec_label_pc_404c27

dec_label_pc_404c27:                              ; preds = %dec_label_pc_404c9a, %dec_label_pc_404c26
  %v0_404c27 = load i32, i32* @edx, align 4
  %v1_404c27 = add i32 %v0_404c27, 54
  %v2_404c27 = inttoptr i32 %v1_404c27 to i64*
  %v3_404c27 = load i64, i64* %v2_404c27, align 4
  %v4_404c27 = call i32 @__asm_bound(i64 %v3_404c27)
  store i32 %v4_404c27, i32* @edx, align 4
  br label %dec_label_pc_404c2a

dec_label_pc_404c2a:                              ; preds = %dec_label_pc_404c27, %dec_label_pc_404bb8
  %v0_404c2a = load i32, i32* @edi, align 4
  %v1_404c2a = add i32 %v0_404c2a, 1
  store i32 %v1_404c2a, i32* @edi, align 4
  %v0_404c2b = load i32, i32* @ecx, align 4
  %v1_404c2b = load i32, i32* @esp, align 4
  %v2_404c2b = add i32 %v1_404c2b, -4
  %v3_404c2b = inttoptr i32 %v2_404c2b to i32*
  store i32 %v0_404c2b, i32* %v3_404c2b, align 4
  %v0_404c2c = load i32, i32* @edx, align 4
  %v1_404c2c = add i32 %v0_404c2c, 1
  %v2_404c2c = urem i32 %v0_404c2c, 16
  %v4_404c2c = icmp eq i32 %v2_404c2c, 15
  %tmp499 = xor i32 %v0_404c2c, -2147483648
  %v7_404c2c = and i32 %v1_404c2c, %tmp499
  %v8_404c2c = icmp slt i32 %v7_404c2c, 0
  store i1 %v4_404c2c, i1* %az.global-to-local, align 1
  store i1 %v8_404c2c, i1* %of.global-to-local, align 1
  %v9_404c2c = icmp eq i32 %v1_404c2c, 0
  store i1 %v9_404c2c, i1* %zf.global-to-local, align 1
  %v10_404c2c = icmp slt i32 %v1_404c2c, 0
  store i1 %v10_404c2c, i1* %sf.global-to-local, align 1
  store i32 %v1_404c2c, i32* @edx, align 4
  br label %dec_label_pc_404c2d

dec_label_pc_404c2d:                              ; preds = %dec_label_pc_404bc1.dec_label_pc_404c2d_crit_edge, %dec_label_pc_404c2a
  %v0_404c2d = phi i1 [ %v0_404c2d.pre, %dec_label_pc_404bc1.dec_label_pc_404c2d_crit_edge ], [ %v10_404c2c, %dec_label_pc_404c2a ]
  %v1_404c2d = icmp eq i1 %v0_404c2d, false
  br i1 %v1_404c2d, label %dec_label_pc_404ca8, label %dec_label_pc_404c2f

dec_label_pc_404c2f:                              ; preds = %dec_label_pc_404c2d
  %v0_404c2f = load i32, i32* @esp, align 4
  %v1_404c2f = inttoptr i32 %v0_404c2f to i32*
  %v2_404c2f = load i32, i32* %v1_404c2f, align 4
  store i32 %v2_404c2f, i32* @ecx, align 4
  %v0_404c30 = load i32, i32* @edi, align 4
  store i32 %v0_404c30, i32* %v1_404c2f, align 4
  %v0_404c33 = load i32, i32* @eax, align 4
  %v2_404c33 = add i32 %v0_404c2f, -4
  %v3_404c33 = inttoptr i32 %v2_404c33 to i32*
  store i32 %v0_404c33, i32* %v3_404c33, align 4
  %v1_404c34 = add i32 %v0_404c2f, -3
  %v2_404c34 = urem i32 %v2_404c33, 16
  %v4_404c34 = icmp eq i32 %v2_404c34, 15
  %tmp500 = sub i32 3, %v0_404c2f
  %v7_404c34 = and i32 %v1_404c34, %tmp500
  %v8_404c34 = icmp slt i32 %v7_404c34, 0
  store i1 %v4_404c34, i1* %az.global-to-local, align 1
  store i1 %v8_404c34, i1* %of.global-to-local, align 1
  %v9_404c34 = icmp eq i32 %v1_404c34, 0
  store i1 %v9_404c34, i1* %zf.global-to-local, align 1
  %v10_404c34 = icmp slt i32 %v1_404c34, 0
  store i1 %v10_404c34, i1* %sf.global-to-local, align 1
  %v1_404c35 = icmp eq i1 %v9_404c34, false
  br i1 %v1_404c35, label %dec_label_pc_404c87, label %dec_label_pc_404c37

dec_label_pc_404c37:                              ; preds = %dec_label_pc_404c2f
  %v0_404c37 = load i32, i32* @ebx, align 4
  %v1_404c37 = add i32 %v0_404c37, 1
  store i32 %v1_404c37, i32* @ebx, align 4
  %v0_404c38 = load i32, i32* @ebp, align 4
  %v1_404c38 = add i32 %v0_404c38, 1
  %v2_404c38 = urem i32 %v0_404c38, 16
  %v4_404c38 = icmp eq i32 %v2_404c38, 15
  %tmp501 = xor i32 %v0_404c38, -2147483648
  %v7_404c38 = and i32 %v1_404c38, %tmp501
  %v8_404c38 = icmp slt i32 %v7_404c38, 0
  store i1 %v4_404c38, i1* %az.global-to-local, align 1
  store i1 %v8_404c38, i1* %of.global-to-local, align 1
  %v9_404c38 = icmp eq i32 %v1_404c38, 0
  store i1 %v9_404c38, i1* %zf.global-to-local, align 1
  %v10_404c38 = icmp slt i32 %v1_404c38, 0
  store i1 %v10_404c38, i1* %sf.global-to-local, align 1
  store i32 %v1_404c38, i32* @ebp, align 4
  %v0_404c39 = load i1, i1* %cf.global-to-local, align 1
  %v2_404c39 = or i1 %v9_404c38, %v0_404c39
  %v0_404c3b = load i32, i32* @ecx, align 4
  br i1 %v2_404c39, label %dec_label_pc_404c3b, label %dec_label_pc_404cae

dec_label_pc_404c3b:                              ; preds = %dec_label_pc_404c37
  %v1_404c3b = add i32 %v0_404c3b, 1
  %v2_404c3b = urem i32 %v0_404c3b, 16
  %v4_404c3b = icmp eq i32 %v2_404c3b, 15
  %tmp502 = xor i32 %v0_404c3b, -2147483648
  %v7_404c3b = and i32 %v1_404c3b, %tmp502
  %v8_404c3b = icmp slt i32 %v7_404c3b, 0
  store i1 %v4_404c3b, i1* %az.global-to-local, align 1
  store i1 %v8_404c3b, i1* %of.global-to-local, align 1
  %v9_404c3b = icmp eq i32 %v1_404c3b, 0
  store i1 %v9_404c3b, i1* %zf.global-to-local, align 1
  %v10_404c3b = icmp slt i32 %v1_404c3b, 0
  store i1 %v10_404c3b, i1* %sf.global-to-local, align 1
  store i32 %v1_404c3b, i32* @ecx, align 4
  br label %dec_label_pc_404c3d

dec_label_pc_404c3d:                              ; preds = %dec_label_pc_404bc8.dec_label_pc_404c3d_crit_edge, %dec_label_pc_404c3b
  %v1_404c3d = phi i32 [ %v1_404c3d.pre, %dec_label_pc_404bc8.dec_label_pc_404c3d_crit_edge ], [ %v1_404c34, %dec_label_pc_404c3b ]
  %v0_404c3d = phi i32 [ %v0_404c3d.pre, %dec_label_pc_404bc8.dec_label_pc_404c3d_crit_edge ], [ %v1_404c37, %dec_label_pc_404c3b ]
  %v2_404c3d = add i32 %v1_404c3d, -4
  %v3_404c3d = inttoptr i32 %v2_404c3d to i32*
  store i32 %v0_404c3d, i32* %v3_404c3d, align 4
  %v1_404c3e.pre = load i32, i32* @edi, align 4
  %v1_404c45.pre = load i32, i32* @ebp, align 4
  %v7_404c45.pre = load i32, i32* @edx, align 4
  br label %dec_label_pc_404c3e

dec_label_pc_404c3e:                              ; preds = %dec_label_pc_404c3d, %dec_label_pc_404bd7
  %v7_404c45 = phi i32 [ %v7_404c45.pre, %dec_label_pc_404c3d ], [ %v17_404b31, %dec_label_pc_404bd7 ]
  %v25_404c45 = phi i32 [ %v1_404c45.pre, %dec_label_pc_404c3d ], [ %v13_404b31, %dec_label_pc_404bd7 ]
  %v0_404c44 = phi i32 [ %v2_404c3d, %dec_label_pc_404c3d ], [ %v7_404b31, %dec_label_pc_404bd7 ]
  %v0_404c43 = phi i32 [ %v1_404c3e.pre, %dec_label_pc_404c3d ], [ %v9_404b31, %dec_label_pc_404bd7 ]
  %v2_404c3e = add i32 %v0_404c43, 86
  %v3_404c3e = inttoptr i32 %v2_404c3e to i32*
  %v4_404c3e = load i32, i32* %v3_404c3e, align 4
  %v6_404c3e = mul i32 %v4_404c3e, 107
  store i32 %v6_404c3e, i32* @ecx, align 4
  store i1 false, i1* %cf.global-to-local, align 1
  %v1_404c43 = add i32 %v0_404c43, 1
  store i32 %v1_404c43, i32* @edi, align 4
  %v1_404c44 = add i32 %v0_404c44, -1
  %v2_404c44 = urem i32 %v0_404c44, 16
  %v3_404c44 = add nsw i32 %v2_404c44, -1
  %v4_404c44 = icmp ugt i32 %v3_404c44, 15
  %tmp503 = sub i32 0, %v0_404c44
  %v7_404c44 = and i32 %v0_404c44, %tmp503
  %v8_404c44 = icmp slt i32 %v7_404c44, 0
  store i1 %v4_404c44, i1* %az.global-to-local, align 1
  store i1 %v8_404c44, i1* %of.global-to-local, align 1
  %v9_404c44 = icmp eq i32 %v1_404c44, 0
  store i1 %v9_404c44, i1* %zf.global-to-local, align 1
  %v10_404c44 = icmp slt i32 %v1_404c44, 0
  store i1 %v10_404c44, i1* %sf.global-to-local, align 1
  %v2_404c45 = mul i32 %v25_404c45, 2
  %v4_404c45 = add i32 %v2_404c3e, %v2_404c45
  %v5_404c45 = inttoptr i32 %v4_404c45 to i8*
  %v6_404c45 = load i8, i8* %v5_404c45, align 1
  %v8_404c45 = trunc i32 %v7_404c45 to i8
  %v9_404c45 = add i8 %v6_404c45, %v8_404c45
  %v10_404c45 = urem i8 %v6_404c45, 16
  %v11_404c45 = urem i8 %v8_404c45, 16
  %v12_404c45 = add nuw nsw i8 %v10_404c45, %v11_404c45
  %v13_404c45 = icmp ugt i8 %v12_404c45, 15
  %v14_404c45 = icmp ult i8 %v9_404c45, %v6_404c45
  %v15_404c45 = xor i8 %v9_404c45, %v6_404c45
  %v16_404c45 = xor i8 %v9_404c45, %v8_404c45
  %v17_404c45 = and i8 %v15_404c45, %v16_404c45
  %v18_404c45 = icmp slt i8 %v17_404c45, 0
  store i1 %v13_404c45, i1* %az.global-to-local, align 1
  store i1 %v14_404c45, i1* %cf.global-to-local, align 1
  store i1 %v18_404c45, i1* %of.global-to-local, align 1
  %v19_404c45 = icmp eq i8 %v9_404c45, 0
  store i1 %v19_404c45, i1* %zf.global-to-local, align 1
  %v20_404c45 = icmp slt i8 %v9_404c45, 0
  store i1 %v20_404c45, i1* %sf.global-to-local, align 1
  store i8 %v9_404c45, i8* %v5_404c45, align 1
  %v0_404c49.pre = load i1, i1* %of.global-to-local, align 1
  br i1 %v0_404c49.pre, label %dec_label_pc_404cbb, label %dec_label_pc_404c4b

dec_label_pc_404c49:                              ; preds = %dec_label_pc_404bf6
  br i1 %v9_404c04, label %dec_label_pc_404cbb, label %dec_label_pc_404c4e

dec_label_pc_404c4b:                              ; preds = %dec_label_pc_404c3e
  %v0_404c4b.pr = load i1, i1* %cf.global-to-local, align 1
  br i1 %v0_404c4b.pr, label %dec_label_pc_404c97, label %dec_label_pc_404c4e

dec_label_pc_404c4e:                              ; preds = %dec_label_pc_404c49, %dec_label_pc_404c4b
  %v0_404c4e = load i32, i32* @edx, align 4
  %v1_404c4e = trunc i32 %v0_404c4e to i16
  %v2_404c4e = load i32, i32* @esi, align 4
  %v3_404c4e = inttoptr i32 %v2_404c4e to i8*
  %v4_404c4e = load i8, i8* %v3_404c4e, align 1
  call void @__asm_outsb(i16 %v1_404c4e, i8 %v4_404c4e)
  %v0_404c4f.pre = load i1, i1* %cf.global-to-local, align 1
  %v1_404c4f.pre = load i1, i1* %zf.global-to-local, align 1
  br label %dec_label_pc_404c4f

dec_label_pc_404c4f:                              ; preds = %dec_label_pc_404c4e, %dec_label_pc_404c0f
  %v1_404c4f = phi i1 [ %v1_404c4f.pre, %dec_label_pc_404c4e ], [ %v9_404c13, %dec_label_pc_404c0f ]
  %v0_404c4f = phi i1 [ %v0_404c4f.pre, %dec_label_pc_404c4e ], [ false, %dec_label_pc_404c0f ]
  %v2_404c4f = or i1 %v1_404c4f, %v0_404c4f
  br i1 %v2_404c4f, label %dec_label_pc_404cb2, label %dec_label_pc_404c53

dec_label_pc_404c53:                              ; preds = %dec_label_pc_404c4f
  %v0_404c53 = load i32, i32* @esp, align 4
  %v1_404c53 = add i32 %v0_404c53, 4
  %v2_404c53 = add i32 %v0_404c53, 8
  %v3_404c53 = add i32 %v0_404c53, 16
  %v4_404c53 = add i32 %v0_404c53, 20
  %v5_404c53 = add i32 %v0_404c53, 24
  %v6_404c53 = add i32 %v0_404c53, 28
  %v8_404c53 = inttoptr i32 %v0_404c53 to i32*
  %v9_404c53 = load i32, i32* %v8_404c53, align 4
  %v10_404c53 = inttoptr i32 %v1_404c53 to i32*
  %v11_404c53 = load i32, i32* %v10_404c53, align 4
  %v12_404c53 = inttoptr i32 %v2_404c53 to i32*
  %v13_404c53 = load i32, i32* %v12_404c53, align 4
  %v14_404c53 = inttoptr i32 %v3_404c53 to i32*
  %v15_404c53 = load i32, i32* %v14_404c53, align 4
  %v16_404c53 = inttoptr i32 %v4_404c53 to i32*
  %v17_404c53 = load i32, i32* %v16_404c53, align 4
  %v18_404c53 = inttoptr i32 %v5_404c53 to i32*
  %v19_404c53 = load i32, i32* %v18_404c53, align 4
  %v20_404c53 = inttoptr i32 %v6_404c53 to i32*
  %v21_404c53 = load i32, i32* %v20_404c53, align 4
  store i32 %v9_404c53, i32* @edi, align 4
  store i32 %v11_404c53, i32* @esi, align 4
  store i32 %v13_404c53, i32* @ebp, align 4
  store i32 %v15_404c53, i32* @ebx, align 4
  %v22_404c53 = trunc i32 %v17_404c53 to i16
  store i32 %v17_404c53, i32* @edx, align 4
  store i32 %v19_404c53, i32* @ecx, align 4
  store i32 %v21_404c53, i32* @eax, align 4
  %v4_404c54 = inttoptr i32 %v11_404c53 to i8*
  %v5_404c54 = load i8, i8* %v4_404c54, align 1
  call void @__asm_outsb(i16 %v22_404c53, i8 %v5_404c54)
  %v6_404c54 = load i32, i32* @eax, align 4
  ret i32 %v6_404c54

dec_label_pc_404c56:                              ; preds = %dec_label_pc_404bea
  %v0_404c56 = load i32, i32* @ebp, align 4
  %v1_404c56 = add i32 %v0_404c56, 109
  %v2_404c56 = inttoptr i32 %v1_404c56 to i8*
  %v3_404c56 = load i8, i8* %v2_404c56, align 1
  %v4_404c56 = load i32, i32* @ecx, align 4
  %v5_404c56 = udiv i32 %v4_404c56, 256
  %v6_404c56 = trunc i32 %v5_404c56 to i8
  %v7_404c56 = add i8 %v3_404c56, %v6_404c56
  store i8 %v7_404c56, i8* %v2_404c56, align 1
  %v0_404c59 = load i32, i32* @ecx, align 4
  %v1_404c59 = add i32 %v0_404c59, -1
  store i32 %v1_404c59, i32* @ecx, align 4
  %v1_404c5d = add i32 %v0_404c59, 97
  %v2_404c5d = inttoptr i32 %v1_404c5d to i32*
  %v3_404c5d = load i32, i32* %v2_404c5d, align 4
  %v4_404c5d = load i32, i32* @edi, align 4
  %v5_404c5d = xor i32 %v4_404c5d, %v3_404c5d
  store i1 false, i1* %az.global-to-local, align 1
  store i1 false, i1* %cf.global-to-local, align 1
  store i1 false, i1* %of.global-to-local, align 1
  %v6_404c5d = icmp eq i32 %v5_404c5d, 0
  store i1 %v6_404c5d, i1* %zf.global-to-local, align 1
  %v7_404c5d = icmp slt i32 %v5_404c5d, 0
  store i1 %v7_404c5d, i1* %sf.global-to-local, align 1
  store i32 %v5_404c5d, i32* %v2_404c5d, align 4
  %v15_404c5d = load i32, i32* @eax, align 4
  ret i32 %v15_404c5d

dec_label_pc_404c62:                              ; preds = %dec_label_pc_404bed
  %v0_404c62 = load i32, i32* @edx, align 4
  %v1_404c62 = add i32 %v0_404c62, 97
  %v2_404c62 = inttoptr i32 %v1_404c62 to i16*
  %v3_404c62 = load i16, i16* %v2_404c62, align 2
  %v5_404c62 = trunc i32 %v0_404c62 to i16
  call void @__asm_arpl(i16 %v3_404c62, i16 %v5_404c62)
  %v0_404c65 = load i32, i32* @eax, align 4
  store i1 false, i1* %cf.global-to-local, align 1
  %v11_404c65 = xor i32 %v0_404c65, 107
  store i32 %v11_404c65, i32* @eax, align 4
  %v1_404c67 = load i32, i32* @esp, align 4
  %v2_404c67 = add i32 %v1_404c67, -4
  %v3_404c67 = inttoptr i32 %v2_404c67 to i32*
  store i32 %v11_404c65, i32* %v3_404c67, align 4
  %v1_404c68 = add i32 %v1_404c67, -3
  %v2_404c68 = urem i32 %v2_404c67, 16
  %v4_404c68 = icmp eq i32 %v2_404c68, 15
  %tmp504 = sub i32 3, %v1_404c67
  %v7_404c68 = and i32 %v1_404c68, %tmp504
  %v8_404c68 = icmp slt i32 %v7_404c68, 0
  store i1 %v4_404c68, i1* %az.global-to-local, align 1
  store i1 %v8_404c68, i1* %of.global-to-local, align 1
  %v9_404c68 = icmp eq i32 %v1_404c68, 0
  store i1 %v9_404c68, i1* %zf.global-to-local, align 1
  %v10_404c68 = icmp slt i32 %v1_404c68, 0
  store i1 %v10_404c68, i1* %sf.global-to-local, align 1
  %v0_404c69 = load i32, i32* @ebx, align 4
  %v1_404c69 = add i32 %v0_404c69, 116
  %v2_404c69 = inttoptr i32 %v1_404c69 to i8*
  %v3_404c69 = load i8, i8* %v2_404c69, align 1
  %v4_404c69 = load i32, i32* @edx, align 4
  %v5_404c69 = trunc i32 %v4_404c69 to i8
  %v6_404c69 = add i8 %v3_404c69, %v5_404c69
  %v7_404c69 = urem i8 %v3_404c69, 16
  %v8_404c69 = urem i8 %v5_404c69, 16
  %v9_404c69 = add nuw nsw i8 %v8_404c69, %v7_404c69
  %v10_404c69 = icmp ugt i8 %v9_404c69, 15
  %v11_404c69 = icmp ult i8 %v6_404c69, %v3_404c69
  %v12_404c69 = xor i8 %v6_404c69, %v3_404c69
  %v13_404c69 = xor i8 %v6_404c69, %v5_404c69
  %v14_404c69 = and i8 %v12_404c69, %v13_404c69
  %v15_404c69 = icmp slt i8 %v14_404c69, 0
  store i1 %v10_404c69, i1* %az.global-to-local, align 1
  store i1 %v11_404c69, i1* %cf.global-to-local, align 1
  store i1 %v15_404c69, i1* %of.global-to-local, align 1
  %v16_404c69 = icmp eq i8 %v6_404c69, 0
  store i1 %v16_404c69, i1* %zf.global-to-local, align 1
  %v17_404c69 = icmp slt i8 %v6_404c69, 0
  store i1 %v17_404c69, i1* %sf.global-to-local, align 1
  store i8 %v6_404c69, i8* %v2_404c69, align 1
  %v0_404c6c = load i1, i1* %cf.global-to-local, align 1
  br i1 %v0_404c6c, label %dec_label_pc_404c62.dec_label_pc_404cd7_crit_edge, label %dec_label_pc_404c6e

dec_label_pc_404c62.dec_label_pc_404cd7_crit_edge: ; preds = %dec_label_pc_404c62
  %v0_404cd7.pre = load i32, i32* @ebp, align 4
  %v4_404cd7.pre = load i32, i32* @eax, align 4
  br label %dec_label_pc_404cd7

dec_label_pc_404c6e:                              ; preds = %dec_label_pc_404c62
  %v0_404c6e = load i32, i32* @edx, align 4
  %v1_404c6e = trunc i32 %v0_404c6e to i16
  %v2_404c6e = load i32, i32* @esi, align 4
  %v3_404c6e = inttoptr i32 %v2_404c6e to i8*
  %v4_404c6e = load i8, i8* %v3_404c6e, align 1
  call void @__asm_outsb(i16 %v1_404c6e, i8 %v4_404c6e)
  %v0_404c6f = load i32, i32* @ebx, align 4
  %v2_404c6f = load i32, i32* @edi, align 4
  %v5_404c6f = add i32 %v0_404c6f, 110
  %v6_404c6f = add i32 %v5_404c6f, %v2_404c6f
  %tmp505 = urem i32 %v6_404c6f, 65536
  %v7_404c6f = inttoptr i32 %tmp505 to i8*
  %v8_404c6f = load i8, i8* %v7_404c6f, align 1
  %v9_404c6f = load i32, i32* @ecx, align 4
  %v10_404c6f = trunc i32 %v9_404c6f to i8
  %v11_404c6f = add i8 %v8_404c6f, %v10_404c6f
  %v12_404c6f = urem i8 %v8_404c6f, 16
  %v13_404c6f = urem i8 %v10_404c6f, 16
  %v14_404c6f = add nuw nsw i8 %v13_404c6f, %v12_404c6f
  %v15_404c6f = icmp ugt i8 %v14_404c6f, 15
  %v16_404c6f = icmp ult i8 %v11_404c6f, %v8_404c6f
  %v17_404c6f = xor i8 %v11_404c6f, %v8_404c6f
  %v18_404c6f = xor i8 %v11_404c6f, %v10_404c6f
  %v19_404c6f = and i8 %v17_404c6f, %v18_404c6f
  %v20_404c6f = icmp slt i8 %v19_404c6f, 0
  store i1 %v15_404c6f, i1* %az.global-to-local, align 1
  store i1 %v16_404c6f, i1* %cf.global-to-local, align 1
  store i1 %v20_404c6f, i1* %of.global-to-local, align 1
  %v21_404c6f = icmp eq i8 %v11_404c6f, 0
  store i1 %v21_404c6f, i1* %zf.global-to-local, align 1
  %v22_404c6f = icmp slt i8 %v11_404c6f, 0
  store i1 %v22_404c6f, i1* %sf.global-to-local, align 1
  store i8 %v11_404c6f, i8* %v7_404c6f, align 1
  %v0_404c73 = load i1, i1* %sf.global-to-local, align 1
  br i1 %v0_404c73, label %dec_label_pc_404cc6, label %dec_label_pc_404c77

dec_label_pc_404c77:                              ; preds = %dec_label_pc_404c6e
  %v0_404c77 = load i32, i32* @ebx, align 4
  %v1_404c77 = add i32 %v0_404c77, 54
  %v2_404c77 = inttoptr i32 %v1_404c77 to i8*
  %v3_404c77 = load i8, i8* %v2_404c77, align 1
  %v4_404c77 = load i32, i32* @ecx, align 4
  %v5_404c77 = trunc i32 %v4_404c77 to i8
  %v6_404c77 = add i8 %v3_404c77, %v5_404c77
  %v11_404c77 = icmp ult i8 %v6_404c77, %v3_404c77
  store i1 %v11_404c77, i1* %cf.global-to-local, align 1
  store i8 %v6_404c77, i8* %v2_404c77, align 1
  %v0_404c7b = load i32, i32* @edx, align 4
  %v1_404c7b = add i32 %v0_404c7b, -1
  store i32 %v1_404c7b, i32* @edx, align 4
  %v0_404c7c = load i32, i32* @esp, align 4
  %v1_404c7c = inttoptr i32 %v0_404c7c to i32*
  %v2_404c7c = load i32, i32* %v1_404c7c, align 4
  store i32 %v2_404c7c, i32* @ecx, align 4
  %v1_404c7d = add i32 %v0_404c7b, -2
  %v2_404c7d = urem i32 %v1_404c7b, 16
  %v3_404c7d = add nsw i32 %v2_404c7d, -1
  %v4_404c7d = icmp ugt i32 %v3_404c7d, 15
  %tmp506 = sub i32 1, %v0_404c7b
  %v7_404c7d = and i32 %v1_404c7b, %tmp506
  %v8_404c7d = icmp slt i32 %v7_404c7d, 0
  store i1 %v4_404c7d, i1* %az.global-to-local, align 1
  store i1 %v8_404c7d, i1* %of.global-to-local, align 1
  %v9_404c7d = icmp eq i32 %v1_404c7d, 0
  store i1 %v9_404c7d, i1* %zf.global-to-local, align 1
  %v10_404c7d = icmp slt i32 %v1_404c7d, 0
  store i1 %v10_404c7d, i1* %sf.global-to-local, align 1
  store i32 %v1_404c7d, i32* @edx, align 4
  %v1_404c7e = add i32 %v2_404c7c, 113
  %v2_404c7e = inttoptr i32 %v1_404c7e to i64*
  %v3_404c7e = load i64, i64* %v2_404c7e, align 4
  %v4_404c7e = call i32 @__asm_bound(i64 %v3_404c7e)
  store i32 %v4_404c7e, i32* @edi, align 4
  %v1_404c81 = add i32 %v4_404c7e, 89
  %v2_404c81 = inttoptr i32 %v1_404c81 to i16*
  %v3_404c81 = load i16, i16* %v2_404c81, align 2
  %v4_404c81 = load i32, i32* @esp, align 4
  %v5_404c81 = trunc i32 %v4_404c81 to i16
  call void @__asm_arpl(i16 %v3_404c81, i16 %v5_404c81)
  %v0_404c84 = load i1, i1* %sf.global-to-local, align 1
  %v0_404ce9 = load i32, i32* @edi, align 4
  br i1 %v0_404c84, label %dec_label_pc_404ce9, label %dec_label_pc_404c86

dec_label_pc_404c86:                              ; preds = %dec_label_pc_404c77
  %v1_404c86 = load i32, i32* @esp, align 4
  %v2_404c86 = add i32 %v1_404c86, -4
  %v3_404c86 = inttoptr i32 %v2_404c86 to i32*
  store i32 %v0_404ce9, i32* %v3_404c86, align 4
  br label %dec_label_pc_404c87

dec_label_pc_404c87:                              ; preds = %dec_label_pc_404c86, %dec_label_pc_404c2f
  %v0_404c89 = phi i32 [ %v2_404c86, %dec_label_pc_404c86 ], [ %v1_404c34, %dec_label_pc_404c2f ]
  %v0_404c87 = load i32, i32* @ecx, align 4
  %v1_404c87 = add i32 %v0_404c87, 1
  store i32 %v1_404c87, i32* @ecx, align 4
  %v0_404c88 = load i32, i32* @ebx, align 4
  %v1_404c88 = add i32 %v0_404c88, -1
  %v2_404c88 = urem i32 %v0_404c88, 16
  %v3_404c88 = add nsw i32 %v2_404c88, -1
  %v4_404c88 = icmp ugt i32 %v3_404c88, 15
  %tmp507 = sub i32 0, %v0_404c88
  %v7_404c88 = and i32 %v0_404c88, %tmp507
  %v8_404c88 = icmp slt i32 %v7_404c88, 0
  store i1 %v4_404c88, i1* %az.global-to-local, align 1
  store i1 %v8_404c88, i1* %of.global-to-local, align 1
  %v9_404c88 = icmp eq i32 %v1_404c88, 0
  store i1 %v9_404c88, i1* %zf.global-to-local, align 1
  %v10_404c88 = icmp slt i32 %v1_404c88, 0
  store i1 %v10_404c88, i1* %sf.global-to-local, align 1
  store i32 %v1_404c88, i32* @ebx, align 4
  %v2_404c89 = add i32 %v0_404c89, -4
  %v3_404c89 = inttoptr i32 %v2_404c89 to i32*
  store i32 %v0_404c89, i32* %v3_404c89, align 4
  %v0_404c8a = load i32, i32* @ebx, align 4
  %v2_404c8a = add i32 %v0_404c8a, 101
  %tmp508 = urem i32 %v2_404c8a, 65536
  %v3_404c8a = inttoptr i32 %tmp508 to i8*
  %v4_404c8a = load i8, i8* %v3_404c8a, align 1
  %v5_404c8a = load i32, i32* @eax, align 4
  %v6_404c8a = trunc i32 %v5_404c8a to i8
  %v7_404c8a = add i8 %v4_404c8a, %v6_404c8a
  %v8_404c8a = urem i8 %v4_404c8a, 16
  %v9_404c8a = urem i8 %v6_404c8a, 16
  %v10_404c8a = add nuw nsw i8 %v9_404c8a, %v8_404c8a
  %v11_404c8a = icmp ugt i8 %v10_404c8a, 15
  %v12_404c8a = icmp ult i8 %v7_404c8a, %v4_404c8a
  %v13_404c8a = xor i8 %v7_404c8a, %v4_404c8a
  %v14_404c8a = xor i8 %v7_404c8a, %v6_404c8a
  %v15_404c8a = and i8 %v13_404c8a, %v14_404c8a
  %v16_404c8a = icmp slt i8 %v15_404c8a, 0
  store i1 %v11_404c8a, i1* %az.global-to-local, align 1
  store i1 %v12_404c8a, i1* %cf.global-to-local, align 1
  store i1 %v16_404c8a, i1* %of.global-to-local, align 1
  %v17_404c8a = icmp eq i8 %v7_404c8a, 0
  store i1 %v17_404c8a, i1* %zf.global-to-local, align 1
  %v18_404c8a = icmp slt i8 %v7_404c8a, 0
  store i1 %v18_404c8a, i1* %sf.global-to-local, align 1
  store i8 %v7_404c8a, i8* %v3_404c8a, align 1
  %v0_404c8e = load i1, i1* %zf.global-to-local, align 1
  br i1 %v0_404c8e, label %dec_label_pc_404ce4, label %dec_label_pc_404c90

dec_label_pc_404c90:                              ; preds = %dec_label_pc_404c87
  %v0_404c90 = load i1, i1* %sf.global-to-local, align 1
  %v1_404c90 = icmp eq i1 %v0_404c90, false
  br i1 %v1_404c90, label %dec_label_pc_404d02, label %dec_label_pc_404c92

dec_label_pc_404c92:                              ; preds = %dec_label_pc_404c90
  %v0_404c92 = load i32, i32* @esi, align 4
  %v1_404c92 = add i32 %v0_404c92, 1
  %v2_404c92 = urem i32 %v0_404c92, 16
  %v4_404c92 = icmp eq i32 %v2_404c92, 15
  %tmp509 = xor i32 %v0_404c92, -2147483648
  %v7_404c92 = and i32 %v1_404c92, %tmp509
  %v8_404c92 = icmp slt i32 %v7_404c92, 0
  store i1 %v4_404c92, i1* %az.global-to-local, align 1
  store i1 %v8_404c92, i1* %of.global-to-local, align 1
  %v9_404c92 = icmp eq i32 %v1_404c92, 0
  store i1 %v9_404c92, i1* %zf.global-to-local, align 1
  %v10_404c92 = icmp slt i32 %v1_404c92, 0
  store i1 %v10_404c92, i1* %sf.global-to-local, align 1
  store i32 %v1_404c92, i32* @esi, align 4
  %v0_404c94 = load i1, i1* %cf.global-to-local, align 1
  br i1 %v0_404c94, label %dec_label_pc_404d05, label %dec_label_pc_404c96

dec_label_pc_404c96:                              ; preds = %dec_label_pc_404c92
  %v0_404c96 = load i32, i32* @edx, align 4
  %v1_404c96 = trunc i32 %v0_404c96 to i16
  %v2_404c96 = call i32 @__asm_insd(i16 %v1_404c96)
  %v3_404c96 = load i32, i32* @edi, align 4
  %v4_404c96 = inttoptr i32 %v3_404c96 to i32*
  store i32 %v2_404c96, i32* %v4_404c96, align 4
  br label %dec_label_pc_404c97

dec_label_pc_404c97:                              ; preds = %dec_label_pc_404c96, %dec_label_pc_404c4b
  %v0_404c97 = load i32, i32* @eax, align 4
  %v1_404c97 = add i32 %v0_404c97, -1
  %v2_404c97 = urem i32 %v0_404c97, 16
  %v3_404c97 = add nsw i32 %v2_404c97, -1
  %v4_404c97 = icmp ugt i32 %v3_404c97, 15
  %tmp510 = sub i32 0, %v0_404c97
  %v7_404c97 = and i32 %v0_404c97, %tmp510
  %v8_404c97 = icmp slt i32 %v7_404c97, 0
  store i1 %v4_404c97, i1* %az.global-to-local, align 1
  store i1 %v8_404c97, i1* %of.global-to-local, align 1
  %v9_404c97 = icmp eq i32 %v1_404c97, 0
  store i1 %v9_404c97, i1* %zf.global-to-local, align 1
  %v10_404c97 = icmp slt i32 %v1_404c97, 0
  store i1 %v10_404c97, i1* %sf.global-to-local, align 1
  store i32 %v1_404c97, i32* @eax, align 4
  %v0_404c98 = load i32, i32* @esp, align 4
  %v1_404c98 = add i32 %v0_404c98, 4
  %v2_404c98 = add i32 %v0_404c98, 8
  %v3_404c98 = add i32 %v0_404c98, 16
  %v4_404c98 = add i32 %v0_404c98, 20
  %v5_404c98 = add i32 %v0_404c98, 24
  %v6_404c98 = add i32 %v0_404c98, 28
  %v8_404c98 = inttoptr i32 %v0_404c98 to i32*
  %v9_404c98 = load i32, i32* %v8_404c98, align 4
  %v10_404c98 = inttoptr i32 %v1_404c98 to i32*
  %v11_404c98 = load i32, i32* %v10_404c98, align 4
  %v12_404c98 = inttoptr i32 %v2_404c98 to i32*
  %v13_404c98 = load i32, i32* %v12_404c98, align 4
  %v14_404c98 = inttoptr i32 %v3_404c98 to i32*
  %v15_404c98 = load i32, i32* %v14_404c98, align 4
  %v16_404c98 = inttoptr i32 %v4_404c98 to i32*
  %v17_404c98 = load i32, i32* %v16_404c98, align 4
  %v18_404c98 = inttoptr i32 %v5_404c98 to i32*
  %v19_404c98 = load i32, i32* %v18_404c98, align 4
  %v20_404c98 = inttoptr i32 %v6_404c98 to i32*
  %v21_404c98 = load i32, i32* %v20_404c98, align 4
  store i32 %v9_404c98, i32* @edi, align 4
  store i32 %v11_404c98, i32* @esi, align 4
  store i32 %v13_404c98, i32* @ebp, align 4
  store i32 %v15_404c98, i32* @ebx, align 4
  store i32 %v17_404c98, i32* @edx, align 4
  store i32 %v19_404c98, i32* @ecx, align 4
  store i32 %v21_404c98, i32* @eax, align 4
  %v1_404c99 = trunc i32 %v17_404c98 to i16
  %v3_404c99 = inttoptr i32 %v11_404c98 to i8*
  %v4_404c99 = load i8, i8* %v3_404c99, align 1
  call void @__asm_outsb(i16 %v1_404c99, i8 %v4_404c99)
  br label %dec_label_pc_404c9a

dec_label_pc_404c9a:                              ; preds = %dec_label_pc_404c97, %dec_label_pc_404c20
  %v0_404c9a = load i32, i32* @edx, align 4
  %v1_404c9a = trunc i32 %v0_404c9a to i16
  %v2_404c9a = call i8 @__asm_insb(i16 %v1_404c9a)
  %v3_404c9a = load i32, i32* @edi, align 4
  %v4_404c9a = inttoptr i32 %v3_404c9a to i8*
  store i8 %v2_404c9a, i8* %v4_404c9a, align 1
  %v0_404c9c = load i32, i32* @ebp, align 4
  %v1_404c9c = add i32 %v0_404c9c, 115
  %v3_404c9c = call i8 @__readgsbyte(i32 %v1_404c9c)
  %v4_404c9c = load i32, i32* @edx, align 4
  %v5_404c9c = trunc i32 %v4_404c9c to i8
  %v6_404c9c = add i8 %v3_404c9c, %v5_404c9c
  %v7_404c9c = urem i8 %v3_404c9c, 16
  %v8_404c9c = urem i8 %v5_404c9c, 16
  %v9_404c9c = add nuw nsw i8 %v8_404c9c, %v7_404c9c
  %v10_404c9c = icmp ugt i8 %v9_404c9c, 15
  %v11_404c9c = icmp ult i8 %v6_404c9c, %v3_404c9c
  %v12_404c9c = xor i8 %v6_404c9c, %v3_404c9c
  %v13_404c9c = xor i8 %v6_404c9c, %v5_404c9c
  %v14_404c9c = and i8 %v12_404c9c, %v13_404c9c
  %v15_404c9c = icmp slt i8 %v14_404c9c, 0
  store i1 %v10_404c9c, i1* %az.global-to-local, align 1
  store i1 %v11_404c9c, i1* %cf.global-to-local, align 1
  store i1 %v15_404c9c, i1* %of.global-to-local, align 1
  %v16_404c9c = icmp eq i8 %v6_404c9c, 0
  store i1 %v16_404c9c, i1* %zf.global-to-local, align 1
  %v17_404c9c = icmp slt i8 %v6_404c9c, 0
  store i1 %v17_404c9c, i1* %sf.global-to-local, align 1
  %v21_404c9c = load i32, i32* @ebp, align 4
  %v22_404c9c = add i32 %v21_404c9c, 115
  call void @__writegsbyte(i32 %v22_404c9c, i8 %v6_404c9c)
  %v0_404ca3 = load i32, i32* @ecx, align 4
  %v1_404ca3 = add i32 %v0_404ca3, -1
  store i32 %v1_404ca3, i32* @ecx, align 4
  %v2_404ca3 = icmp ne i32 %v1_404ca3, 0
  %v3_404ca3 = load i1, i1* %zf.global-to-local, align 1
  %v4_404ca3 = icmp eq i1 %v2_404ca3, %v3_404ca3
  br i1 %v4_404ca3, label %dec_label_pc_404c27, label %dec_label_pc_404ca5

dec_label_pc_404ca5:                              ; preds = %dec_label_pc_404c9a
  %v0_404ca5 = load i32, i32* @ebx, align 4
  %v1_404ca5 = and i32 %v0_404ca5, -256
  store i32 %v1_404ca5, i32* @ebx, align 4
  %v2_404ca5 = load i32, i32* @eax, align 4
  ret i32 %v2_404ca5

dec_label_pc_404ca8:                              ; preds = %dec_label_pc_404c2d
  %v3_404ca5 = load i32, i32* @eax, align 4
  ret i32 %v3_404ca5

dec_label_pc_404cae:                              ; preds = %dec_label_pc_404c37
  %v1_404cae = add i32 %v0_404c3b, -1
  store i32 %v1_404cae, i32* @ecx, align 4
  %v2_404cae = load i32, i32* @eax, align 4
  ret i32 %v2_404cae

dec_label_pc_404cb2:                              ; preds = %dec_label_pc_404c4f
  %v0_404cb2 = load i32, i32* @esi, align 4
  %v1_404cb2 = add i32 %v0_404cb2, -1
  %v2_404cb2 = urem i32 %v0_404cb2, 16
  %v3_404cb2 = add nsw i32 %v2_404cb2, -1
  %v4_404cb2 = icmp ugt i32 %v3_404cb2, 15
  %tmp511 = sub i32 0, %v0_404cb2
  %v7_404cb2 = and i32 %v0_404cb2, %tmp511
  %v8_404cb2 = icmp slt i32 %v7_404cb2, 0
  store i1 %v4_404cb2, i1* %az.global-to-local, align 1
  store i1 %v8_404cb2, i1* %of.global-to-local, align 1
  %v9_404cb2 = icmp eq i32 %v1_404cb2, 0
  store i1 %v9_404cb2, i1* %zf.global-to-local, align 1
  %v10_404cb2 = icmp slt i32 %v1_404cb2, 0
  store i1 %v10_404cb2, i1* %sf.global-to-local, align 1
  store i32 %v1_404cb2, i32* @esi, align 4
  %v0_404cb3 = load i32, i32* @ebx, align 4
  %v1_404cb3 = add i32 %v0_404cb3, 112
  %v2_404cb3 = inttoptr i32 %v1_404cb3 to i64*
  %v3_404cb3 = load i64, i64* %v2_404cb3, align 4
  %v4_404cb3 = call i32 @__asm_bound(i64 %v3_404cb3)
  %v1_404cb6 = load i32, i32* @eax, align 4
  %v2_404cb6 = add i32 %v1_404cb6, 85
  %v3_404cb6 = inttoptr i32 %v2_404cb6 to i32*
  %v4_404cb6 = load i32, i32* %v3_404cb6, align 4
  %v6_404cb6 = mul i32 %v4_404cb6, 67
  store i32 %v6_404cb6, i32* @ebp, align 4
  store i1 false, i1* %of.global-to-local, align 1
  store i1 false, i1* %cf.global-to-local, align 1
  %v0_404cba = load i32, i32* @edi, align 4
  %v2_404cba = add i32 %v4_404cb3, -4
  %v3_404cba = inttoptr i32 %v2_404cba to i32*
  store i32 %v0_404cba, i32* %v3_404cba, align 4
  br label %dec_label_pc_404cbb

dec_label_pc_404cbb:                              ; preds = %dec_label_pc_404c3e, %dec_label_pc_404cb2, %dec_label_pc_404c49
  %v4_404cba = load i32, i32* @eax, align 4
  ret i32 %v4_404cba

dec_label_pc_404cc6:                              ; preds = %dec_label_pc_404c6e
  %v0_404cc6 = load i32, i32* @eax, align 4
  %v1_404cc6 = load i32, i32* @esi, align 4
  %v2_404cc6 = add i32 %v1_404cc6, 103
  %v3_404cc6 = inttoptr i32 %v2_404cc6 to i32*
  %v4_404cc6 = load i32, i32* %v3_404cc6, align 4
  %v5_404cc6 = xor i32 %v4_404cc6, %v0_404cc6
  store i1 false, i1* %az.global-to-local, align 1
  store i1 false, i1* %cf.global-to-local, align 1
  store i1 false, i1* %of.global-to-local, align 1
  %v6_404cc6 = icmp eq i32 %v5_404cc6, 0
  store i1 %v6_404cc6, i1* %zf.global-to-local, align 1
  %v7_404cc6 = icmp slt i32 %v5_404cc6, 0
  store i1 %v7_404cc6, i1* %sf.global-to-local, align 1
  store i32 %v5_404cc6, i32* @eax, align 4
  %v0_404cc9 = load i32, i32* @edx, align 4
  %v1_404cc9 = trunc i32 %v0_404cc9 to i16
  %v2_404cc9 = call i32 @__asm_insd(i16 %v1_404cc9)
  %v3_404cc9 = load i32, i32* @edi, align 4
  %v4_404cc9 = inttoptr i32 %v3_404cc9 to i32*
  store i32 %v2_404cc9, i32* %v4_404cc9, align 4
  %v0_404cca = load i32, i32* @eax, align 4
  %v1_404cca = inttoptr i32 %v0_404cca to i64*
  %v2_404cca = load i64, i64* %v1_404cca, align 4
  %v3_404cca = call i32 @__asm_bound(i64 %v2_404cca)
  store i32 %v3_404cca, i32* @esi, align 4
  %v0_404ccc = load i32, i32* @esp, align 4
  %v1_404ccc = add i32 %v0_404ccc, -1
  %v2_404ccc = urem i32 %v0_404ccc, 16
  %v3_404ccc = add nsw i32 %v2_404ccc, -1
  %v4_404ccc = icmp ugt i32 %v3_404ccc, 15
  %tmp512 = sub i32 0, %v0_404ccc
  %v7_404ccc = and i32 %v0_404ccc, %tmp512
  %v8_404ccc = icmp slt i32 %v7_404ccc, 0
  store i1 %v4_404ccc, i1* %az.global-to-local, align 1
  store i1 %v8_404ccc, i1* %of.global-to-local, align 1
  %v9_404ccc = icmp eq i32 %v1_404ccc, 0
  store i1 %v9_404ccc, i1* %zf.global-to-local, align 1
  %v10_404ccc = icmp slt i32 %v1_404ccc, 0
  store i1 %v10_404ccc, i1* %sf.global-to-local, align 1
  br i1 %v9_404ccc, label %dec_label_pc_404cc6.dec_label_pc_404d38_crit_edge, label %dec_label_pc_404cd0

dec_label_pc_404cc6.dec_label_pc_404d38_crit_edge: ; preds = %dec_label_pc_404cc6
  %v0_404d39.pre = load i32, i32* @ebx, align 4
  br label %dec_label_pc_404d38

dec_label_pc_404cd0:                              ; preds = %dec_label_pc_404cc6
  %v0_404cd0 = load i32, i32* @ebp, align 4
  %v1_404cd0 = add i32 %v0_404cd0, 1
  store i32 %v1_404cd0, i32* @ebp, align 4
  %v1_404cd1 = trunc i32 %v1_404cd0 to i16
  %v3_404cd1 = add i32 %v0_404ccc, -3
  %v4_404cd1 = inttoptr i32 %v3_404cd1 to i16*
  store i16 %v1_404cd1, i16* %v4_404cd1, align 2
  %v1_404cd4 = add i32 %v0_404ccc, 1
  %v2_404cd4 = add i32 %v0_404ccc, 5
  %v3_404cd4 = add i32 %v0_404ccc, 13
  %v4_404cd4 = add i32 %v0_404ccc, 17
  %v5_404cd4 = add i32 %v0_404ccc, 21
  %v6_404cd4 = add i32 %v0_404ccc, 25
  %v8_404cd4 = inttoptr i32 %v3_404cd1 to i32*
  %v9_404cd4 = load i32, i32* %v8_404cd4, align 4
  %v10_404cd4 = inttoptr i32 %v1_404cd4 to i32*
  %v11_404cd4 = load i32, i32* %v10_404cd4, align 4
  %v12_404cd4 = inttoptr i32 %v2_404cd4 to i32*
  %v13_404cd4 = load i32, i32* %v12_404cd4, align 4
  %v14_404cd4 = inttoptr i32 %v3_404cd4 to i32*
  %v15_404cd4 = load i32, i32* %v14_404cd4, align 4
  %v16_404cd4 = inttoptr i32 %v4_404cd4 to i32*
  %v17_404cd4 = load i32, i32* %v16_404cd4, align 4
  %v18_404cd4 = inttoptr i32 %v5_404cd4 to i32*
  %v19_404cd4 = load i32, i32* %v18_404cd4, align 4
  %v20_404cd4 = inttoptr i32 %v6_404cd4 to i32*
  %v21_404cd4 = load i32, i32* %v20_404cd4, align 4
  store i32 %v9_404cd4, i32* @edi, align 4
  store i32 %v11_404cd4, i32* @esi, align 4
  store i32 %v13_404cd4, i32* @ebp, align 4
  store i32 %v15_404cd4, i32* @ebx, align 4
  store i32 %v17_404cd4, i32* @edx, align 4
  store i32 %v21_404cd4, i32* @eax, align 4
  %v1_404cd5 = add i32 %v19_404cd4, -1
  %v2_404cd5 = urem i32 %v19_404cd4, 16
  %v3_404cd5 = add nsw i32 %v2_404cd5, -1
  %v4_404cd5 = icmp ugt i32 %v3_404cd5, 15
  %tmp513 = sub i32 0, %v19_404cd4
  %v7_404cd5 = and i32 %v19_404cd4, %tmp513
  %v8_404cd5 = icmp slt i32 %v7_404cd5, 0
  store i1 %v4_404cd5, i1* %az.global-to-local, align 1
  store i1 %v8_404cd5, i1* %of.global-to-local, align 1
  %v9_404cd5 = icmp eq i32 %v1_404cd5, 0
  store i1 %v9_404cd5, i1* %zf.global-to-local, align 1
  %v10_404cd5 = icmp slt i32 %v1_404cd5, 0
  store i1 %v10_404cd5, i1* %sf.global-to-local, align 1
  store i32 %v1_404cd5, i32* @ecx, align 4
  br label %dec_label_pc_404cd7

dec_label_pc_404cd7:                              ; preds = %dec_label_pc_404c62.dec_label_pc_404cd7_crit_edge, %dec_label_pc_404cd0
  %v4_404cd7 = phi i32 [ %v4_404cd7.pre, %dec_label_pc_404c62.dec_label_pc_404cd7_crit_edge ], [ %v21_404cd4, %dec_label_pc_404cd0 ]
  %v21_404cd7 = phi i32 [ %v0_404cd7.pre, %dec_label_pc_404c62.dec_label_pc_404cd7_crit_edge ], [ %v13_404cd4, %dec_label_pc_404cd0 ]
  %v1_404cd7 = add i32 %v21_404cd7, 113
  %v2_404cd7 = inttoptr i32 %v1_404cd7 to i8*
  %v3_404cd7 = load i8, i8* %v2_404cd7, align 1
  %v5_404cd7 = trunc i32 %v4_404cd7 to i8
  %v6_404cd7 = add i8 %v3_404cd7, %v5_404cd7
  %v7_404cd7 = urem i8 %v3_404cd7, 16
  %v8_404cd7 = urem i8 %v5_404cd7, 16
  %v9_404cd7 = add nuw nsw i8 %v7_404cd7, %v8_404cd7
  %v10_404cd7 = icmp ugt i8 %v9_404cd7, 15
  %v11_404cd7 = icmp ult i8 %v6_404cd7, %v3_404cd7
  %v12_404cd7 = xor i8 %v6_404cd7, %v3_404cd7
  %v13_404cd7 = xor i8 %v6_404cd7, %v5_404cd7
  %v14_404cd7 = and i8 %v12_404cd7, %v13_404cd7
  %v15_404cd7 = icmp slt i8 %v14_404cd7, 0
  store i1 %v10_404cd7, i1* %az.global-to-local, align 1
  store i1 %v11_404cd7, i1* %cf.global-to-local, align 1
  store i1 %v15_404cd7, i1* %of.global-to-local, align 1
  %v16_404cd7 = icmp eq i8 %v6_404cd7, 0
  store i1 %v16_404cd7, i1* %zf.global-to-local, align 1
  %v17_404cd7 = icmp slt i8 %v6_404cd7, 0
  store i1 %v17_404cd7, i1* %sf.global-to-local, align 1
  store i8 %v6_404cd7, i8* %v2_404cd7, align 1
  %v0_404cda = load i1, i1* %zf.global-to-local, align 1
  %v1_404cda = icmp eq i1 %v0_404cda, false
  br i1 %v1_404cda, label %dec_label_pc_404d3d, label %dec_label_pc_404cdc

dec_label_pc_404cdc:                              ; preds = %dec_label_pc_404cd7
  %v0_404cdc = load i32, i32* @edx, align 4
  %v1_404cdc = trunc i32 %v0_404cdc to i16
  %v2_404cdc = call i8 @__asm_insb(i16 %v1_404cdc)
  %v3_404cdc = load i32, i32* @edi, align 4
  %v4_404cdc = inttoptr i32 %v3_404cdc to i8*
  store i8 %v2_404cdc, i8* %v4_404cdc, align 1
  %v0_404cdf = load i32, i32* @edi, align 4
  %v1_404cdf = add i32 %v0_404cdf, 1
  %v2_404cdf = urem i32 %v0_404cdf, 16
  %v4_404cdf = icmp eq i32 %v2_404cdf, 15
  %tmp514 = xor i32 %v0_404cdf, -2147483648
  %v7_404cdf = and i32 %v1_404cdf, %tmp514
  %v8_404cdf = icmp slt i32 %v7_404cdf, 0
  store i1 %v4_404cdf, i1* %az.global-to-local, align 1
  store i1 %v8_404cdf, i1* %of.global-to-local, align 1
  %v9_404cdf = icmp eq i32 %v1_404cdf, 0
  store i1 %v9_404cdf, i1* %zf.global-to-local, align 1
  %v10_404cdf = icmp slt i32 %v1_404cdf, 0
  store i1 %v10_404cdf, i1* %sf.global-to-local, align 1
  store i32 %v1_404cdf, i32* @edi, align 4
  br i1 %v9_404cdf, label %dec_label_pc_404d2b, label %dec_label_pc_404ce3

dec_label_pc_404ce3:                              ; preds = %dec_label_pc_404cdc
  %v0_404ce3 = load i32, i32* @esp, align 4
  %v1_404ce3 = add i32 %v0_404ce3, 4
  %v2_404ce3 = add i32 %v0_404ce3, 8
  %v3_404ce3 = add i32 %v0_404ce3, 16
  %v4_404ce3 = add i32 %v0_404ce3, 20
  %v5_404ce3 = add i32 %v0_404ce3, 24
  %v6_404ce3 = add i32 %v0_404ce3, 28
  %v8_404ce3 = inttoptr i32 %v0_404ce3 to i32*
  %v9_404ce3 = load i32, i32* %v8_404ce3, align 4
  %v10_404ce3 = inttoptr i32 %v1_404ce3 to i32*
  %v11_404ce3 = load i32, i32* %v10_404ce3, align 4
  %v12_404ce3 = inttoptr i32 %v2_404ce3 to i32*
  %v13_404ce3 = load i32, i32* %v12_404ce3, align 4
  %v14_404ce3 = inttoptr i32 %v3_404ce3 to i32*
  %v15_404ce3 = load i32, i32* %v14_404ce3, align 4
  %v16_404ce3 = inttoptr i32 %v4_404ce3 to i32*
  %v17_404ce3 = load i32, i32* %v16_404ce3, align 4
  %v18_404ce3 = inttoptr i32 %v5_404ce3 to i32*
  %v19_404ce3 = load i32, i32* %v18_404ce3, align 4
  %v20_404ce3 = inttoptr i32 %v6_404ce3 to i32*
  %v21_404ce3 = load i32, i32* %v20_404ce3, align 4
  store i32 %v9_404ce3, i32* @edi, align 4
  store i32 %v11_404ce3, i32* @esi, align 4
  store i32 %v13_404ce3, i32* @ebp, align 4
  store i32 %v15_404ce3, i32* @ebx, align 4
  store i32 %v17_404ce3, i32* @edx, align 4
  store i32 %v19_404ce3, i32* @ecx, align 4
  store i32 %v21_404ce3, i32* @eax, align 4
  br label %dec_label_pc_404ce4

dec_label_pc_404ce4:                              ; preds = %dec_label_pc_404ce3, %dec_label_pc_404c87
  %v0_404ce4 = load i1, i1* %cf.global-to-local, align 1
  %v1_404ce4 = icmp eq i1 %v0_404ce4, false
  br i1 %v1_404ce4, label %dec_label_pc_404d4e, label %dec_label_pc_404ce6

dec_label_pc_404ce6:                              ; preds = %dec_label_pc_404ce4
  %v0_404ce6 = load i32, i32* @ebx, align 4
  %v1_404ce6 = add i32 %v0_404ce6, 1
  %v2_404ce6 = urem i32 %v0_404ce6, 16
  %v4_404ce6 = icmp eq i32 %v2_404ce6, 15
  %tmp515 = xor i32 %v0_404ce6, -2147483648
  %v7_404ce6 = and i32 %v1_404ce6, %tmp515
  %v8_404ce6 = icmp slt i32 %v7_404ce6, 0
  store i1 %v4_404ce6, i1* %az.global-to-local, align 1
  store i1 %v8_404ce6, i1* %of.global-to-local, align 1
  %v9_404ce6 = icmp eq i32 %v1_404ce6, 0
  store i1 %v9_404ce6, i1* %zf.global-to-local, align 1
  %v10_404ce6 = icmp slt i32 %v1_404ce6, 0
  store i1 %v10_404ce6, i1* %sf.global-to-local, align 1
  store i32 %v1_404ce6, i32* @ebx, align 4
  %v0_404ce7 = load i32, i32* @edx, align 4
  %v1_404ce7 = trunc i32 %v0_404ce7 to i16
  %v2_404ce7 = load i32, i32* @esi, align 4
  %v3_404ce7 = inttoptr i32 %v2_404ce7 to i32*
  %v4_404ce7 = load i32, i32* %v3_404ce7, align 4
  call void @__asm_outsd(i16 %v1_404ce7, i32 %v4_404ce7)
  %v5_404ce7 = load i32, i32* @eax, align 4
  ret i32 %v5_404ce7

dec_label_pc_404ce9:                              ; preds = %dec_label_pc_404c77
  %v1_404ce9 = load i32, i32* @ebp, align 4
  %v2_404ce9 = mul i32 %v1_404ce9, 2
  %v3_404ce9 = add i32 %v0_404ce9, 83
  %v4_404ce9 = add i32 %v3_404ce9, %v2_404ce9
  %v6_404ce9 = call i8 @__readgsbyte(i32 %v4_404ce9)
  %v7_404ce9 = load i32, i32* @edx, align 4
  %v8_404ce9 = trunc i32 %v7_404ce9 to i8
  %v9_404ce9 = add i8 %v6_404ce9, %v8_404ce9
  %v10_404ce9 = urem i8 %v6_404ce9, 16
  %v11_404ce9 = urem i8 %v8_404ce9, 16
  %v12_404ce9 = add nuw nsw i8 %v11_404ce9, %v10_404ce9
  %v13_404ce9 = icmp ugt i8 %v12_404ce9, 15
  %v14_404ce9 = icmp ult i8 %v9_404ce9, %v6_404ce9
  %v15_404ce9 = xor i8 %v9_404ce9, %v6_404ce9
  %v16_404ce9 = xor i8 %v9_404ce9, %v8_404ce9
  %v17_404ce9 = and i8 %v15_404ce9, %v16_404ce9
  %v18_404ce9 = icmp slt i8 %v17_404ce9, 0
  store i1 %v13_404ce9, i1* %az.global-to-local, align 1
  store i1 %v14_404ce9, i1* %cf.global-to-local, align 1
  store i1 %v18_404ce9, i1* %of.global-to-local, align 1
  %v19_404ce9 = icmp eq i8 %v9_404ce9, 0
  store i1 %v19_404ce9, i1* %zf.global-to-local, align 1
  %v20_404ce9 = icmp slt i8 %v9_404ce9, 0
  store i1 %v20_404ce9, i1* %sf.global-to-local, align 1
  %v24_404ce9 = load i32, i32* @edi, align 4
  %v25_404ce9 = load i32, i32* @ebp, align 4
  %v26_404ce9 = mul i32 %v25_404ce9, 2
  %v27_404ce9 = add i32 %v24_404ce9, 83
  %v28_404ce9 = add i32 %v27_404ce9, %v26_404ce9
  call void @__writegsbyte(i32 %v28_404ce9, i8 %v9_404ce9)
  %v0_404cee = load i1, i1* %zf.global-to-local, align 1
  %v0_404d64 = load i32, i32* @esi, align 4
  br i1 %v0_404cee, label %dec_label_pc_404d64, label %dec_label_pc_404cf0

dec_label_pc_404cf0:                              ; preds = %dec_label_pc_404ce9
  %v2_404cf0 = add i32 %v0_404d64, 103
  %v3_404cf0 = inttoptr i32 %v2_404cf0 to i32*
  %v4_404cf0 = load i32, i32* %v3_404cf0, align 4
  %v6_404cf0 = mul i32 %v4_404cf0, 1090540544
  store i32 %v6_404cf0, i32* @ebp, align 4
  store i1 false, i1* %of.global-to-local, align 1
  store i1 false, i1* %cf.global-to-local, align 1
  %v0_404cf7 = load i32, i32* @ecx, align 4
  %v2_404cf7 = mul i32 %v4_404cf0, -2113886208
  %v3_404cf7 = or i32 %v2_404cf7, 118
  %v4_404cf7 = add i32 %v0_404cf7, %v3_404cf7
  %v5_404cf7 = inttoptr i32 %v4_404cf7 to i16*
  %v6_404cf7 = load i16, i16* %v5_404cf7, align 2
  %v8_404cf7 = trunc i32 %v0_404d64 to i16
  call void @__asm_arpl(i16 %v6_404cf7, i16 %v8_404cf7)
  %v0_404cfb = load i32, i32* @esp, align 4
  %v1_404cfb = add i32 %v0_404cfb, 4
  %v2_404cfb = add i32 %v0_404cfb, 8
  %v3_404cfb = add i32 %v0_404cfb, 16
  %v4_404cfb = add i32 %v0_404cfb, 20
  %v5_404cfb = add i32 %v0_404cfb, 24
  %v6_404cfb = add i32 %v0_404cfb, 28
  %v8_404cfb = inttoptr i32 %v0_404cfb to i32*
  %v9_404cfb = load i32, i32* %v8_404cfb, align 4
  %v10_404cfb = inttoptr i32 %v1_404cfb to i32*
  %v11_404cfb = load i32, i32* %v10_404cfb, align 4
  %v12_404cfb = inttoptr i32 %v2_404cfb to i32*
  %v13_404cfb = load i32, i32* %v12_404cfb, align 4
  %v14_404cfb = inttoptr i32 %v3_404cfb to i32*
  %v15_404cfb = load i32, i32* %v14_404cfb, align 4
  %v16_404cfb = inttoptr i32 %v4_404cfb to i32*
  %v17_404cfb = load i32, i32* %v16_404cfb, align 4
  %v18_404cfb = inttoptr i32 %v5_404cfb to i32*
  %v19_404cfb = load i32, i32* %v18_404cfb, align 4
  %v20_404cfb = inttoptr i32 %v6_404cfb to i32*
  %v21_404cfb = load i32, i32* %v20_404cfb, align 4
  store i32 %v9_404cfb, i32* @edi, align 4
  store i32 %v11_404cfb, i32* @esi, align 4
  store i32 %v13_404cfb, i32* @ebp, align 4
  store i32 %v15_404cfb, i32* @ebx, align 4
  store i32 %v17_404cfb, i32* @edx, align 4
  store i32 %v19_404cfb, i32* @ecx, align 4
  store i32 %v21_404cfb, i32* @eax, align 4
  %v0_404cfc = load i1, i1* %zf.global-to-local, align 1
  br i1 %v0_404cfc, label %dec_label_pc_404d6d, label %dec_label_pc_404d00

dec_label_pc_404d00:                              ; preds = %dec_label_pc_404cf0
  %v1_404d00 = add i32 %v15_404cfb, 1
  %v2_404d00 = urem i32 %v15_404cfb, 16
  %v4_404d00 = icmp eq i32 %v2_404d00, 15
  %tmp516 = xor i32 %v15_404cfb, -2147483648
  %v7_404d00 = and i32 %v1_404d00, %tmp516
  %v8_404d00 = icmp slt i32 %v7_404d00, 0
  store i1 %v4_404d00, i1* %az.global-to-local, align 1
  store i1 %v8_404d00, i1* %of.global-to-local, align 1
  %v9_404d00 = icmp eq i32 %v1_404d00, 0
  store i1 %v9_404d00, i1* %zf.global-to-local, align 1
  %v10_404d00 = icmp slt i32 %v1_404d00, 0
  store i1 %v10_404d00, i1* %sf.global-to-local, align 1
  store i32 %v1_404d00, i32* @ebx, align 4
  ret i32 %v21_404cfb

dec_label_pc_404d02:                              ; preds = %dec_label_pc_404c90
  %v0_404d02 = load i32, i32* @esp, align 4
  %v1_404d02 = add i32 %v0_404d02, 4
  %v2_404d02 = add i32 %v0_404d02, 8
  %v3_404d02 = add i32 %v0_404d02, 16
  %v4_404d02 = add i32 %v0_404d02, 20
  %v5_404d02 = add i32 %v0_404d02, 24
  %v6_404d02 = add i32 %v0_404d02, 28
  %v8_404d02 = inttoptr i32 %v0_404d02 to i32*
  %v9_404d02 = load i32, i32* %v8_404d02, align 4
  %v10_404d02 = inttoptr i32 %v1_404d02 to i32*
  %v11_404d02 = load i32, i32* %v10_404d02, align 4
  %v12_404d02 = inttoptr i32 %v2_404d02 to i32*
  %v13_404d02 = load i32, i32* %v12_404d02, align 4
  %v14_404d02 = inttoptr i32 %v3_404d02 to i32*
  %v15_404d02 = load i32, i32* %v14_404d02, align 4
  %v16_404d02 = inttoptr i32 %v4_404d02 to i32*
  %v17_404d02 = load i32, i32* %v16_404d02, align 4
  %v18_404d02 = inttoptr i32 %v5_404d02 to i32*
  %v19_404d02 = load i32, i32* %v18_404d02, align 4
  %v20_404d02 = inttoptr i32 %v6_404d02 to i32*
  %v21_404d02 = load i32, i32* %v20_404d02, align 4
  store i32 %v9_404d02, i32* @edi, align 4
  store i32 %v11_404d02, i32* @esi, align 4
  store i32 %v13_404d02, i32* @ebp, align 4
  store i32 %v15_404d02, i32* @ebx, align 4
  store i32 %v17_404d02, i32* @edx, align 4
  store i32 %v19_404d02, i32* @ecx, align 4
  store i32 %v21_404d02, i32* @eax, align 4
  ret i32 %v21_404d02

dec_label_pc_404d05:                              ; preds = %dec_label_pc_404c92
  %v0_404d05 = load i32, i32* @ecx, align 4
  %v1_404d05 = add i32 %v0_404d05, -1
  %v2_404d05 = urem i32 %v0_404d05, 16
  %v3_404d05 = add nsw i32 %v2_404d05, -1
  %v4_404d05 = icmp ugt i32 %v3_404d05, 15
  %tmp517 = sub i32 0, %v0_404d05
  %v7_404d05 = and i32 %v0_404d05, %tmp517
  %v8_404d05 = icmp slt i32 %v7_404d05, 0
  store i1 %v4_404d05, i1* %az.global-to-local, align 1
  store i1 %v8_404d05, i1* %of.global-to-local, align 1
  %v9_404d05 = icmp eq i32 %v1_404d05, 0
  store i1 %v9_404d05, i1* %zf.global-to-local, align 1
  %v10_404d05 = icmp slt i32 %v1_404d05, 0
  store i1 %v10_404d05, i1* %sf.global-to-local, align 1
  store i32 %v1_404d05, i32* @ecx, align 4
  %v0_404d07 = load i32, i32* @edx, align 4
  %v1_404d07 = trunc i32 %v0_404d07 to i16
  %v3_404d07 = inttoptr i32 %v1_404c92 to i8*
  %v4_404d07 = load i8, i8* %v3_404d07, align 1
  call void @__asm_outsb(i16 %v1_404d07, i8 %v4_404d07)
  %v0_404d08 = load i1, i1* %cf.global-to-local, align 1
  %v1_404d08 = icmp eq i1 %v0_404d08, false
  br i1 %v1_404d08, label %dec_label_pc_404d7e, label %dec_label_pc_404d0a

dec_label_pc_404d0a:                              ; preds = %dec_label_pc_404d05
  %v0_404d0a = load i32, i32* @esp, align 4
  %v1_404d0a = add i32 %v0_404d0a, 4
  %v2_404d0a = add i32 %v0_404d0a, 8
  %v3_404d0a = add i32 %v0_404d0a, 16
  %v4_404d0a = add i32 %v0_404d0a, 20
  %v5_404d0a = add i32 %v0_404d0a, 24
  %v6_404d0a = add i32 %v0_404d0a, 28
  %v7_404d0a = add i32 %v0_404d0a, 32
  %v8_404d0a = inttoptr i32 %v0_404d0a to i32*
  %v9_404d0a = load i32, i32* %v8_404d0a, align 4
  %v10_404d0a = inttoptr i32 %v1_404d0a to i32*
  %v11_404d0a = load i32, i32* %v10_404d0a, align 4
  %v12_404d0a = inttoptr i32 %v2_404d0a to i32*
  %v13_404d0a = load i32, i32* %v12_404d0a, align 4
  %v14_404d0a = inttoptr i32 %v3_404d0a to i32*
  %v15_404d0a = load i32, i32* %v14_404d0a, align 4
  %v16_404d0a = inttoptr i32 %v4_404d0a to i32*
  %v17_404d0a = load i32, i32* %v16_404d0a, align 4
  %v18_404d0a = inttoptr i32 %v5_404d0a to i32*
  %v19_404d0a = load i32, i32* %v18_404d0a, align 4
  %v20_404d0a = inttoptr i32 %v6_404d0a to i32*
  %v21_404d0a = load i32, i32* %v20_404d0a, align 4
  store i32 %v9_404d0a, i32* @edi, align 4
  store i32 %v11_404d0a, i32* @esi, align 4
  store i32 %v13_404d0a, i32* @ebp, align 4
  store i32 %v15_404d0a, i32* @ebx, align 4
  store i32 %v17_404d0a, i32* @edx, align 4
  store i32 %v19_404d0a, i32* @ecx, align 4
  store i32 %v21_404d0a, i32* @eax, align 4
  %v22_404d0a = trunc i32 %v7_404d0a to i16
  %v1_404d0b = trunc i32 %v17_404d0a to i16
  %v3_404d0b = inttoptr i32 %v11_404d0a to i8*
  %v4_404d0b = load i8, i8* %v3_404d0b, align 1
  call void @__asm_outsb(i16 %v1_404d0b, i8 %v4_404d0b)
  %v0_404d0c = load i32, i32* @ebp, align 4
  %v1_404d0c = inttoptr i32 %v0_404d0c to i16*
  %v2_404d0c = load i16, i16* %v1_404d0c, align 2
  call void @__asm_arpl(i16 %v2_404d0c, i16 %v22_404d0a)
  %v0_404d0f = load i32, i32* @ebx, align 4
  %v1_404d0f = add i32 %v0_404d0f, 1
  %v2_404d0f = urem i32 %v0_404d0f, 16
  %v4_404d0f = icmp eq i32 %v2_404d0f, 15
  %tmp518 = xor i32 %v0_404d0f, -2147483648
  %v7_404d0f = and i32 %v1_404d0f, %tmp518
  %v8_404d0f = icmp slt i32 %v7_404d0f, 0
  store i1 %v4_404d0f, i1* %az.global-to-local, align 1
  store i1 %v8_404d0f, i1* %of.global-to-local, align 1
  %v9_404d0f = icmp eq i32 %v1_404d0f, 0
  store i1 %v9_404d0f, i1* %zf.global-to-local, align 1
  %v10_404d0f = icmp slt i32 %v1_404d0f, 0
  store i1 %v10_404d0f, i1* %sf.global-to-local, align 1
  store i32 %v1_404d0f, i32* @ebx, align 4
  %v0_404d10 = load i32, i32* @eax, align 4
  %sext32 = mul i32 %v7_404d0a, 65536
  %v2_404d10 = sdiv i32 %sext32, 65536
  %v3_404d10 = add nsw i32 %v2_404d10, -4
  %v4_404d10 = inttoptr i32 %v3_404d10 to i32*
  store i32 %v0_404d10, i32* %v4_404d10, align 4
  %v0_404d11 = load i32, i32* @edx, align 4
  %v2_404d11 = add nsw i32 %v2_404d10, -8
  %v3_404d11 = inttoptr i32 %v2_404d11 to i32*
  store i32 %v0_404d11, i32* %v3_404d11, align 4
  %v0_404d12 = load i32, i32* @edx, align 4
  %v1_404d12 = trunc i32 %v0_404d12 to i16
  %v2_404d12 = call i8 @__asm_insb(i16 %v1_404d12)
  %v3_404d12 = load i32, i32* @edi, align 4
  %v4_404d12 = inttoptr i32 %v3_404d12 to i8*
  store i8 %v2_404d12, i8* %v4_404d12, align 1
  %v0_404d13 = load i1, i1* %cf.global-to-local, align 1
  %v1_404d13 = load i1, i1* %zf.global-to-local, align 1
  %v2_404d13 = or i1 %v0_404d13, %v1_404d13
  br i1 %v2_404d13, label %dec_label_pc_404d4e, label %dec_label_pc_404d1b

dec_label_pc_404d1b:                              ; preds = %dec_label_pc_404d0a
  %v0_404d15 = load i32, i32* @edi, align 4
  %v1_404d15 = add i32 %v0_404d15, -1
  %v2_404d15 = urem i32 %v0_404d15, 16
  %v3_404d15 = add nsw i32 %v2_404d15, -1
  %v4_404d15 = icmp ugt i32 %v3_404d15, 15
  %tmp519 = sub i32 0, %v0_404d15
  %v7_404d15 = and i32 %v0_404d15, %tmp519
  %v8_404d15 = icmp slt i32 %v7_404d15, 0
  store i1 %v4_404d15, i1* %az.global-to-local, align 1
  store i1 %v8_404d15, i1* %of.global-to-local, align 1
  %v9_404d15 = icmp eq i32 %v1_404d15, 0
  store i1 %v9_404d15, i1* %zf.global-to-local, align 1
  %v10_404d15 = icmp slt i32 %v1_404d15, 0
  store i1 %v10_404d15, i1* %sf.global-to-local, align 1
  store i32 %v1_404d15, i32* @edi, align 4
  %v0_404d17 = load i32, i32* @edx, align 4
  %v1_404d17 = trunc i32 %v0_404d17 to i16
  %v2_404d17 = call i8 @__asm_insb(i16 %v1_404d17)
  %v3_404d17 = load i32, i32* @edi, align 4
  %v4_404d17 = inttoptr i32 %v3_404d17 to i8*
  store i8 %v2_404d17, i8* %v4_404d17, align 1
  %v0_404d18 = load i32, i32* @edx, align 4
  %v1_404d18 = trunc i32 %v0_404d18 to i16
  %v2_404d18 = call i32 @__asm_insd(i16 %v1_404d18)
  %v3_404d18 = load i32, i32* @edi, align 4
  %v4_404d18 = inttoptr i32 %v3_404d18 to i32*
  store i32 %v2_404d18, i32* %v4_404d18, align 4
  %v0_404d19 = load i32, i32* @esp, align 4
  %v0_404d1a = load i32, i32* @eax, align 4
  %v1_404d1a = add i32 %v0_404d1a, -1
  %v2_404d1a = urem i32 %v0_404d1a, 16
  %v3_404d1a = add nsw i32 %v2_404d1a, -1
  %v4_404d1a = icmp ugt i32 %v3_404d1a, 15
  %tmp520 = sub i32 0, %v0_404d1a
  %v7_404d1a = and i32 %v0_404d1a, %tmp520
  %v8_404d1a = icmp slt i32 %v7_404d1a, 0
  store i1 %v4_404d1a, i1* %az.global-to-local, align 1
  store i1 %v8_404d1a, i1* %of.global-to-local, align 1
  %v9_404d1a = icmp eq i32 %v1_404d1a, 0
  store i1 %v9_404d1a, i1* %zf.global-to-local, align 1
  %v10_404d1a = icmp slt i32 %v1_404d1a, 0
  store i1 %v10_404d1a, i1* %sf.global-to-local, align 1
  store i32 %v1_404d1a, i32* @eax, align 4
  br i1 %v9_404d1a, label %dec_label_pc_404d96, label %dec_label_pc_404d1d

dec_label_pc_404d1d:                              ; preds = %dec_label_pc_404d1b
  %v1_404d1d = load i32, i32* @ecx, align 4
  %v2_404d1d = add i32 %v1_404d1d, 97
  %v3_404d1d = inttoptr i32 %v2_404d1d to i32*
  %v4_404d1d = load i32, i32* %v3_404d1d, align 4
  %v6_404d1d = mul i32 %v4_404d1d, 1398145076
  store i32 %v6_404d1d, i32* @esi, align 4
  store i1 false, i1* %of.global-to-local, align 1
  store i1 false, i1* %cf.global-to-local, align 1
  %v0_404d24 = load i32, i32* @edi, align 4
  %v2_404d24 = add i32 %v0_404d19, -3
  %v3_404d24 = inttoptr i32 %v2_404d24 to i32*
  store i32 %v0_404d24, i32* %v3_404d24, align 4
  %v0_404d25 = load i32, i32* @edx, align 4
  %v1_404d25 = trunc i32 %v0_404d25 to i16
  %v2_404d25 = call i8 @__asm_insb(i16 %v1_404d25)
  %v3_404d25 = load i32, i32* @edi, align 4
  %v4_404d25 = inttoptr i32 %v3_404d25 to i8*
  store i8 %v2_404d25, i8* %v4_404d25, align 1
  %v5_404d25 = load i32, i32* @eax, align 4
  ret i32 %v5_404d25

dec_label_pc_404d2b:                              ; preds = %dec_label_pc_404cdc
  %v0_404d2b = load i32, i32* @eax, align 4
  %v1_404d2b = xor i32 %v0_404d2b, 862536549
  store i1 false, i1* %az.global-to-local, align 1
  store i1 false, i1* %cf.global-to-local, align 1
  store i1 false, i1* %of.global-to-local, align 1
  %v2_404d2b = icmp eq i32 %v1_404d2b, 0
  store i1 %v2_404d2b, i1* %zf.global-to-local, align 1
  %v3_404d2b = icmp slt i32 %v1_404d2b, 0
  store i1 %v3_404d2b, i1* %sf.global-to-local, align 1
  %v4_404d2b = trunc i32 %v1_404d2b to i8
  store i32 %v1_404d2b, i32* @eax, align 4
  %v0_404d30 = load i32, i32* @esi, align 4
  %v1_404d30 = add i32 %v0_404d30, 52
  %v2_404d30 = inttoptr i32 %v1_404d30 to i8*
  %v3_404d30 = load i8, i8* %v2_404d30, align 1
  %v4_404d30 = load i32, i32* @edx, align 4
  %v5_404d30 = udiv i32 %v4_404d30, 256
  %v6_404d30 = trunc i32 %v5_404d30 to i8
  %v12_404d30 = icmp ult i8 %v3_404d30, %v6_404d30
  store i1 %v12_404d30, i1* %cf.global-to-local, align 1
  %v0_404d35 = load i32, i32* @ebx, align 4
  %v1_404d35 = add i32 %v0_404d35, -1
  %v2_404d35 = urem i32 %v0_404d35, 16
  %v3_404d35 = add nsw i32 %v2_404d35, -1
  %v4_404d35 = icmp ugt i32 %v3_404d35, 15
  %tmp521 = sub i32 0, %v0_404d35
  %v7_404d35 = and i32 %v0_404d35, %tmp521
  %v8_404d35 = icmp slt i32 %v7_404d35, 0
  store i1 %v4_404d35, i1* %az.global-to-local, align 1
  store i1 %v8_404d35, i1* %of.global-to-local, align 1
  %v9_404d35 = icmp eq i32 %v1_404d35, 0
  store i1 %v9_404d35, i1* %zf.global-to-local, align 1
  %v10_404d35 = icmp slt i32 %v1_404d35, 0
  store i1 %v10_404d35, i1* %sf.global-to-local, align 1
  store i32 %v1_404d35, i32* @ebx, align 4
  br i1 %v9_404d35, label %dec_label_pc_404d90, label %dec_label_pc_404d2b.dec_label_pc_404d38_crit_edge

dec_label_pc_404d2b.dec_label_pc_404d38_crit_edge: ; preds = %dec_label_pc_404d2b
  %v0_404d38.pre = load i32, i32* @esp, align 4
  br label %dec_label_pc_404d38

dec_label_pc_404d38:                              ; preds = %dec_label_pc_404cc6.dec_label_pc_404d38_crit_edge, %dec_label_pc_404d2b.dec_label_pc_404d38_crit_edge
  %v0_404d39 = phi i32 [ %v1_404d35, %dec_label_pc_404d2b.dec_label_pc_404d38_crit_edge ], [ %v0_404d39.pre, %dec_label_pc_404cc6.dec_label_pc_404d38_crit_edge ]
  %v0_404d38 = phi i32 [ %v0_404d38.pre, %dec_label_pc_404d2b.dec_label_pc_404d38_crit_edge ], [ 0, %dec_label_pc_404cc6.dec_label_pc_404d38_crit_edge ]
  %v1_404d38 = inttoptr i32 %v0_404d38 to i32*
  %v2_404d38 = load i32, i32* %v1_404d38, align 4
  store i32 %v2_404d38, i32* @ecx, align 4
  store i32 %v0_404d39, i32* %v1_404d38, align 4
  %v0_404d3a = load i32, i32* @edi, align 4
  %v1_404d3a = add i32 %v0_404d3a, -1
  store i32 %v1_404d3a, i32* @edi, align 4
  %v1_404d3b = add i32 %v0_404d38, 1
  %v2_404d3b = urem i32 %v0_404d38, 16
  %v4_404d3b = icmp eq i32 %v2_404d3b, 15
  %tmp522 = xor i32 %v0_404d38, -2147483648
  %v7_404d3b = and i32 %v1_404d3b, %tmp522
  %v8_404d3b = icmp slt i32 %v7_404d3b, 0
  store i1 %v4_404d3b, i1* %az.global-to-local, align 1
  store i1 %v8_404d3b, i1* %of.global-to-local, align 1
  %v9_404d3b = icmp eq i32 %v1_404d3b, 0
  store i1 %v9_404d3b, i1* %zf.global-to-local, align 1
  %v10_404d3b = icmp slt i32 %v1_404d3b, 0
  store i1 %v10_404d3b, i1* %sf.global-to-local, align 1
  %v15_404d3b = load i32, i32* @eax, align 4
  ret i32 %v15_404d3b

dec_label_pc_404d3d:                              ; preds = %dec_label_pc_404cd7
  %v0_404d3d = load i32, i32* @eax, align 4
  %v1_404d3d = trunc i32 %v0_404d3d to i8
  %v2_404d3d = xor i8 %v1_404d3d, 78
  store i1 false, i1* %az.global-to-local, align 1
  store i1 false, i1* %cf.global-to-local, align 1
  store i1 false, i1* %of.global-to-local, align 1
  %v3_404d3d = icmp eq i8 %v2_404d3d, 0
  store i1 %v3_404d3d, i1* %zf.global-to-local, align 1
  %v4_404d3d = icmp slt i8 %v2_404d3d, 0
  store i1 %v4_404d3d, i1* %sf.global-to-local, align 1
  %v8_404d3d = zext i8 %v2_404d3d to i32
  %v10_404d3d = and i32 %v0_404d3d, -256
  %v11_404d3d = or i32 %v10_404d3d, %v8_404d3d
  store i32 %v11_404d3d, i32* @eax, align 4
  %v0_404d3f = load i32, i32* @edi, align 4
  %v1_404d3f = add i32 %v0_404d3f, 107
  %v2_404d3f = inttoptr i32 %v1_404d3f to i64*
  %v3_404d3f = load i64, i64* %v2_404d3f, align 4
  %v4_404d3f = call i32 @__asm_bound(i64 %v3_404d3f)
  %v0_404d42 = load i32, i32* @ebp, align 4
  %v1_404d42 = add i32 %v0_404d42, -1
  store i32 %v1_404d42, i32* @ebp, align 4
  %v1_404d43 = add i32 %v4_404d3f, 1
  %v2_404d43 = urem i32 %v4_404d3f, 16
  %v4_404d43 = icmp eq i32 %v2_404d43, 15
  %tmp523 = xor i32 %v4_404d3f, -2147483648
  %v7_404d43 = and i32 %v1_404d43, %tmp523
  %v8_404d43 = icmp slt i32 %v7_404d43, 0
  store i1 %v4_404d43, i1* %az.global-to-local, align 1
  store i1 %v8_404d43, i1* %of.global-to-local, align 1
  %v9_404d43 = icmp eq i32 %v1_404d43, 0
  store i1 %v9_404d43, i1* %zf.global-to-local, align 1
  %v10_404d43 = icmp slt i32 %v1_404d43, 0
  store i1 %v10_404d43, i1* %sf.global-to-local, align 1
  %v1_404d44 = icmp eq i1 %v10_404d43, false
  br i1 %v1_404d44, label %dec_label_pc_404dc7, label %dec_label_pc_404d48

dec_label_pc_404d48:                              ; preds = %dec_label_pc_404d3d
  %v0_404d48 = load i32, i32* @edi, align 4
  %v1_404d48 = add i32 %v0_404d48, 1
  %v2_404d48 = urem i32 %v0_404d48, 16
  %v4_404d48 = icmp eq i32 %v2_404d48, 15
  %tmp524 = xor i32 %v0_404d48, -2147483648
  %v7_404d48 = and i32 %v1_404d48, %tmp524
  %v8_404d48 = icmp slt i32 %v7_404d48, 0
  store i1 %v4_404d48, i1* %az.global-to-local, align 1
  store i1 %v8_404d48, i1* %of.global-to-local, align 1
  %v9_404d48 = icmp eq i32 %v1_404d48, 0
  store i1 %v9_404d48, i1* %zf.global-to-local, align 1
  %v10_404d48 = icmp slt i32 %v1_404d48, 0
  store i1 %v10_404d48, i1* %sf.global-to-local, align 1
  store i32 %v1_404d48, i32* @edi, align 4
  %v2_404d49 = add i32 %v4_404d3f, -3
  %v3_404d49 = inttoptr i32 %v2_404d49 to i32*
  store i32 %v1_404d42, i32* %v3_404d49, align 4
  %v0_404d4a = load i32, i32* @edi, align 4
  %v2_404d4a = add i32 %v4_404d3f, -7
  %v3_404d4a = inttoptr i32 %v2_404d4a to i32*
  store i32 %v0_404d4a, i32* %v3_404d4a, align 4
  %v4_404d4a = load i32, i32* @eax, align 4
  ret i32 %v4_404d4a

dec_label_pc_404d4e:                              ; preds = %dec_label_pc_404d0a, %dec_label_pc_404ce4
  %v0_404d4e = load i32, i32* @edx, align 4
  %v1_404d4e = trunc i32 %v0_404d4e to i16
  %v2_404d4e = call i32 @__asm_insd(i16 %v1_404d4e)
  %v3_404d4e = load i32, i32* @edi, align 4
  %v4_404d4e = inttoptr i32 %v3_404d4e to i32*
  store i32 %v2_404d4e, i32* %v4_404d4e, align 4
  %v0_404d4f = load i32, i32* @ebx, align 4
  %v1_404d4f = load i32, i32* @esp, align 4
  %v2_404d4f = add i32 %v1_404d4f, -4
  %v3_404d4f = inttoptr i32 %v2_404d4f to i32*
  store i32 %v0_404d4f, i32* %v3_404d4f, align 4
  %v0_404d50 = load i1, i1* %cf.global-to-local, align 1
  br i1 %v0_404d50, label %dec_label_pc_404d9c, label %dec_label_pc_404dbc

dec_label_pc_404d64:                              ; preds = %dec_label_pc_404ce9
  %v1_404d64 = add i32 %v0_404d64, 1
  store i32 %v1_404d64, i32* @esi, align 4
  %v1_404d65 = load i32, i32* @edx, align 4
  %v2_404d65 = add i32 %v1_404d65, 98
  %v3_404d65 = inttoptr i32 %v2_404d65 to i32*
  %v4_404d65 = load i32, i32* %v3_404d65, align 4
  %v6_404d65 = mul i32 %v4_404d65, 1262830434
  store i32 %v6_404d65, i32* @edx, align 4
  store i1 false, i1* %cf.global-to-local, align 1
  %v0_404d6c = load i32, i32* @ebx, align 4
  %v1_404d6c = add i32 %v0_404d6c, 1
  %v2_404d6c = urem i32 %v0_404d6c, 16
  %v4_404d6c = icmp eq i32 %v2_404d6c, 15
  %tmp525 = xor i32 %v0_404d6c, -2147483648
  %v7_404d6c = and i32 %v1_404d6c, %tmp525
  %v8_404d6c = icmp slt i32 %v7_404d6c, 0
  store i1 %v4_404d6c, i1* %az.global-to-local, align 1
  store i1 %v8_404d6c, i1* %of.global-to-local, align 1
  %v9_404d6c = icmp eq i32 %v1_404d6c, 0
  store i1 %v9_404d6c, i1* %zf.global-to-local, align 1
  %v10_404d6c = icmp slt i32 %v1_404d6c, 0
  store i1 %v10_404d6c, i1* %sf.global-to-local, align 1
  store i32 %v1_404d6c, i32* @ebx, align 4
  br label %dec_label_pc_404d6d

dec_label_pc_404d6d:                              ; preds = %dec_label_pc_404d64, %dec_label_pc_404cf0
  %v4_404d6d = phi i32 [ %v1_404d6c, %dec_label_pc_404d64 ], [ %v15_404cfb, %dec_label_pc_404cf0 ]
  %v22_404d6d = phi i32 [ %v6_404d65, %dec_label_pc_404d64 ], [ %v17_404cfb, %dec_label_pc_404cf0 ]
  %v1_404d6d = add i32 %v22_404d6d, 89
  %v2_404d6d = inttoptr i32 %v1_404d6d to i8*
  %v3_404d6d = load i8, i8* %v2_404d6d, align 1
  %v5_404d6d = udiv i32 %v4_404d6d, 256
  %v6_404d6d = trunc i32 %v5_404d6d to i8
  %v7_404d6d = add i8 %v3_404d6d, %v6_404d6d
  %v12_404d6d = icmp ult i8 %v7_404d6d, %v3_404d6d
  store i1 %v12_404d6d, i1* %cf.global-to-local, align 1
  store i8 %v7_404d6d, i8* %v2_404d6d, align 1
  %v0_404d70 = load i32, i32* @ecx, align 4
  %v1_404d70 = add i32 %v0_404d70, -1
  %v2_404d70 = urem i32 %v0_404d70, 16
  %v3_404d70 = add nsw i32 %v2_404d70, -1
  %v4_404d70 = icmp ugt i32 %v3_404d70, 15
  %tmp526 = sub i32 0, %v0_404d70
  %v7_404d70 = and i32 %v0_404d70, %tmp526
  %v8_404d70 = icmp slt i32 %v7_404d70, 0
  store i1 %v4_404d70, i1* %az.global-to-local, align 1
  store i1 %v8_404d70, i1* %of.global-to-local, align 1
  %v9_404d70 = icmp eq i32 %v1_404d70, 0
  store i1 %v9_404d70, i1* %zf.global-to-local, align 1
  %v10_404d70 = icmp slt i32 %v1_404d70, 0
  store i1 %v10_404d70, i1* %sf.global-to-local, align 1
  store i32 %v1_404d70, i32* @ecx, align 4
  %v0_404d71 = load i32, i32* @esp, align 4
  %v2_404d71 = add i32 %v0_404d71, -4
  %v3_404d71 = inttoptr i32 %v2_404d71 to i32*
  store i32 %v0_404d71, i32* %v3_404d71, align 4
  %v0_404d72 = load i32, i32* @edx, align 4
  %v1_404d72 = udiv i32 %v0_404d72, 256
  %v2_404d72 = trunc i32 %v1_404d72 to i8
  %v3_404d72 = load i32, i32* @eax, align 4
  %v4_404d72 = load i32, i32* @esi, align 4
  %v5_404d72 = mul i32 %v4_404d72, 2
  %v6_404d72 = add i32 %v3_404d72, 85
  %v7_404d72 = add i32 %v6_404d72, %v5_404d72
  %v8_404d72 = inttoptr i32 %v7_404d72 to i8*
  %v9_404d72 = load i8, i8* %v8_404d72, align 1
  %v10_404d72 = xor i8 %v9_404d72, %v2_404d72
  store i1 false, i1* %cf.global-to-local, align 1
  %v16_404d72 = zext i8 %v10_404d72 to i32
  %v18_404d72 = mul i32 %v16_404d72, 256
  %v19_404d72 = and i32 %v0_404d72, -65281
  %v20_404d72 = or i32 %v18_404d72, %v19_404d72
  store i32 %v20_404d72, i32* @edx, align 4
  %v0_404d76 = load i32, i32* @ecx, align 4
  %v1_404d76 = add i32 %v0_404d76, 1
  %v2_404d76 = urem i32 %v0_404d76, 16
  %v4_404d76 = icmp eq i32 %v2_404d76, 15
  %tmp527 = xor i32 %v0_404d76, -2147483648
  %v7_404d76 = and i32 %v1_404d76, %tmp527
  %v8_404d76 = icmp slt i32 %v7_404d76, 0
  store i1 %v4_404d76, i1* %az.global-to-local, align 1
  store i1 %v8_404d76, i1* %of.global-to-local, align 1
  %v9_404d76 = icmp eq i32 %v1_404d76, 0
  store i1 %v9_404d76, i1* %zf.global-to-local, align 1
  %v10_404d76 = icmp slt i32 %v1_404d76, 0
  store i1 %v10_404d76, i1* %sf.global-to-local, align 1
  store i32 %v1_404d76, i32* @ecx, align 4
  br i1 %v8_404d76, label %dec_label_pc_404def, label %dec_label_pc_404d79

dec_label_pc_404d79:                              ; preds = %dec_label_pc_404d6d
  br i1 %v9_404d76, label %dec_label_pc_404d7b, label %dec_label_pc_404dac

dec_label_pc_404d7b:                              ; preds = %dec_label_pc_404d79
  %v1_404d7b = inttoptr i32 %v4_404d72 to i8 addrspace(258)*
  %v2_404d7b = load i8, i8 addrspace(258)* %v1_404d7b, align 1
  %v6_404d7b = xor i8 %v2_404d7b, %v10_404d72
  store i1 false, i1* %az.global-to-local, align 1
  store i1 false, i1* %cf.global-to-local, align 1
  store i1 false, i1* %of.global-to-local, align 1
  %v7_404d7b = icmp eq i8 %v6_404d7b, 0
  store i1 %v7_404d7b, i1* %zf.global-to-local, align 1
  %v8_404d7b = icmp slt i8 %v6_404d7b, 0
  store i1 %v8_404d7b, i1* %sf.global-to-local, align 1
  store i8 %v6_404d7b, i8 addrspace(258)* %v1_404d7b, align 1
  br label %dec_label_pc_404d7e

dec_label_pc_404d7e:                              ; preds = %dec_label_pc_404d7b, %dec_label_pc_404d05
  %v0_404d7e = load i32, i32* @edx, align 4
  %v1_404d7e = udiv i32 %v0_404d7e, 256
  %v2_404d7e = trunc i32 %v1_404d7e to i8
  %v3_404d7e = load i32, i32* @eax, align 4
  %factor55 = mul i32 %v3_404d7e, 2
  %v7_404d7e = add i32 %factor55, 74
  %v8_404d7e = inttoptr i32 %v7_404d7e to i8*
  %v9_404d7e = load i8, i8* %v8_404d7e, align 2
  %v10_404d7e = xor i8 %v9_404d7e, %v2_404d7e
  store i1 false, i1* %az.global-to-local, align 1
  %v11_404d7e = icmp eq i8 %v10_404d7e, 0
  store i1 %v11_404d7e, i1* %zf.global-to-local, align 1
  %v12_404d7e = icmp slt i8 %v10_404d7e, 0
  store i1 %v12_404d7e, i1* %sf.global-to-local, align 1
  %v16_404d7e = zext i8 %v10_404d7e to i32
  %v18_404d7e = mul i32 %v16_404d7e, 256
  %v19_404d7e = and i32 %v0_404d7e, -65281
  %v20_404d7e = or i32 %v18_404d7e, %v19_404d7e
  store i32 %v20_404d7e, i32* @edx, align 4
  %v1_404d82 = load i32, i32* @esp, align 4
  %v3_404d82 = mul i32 %v20_404d7e, 2
  %v4_404d82 = add i32 %v1_404d82, 100
  %v5_404d82 = add i32 %v4_404d82, %v3_404d82
  %v6_404d82 = inttoptr i32 %v5_404d82 to i32*
  %v7_404d82 = load i32, i32* %v6_404d82, align 4
  %v9_404d82 = mul i32 %v7_404d82, 2035429936
  store i32 %v9_404d82, i32* @esi, align 4
  store i1 false, i1* %of.global-to-local, align 1
  store i1 false, i1* %cf.global-to-local, align 1
  %v1_404dde = trunc i32 %v20_404d7e to i16
  %v2_404dde = call i8 @__asm_insb(i16 %v1_404dde)
  %v3_404dde = load i32, i32* @edi, align 4
  %v4_404dde = inttoptr i32 %v3_404dde to i8*
  store i8 %v2_404dde, i8* %v4_404dde, align 1
  %v0_404ddf = load i32, i32* @esi, align 4
  %v1_404ddf = load i32, i32* @edx, align 4
  %v2_404ddf = inttoptr i32 %v1_404ddf to i32*
  %v3_404ddf = load i32, i32* %v2_404ddf, align 4
  %v4_404ddf = xor i32 %v3_404ddf, %v0_404ddf
  store i1 false, i1* %az.global-to-local, align 1
  store i1 false, i1* %cf.global-to-local, align 1
  store i1 false, i1* %of.global-to-local, align 1
  %v5_404ddf = icmp eq i32 %v4_404ddf, 0
  store i1 %v5_404ddf, i1* %zf.global-to-local, align 1
  %v6_404ddf = icmp slt i32 %v4_404ddf, 0
  store i1 %v6_404ddf, i1* %sf.global-to-local, align 1
  store i32 %v4_404ddf, i32* @esi, align 4
  %v4_404de1.pre = load i32, i32* @eax, align 4
  br label %dec_label_pc_404de1

dec_label_pc_404d90:                              ; preds = %dec_label_pc_404d2b
  %v1_404d90 = add i32 %v4_404d30, -1
  store i32 %v1_404d90, i32* @edx, align 4
  %v2_404d91 = xor i8 %v4_404d2b, 111
  store i1 false, i1* %az.global-to-local, align 1
  store i1 false, i1* %cf.global-to-local, align 1
  store i1 false, i1* %of.global-to-local, align 1
  %v3_404d91 = icmp eq i8 %v2_404d91, 0
  store i1 %v3_404d91, i1* %zf.global-to-local, align 1
  %v4_404d91 = icmp slt i8 %v2_404d91, 0
  store i1 %v4_404d91, i1* %sf.global-to-local, align 1
  %v8_404d91 = zext i8 %v2_404d91 to i32
  %v10_404d91 = and i32 %v1_404d2b, -256
  %v11_404d91 = or i32 %v10_404d91, %v8_404d91
  store i32 %v11_404d91, i32* @eax, align 4
  %v3_404d93 = load i8, i8* inttoptr (i32 74 to i8*), align 2
  %v6_404d93 = add i8 %v3_404d93, %v2_404d91
  %v7_404d93 = urem i8 %v3_404d93, 16
  %v8_404d93 = urem i8 %v2_404d91, 16
  %v9_404d93 = add nuw nsw i8 %v7_404d93, %v8_404d93
  %v10_404d93 = icmp ugt i8 %v9_404d93, 15
  %v11_404d93 = icmp ult i8 %v6_404d93, %v3_404d93
  %v12_404d93 = xor i8 %v6_404d93, %v3_404d93
  %v13_404d93 = xor i8 %v6_404d93, %v4_404d2b
  %v14_404d93 = and i8 %v12_404d93, %v13_404d93
  %v15_404d93 = icmp slt i8 %v14_404d93, 0
  store i1 %v10_404d93, i1* %az.global-to-local, align 1
  store i1 %v11_404d93, i1* %cf.global-to-local, align 1
  store i1 %v15_404d93, i1* %of.global-to-local, align 1
  %v16_404d93 = icmp eq i8 %v6_404d93, 0
  store i1 %v16_404d93, i1* %zf.global-to-local, align 1
  %v17_404d93 = icmp slt i8 %v6_404d93, 0
  store i1 %v17_404d93, i1* %sf.global-to-local, align 1
  store i8 %v6_404d93, i8* inttoptr (i32 74 to i8*), align 2
  %v15_404d96.pre = load i32, i32* @eax, align 4
  br label %dec_label_pc_404d96

dec_label_pc_404d96:                              ; preds = %dec_label_pc_404d90, %dec_label_pc_404d1b
  %v15_404d96 = phi i32 [ %v15_404d96.pre, %dec_label_pc_404d90 ], [ 0, %dec_label_pc_404d1b ]
  %v0_404d96 = load i32, i32* @edx, align 4
  %v1_404d96 = add i32 %v0_404d96, -1
  %v2_404d96 = urem i32 %v0_404d96, 16
  %v3_404d96 = add nsw i32 %v2_404d96, -1
  %v4_404d96 = icmp ugt i32 %v3_404d96, 15
  %tmp528 = sub i32 0, %v0_404d96
  %v7_404d96 = and i32 %v0_404d96, %tmp528
  %v8_404d96 = icmp slt i32 %v7_404d96, 0
  store i1 %v4_404d96, i1* %az.global-to-local, align 1
  store i1 %v8_404d96, i1* %of.global-to-local, align 1
  %v9_404d96 = icmp eq i32 %v1_404d96, 0
  store i1 %v9_404d96, i1* %zf.global-to-local, align 1
  %v10_404d96 = icmp slt i32 %v1_404d96, 0
  store i1 %v10_404d96, i1* %sf.global-to-local, align 1
  store i32 %v1_404d96, i32* @edx, align 4
  ret i32 %v15_404d96

dec_label_pc_404d9c:                              ; preds = %dec_label_pc_404d4e
  %v2_404d9c = load i32, i32* %v3_404d4f, align 4
  store i32 %v2_404d9c, i32* @ecx, align 4
  %v0_404d9d = load i32, i32* @eax, align 4
  %v1_404d9d = xor i32 %v0_404d9d, 1197101686
  store i1 false, i1* %az.global-to-local, align 1
  store i1 false, i1* %cf.global-to-local, align 1
  store i1 false, i1* %of.global-to-local, align 1
  %v2_404d9d = icmp eq i32 %v1_404d9d, 0
  store i1 %v2_404d9d, i1* %zf.global-to-local, align 1
  %v3_404d9d = icmp slt i32 %v1_404d9d, 0
  store i1 %v3_404d9d, i1* %sf.global-to-local, align 1
  store i32 %v1_404d9d, i32* @eax, align 4
  %v0_404da2 = load i32, i32* @edi, align 4
  %v1_404da2 = add i32 %v0_404da2, 107
  %v2_404da2 = inttoptr i32 %v1_404da2 to i64*
  %v3_404da2 = load i64, i64* %v2_404da2, align 4
  %v4_404da2 = call i32 @__asm_bound(i64 %v3_404da2)
  store i32 %v4_404da2, i32* @esi, align 4
  %v0_404da5 = load i32, i32* @eax, align 4
  %v1_404da5 = inttoptr i32 %v0_404da5 to i64*
  %v2_404da5 = load i64, i64* %v1_404da5, align 4
  %v3_404da5 = call i32 @__asm_bound(i64 %v2_404da5)
  store i32 %v3_404da5, i32* @eax, align 4
  %v0_404da7 = load i1, i1* %cf.global-to-local, align 1
  %v1_404da7 = load i1, i1* %zf.global-to-local, align 1
  %v2_404da7 = or i1 %v0_404da7, %v1_404da7
  br i1 %v2_404da7, label %dec_label_pc_404da9, label %dec_label_pc_404de1

dec_label_pc_404da9:                              ; preds = %dec_label_pc_404d9c
  %v1_404da9 = add i32 %v3_404da5, 84
  %v2_404da9 = inttoptr i32 %v1_404da9 to i8*
  %v3_404da9 = load i8, i8* %v2_404da9, align 1
  %v4_404da9 = load i32, i32* @ecx, align 4
  %v5_404da9 = trunc i32 %v4_404da9 to i8
  %v6_404da9 = sub i8 %v3_404da9, %v5_404da9
  %v7_404da9 = urem i8 %v3_404da9, 16
  %v8_404da9 = urem i8 %v5_404da9, 16
  %v9_404da9 = sub nsw i8 %v7_404da9, %v8_404da9
  %v10_404da9 = icmp ugt i8 %v9_404da9, 15
  %v11_404da9 = icmp ult i8 %v3_404da9, %v5_404da9
  %v12_404da9 = xor i8 %v3_404da9, %v5_404da9
  %v13_404da9 = xor i8 %v6_404da9, %v3_404da9
  %v14_404da9 = and i8 %v13_404da9, %v12_404da9
  %v15_404da9 = icmp slt i8 %v14_404da9, 0
  store i1 %v10_404da9, i1* %az.global-to-local, align 1
  store i1 %v11_404da9, i1* %cf.global-to-local, align 1
  store i1 %v15_404da9, i1* %of.global-to-local, align 1
  %v16_404da9 = icmp eq i8 %v6_404da9, 0
  store i1 %v16_404da9, i1* %zf.global-to-local, align 1
  %v17_404da9 = icmp slt i8 %v6_404da9, 0
  store i1 %v17_404da9, i1* %sf.global-to-local, align 1
  %v0_404dae.pre = load i32, i32* @esp, align 4
  %.pre133 = inttoptr i32 %v0_404dae.pre to i32*
  br label %dec_label_pc_404dac

dec_label_pc_404dac:                              ; preds = %dec_label_pc_404d79, %dec_label_pc_404da9
  %v1_404dae.pre-phi = phi i32* [ %v3_404d71, %dec_label_pc_404d79 ], [ %.pre133, %dec_label_pc_404da9 ]
  %v0_404dae = phi i32 [ %v2_404d71, %dec_label_pc_404d79 ], [ %v0_404dae.pre, %dec_label_pc_404da9 ]
  %v5_404dac = phi i1 [ %v4_404d76, %dec_label_pc_404d79 ], [ %v10_404da9, %dec_label_pc_404da9 ]
  %v0_404dac = phi i32 [ %v3_404d72, %dec_label_pc_404d79 ], [ %v3_404da5, %dec_label_pc_404da9 ]
  %v6_404dac20 = and i32 %v0_404dac, 14
  %v7_404dac = icmp ugt i32 %v6_404dac20, 9
  %v8_404dac = or i1 %v5_404dac, %v7_404dac
  %v9_404dac = add i32 %v0_404dac, 6
  %v11_404dac = select i1 %v8_404dac, i32 %v9_404dac, i32 %v0_404dac
  %v10_404dac = zext i1 %v8_404dac to i32
  %v15_404dac = urem i32 %v11_404dac, 16
  %v18_404dac = and i32 %v0_404dac, -65536
  %v19_404dac = or i32 %v15_404dac, %v18_404dac
  %v12_404dac15 = mul i32 %v10_404dac, 256
  %v3_404dac16 = add i32 %v12_404dac15, %v0_404dac
  %v22_404dac = and i32 %v3_404dac16, 65280
  %v24_404dac = or i32 %v19_404dac, %v22_404dac
  store i32 %v24_404dac, i32* @eax, align 4
  store i1 %v8_404dac, i1* %az.global-to-local, align 1
  store i1 %v8_404dac, i1* %cf.global-to-local, align 1
  %v2_404dae = load i32, i32* %v1_404dae.pre-phi, align 4
  store i32 %v2_404dae, i32* @ecx, align 4
  %v3_404dae = add i32 %v0_404dae, 4
  %v1_404daf = inttoptr i32 %v3_404dae to i32*
  %v2_404daf = load i32, i32* %v1_404daf, align 4
  store i32 %v2_404daf, i32* @ecx, align 4
  ret i32 %v24_404dac

dec_label_pc_404dbc:                              ; preds = %dec_label_pc_404d4e
  %v24_404db9.pre = load i32, i32* @eax, align 4
  ret i32 %v24_404db9.pre

dec_label_pc_404dc7:                              ; preds = %dec_label_pc_404d3d
  %v0_404dbd = load i32, i32* @eax, align 4
  %v2_404dbd = add i32 %v4_404d3f, -3
  %v3_404dbd = inttoptr i32 %v2_404dbd to i32*
  store i32 %v0_404dbd, i32* %v3_404dbd, align 4
  %v0_404dbf = load i32, i32* @eax, align 4
  %v1_404dbf = xor i32 %v0_404dbf, 1363436882
  store i1 false, i1* %cf.global-to-local, align 1
  store i32 %v1_404dbf, i32* @eax, align 4
  %v0_404dc4 = load i32, i32* @ebx, align 4
  %v1_404dc4 = add i32 %v0_404dc4, -1
  %v2_404dc4 = urem i32 %v0_404dc4, 16
  %v3_404dc4 = add nsw i32 %v2_404dc4, -1
  %v4_404dc4 = icmp ugt i32 %v3_404dc4, 15
  %tmp529 = sub i32 0, %v0_404dc4
  %v7_404dc4 = and i32 %v0_404dc4, %tmp529
  %v8_404dc4 = icmp slt i32 %v7_404dc4, 0
  store i1 %v4_404dc4, i1* %az.global-to-local, align 1
  store i1 %v8_404dc4, i1* %of.global-to-local, align 1
  %v9_404dc4 = icmp eq i32 %v1_404dc4, 0
  store i1 %v9_404dc4, i1* %zf.global-to-local, align 1
  %v10_404dc4 = icmp slt i32 %v1_404dc4, 0
  store i1 %v10_404dc4, i1* %sf.global-to-local, align 1
  store i32 %v1_404dc4, i32* @ebx, align 4
  %v1_404dc7 = add i32 %v1_404dbf, 115
  %v2_404dc7 = inttoptr i32 %v1_404dc7 to i8*
  %v3_404dc7 = load i8, i8* %v2_404dc7, align 1
  %v4_404dc7 = load i32, i32* @ecx, align 4
  %v5_404dc7 = trunc i32 %v4_404dc7 to i8
  %v6_404dc7 = sub i8 %v3_404dc7, %v5_404dc7
  %v7_404dc7 = urem i8 %v3_404dc7, 16
  %v8_404dc7 = urem i8 %v5_404dc7, 16
  %v9_404dc7 = sub nsw i8 %v7_404dc7, %v8_404dc7
  %v10_404dc7 = icmp ugt i8 %v9_404dc7, 15
  %v11_404dc7 = icmp ult i8 %v3_404dc7, %v5_404dc7
  %v12_404dc7 = xor i8 %v3_404dc7, %v5_404dc7
  %v13_404dc7 = xor i8 %v6_404dc7, %v3_404dc7
  %v14_404dc7 = and i8 %v13_404dc7, %v12_404dc7
  %v15_404dc7 = icmp slt i8 %v14_404dc7, 0
  store i1 %v10_404dc7, i1* %az.global-to-local, align 1
  store i1 %v11_404dc7, i1* %cf.global-to-local, align 1
  store i1 %v15_404dc7, i1* %of.global-to-local, align 1
  %v16_404dc7 = icmp eq i8 %v6_404dc7, 0
  store i1 %v16_404dc7, i1* %zf.global-to-local, align 1
  %v17_404dc7 = icmp slt i8 %v6_404dc7, 0
  store i1 %v17_404dc7, i1* %sf.global-to-local, align 1
  %v1_404dca = add i32 %v4_404d3f, -7
  %v2_404dca = inttoptr i32 %v1_404dca to i32*
  store i32 107, i32* %v2_404dca, align 4
  %v0_404dcc = load i32, i32* @ecx, align 4
  %v1_404dcc = add i32 %v0_404dcc, 110
  %v2_404dcc = inttoptr i32 %v1_404dcc to i8*
  %v3_404dcc = load i8, i8* %v2_404dcc, align 1
  %v5_404dcc = trunc i32 %v0_404dcc to i8
  %v6_404dcc = add i8 %v3_404dcc, %v5_404dcc
  %v7_404dcc = urem i8 %v3_404dcc, 16
  %v8_404dcc = urem i8 %v5_404dcc, 16
  %v9_404dcc = add nuw nsw i8 %v7_404dcc, %v8_404dcc
  %v10_404dcc = icmp ugt i8 %v9_404dcc, 15
  %v11_404dcc = icmp ult i8 %v6_404dcc, %v3_404dcc
  %v12_404dcc = xor i8 %v6_404dcc, %v3_404dcc
  %v13_404dcc = xor i8 %v6_404dcc, %v5_404dcc
  %v14_404dcc = and i8 %v12_404dcc, %v13_404dcc
  %v15_404dcc = icmp slt i8 %v14_404dcc, 0
  store i1 %v10_404dcc, i1* %az.global-to-local, align 1
  store i1 %v11_404dcc, i1* %cf.global-to-local, align 1
  store i1 %v15_404dcc, i1* %of.global-to-local, align 1
  %v16_404dcc = icmp eq i8 %v6_404dcc, 0
  store i1 %v16_404dcc, i1* %zf.global-to-local, align 1
  %v17_404dcc = icmp slt i8 %v6_404dcc, 0
  store i1 %v17_404dcc, i1* %sf.global-to-local, align 1
  store i8 %v6_404dcc, i8* %v2_404dcc, align 1
  %v0_404dcf = load i1, i1* %zf.global-to-local, align 1
  %v3_404e05 = load i32, i32* @eax, align 4
  br i1 %v0_404dcf, label %dec_label_pc_404838, label %dec_label_pc_404dd1

dec_label_pc_404dd1:                              ; preds = %dec_label_pc_404dc7
  %v1_404dd1 = trunc i32 %v3_404e05 to i8
  store i1 false, i1* %az.global-to-local, align 1
  store i1 false, i1* %cf.global-to-local, align 1
  store i1 false, i1* %of.global-to-local, align 1
  %v3_404dd1 = icmp eq i8 %v1_404dd1, 0
  store i1 %v3_404dd1, i1* %zf.global-to-local, align 1
  %v4_404dd1 = icmp slt i8 %v1_404dd1, 0
  store i1 %v4_404dd1, i1* %sf.global-to-local, align 1
  %v0_404dd3 = load i32, i32* @ebx, align 4
  %v1_404dd3 = load i32, i32* @esp, align 4
  %v2_404dd3 = add i32 %v1_404dd3, -4
  %v3_404dd3 = inttoptr i32 %v2_404dd3 to i32*
  store i32 %v0_404dd3, i32* %v3_404dd3, align 4
  %v0_404dd4 = load i32, i32* @edx, align 4
  %v1_404dd4 = trunc i32 %v0_404dd4 to i16
  %v2_404dd4 = call i8 @__asm_insb(i16 %v1_404dd4)
  %v3_404dd4 = load i32, i32* @edi, align 4
  %v4_404dd4 = inttoptr i32 %v3_404dd4 to i8*
  store i8 %v2_404dd4, i8* %v4_404dd4, align 1
  store i1 false, i1* %of.global-to-local, align 1
  store i1 false, i1* %cf.global-to-local, align 1
  %v9_404dd9 = load i32, i32* @eax, align 4
  ret i32 %v9_404dd9

dec_label_pc_404de1:                              ; preds = %dec_label_pc_404d9c, %dec_label_pc_404d7e
  %v4_404de1 = phi i32 [ %v3_404da5, %dec_label_pc_404d9c ], [ %v4_404de1.pre, %dec_label_pc_404d7e ]
  %v0_404de1 = load i32, i32* @ecx, align 4
  %v1_404de1 = add i32 %v0_404de1, 115
  %v2_404de1 = inttoptr i32 %v1_404de1 to i8*
  %v3_404de1 = load i8, i8* %v2_404de1, align 1
  %v5_404de1 = trunc i32 %v4_404de1 to i8
  %v6_404de1 = add i8 %v3_404de1, %v5_404de1
  %v7_404de1 = urem i8 %v3_404de1, 16
  %v8_404de1 = urem i8 %v5_404de1, 16
  %v9_404de1 = add nuw nsw i8 %v7_404de1, %v8_404de1
  %v10_404de1 = icmp ugt i8 %v9_404de1, 15
  %v11_404de1 = icmp ult i8 %v6_404de1, %v3_404de1
  %v12_404de1 = xor i8 %v6_404de1, %v3_404de1
  %v13_404de1 = xor i8 %v6_404de1, %v5_404de1
  %v14_404de1 = and i8 %v12_404de1, %v13_404de1
  %v15_404de1 = icmp slt i8 %v14_404de1, 0
  store i1 %v10_404de1, i1* %az.global-to-local, align 1
  store i1 %v11_404de1, i1* %cf.global-to-local, align 1
  store i1 %v15_404de1, i1* %of.global-to-local, align 1
  %v16_404de1 = icmp eq i8 %v6_404de1, 0
  store i1 %v16_404de1, i1* %zf.global-to-local, align 1
  %v17_404de1 = icmp slt i8 %v6_404de1, 0
  store i1 %v17_404de1, i1* %sf.global-to-local, align 1
  store i8 %v6_404de1, i8* %v2_404de1, align 1
  %v0_404de4 = load i1, i1* %cf.global-to-local, align 1
  %v1_404de4 = icmp eq i1 %v0_404de4, false
  %v0_404e4b = load i32, i32* @edx, align 4
  %v1_404e4b = trunc i32 %v0_404e4b to i16
  br i1 %v1_404de4, label %dec_label_pc_404e4b, label %dec_label_pc_404de6

dec_label_pc_404de6:                              ; preds = %dec_label_pc_404de1
  %v2_404de6 = call i32 @__asm_insd(i16 %v1_404e4b)
  %v3_404de6 = load i32, i32* @edi, align 4
  %v4_404de6 = inttoptr i32 %v3_404de6 to i32*
  store i32 %v2_404de6, i32* %v4_404de6, align 4
  %v0_404de7 = load i32, i32* @ecx, align 4
  %v1_404de7 = load i32, i32* @edi, align 4
  %v2_404de7 = mul i32 %v1_404de7, 2
  %v3_404de7 = add i32 %v2_404de7, %v0_404de7
  %v4_404de7 = inttoptr i32 %v3_404de7 to i64*
  %v5_404de7 = load i64, i64* %v4_404de7, align 4
  %v6_404de7 = call i32 @__asm_bound(i64 %v5_404de7)
  store i32 %v6_404de7, i32* @ebp, align 4
  %v0_404deb = load i32, i32* @ebx, align 4
  %v1_404deb = load i32, i32* @esp, align 4
  %v2_404deb = add i32 %v1_404deb, -4
  %v3_404deb = inttoptr i32 %v2_404deb to i32*
  store i32 %v0_404deb, i32* %v3_404deb, align 4
  %v0_404dec = load i1, i1* %sf.global-to-local, align 1
  %v1_404dec = icmp eq i1 %v0_404dec, false
  br i1 %v1_404dec, label %dec_label_pc_404e61, label %dec_label_pc_404dee

dec_label_pc_404dee:                              ; preds = %dec_label_pc_404de6
  %v2_404dec = load i32, i32* @eax, align 4
  ret i32 %v2_404dec

dec_label_pc_404def:                              ; preds = %dec_label_pc_404d6d
  %v1_404def = trunc i32 %v20_404d72 to i16
  %v2_404def = call i32 @__asm_insd(i16 %v1_404def)
  %v3_404def = load i32, i32* @edi, align 4
  %v4_404def = inttoptr i32 %v3_404def to i32*
  store i32 %v2_404def, i32* %v4_404def, align 4
  %v0_404df1 = load i32, i32* @edx, align 4
  %v1_404df1 = load i32, i32* @esp, align 4
  %v2_404df1 = add i32 %v1_404df1, -4
  %v3_404df1 = inttoptr i32 %v2_404df1 to i32*
  store i32 %v0_404df1, i32* %v3_404df1, align 4
  %v0_404df3 = load i32, i32* @edx, align 4
  %v1_404df3 = trunc i32 %v0_404df3 to i16
  %v2_404df3 = call i8 @__asm_insb(i16 %v1_404df3)
  %v3_404df3 = load i32, i32* @edi, align 4
  %v4_404df3 = inttoptr i32 %v3_404df3 to i8*
  store i8 %v2_404df3, i8* %v4_404df3, align 1
  %v0_404df6 = load i32, i32* @ecx, align 4
  %v1_404df6 = load i32, i32* @ebp, align 4
  %v2_404df6 = mul i32 %v1_404df6, 2
  %v3_404df6 = add i32 %v0_404df6, 111
  %v4_404df6 = add i32 %v3_404df6, %v2_404df6
  %v6_404df6 = call i16 @__readgsword(i32 %v4_404df6)
  %v7_404df6 = load i32, i32* @esi, align 4
  %v8_404df6 = trunc i32 %v7_404df6 to i16
  call void @__asm_arpl(i16 %v6_404df6, i16 %v8_404df6)
  %v0_404dfb = load i32, i32* @edx, align 4
  %v1_404dfb = trunc i32 %v0_404dfb to i16
  %v2_404dfb = load i32, i32* @esi, align 4
  %v3_404dfb = inttoptr i32 %v2_404dfb to i8*
  %v4_404dfb = load i8, i8* %v3_404dfb, align 1
  call void @__asm_outsb(i16 %v1_404dfb, i8 %v4_404dfb)
  %v5_404dfb = load i32, i32* @eax, align 4
  ret i32 %v5_404dfb

dec_label_pc_404e4b:                              ; preds = %dec_label_pc_404de1
  %v2_404e4b = load i32, i32* @esi, align 4
  %v3_404e4b = inttoptr i32 %v2_404e4b to i32*
  %v4_404e4b = load i32, i32* %v3_404e4b, align 4
  call void @__asm_outsd(i16 %v1_404e4b, i32 %v4_404e4b)
  %v0_404e4c = load i32, i32* @edx, align 4
  %v1_404e4c = trunc i32 %v0_404e4c to i16
  %v2_404e4c = load i32, i32* @esi, align 4
  %v3_404e4c = inttoptr i32 %v2_404e4c to i8*
  %v4_404e4c = load i8, i8* %v3_404e4c, align 1
  call void @__asm_outsb(i16 %v1_404e4c, i8 %v4_404e4c)
  %v5_404e4c = load i32, i32* @eax, align 4
  ret i32 %v5_404e4c

dec_label_pc_404e61:                              ; preds = %dec_label_pc_404de6
  %v0_404e61.pre = load i32, i32* @ecx, align 4
  %v2_404e61 = add i32 %v1_404deb, -8
  %v3_404e61 = inttoptr i32 %v2_404e61 to i32*
  store i32 %v0_404e61.pre, i32* %v3_404e61, align 4
  %v0_404e62 = load i32, i32* @eax, align 4
  %v1_404e62 = add i32 %v0_404e62, 84
  %v2_404e62 = inttoptr i32 %v1_404e62 to i8*
  %v3_404e62 = load i8, i8* %v2_404e62, align 1
  %v4_404e62 = load i32, i32* @ecx, align 4
  %v5_404e62 = udiv i32 %v4_404e62, 256
  %v6_404e62 = trunc i32 %v5_404e62 to i8
  %v7_404e62 = xor i8 %v3_404e62, %v6_404e62
  store i1 false, i1* %az.global-to-local, align 1
  store i1 false, i1* %cf.global-to-local, align 1
  store i1 false, i1* %of.global-to-local, align 1
  %v8_404e62 = icmp eq i8 %v7_404e62, 0
  store i1 %v8_404e62, i1* %zf.global-to-local, align 1
  %v9_404e62 = icmp slt i8 %v7_404e62, 0
  store i1 %v9_404e62, i1* %sf.global-to-local, align 1
  store i8 %v7_404e62, i8* %v2_404e62, align 1
  %v0_404e65 = load i1, i1* %sf.global-to-local, align 1
  br i1 %v0_404e65, label %dec_label_pc_404ebb, label %dec_label_pc_404e67

dec_label_pc_404e67:                              ; preds = %dec_label_pc_404e61
  %v0_404e67 = load i32, i32* @ecx, align 4
  %v1_404e67 = add i32 %v0_404e67, 1
  store i32 %v1_404e67, i32* @ecx, align 4
  %v0_404e69 = load i32, i32* @ebp, align 4
  %v1_404e69 = add i32 %v0_404e69, -1
  %v2_404e69 = urem i32 %v0_404e69, 16
  %v3_404e69 = add nsw i32 %v2_404e69, -1
  %v4_404e69 = icmp ugt i32 %v3_404e69, 15
  %tmp530 = sub i32 0, %v0_404e69
  %v7_404e69 = and i32 %v0_404e69, %tmp530
  %v8_404e69 = icmp slt i32 %v7_404e69, 0
  store i1 %v4_404e69, i1* %az.global-to-local, align 1
  store i1 %v8_404e69, i1* %of.global-to-local, align 1
  %v9_404e69 = icmp eq i32 %v1_404e69, 0
  store i1 %v9_404e69, i1* %zf.global-to-local, align 1
  %v10_404e69 = icmp slt i32 %v1_404e69, 0
  store i1 %v10_404e69, i1* %sf.global-to-local, align 1
  store i32 %v1_404e69, i32* @ebp, align 4
  %v1_404e6a = icmp eq i1 %v9_404e69, false
  br i1 %v1_404e6a, label %dec_label_pc_404ea4, label %dec_label_pc_404e74

dec_label_pc_404e74:                              ; preds = %dec_label_pc_404e67
  %v0_404e6c = load i32, i32* @esp, align 4
  %v0_404e6d = load i32, i32* @eax, align 4
  %v2_404e6d = add i32 %v0_404e6c, -3
  %v3_404e6d = inttoptr i32 %v2_404e6d to i32*
  store i32 %v0_404e6d, i32* %v3_404e6d, align 4
  %v1_404e6e.pre = load i32, i32* @ebx, align 4
  %v0_404e71.pre = load i32, i32* @ebp, align 4
  %v0_404e76.pre = load i32, i32* @edi, align 4
  %v9_404e77.pre = load i32, i32* @eax, align 4
  %v1_404e74 = add i32 %v1_404e6e.pre, 1
  store i32 %v1_404e74, i32* @ebx, align 4
  %v1_404e76 = add i32 %v0_404e76.pre, -1
  %v2_404e76 = urem i32 %v0_404e76.pre, 16
  %v3_404e76 = add nsw i32 %v2_404e76, -1
  %v4_404e76 = icmp ugt i32 %v3_404e76, 15
  store i1 %v4_404e76, i1* %az.global-to-local, align 1
  %v9_404e76 = icmp eq i32 %v1_404e76, 0
  store i1 %v9_404e76, i1* %zf.global-to-local, align 1
  %v10_404e76 = icmp slt i32 %v1_404e76, 0
  store i1 %v10_404e76, i1* %sf.global-to-local, align 1
  store i32 %v1_404e76, i32* @edi, align 4
  %v2_404e77 = add i32 %v0_404e71.pre, 100
  %v3_404e77 = inttoptr i32 %v2_404e77 to i32*
  %v4_404e77 = load i32, i32* %v3_404e77, align 4
  %v6_404e77 = mul i32 %v4_404e77, 110
  store i32 %v6_404e77, i32* @ecx, align 4
  store i1 false, i1* %of.global-to-local, align 1
  store i1 false, i1* %cf.global-to-local, align 1
  ret i32 %v9_404e77.pre

dec_label_pc_404ea4:                              ; preds = %dec_label_pc_404e67
  %v1_404ea2.pre = load i32, i32* @eax, align 4
  ret i32 %v1_404ea2.pre

dec_label_pc_404ebb:                              ; preds = %dec_label_pc_404e61
  %v0_404ebb = load i32, i32* @esi, align 4
  %v1_404ebb = add i32 %v0_404ebb, 1
  %v2_404ebb = urem i32 %v0_404ebb, 16
  %v4_404ebb = icmp eq i32 %v2_404ebb, 15
  %tmp531 = xor i32 %v0_404ebb, -2147483648
  %v7_404ebb = and i32 %v1_404ebb, %tmp531
  %v8_404ebb = icmp slt i32 %v7_404ebb, 0
  store i1 %v4_404ebb, i1* %az.global-to-local, align 1
  store i1 %v8_404ebb, i1* %of.global-to-local, align 1
  %v9_404ebb = icmp eq i32 %v1_404ebb, 0
  store i1 %v9_404ebb, i1* %zf.global-to-local, align 1
  %v10_404ebb = icmp slt i32 %v1_404ebb, 0
  store i1 %v10_404ebb, i1* %sf.global-to-local, align 1
  store i32 %v1_404ebb, i32* @esi, align 4
  br i1 %v8_404ebb, label %dec_label_pc_404ef8, label %dec_label_pc_404ec3

dec_label_pc_404ec3:                              ; preds = %dec_label_pc_404ebb
  %v0_404ebe = load i32, i32* @ecx, align 4
  %v1_404ebe = load i32, i32* @esp, align 4
  %v2_404ebe = add i32 %v1_404ebe, -4
  %v3_404ebe = inttoptr i32 %v2_404ebe to i32*
  store i32 %v0_404ebe, i32* %v3_404ebe, align 4
  %v0_404ebf = load i32, i32* @ebp, align 4
  %v1_404ebf = add i32 %v0_404ebf, 79
  %v2_404ebf = inttoptr i32 %v1_404ebf to i8*
  %v3_404ebf = load i8, i8* %v2_404ebf, align 1
  %v4_404ebf = load i32, i32* @ecx, align 4
  %v5_404ebf = trunc i32 %v4_404ebf to i8
  %v6_404ebf = add i8 %v3_404ebf, %v5_404ebf
  %v7_404ebf = urem i8 %v3_404ebf, 16
  %v8_404ebf = urem i8 %v5_404ebf, 16
  %v9_404ebf = add nuw nsw i8 %v8_404ebf, %v7_404ebf
  %v10_404ebf = icmp ugt i8 %v9_404ebf, 15
  %v11_404ebf = icmp ult i8 %v6_404ebf, %v3_404ebf
  %v12_404ebf = xor i8 %v6_404ebf, %v3_404ebf
  %v13_404ebf = xor i8 %v6_404ebf, %v5_404ebf
  %v14_404ebf = and i8 %v12_404ebf, %v13_404ebf
  %v15_404ebf = icmp slt i8 %v14_404ebf, 0
  store i1 %v10_404ebf, i1* %az.global-to-local, align 1
  store i1 %v11_404ebf, i1* %cf.global-to-local, align 1
  store i1 %v15_404ebf, i1* %of.global-to-local, align 1
  %v16_404ebf = icmp eq i8 %v6_404ebf, 0
  store i1 %v16_404ebf, i1* %zf.global-to-local, align 1
  %v17_404ebf = icmp slt i8 %v6_404ebf, 0
  store i1 %v17_404ebf, i1* %sf.global-to-local, align 1
  store i8 %v6_404ebf, i8* %v2_404ebf, align 1
  %v0_404ec2 = load i32, i32* @esp, align 4
  %v1_404ec2 = inttoptr i32 %v0_404ec2 to i32*
  %v2_404ec2 = load i32, i32* %v1_404ec2, align 4
  store i32 %v2_404ec2, i32* @ecx, align 4
  %v3_404ec2 = add i32 %v0_404ec2, 4
  %v0_404ec3.pre = load i1, i1* %sf.global-to-local, align 1
  br i1 %v0_404ec3.pre, label %dec_label_pc_404ec3.dec_label_pc_404f39_crit_edge, label %dec_label_pc_404ec5

dec_label_pc_404ec3.dec_label_pc_404f39_crit_edge: ; preds = %dec_label_pc_404ec3
  %v0_404f3a.pre = load i32, i32* @edx, align 4
  br label %dec_label_pc_404f39

dec_label_pc_404ec5:                              ; preds = %dec_label_pc_404ec3
  %v0_404ec8 = load i32, i32* @eax, align 4
  %v11_404ec8 = xor i32 %v0_404ec8, 109
  store i32 %v11_404ec8, i32* @eax, align 4
  %v1_404eca = add i32 %v0_404ec2, 8
  %v2_404eca = add i32 %v0_404ec2, 12
  %v3_404eca = add i32 %v0_404ec2, 20
  %v4_404eca = add i32 %v0_404ec2, 24
  %v5_404eca = add i32 %v0_404ec2, 28
  %v6_404eca = add i32 %v0_404ec2, 32
  %v8_404eca = inttoptr i32 %v3_404ec2 to i32*
  %v9_404eca = load i32, i32* %v8_404eca, align 4
  %v10_404eca = inttoptr i32 %v1_404eca to i32*
  %v11_404eca = load i32, i32* %v10_404eca, align 4
  %v12_404eca = inttoptr i32 %v2_404eca to i32*
  %v13_404eca = load i32, i32* %v12_404eca, align 4
  %v14_404eca = inttoptr i32 %v3_404eca to i32*
  %v15_404eca = load i32, i32* %v14_404eca, align 4
  %v16_404eca = inttoptr i32 %v4_404eca to i32*
  %v17_404eca = load i32, i32* %v16_404eca, align 4
  %v18_404eca = inttoptr i32 %v5_404eca to i32*
  %v19_404eca = load i32, i32* %v18_404eca, align 4
  %v20_404eca = inttoptr i32 %v6_404eca to i32*
  %v21_404eca = load i32, i32* %v20_404eca, align 4
  store i32 %v11_404eca, i32* @esi, align 4
  store i32 %v13_404eca, i32* @ebp, align 4
  store i32 %v15_404eca, i32* @ebx, align 4
  store i32 %v17_404eca, i32* @edx, align 4
  store i32 %v19_404eca, i32* @ecx, align 4
  store i32 %v21_404eca, i32* @eax, align 4
  %v1_404ecb = add i32 %v9_404eca, -1
  store i32 %v1_404ecb, i32* @edi, align 4
  %v1_404ecc = add i32 %v11_404eca, 73
  %v2_404ecc = inttoptr i32 %v1_404ecc to i32*
  %v3_404ecc = load i32, i32* %v2_404ecc, align 4
  %v10_404ecc = icmp ult i32 %v3_404ecc, %v11_404eca
  store i1 %v10_404ecc, i1* %cf.global-to-local, align 1
  %v1_404ecf = add i32 %v21_404eca, -1
  %v2_404ecf = urem i32 %v21_404eca, 16
  %v3_404ecf = add nsw i32 %v2_404ecf, -1
  %v4_404ecf = icmp ugt i32 %v3_404ecf, 15
  %tmp532 = sub i32 0, %v21_404eca
  %v7_404ecf = and i32 %v21_404eca, %tmp532
  %v8_404ecf = icmp slt i32 %v7_404ecf, 0
  store i1 %v4_404ecf, i1* %az.global-to-local, align 1
  store i1 %v8_404ecf, i1* %of.global-to-local, align 1
  %v9_404ecf = icmp eq i32 %v1_404ecf, 0
  store i1 %v9_404ecf, i1* %zf.global-to-local, align 1
  %v10_404ecf = icmp slt i32 %v1_404ecf, 0
  store i1 %v10_404ecf, i1* %sf.global-to-local, align 1
  store i32 %v1_404ecf, i32* @eax, align 4
  %v1_404ed0 = inttoptr i32 %v1_404ecb to i16*
  %v2_404ed0 = load i16, i16* %v1_404ed0, align 2
  %v4_404ed0 = trunc i32 %v13_404eca to i16
  call void @__asm_arpl(i16 %v2_404ed0, i16 %v4_404ed0)
  %v0_404ed3 = load i1, i1* %zf.global-to-local, align 1
  %v1_404ed3 = icmp eq i1 %v0_404ed3, false
  br i1 %v1_404ed3, label %dec_label_pc_404f2e, label %dec_label_pc_404ed5

dec_label_pc_404ed5:                              ; preds = %dec_label_pc_404ec5
  %v0_404ed5.pr = load i1, i1* %of.global-to-local, align 1
  %v1_404ed5 = icmp eq i1 %v0_404ed5.pr, false
  br i1 %v1_404ed5, label %dec_label_pc_404fa5, label %dec_label_pc_404ed7

dec_label_pc_404ed7:                              ; preds = %dec_label_pc_404ed5
  %v0_404ed7.pre = load i1, i1* %sf.global-to-local, align 1
  %v1_404ed7 = icmp eq i1 %v0_404ed7.pre, false
  br i1 %v1_404ed7, label %dec_label_pc_404f22, label %dec_label_pc_404edb

dec_label_pc_404edb:                              ; preds = %dec_label_pc_404ed7
  %v0_404edc = load i32, i32* @ecx, align 4
  %v1_404edc = trunc i32 %v0_404edc to i16
  %v2_404edc = add i16 %v1_404edc, 1
  %v3_404edc = urem i16 %v1_404edc, 16
  %v5_404edc = icmp eq i16 %v3_404edc, 15
  %tmp533 = xor i16 %v1_404edc, -32768
  %v8_404edc = and i16 %v2_404edc, %tmp533
  %v9_404edc = icmp slt i16 %v8_404edc, 0
  store i1 %v5_404edc, i1* %az.global-to-local, align 1
  store i1 %v9_404edc, i1* %of.global-to-local, align 1
  %v10_404edc = icmp eq i16 %v2_404edc, 0
  store i1 %v10_404edc, i1* %zf.global-to-local, align 1
  %v11_404edc = icmp slt i16 %v2_404edc, 0
  store i1 %v11_404edc, i1* %sf.global-to-local, align 1
  %v16_404edc = zext i16 %v2_404edc to i32
  %v18_404edc = and i32 %v0_404edc, -65536
  %v19_404edc = or i32 %v18_404edc, %v16_404edc
  store i32 %v19_404edc, i32* @ecx, align 4
  %v1_404edf = icmp eq i1 %v9_404edc, false
  br i1 %v1_404edf, label %dec_label_pc_404f22, label %dec_label_pc_404ee1

dec_label_pc_404ee1:                              ; preds = %dec_label_pc_404edb
  %v0_404ee1 = load i32, i32* @edx, align 4
  %v1_404ee1 = trunc i32 %v0_404ee1 to i16
  %v2_404ee1 = call i32 @__asm_insd(i16 %v1_404ee1)
  %v3_404ee1 = load i32, i32* @edi, align 4
  %v4_404ee1 = inttoptr i32 %v3_404ee1 to i32*
  store i32 %v2_404ee1, i32* %v4_404ee1, align 4
  %v0_404ee2 = load i32, i32* @edx, align 4
  %v1_404ee2 = trunc i32 %v0_404ee2 to i16
  %v2_404ee2 = call i8 @__asm_insb(i16 %v1_404ee2)
  %v3_404ee2 = load i32, i32* @edi, align 4
  %v4_404ee2 = inttoptr i32 %v3_404ee2 to i8*
  store i8 %v2_404ee2, i8* %v4_404ee2, align 1
  %v0_404ee3 = load i32, i32* @eax, align 4
  %v5_404ee3 = load i1, i1* %az.global-to-local, align 1
  %v6_404ee321 = and i32 %v0_404ee3, 14
  %v7_404ee3 = icmp ugt i32 %v6_404ee321, 9
  %v8_404ee3 = or i1 %v5_404ee3, %v7_404ee3
  %v9_404ee3 = add i32 %v0_404ee3, 6
  %v11_404ee3 = select i1 %v8_404ee3, i32 %v9_404ee3, i32 %v0_404ee3
  %v10_404ee3 = zext i1 %v8_404ee3 to i32
  %v15_404ee3 = urem i32 %v11_404ee3, 16
  %v18_404ee3 = and i32 %v0_404ee3, -65536
  %v19_404ee3 = or i32 %v15_404ee3, %v18_404ee3
  %v12_404ee37 = mul i32 %v10_404ee3, 256
  %v3_404ee38 = add i32 %v12_404ee37, %v0_404ee3
  %v22_404ee3 = and i32 %v3_404ee38, 65280
  %v24_404ee3 = or i32 %v19_404ee3, %v22_404ee3
  store i32 %v24_404ee3, i32* @eax, align 4
  store i1 %v8_404ee3, i1* %az.global-to-local, align 1
  store i1 %v8_404ee3, i1* %cf.global-to-local, align 1
  ret i32 %v24_404ee3

dec_label_pc_404ef8:                              ; preds = %dec_label_pc_404ebb
  %v0_404eef = load i32, i32* @esp, align 4
  %v1_404eef = add i32 %v0_404eef, 4
  %v2_404eef = add i32 %v0_404eef, 8
  %v3_404eef = add i32 %v0_404eef, 16
  %v4_404eef = add i32 %v0_404eef, 20
  %v5_404eef = add i32 %v0_404eef, 24
  %v6_404eef = add i32 %v0_404eef, 28
  %v7_404eef = add i32 %v0_404eef, 32
  %v8_404eef = inttoptr i32 %v0_404eef to i32*
  %v9_404eef = load i32, i32* %v8_404eef, align 4
  %v10_404eef = inttoptr i32 %v1_404eef to i32*
  %v11_404eef = load i32, i32* %v10_404eef, align 4
  %v12_404eef = inttoptr i32 %v2_404eef to i32*
  %v13_404eef = load i32, i32* %v12_404eef, align 4
  %v14_404eef = inttoptr i32 %v3_404eef to i32*
  %v15_404eef = load i32, i32* %v14_404eef, align 4
  %v16_404eef = inttoptr i32 %v4_404eef to i32*
  %v17_404eef = load i32, i32* %v16_404eef, align 4
  %v18_404eef = inttoptr i32 %v5_404eef to i32*
  %v19_404eef = load i32, i32* %v18_404eef, align 4
  %v20_404eef = inttoptr i32 %v6_404eef to i32*
  %v21_404eef = load i32, i32* %v20_404eef, align 4
  store i32 %v9_404eef, i32* @edi, align 4
  store i32 %v11_404eef, i32* @esi, align 4
  store i32 %v13_404eef, i32* @ebp, align 4
  store i32 %v15_404eef, i32* @ebx, align 4
  store i32 %v17_404eef, i32* @edx, align 4
  store i32 %v19_404eef, i32* @ecx, align 4
  store i32 %v21_404eef, i32* @eax, align 4
  %v1_404ef0 = inttoptr i32 %v7_404eef to i32*
  %v2_404ef0 = load i32, i32* %v1_404ef0, align 4
  store i32 %v2_404ef0, i32* @ecx, align 4
  %v3_404ef0 = add i32 %v0_404eef, 36
  %v1_404ef1 = inttoptr i32 %v3_404ef0 to i32*
  %v2_404ef1 = load i32, i32* %v1_404ef1, align 4
  store i32 %v2_404ef1, i32* @edx, align 4
  %v1_404ef2 = trunc i32 %v2_404ef1 to i16
  %v2_404ef2 = call i8 @__asm_insb(i16 %v1_404ef2)
  %v3_404ef2 = load i32, i32* @edi, align 4
  %v4_404ef2 = inttoptr i32 %v3_404ef2 to i8*
  store i8 %v2_404ef2, i8* %v4_404ef2, align 1
  %v0_404ef3 = load i32, i32* @ecx, align 4
  %v1_404ef3 = load i32, i32* @esp, align 4
  %v2_404ef3 = add i32 %v1_404ef3, -4
  %v3_404ef3 = inttoptr i32 %v2_404ef3 to i32*
  store i32 %v0_404ef3, i32* %v3_404ef3, align 4
  %v0_404ef4 = load i32, i32* @eax, align 4
  store i1 false, i1* %cf.global-to-local, align 1
  %v11_404ef4 = xor i32 %v0_404ef4, 102
  store i32 %v11_404ef4, i32* @eax, align 4
  %v0_404ef8 = load i32, i32* @esi, align 4
  %v1_404ef8 = add i32 %v0_404ef8, 1
  store i32 %v1_404ef8, i32* @esi, align 4
  %v0_404ef9 = load i32, i32* @ecx, align 4
  %v1_404ef9 = add i32 %v0_404ef9, -1
  %v2_404ef9 = urem i32 %v0_404ef9, 16
  %v3_404ef9 = add nsw i32 %v2_404ef9, -1
  %v4_404ef9 = icmp ugt i32 %v3_404ef9, 15
  %tmp534 = sub i32 0, %v0_404ef9
  %v7_404ef9 = and i32 %v0_404ef9, %tmp534
  %v8_404ef9 = icmp slt i32 %v7_404ef9, 0
  store i1 %v4_404ef9, i1* %az.global-to-local, align 1
  store i1 %v8_404ef9, i1* %of.global-to-local, align 1
  %v9_404ef9 = icmp eq i32 %v1_404ef9, 0
  store i1 %v9_404ef9, i1* %zf.global-to-local, align 1
  %v10_404ef9 = icmp slt i32 %v1_404ef9, 0
  store i1 %v10_404ef9, i1* %sf.global-to-local, align 1
  store i32 %v1_404ef9, i32* @ecx, align 4
  ret i32 %v11_404ef4

dec_label_pc_404f22:                              ; preds = %dec_label_pc_404edb, %dec_label_pc_404ed7
  %v0_404f22 = load i32, i32* @edx, align 4
  %v1_404f22 = add i32 %v0_404f22, -1
  %v2_404f22 = urem i32 %v0_404f22, 16
  %v3_404f22 = add nsw i32 %v2_404f22, -1
  %v4_404f22 = icmp ugt i32 %v3_404f22, 15
  %tmp535 = sub i32 0, %v0_404f22
  %v7_404f22 = and i32 %v0_404f22, %tmp535
  %v8_404f22 = icmp slt i32 %v7_404f22, 0
  store i1 %v4_404f22, i1* %az.global-to-local, align 1
  store i1 %v8_404f22, i1* %of.global-to-local, align 1
  %v9_404f22 = icmp eq i32 %v1_404f22, 0
  store i1 %v9_404f22, i1* %zf.global-to-local, align 1
  %v10_404f22 = icmp slt i32 %v1_404f22, 0
  store i1 %v10_404f22, i1* %sf.global-to-local, align 1
  store i32 %v1_404f22, i32* @edx, align 4
  %v0_404f23 = load i32, i32* @edi, align 4
  %v1_404f23 = add i32 %v0_404f23, 101
  %v2_404f23 = inttoptr i32 %v1_404f23 to i8*
  %v3_404f23 = load i8, i8* %v2_404f23, align 1
  %v4_404f23 = load i32, i32* @eax, align 4
  %v5_404f23 = udiv i32 %v4_404f23, 256
  %v6_404f23 = trunc i32 %v5_404f23 to i8
  %v7_404f23 = add i8 %v3_404f23, %v6_404f23
  %v8_404f23 = urem i8 %v3_404f23, 16
  %v9_404f23 = urem i8 %v6_404f23, 16
  %v10_404f23 = add nuw nsw i8 %v9_404f23, %v8_404f23
  %v11_404f23 = icmp ugt i8 %v10_404f23, 15
  %v12_404f23 = icmp ult i8 %v7_404f23, %v3_404f23
  %v13_404f23 = xor i8 %v7_404f23, %v3_404f23
  %v14_404f23 = xor i8 %v7_404f23, %v6_404f23
  %v15_404f23 = and i8 %v13_404f23, %v14_404f23
  %v16_404f23 = icmp slt i8 %v15_404f23, 0
  store i1 %v11_404f23, i1* %az.global-to-local, align 1
  store i1 %v12_404f23, i1* %cf.global-to-local, align 1
  store i1 %v16_404f23, i1* %of.global-to-local, align 1
  %v17_404f23 = icmp eq i8 %v7_404f23, 0
  store i1 %v17_404f23, i1* %zf.global-to-local, align 1
  %v18_404f23 = icmp slt i8 %v7_404f23, 0
  store i1 %v18_404f23, i1* %sf.global-to-local, align 1
  store i8 %v7_404f23, i8* %v2_404f23, align 1
  %v0_404f26 = load i1, i1* %zf.global-to-local, align 1
  br i1 %v0_404f26, label %dec_label_pc_404fdf, label %dec_label_pc_404f28

dec_label_pc_404f28:                              ; preds = %dec_label_pc_404f22
  %v0_404f28 = load i32, i32* @esp, align 4
  %v1_404f28 = add i32 %v0_404f28, -1
  %v2_404f28 = urem i32 %v0_404f28, 16
  %v3_404f28 = add nsw i32 %v2_404f28, -1
  %v4_404f28 = icmp ugt i32 %v3_404f28, 15
  %tmp536 = sub i32 0, %v0_404f28
  %v7_404f28 = and i32 %v0_404f28, %tmp536
  %v8_404f28 = icmp slt i32 %v7_404f28, 0
  store i1 %v4_404f28, i1* %az.global-to-local, align 1
  store i1 %v8_404f28, i1* %of.global-to-local, align 1
  %v9_404f28 = icmp eq i32 %v1_404f28, 0
  store i1 %v9_404f28, i1* %zf.global-to-local, align 1
  %v10_404f28 = icmp slt i32 %v1_404f28, 0
  store i1 %v10_404f28, i1* %sf.global-to-local, align 1
  %v0_404f29 = load i32, i32* @edx, align 4
  %v1_404f29 = trunc i32 %v0_404f29 to i16
  %v2_404f29 = load i32, i32* @esi, align 4
  %v4_404f29 = call i8 @__readgsbyte(i32 %v2_404f29)
  call void @__asm_outsb(i16 %v1_404f29, i8 %v4_404f29)
  %v5_404f29 = load i32, i32* @eax, align 4
  ret i32 %v5_404f29

dec_label_pc_404f2e:                              ; preds = %dec_label_pc_404ec5
  %v0_404f2e = load i32, i32* @ebx, align 4
  %v1_404f2e = add i32 %v0_404f2e, 57
  %v2_404f2e = inttoptr i32 %v1_404f2e to i8*
  %v3_404f2e = load i8, i8* %v2_404f2e, align 1
  %v4_404f2e = load i32, i32* @eax, align 4
  %v5_404f2e = trunc i32 %v4_404f2e to i8
  %v6_404f2e = add i8 %v3_404f2e, %v5_404f2e
  store i8 %v6_404f2e, i8* %v2_404f2e, align 1
  %v0_404f31 = load i32, i32* @ebp, align 4
  %v1_404f31 = add i32 %v0_404f31, 1
  store i32 %v1_404f31, i32* @ebp, align 4
  %v0_404f32 = load i32, i32* @eax, align 4
  %v1_404f32 = load i32, i32* @esp, align 4
  %v2_404f32 = add i32 %v1_404f32, -4
  %v3_404f32 = inttoptr i32 %v2_404f32 to i32*
  store i32 %v0_404f32, i32* %v3_404f32, align 4
  %v0_404f33 = load i32, i32* @esi, align 4
  %v1_404f33 = add i32 %v0_404f33, 1
  store i32 %v1_404f33, i32* @esi, align 4
  %v0_404f34 = load i32, i32* @eax, align 4
  %v1_404f34 = add i32 %v0_404f34, -1
  store i32 %v1_404f34, i32* @eax, align 4
  %v0_404f35 = load i32, i32* @ecx, align 4
  %v1_404f35 = add i32 %v0_404f35, 1
  store i32 %v1_404f35, i32* @ecx, align 4
  %v2_404f36 = add i32 %v1_404f32, 4
  %v3_404f36 = add i32 %v1_404f32, 12
  %v4_404f36 = add i32 %v1_404f32, 16
  %v5_404f36 = add i32 %v1_404f32, 20
  %v6_404f36 = add i32 %v1_404f32, 24
  %v7_404f36 = add i32 %v1_404f32, 28
  %v9_404f36 = load i32, i32* %v3_404f32, align 4
  %v10_404f36 = inttoptr i32 %v1_404f32 to i32*
  %v11_404f36 = load i32, i32* %v10_404f36, align 4
  %v12_404f36 = inttoptr i32 %v2_404f36 to i32*
  %v13_404f36 = load i32, i32* %v12_404f36, align 4
  %v14_404f36 = inttoptr i32 %v3_404f36 to i32*
  %v15_404f36 = load i32, i32* %v14_404f36, align 4
  %v16_404f36 = inttoptr i32 %v4_404f36 to i32*
  %v17_404f36 = load i32, i32* %v16_404f36, align 4
  %v18_404f36 = inttoptr i32 %v5_404f36 to i32*
  %v19_404f36 = load i32, i32* %v18_404f36, align 4
  %v20_404f36 = inttoptr i32 %v6_404f36 to i32*
  %v21_404f36 = load i32, i32* %v20_404f36, align 4
  store i32 %v9_404f36, i32* @edi, align 4
  store i32 %v11_404f36, i32* @esi, align 4
  store i32 %v13_404f36, i32* @ebp, align 4
  store i32 %v15_404f36, i32* @ebx, align 4
  store i32 %v17_404f36, i32* @edx, align 4
  store i32 %v19_404f36, i32* @ecx, align 4
  store i32 %v21_404f36, i32* @eax, align 4
  %v2_404f37 = inttoptr i32 %v11_404f36 to i32*
  %v3_404f37 = load i32, i32* %v2_404f37, align 4
  %v4_404f37 = xor i32 %v3_404f37, %v11_404f36
  store i1 false, i1* %az.global-to-local, align 1
  store i1 false, i1* %cf.global-to-local, align 1
  store i1 false, i1* %of.global-to-local, align 1
  %v5_404f37 = icmp eq i32 %v4_404f37, 0
  store i1 %v5_404f37, i1* %zf.global-to-local, align 1
  %v6_404f37 = icmp slt i32 %v4_404f37, 0
  store i1 %v6_404f37, i1* %sf.global-to-local, align 1
  store i32 %v4_404f37, i32* @esi, align 4
  br label %dec_label_pc_404f39

dec_label_pc_404f39:                              ; preds = %dec_label_pc_404ec3.dec_label_pc_404f39_crit_edge, %dec_label_pc_404f2e
  %v1_404f3a = phi i32 [ %v7_404f36, %dec_label_pc_404f2e ], [ %v3_404ec2, %dec_label_pc_404ec3.dec_label_pc_404f39_crit_edge ]
  %v0_404f3a = phi i32 [ %v17_404f36, %dec_label_pc_404f2e ], [ %v0_404f3a.pre, %dec_label_pc_404ec3.dec_label_pc_404f39_crit_edge ]
  %v0_404f39 = phi i32 [ %v19_404f36, %dec_label_pc_404f2e ], [ %v2_404ec2, %dec_label_pc_404ec3.dec_label_pc_404f39_crit_edge ]
  %v1_404f39 = add i32 %v0_404f39, -1
  store i32 %v1_404f39, i32* @ecx, align 4
  %v2_404f3a = add i32 %v1_404f3a, -4
  %v3_404f3a = inttoptr i32 %v2_404f3a to i32*
  store i32 %v0_404f3a, i32* %v3_404f3a, align 4
  %v0_404f3b = load i32, i32* @ebp, align 4
  %v2_404f3b = add i32 %v1_404f3a, -8
  %v3_404f3b = inttoptr i32 %v2_404f3b to i32*
  store i32 %v0_404f3b, i32* %v3_404f3b, align 4
  %v0_404f3c = load i32, i32* @eax, align 4
  %v1_404f3c = add i32 %v0_404f3c, -1
  store i32 %v1_404f3c, i32* @eax, align 4
  %v0_404f3d = load i32, i32* @esi, align 4
  %v1_404f3d = add i32 %v0_404f3d, 1
  store i32 %v1_404f3d, i32* @esi, align 4
  %v1_404f3f = add i32 %v1_404f3a, -7
  %v2_404f3f = urem i32 %v2_404f3b, 16
  %v4_404f3f = icmp eq i32 %v2_404f3f, 15
  %tmp537 = add i32 %v1_404f3a, 2147483640
  %v7_404f3f = and i32 %v1_404f3f, %tmp537
  %v8_404f3f = icmp slt i32 %v7_404f3f, 0
  store i1 %v4_404f3f, i1* %az.global-to-local, align 1
  store i1 %v8_404f3f, i1* %of.global-to-local, align 1
  %v9_404f3f = icmp eq i32 %v1_404f3f, 0
  store i1 %v9_404f3f, i1* %zf.global-to-local, align 1
  %v10_404f3f = icmp slt i32 %v1_404f3f, 0
  store i1 %v10_404f3f, i1* %sf.global-to-local, align 1
  ret i32 %v1_404f3c

dec_label_pc_404fa5:                              ; preds = %dec_label_pc_404ed5
  %v0_404fa5 = load i1, i1* %cf.global-to-local, align 1
  %v1_404fa5 = icmp eq i1 %v0_404fa5, false
  br i1 %v1_404fa5, label %dec_label_pc_405000, label %dec_label_pc_404fa7

dec_label_pc_404fa7:                              ; preds = %dec_label_pc_404fa5
  %v0_404fa7 = load i32, i32* @esi, align 4
  %v1_404fa7 = load i32, i32* @esp, align 4
  %v2_404fa7 = add i32 %v1_404fa7, -4
  %v3_404fa7 = inttoptr i32 %v2_404fa7 to i32*
  store i32 %v0_404fa7, i32* %v3_404fa7, align 4
  %v4_404fa7 = load i32, i32* @eax, align 4
  ret i32 %v4_404fa7

dec_label_pc_404fdf:                              ; preds = %dec_label_pc_404f22
  %v0_405013 = load i32, i32* @edx, align 4
  %v1_405013 = trunc i32 %v0_405013 to i16
  %v2_405013 = load i32, i32* @esi, align 4
  %v3_405013 = inttoptr i32 %v2_405013 to i8*
  %v4_405013 = load i8, i8* %v3_405013, align 1
  call void @__asm_outsb(i16 %v1_405013, i8 %v4_405013)
  %v0_405014 = load i32, i32* @eax, align 4
  %v1_405014 = inttoptr i32 %v0_405014 to i64*
  %v2_405014 = load i64, i64* %v1_405014, align 4
  %v3_405014 = call i32 @__asm_bound(i64 %v2_405014)
  store i32 %v3_405014, i32* @esi, align 4
  %v0_405016 = load i32, i32* @edi, align 4
  %v1_405016 = add i32 %v0_405016, 101
  %v2_405016 = inttoptr i32 %v1_405016 to i8*
  %v3_405016 = load i8, i8* %v2_405016, align 1
  %v4_405016 = load i32, i32* @eax, align 4
  %v5_405016 = udiv i32 %v4_405016, 256
  %v6_405016 = trunc i32 %v5_405016 to i8
  %v7_405016 = add i8 %v3_405016, %v6_405016
  %v8_405016 = urem i8 %v3_405016, 16
  %v9_405016 = urem i8 %v6_405016, 16
  %v10_405016 = add nuw nsw i8 %v9_405016, %v8_405016
  %v11_405016 = icmp ugt i8 %v10_405016, 15
  %v12_405016 = icmp ult i8 %v7_405016, %v3_405016
  %v13_405016 = xor i8 %v7_405016, %v3_405016
  %v14_405016 = xor i8 %v7_405016, %v6_405016
  %v15_405016 = and i8 %v13_405016, %v14_405016
  %v16_405016 = icmp slt i8 %v15_405016, 0
  store i1 %v11_405016, i1* %az.global-to-local, align 1
  store i1 %v12_405016, i1* %cf.global-to-local, align 1
  store i1 %v16_405016, i1* %of.global-to-local, align 1
  %v17_405016 = icmp eq i8 %v7_405016, 0
  store i1 %v17_405016, i1* %zf.global-to-local, align 1
  %v18_405016 = icmp slt i8 %v7_405016, 0
  store i1 %v18_405016, i1* %sf.global-to-local, align 1
  store i8 %v7_405016, i8* %v2_405016, align 1
  %v0_405019 = load i1, i1* %zf.global-to-local, align 1
  br i1 %v0_405019, label %dec_label_pc_40507c, label %dec_label_pc_40501b

dec_label_pc_405000:                              ; preds = %dec_label_pc_404fa5
  %v6_405048 = load i32, i32* @eax, align 4
  ret i32 %v6_405048

dec_label_pc_40501b:                              ; preds = %dec_label_pc_404fdf
  %v0_40501b = load i32, i32* @ebp, align 4
  %v1_40501b = add i32 %v0_40501b, 1
  %v2_40501b = urem i32 %v0_40501b, 16
  %v4_40501b = icmp eq i32 %v2_40501b, 15
  %tmp538 = xor i32 %v0_40501b, -2147483648
  %v7_40501b = and i32 %v1_40501b, %tmp538
  %v8_40501b = icmp slt i32 %v7_40501b, 0
  store i1 %v4_40501b, i1* %az.global-to-local, align 1
  store i1 %v8_40501b, i1* %of.global-to-local, align 1
  %v9_40501b = icmp eq i32 %v1_40501b, 0
  store i1 %v9_40501b, i1* %zf.global-to-local, align 1
  %v10_40501b = icmp slt i32 %v1_40501b, 0
  store i1 %v10_40501b, i1* %sf.global-to-local, align 1
  store i32 %v1_40501b, i32* @ebp, align 4
  %v0_40501c = load i32, i32* @edx, align 4
  %v1_40501c = trunc i32 %v0_40501c to i16
  %v2_40501c = load i32, i32* @esi, align 4
  %v3_40501c = inttoptr i32 %v2_40501c to i8*
  %v4_40501c = load i8, i8* %v3_40501c, align 1
  call void @__asm_outsb(i16 %v1_40501c, i8 %v4_40501c)
  %v0_40501d = load i1, i1* %zf.global-to-local, align 1
  br i1 %v0_40501d, label %dec_label_pc_405093, label %dec_label_pc_40501f

dec_label_pc_40501f:                              ; preds = %dec_label_pc_40501b
  %v0_40501f = load i1, i1* %sf.global-to-local, align 1
  %v1_40501f = icmp eq i1 %v0_40501f, false
  br i1 %v1_40501f, label %dec_label_pc_405071, label %dec_label_pc_405021

dec_label_pc_405021:                              ; preds = %dec_label_pc_40501f
  %v0_405021 = load i32, i32* @edx, align 4
  %v1_405021 = trunc i32 %v0_405021 to i16
  %v2_405021 = load i32, i32* @esi, align 4
  %v3_405021 = inttoptr i32 %v2_405021 to i32*
  %v4_405021 = load i32, i32* %v3_405021, align 4
  call void @__asm_outsd(i16 %v1_405021, i32 %v4_405021)
  %v5_405021 = load i32, i32* @eax, align 4
  ret i32 %v5_405021

dec_label_pc_405071:                              ; preds = %dec_label_pc_40501f
  %v0_405071 = load i32, i32* @ebp, align 4
  %v1_405071 = add i32 %v0_405071, 65
  %v2_405071 = inttoptr i32 %v1_405071 to i64*
  %v3_405071 = load i64, i64* %v2_405071, align 4
  %v4_405071 = call i32 @__asm_bound(i64 %v3_405071)
  store i32 %v4_405071, i32* @ebp, align 4
  %v0_405075 = load i1, i1* %zf.global-to-local, align 1
  %v0_405152.pr = load i1, i1* %cf.global-to-local, align 1
  br i1 %v0_405075, label %dec_label_pc_405152, label %dec_label_pc_405077

dec_label_pc_405077:                              ; preds = %dec_label_pc_405071
  br i1 %v0_405152.pr, label %dec_label_pc_4050e2.thread, label %dec_label_pc_405079

dec_label_pc_4050e2.thread:                       ; preds = %dec_label_pc_405077
  %v0_40514848 = load i32, i32* @ecx, align 4
  br label %dec_label_pc_405148

dec_label_pc_405079:                              ; preds = %dec_label_pc_405077
  %v1_405077 = load i32, i32* @eax, align 4
  ret i32 %v1_405077

dec_label_pc_40507c:                              ; preds = %dec_label_pc_404fdf
  %v0_40507c = load i32, i32* @ebx, align 4
  %v1_40507c = add i32 %v0_40507c, 121
  %v3_40507c = call i8 @__readgsbyte(i32 %v1_40507c)
  %v4_40507c = load i32, i32* @edx, align 4
  %v5_40507c = trunc i32 %v4_40507c to i8
  %v6_40507c = add i8 %v3_40507c, %v5_40507c
  %v7_40507c = urem i8 %v3_40507c, 16
  %v8_40507c = urem i8 %v5_40507c, 16
  %v9_40507c = add nuw nsw i8 %v8_40507c, %v7_40507c
  %v10_40507c = icmp ugt i8 %v9_40507c, 15
  %v11_40507c = icmp ult i8 %v6_40507c, %v3_40507c
  %v12_40507c = xor i8 %v6_40507c, %v3_40507c
  %v13_40507c = xor i8 %v6_40507c, %v5_40507c
  %v14_40507c = and i8 %v12_40507c, %v13_40507c
  %v15_40507c = icmp slt i8 %v14_40507c, 0
  store i1 %v10_40507c, i1* %az.global-to-local, align 1
  store i1 %v11_40507c, i1* %cf.global-to-local, align 1
  store i1 %v15_40507c, i1* %of.global-to-local, align 1
  %v16_40507c = icmp eq i8 %v6_40507c, 0
  store i1 %v16_40507c, i1* %zf.global-to-local, align 1
  %v17_40507c = icmp slt i8 %v6_40507c, 0
  store i1 %v17_40507c, i1* %sf.global-to-local, align 1
  %v21_40507c = load i32, i32* @ebx, align 4
  %v22_40507c = add i32 %v21_40507c, 121
  call void @__writegsbyte(i32 %v22_40507c, i8 %v6_40507c)
  %v0_405080 = load i1, i1* %cf.global-to-local, align 1
  %v1_405080 = icmp eq i1 %v0_405080, false
  %v0_4050f6 = load i32, i32* @edx, align 4
  %v1_4050f6 = trunc i32 %v0_4050f6 to i16
  br i1 %v1_405080, label %dec_label_pc_4050f6, label %dec_label_pc_405082

dec_label_pc_405082:                              ; preds = %dec_label_pc_40507c
  %v2_405082 = call i32 @__asm_insd(i16 %v1_4050f6)
  %v3_405082 = load i32, i32* @edi, align 4
  %v4_405082 = inttoptr i32 %v3_405082 to i32*
  store i32 %v2_405082, i32* %v4_405082, align 4
  %v0_405084 = load i32, i32* @ebx, align 4
  %v1_405084 = add i32 %v0_405084, 1
  %v2_405084 = urem i32 %v0_405084, 16
  %v4_405084 = icmp eq i32 %v2_405084, 15
  %tmp539 = xor i32 %v0_405084, -2147483648
  %v7_405084 = and i32 %v1_405084, %tmp539
  %v8_405084 = icmp slt i32 %v7_405084, 0
  store i1 %v4_405084, i1* %az.global-to-local, align 1
  store i1 %v8_405084, i1* %of.global-to-local, align 1
  %v9_405084 = icmp eq i32 %v1_405084, 0
  store i1 %v9_405084, i1* %zf.global-to-local, align 1
  %v10_405084 = icmp slt i32 %v1_405084, 0
  store i1 %v10_405084, i1* %sf.global-to-local, align 1
  store i32 %v1_405084, i32* @ebx, align 4
  %v0_405086 = load i32, i32* @edx, align 4
  %v1_405086 = trunc i32 %v0_405086 to i16
  %v2_405086 = load i32, i32* @esi, align 4
  %v3_405086 = inttoptr i32 %v2_405086 to i32*
  %v4_405086 = load i32, i32* %v3_405086, align 4
  call void @__asm_outsd(i16 %v1_405086, i32 %v4_405086)
  %v0_405087 = load i32, i32* @edx, align 4
  %v1_405087 = trunc i32 %v0_405087 to i16
  %v2_405087 = call i32 @__asm_insd(i16 %v1_405087)
  %v3_405087 = load i32, i32* @edi, align 4
  %v4_405087 = inttoptr i32 %v3_405087 to i32*
  store i32 %v2_405087, i32* %v4_405087, align 4
  %v0_405088 = load i1, i1* %of.global-to-local, align 1
  %v0_4050f9 = load i32, i32* @edx, align 4
  %v1_4050f9 = trunc i32 %v0_4050f9 to i16
  %v2_4050f9 = load i32, i32* @esi, align 4
  br i1 %v0_405088, label %dec_label_pc_4050f9, label %dec_label_pc_40508a

dec_label_pc_40508a:                              ; preds = %dec_label_pc_405082
  %v3_40508a = inttoptr i32 %v2_4050f9 to i8*
  %v4_40508a = load i8, i8* %v3_40508a, align 1
  call void @__asm_outsb(i16 %v1_4050f9, i8 %v4_40508a)
  %v0_40508b = load i32, i32* @edx, align 4
  %v1_40508b = trunc i32 %v0_40508b to i16
  %v2_40508b = load i32, i32* @esi, align 4
  %v4_40508b = call i8 @__readgsbyte(i32 %v2_40508b)
  call void @__asm_outsb(i16 %v1_40508b, i8 %v4_40508b)
  %v0_40508d = load i1, i1* %zf.global-to-local, align 1
  br i1 %v0_40508d, label %dec_label_pc_4050dc, label %dec_label_pc_40508f

dec_label_pc_40508f:                              ; preds = %dec_label_pc_40508a
  %v0_40508f = load i32, i32* @edx, align 4
  %v1_40508f = trunc i32 %v0_40508f to i16
  %v2_40508f = load i32, i32* @esi, align 4
  %v3_40508f = inttoptr i32 %v2_40508f to i32*
  %v4_40508f = load i32, i32* %v3_40508f, align 4
  call void @__asm_outsd(i16 %v1_40508f, i32 %v4_40508f)
  %v5_40508f = load i32, i32* @eax, align 4
  ret i32 %v5_40508f

dec_label_pc_405093:                              ; preds = %dec_label_pc_40501b
  %v0_405091 = load i32, i32* @edx, align 4
  %v1_405091 = trunc i32 %v0_405091 to i16
  %v2_405091 = call i8 @__asm_insb(i16 %v1_405091)
  %v3_405091 = load i32, i32* @edi, align 4
  %v4_405091 = inttoptr i32 %v3_405091 to i8*
  store i8 %v2_405091, i8* %v4_405091, align 1
  %v0_405093.pre = load i32, i32* @ebp, align 4
  %v4_405093.pre = load i32, i32* @eax, align 4
  %v1_405093 = add i32 %v0_405093.pre, 100
  %v2_405093 = inttoptr i32 %v1_405093 to i8*
  %v3_405093 = load i8, i8* %v2_405093, align 1
  %v5_405093 = trunc i32 %v4_405093.pre to i8
  %v6_405093 = add i8 %v3_405093, %v5_405093
  %v7_405093 = urem i8 %v3_405093, 16
  %v8_405093 = urem i8 %v5_405093, 16
  %v9_405093 = add nuw nsw i8 %v8_405093, %v7_405093
  %v10_405093 = icmp ugt i8 %v9_405093, 15
  %v11_405093 = icmp ult i8 %v6_405093, %v3_405093
  %v12_405093 = xor i8 %v6_405093, %v3_405093
  %v13_405093 = xor i8 %v6_405093, %v5_405093
  %v14_405093 = and i8 %v12_405093, %v13_405093
  %v15_405093 = icmp slt i8 %v14_405093, 0
  store i1 %v10_405093, i1* %az.global-to-local, align 1
  store i1 %v11_405093, i1* %cf.global-to-local, align 1
  store i1 %v15_405093, i1* %of.global-to-local, align 1
  %v16_405093 = icmp eq i8 %v6_405093, 0
  store i1 %v16_405093, i1* %zf.global-to-local, align 1
  %v17_405093 = icmp slt i8 %v6_405093, 0
  store i1 %v17_405093, i1* %sf.global-to-local, align 1
  store i8 %v6_405093, i8* %v2_405093, align 1
  %v24_405093 = load i32, i32* @eax, align 4
  ret i32 %v24_405093

dec_label_pc_4050dc:                              ; preds = %dec_label_pc_40508a
  %v0_4050dc = load i1, i1* %of.global-to-local, align 1
  br i1 %v0_4050dc, label %dec_label_pc_405129, label %dec_label_pc_4050de

dec_label_pc_4050de:                              ; preds = %dec_label_pc_4050dc
  %v0_4050de = load i1, i1* %sf.global-to-local, align 1
  %v1_4050de = icmp eq i1 %v0_4050de, false
  br i1 %v1_4050de, label %dec_label_pc_405158, label %dec_label_pc_4050e2

dec_label_pc_4050e2:                              ; preds = %dec_label_pc_4050de
  %v0_4050e1 = load i32, i32* @edx, align 4
  %v1_4050e1 = trunc i32 %v0_4050e1 to i16
  %v2_4050e1 = load i32, i32* @esi, align 4
  %v3_4050e1 = inttoptr i32 %v2_4050e1 to i32*
  %v4_4050e1 = load i32, i32* %v3_4050e1, align 4
  call void @__asm_outsd(i16 %v1_4050e1, i32 %v4_4050e1)
  %v0_4050e2.pr = load i1, i1* %cf.global-to-local, align 1
  %v0_405148 = load i32, i32* @ecx, align 4
  br i1 %v0_4050e2.pr, label %dec_label_pc_405148, label %dec_label_pc_4050e4

dec_label_pc_4050e4:                              ; preds = %dec_label_pc_4050e2
  %v1_4050e4 = add i32 %v0_405148, 1
  %v2_4050e4 = urem i32 %v0_405148, 16
  %v4_4050e4 = icmp eq i32 %v2_4050e4, 15
  %tmp540 = xor i32 %v0_405148, -2147483648
  %v7_4050e4 = and i32 %v1_4050e4, %tmp540
  %v8_4050e4 = icmp slt i32 %v7_4050e4, 0
  store i1 %v4_4050e4, i1* %az.global-to-local, align 1
  store i1 %v8_4050e4, i1* %of.global-to-local, align 1
  %v9_4050e4 = icmp eq i32 %v1_4050e4, 0
  store i1 %v9_4050e4, i1* %zf.global-to-local, align 1
  %v10_4050e4 = icmp slt i32 %v1_4050e4, 0
  store i1 %v10_4050e4, i1* %sf.global-to-local, align 1
  store i32 %v1_4050e4, i32* @ecx, align 4
  br i1 %v9_4050e4, label %dec_label_pc_4051c3, label %dec_label_pc_4050e7

dec_label_pc_4050e7:                              ; preds = %dec_label_pc_4050e4
  %v0_4051ca50 = load i32, i32* @eax, align 4
  br label %dec_label_pc_404838

dec_label_pc_4050f6:                              ; preds = %dec_label_pc_40507c
  %v2_4050f6 = load i32, i32* @esi, align 4
  %v3_4050f6 = inttoptr i32 %v2_4050f6 to i32*
  %v4_4050f6 = load i32, i32* %v3_4050f6, align 4
  call void @__asm_outsd(i16 %v1_4050f6, i32 %v4_4050f6)
  %v0_4050f7 = load i32, i32* @edx, align 4
  %v1_4050f7 = trunc i32 %v0_4050f7 to i16
  %v2_4050f7 = call i32 @__asm_insd(i16 %v1_4050f7)
  %v3_4050f7 = load i32, i32* @edi, align 4
  %v4_4050f7 = inttoptr i32 %v3_4050f7 to i32*
  store i32 %v2_4050f7, i32* %v4_4050f7, align 4
  %v5_4050f7 = load i32, i32* @eax, align 4
  ret i32 %v5_4050f7

dec_label_pc_4050f9:                              ; preds = %dec_label_pc_405082
  %v3_4050f9 = inttoptr i32 %v2_4050f9 to i32*
  %v4_4050f9 = load i32, i32* %v3_4050f9, align 4
  call void @__asm_outsd(i16 %v1_4050f9, i32 %v4_4050f9)
  %v0_4050fa = load i32, i32* @edx, align 4
  %v1_4050fa = trunc i32 %v0_4050fa to i16
  %v2_4050fa = load i32, i32* @esi, align 4
  %v3_4050fa = inttoptr i32 %v2_4050fa to i8*
  %v4_4050fa = load i8, i8* %v3_4050fa, align 1
  call void @__asm_outsb(i16 %v1_4050fa, i8 %v4_4050fa)
  %v0_4050fb = load i32, i32* @edx, align 4
  %v1_4050fb = trunc i32 %v0_4050fb to i16
  %v2_4050fb = load i32, i32* @esi, align 4
  %v4_4050fb = call i8 @__readgsbyte(i32 %v2_4050fb)
  call void @__asm_outsb(i16 %v1_4050fb, i8 %v4_4050fb)
  %v0_4050fd = load i1, i1* %zf.global-to-local, align 1
  br i1 %v0_4050fd, label %dec_label_pc_40514c, label %dec_label_pc_4050ff

dec_label_pc_4050ff:                              ; preds = %dec_label_pc_4050f9
  %v0_4050ff = load i32, i32* @edx, align 4
  %v1_4050ff = trunc i32 %v0_4050ff to i16
  %v2_4050ff = load i32, i32* @esi, align 4
  %v3_4050ff = inttoptr i32 %v2_4050ff to i32*
  %v4_4050ff = load i32, i32* %v3_4050ff, align 4
  call void @__asm_outsd(i16 %v1_4050ff, i32 %v4_4050ff)
  %v0_405100 = load i32, i32* @edx, align 4
  %v1_405100 = trunc i32 %v0_405100 to i16
  %v2_405100 = call i8 @__asm_insb(i16 %v1_405100)
  %v3_405100 = load i32, i32* @edi, align 4
  %v4_405100 = inttoptr i32 %v3_405100 to i8*
  store i8 %v2_405100, i8* %v4_405100, align 1
  %v0_405103 = load i32, i32* @esp, align 4
  %v1_405103 = add i32 %v0_405103, 1
  %v2_405103 = urem i32 %v0_405103, 16
  %v4_405103 = icmp eq i32 %v2_405103, 15
  %tmp541 = xor i32 %v0_405103, -2147483648
  %v7_405103 = and i32 %v1_405103, %tmp541
  %v8_405103 = icmp slt i32 %v7_405103, 0
  store i1 %v4_405103, i1* %az.global-to-local, align 1
  store i1 %v8_405103, i1* %of.global-to-local, align 1
  %v9_405103 = icmp eq i32 %v1_405103, 0
  store i1 %v9_405103, i1* %zf.global-to-local, align 1
  %v10_405103 = icmp slt i32 %v1_405103, 0
  store i1 %v10_405103, i1* %sf.global-to-local, align 1
  %v15_405103 = load i32, i32* @eax, align 4
  ret i32 %v15_405103

dec_label_pc_405129:                              ; preds = %dec_label_pc_4050dc
  %v0_405129 = load i32, i32* @esi, align 4
  %v1_405129 = load i32, i32* @esp, align 4
  %v2_405129 = add i32 %v1_405129, -4
  %v3_405129 = inttoptr i32 %v2_405129 to i32*
  store i32 %v0_405129, i32* %v3_405129, align 4
  %v4_405129 = load i32, i32* @eax, align 4
  ret i32 %v4_405129

dec_label_pc_405148:                              ; preds = %dec_label_pc_4050e2.thread, %dec_label_pc_4050e2
  %v0_40514849 = phi i32 [ %v0_40514848, %dec_label_pc_4050e2.thread ], [ %v0_405148, %dec_label_pc_4050e2 ]
  %v1_405148 = add i32 %v0_40514849, -1
  %v2_405148 = urem i32 %v0_40514849, 16
  %v3_405148 = add nsw i32 %v2_405148, -1
  %v4_405148 = icmp ugt i32 %v3_405148, 15
  %tmp542 = sub i32 0, %v0_40514849
  %v7_405148 = and i32 %v0_40514849, %tmp542
  %v8_405148 = icmp slt i32 %v7_405148, 0
  store i1 %v4_405148, i1* %az.global-to-local, align 1
  store i1 %v8_405148, i1* %of.global-to-local, align 1
  %v9_405148 = icmp eq i32 %v1_405148, 0
  store i1 %v9_405148, i1* %zf.global-to-local, align 1
  %v10_405148 = icmp slt i32 %v1_405148, 0
  store i1 %v10_405148, i1* %sf.global-to-local, align 1
  store i32 %v1_405148, i32* @ecx, align 4
  %v0_40514a = load i32, i32* @edx, align 4
  %v1_40514a = trunc i32 %v0_40514a to i16
  %v2_40514a = load i32, i32* @esi, align 4
  %v3_40514a = inttoptr i32 %v2_40514a to i8*
  %v4_40514a = load i8, i8* %v3_40514a, align 1
  call void @__asm_outsb(i16 %v1_40514a, i8 %v4_40514a)
  %v5_40514a = load i32, i32* @eax, align 4
  ret i32 %v5_40514a

dec_label_pc_40514c:                              ; preds = %dec_label_pc_4050f9
  %v0_40514c = load i1, i1* %cf.global-to-local, align 1
  br i1 %v0_40514c, label %dec_label_pc_4051c2, label %dec_label_pc_40514f

dec_label_pc_40514f:                              ; preds = %dec_label_pc_40514c
  %v0_40514f = load i1, i1* %of.global-to-local, align 1
  br i1 %v0_40514f, label %dec_label_pc_4051a4, label %dec_label_pc_405151

dec_label_pc_405151:                              ; preds = %dec_label_pc_40514f
  %v1_40514f = load i32, i32* @eax, align 4
  ret i32 %v1_40514f

dec_label_pc_405152:                              ; preds = %dec_label_pc_405071
  %v0_4051ca = load i32, i32* @eax, align 4
  br i1 %v0_405152.pr, label %dec_label_pc_4051ca, label %dec_label_pc_404838

dec_label_pc_405158:                              ; preds = %dec_label_pc_4050de
  %v6_405155 = load i32, i32* @eax, align 4
  ret i32 %v6_405155

dec_label_pc_4051a4:                              ; preds = %dec_label_pc_40514f
  %v0_4051a4 = load i32, i32* @ecx, align 4
  %v1_4051a4 = inttoptr i32 %v0_4051a4 to i8*
  %v2_4051a4 = load i8, i8* %v1_4051a4, align 1
  %v3_4051a4 = load i32, i32* @eax, align 4
  %v4_4051a4 = trunc i32 %v3_4051a4 to i8
  %v5_4051a4 = add i8 %v2_4051a4, %v4_4051a4
  %v6_4051a4 = urem i8 %v2_4051a4, 16
  %v7_4051a4 = urem i8 %v4_4051a4, 16
  %v8_4051a4 = add nuw nsw i8 %v7_4051a4, %v6_4051a4
  %v9_4051a4 = icmp ugt i8 %v8_4051a4, 15
  %v10_4051a4 = icmp ult i8 %v5_4051a4, %v2_4051a4
  %v11_4051a4 = xor i8 %v5_4051a4, %v2_4051a4
  %v12_4051a4 = xor i8 %v5_4051a4, %v4_4051a4
  %v13_4051a4 = and i8 %v11_4051a4, %v12_4051a4
  %v14_4051a4 = icmp slt i8 %v13_4051a4, 0
  store i1 %v9_4051a4, i1* %az.global-to-local, align 1
  store i1 %v10_4051a4, i1* %cf.global-to-local, align 1
  store i1 %v14_4051a4, i1* %of.global-to-local, align 1
  %v15_4051a4 = icmp eq i8 %v5_4051a4, 0
  store i1 %v15_4051a4, i1* %zf.global-to-local, align 1
  %v16_4051a4 = icmp slt i8 %v5_4051a4, 0
  store i1 %v16_4051a4, i1* %sf.global-to-local, align 1
  store i8 %v5_4051a4, i8* %v1_4051a4, align 1
  %v22_4051a4 = load i32, i32* @eax, align 4
  ret i32 %v22_4051a4

dec_label_pc_4051c2:                              ; preds = %dec_label_pc_40514c
  %v0_4051c0 = load i32, i32* @ebx, align 4
  %v1_4051c0 = inttoptr i32 %v0_4051c0 to i32*
  %v2_4051c0 = load i32, i32* %v1_4051c0, align 4
  %v3_4051c0 = load i32, i32* @eax, align 4
  %v4_4051c0 = add i32 %v3_4051c0, %v2_4051c0
  %v5_4051c0 = urem i32 %v2_4051c0, 16
  %v6_4051c0 = urem i32 %v3_4051c0, 16
  %v7_4051c0 = add nuw nsw i32 %v6_4051c0, %v5_4051c0
  %v8_4051c0 = icmp ugt i32 %v7_4051c0, 15
  %v9_4051c0 = icmp ult i32 %v4_4051c0, %v2_4051c0
  %v10_4051c0 = xor i32 %v4_4051c0, %v2_4051c0
  %v11_4051c0 = xor i32 %v4_4051c0, %v3_4051c0
  %v12_4051c0 = and i32 %v10_4051c0, %v11_4051c0
  %v13_4051c0 = icmp slt i32 %v12_4051c0, 0
  store i1 %v8_4051c0, i1* %az.global-to-local, align 1
  store i1 %v9_4051c0, i1* %cf.global-to-local, align 1
  store i1 %v13_4051c0, i1* %of.global-to-local, align 1
  %v14_4051c0 = icmp eq i32 %v4_4051c0, 0
  store i1 %v14_4051c0, i1* %zf.global-to-local, align 1
  %v15_4051c0 = icmp slt i32 %v4_4051c0, 0
  store i1 %v15_4051c0, i1* %sf.global-to-local, align 1
  store i32 %v4_4051c0, i32* %v1_4051c0, align 4
  %v22_4051c0 = load i32, i32* @eax, align 4
  ret i32 %v22_4051c0

dec_label_pc_4051c3:                              ; preds = %dec_label_pc_4050e4
  %v0_40515d = load i32, i32* @esp, align 4
  %v1_40515d = add i32 %v0_40515d, 4
  %v2_40515d = add i32 %v0_40515d, 8
  %v3_40515d = add i32 %v0_40515d, 16
  %v4_40515d = add i32 %v0_40515d, 20
  %v5_40515d = add i32 %v0_40515d, 24
  %v6_40515d = add i32 %v0_40515d, 28
  %v8_40515d = inttoptr i32 %v0_40515d to i32*
  %v9_40515d = load i32, i32* %v8_40515d, align 4
  %v10_40515d = inttoptr i32 %v1_40515d to i32*
  %v11_40515d = load i32, i32* %v10_40515d, align 4
  %v12_40515d = inttoptr i32 %v2_40515d to i32*
  %v13_40515d = load i32, i32* %v12_40515d, align 4
  %v14_40515d = inttoptr i32 %v3_40515d to i32*
  %v15_40515d = load i32, i32* %v14_40515d, align 4
  %v16_40515d = inttoptr i32 %v4_40515d to i32*
  %v17_40515d = load i32, i32* %v16_40515d, align 4
  %v18_40515d = inttoptr i32 %v5_40515d to i32*
  %v19_40515d = load i32, i32* %v18_40515d, align 4
  %v20_40515d = inttoptr i32 %v6_40515d to i32*
  %v21_40515d = load i32, i32* %v20_40515d, align 4
  store i32 %v9_40515d, i32* @edi, align 4
  store i32 %v11_40515d, i32* @esi, align 4
  store i32 %v13_40515d, i32* @ebp, align 4
  store i32 %v15_40515d, i32* @ebx, align 4
  store i32 %v17_40515d, i32* @edx, align 4
  store i32 %v19_40515d, i32* @ecx, align 4
  store i32 %v21_40515d, i32* @eax, align 4
  store i32 %v15_40515d, i32* %v20_40515d, align 4
  %v0_4051c3 = load i32, i32* @ecx, align 4
  %v1_4051c3 = inttoptr i32 %v0_4051c3 to i8*
  %v2_4051c3 = load i8, i8* %v1_4051c3, align 1
  %v3_4051c3 = load i32, i32* @eax, align 4
  %v4_4051c3 = trunc i32 %v3_4051c3 to i8
  %v5_4051c3 = add i8 %v2_4051c3, %v4_4051c3
  %v6_4051c3 = urem i8 %v2_4051c3, 16
  %v7_4051c3 = urem i8 %v4_4051c3, 16
  %v8_4051c3 = add nuw nsw i8 %v7_4051c3, %v6_4051c3
  %v9_4051c3 = icmp ugt i8 %v8_4051c3, 15
  %v10_4051c3 = icmp ult i8 %v5_4051c3, %v2_4051c3
  %v11_4051c3 = xor i8 %v5_4051c3, %v2_4051c3
  %v12_4051c3 = xor i8 %v5_4051c3, %v4_4051c3
  %v13_4051c3 = and i8 %v11_4051c3, %v12_4051c3
  %v14_4051c3 = icmp slt i8 %v13_4051c3, 0
  store i1 %v9_4051c3, i1* %az.global-to-local, align 1
  store i1 %v10_4051c3, i1* %cf.global-to-local, align 1
  store i1 %v14_4051c3, i1* %of.global-to-local, align 1
  %v15_4051c3 = icmp eq i8 %v5_4051c3, 0
  store i1 %v15_4051c3, i1* %zf.global-to-local, align 1
  %v16_4051c3 = icmp slt i8 %v5_4051c3, 0
  store i1 %v16_4051c3, i1* %sf.global-to-local, align 1
  store i8 %v5_4051c3, i8* %v1_4051c3, align 1
  %v0_4051c5 = load i32, i32* @ecx, align 4
  %v1_4051c5 = inttoptr i32 %v0_4051c5 to i8*
  %v2_4051c5 = load i8, i8* %v1_4051c5, align 1
  %v3_4051c5 = load i32, i32* @eax, align 4
  %v4_4051c5 = trunc i32 %v3_4051c5 to i8
  %v5_4051c5 = or i8 %v2_4051c5, %v4_4051c5
  store i1 false, i1* %az.global-to-local, align 1
  store i1 false, i1* %cf.global-to-local, align 1
  store i1 false, i1* %of.global-to-local, align 1
  %v6_4051c5 = icmp eq i8 %v5_4051c5, 0
  store i1 %v6_4051c5, i1* %zf.global-to-local, align 1
  %v7_4051c5 = icmp slt i8 %v5_4051c5, 0
  store i1 %v7_4051c5, i1* %sf.global-to-local, align 1
  store i8 %v5_4051c5, i8* %v1_4051c5, align 1
  %v0_4051c7 = load i32, i32* @eax, align 4
  %v1_4051c7 = inttoptr i32 %v0_4051c7 to i8*
  %v2_4051c7 = load i8, i8* %v1_4051c7, align 1
  %v3_4051c7 = load i32, i32* @ecx, align 4
  %v4_4051c7 = trunc i32 %v3_4051c7 to i8
  %v5_4051c7 = add i8 %v2_4051c7, %v4_4051c7
  %v6_4051c7 = urem i8 %v2_4051c7, 16
  %v7_4051c7 = urem i8 %v4_4051c7, 16
  %v8_4051c7 = add nuw nsw i8 %v7_4051c7, %v6_4051c7
  %v9_4051c7 = icmp ugt i8 %v8_4051c7, 15
  %v10_4051c7 = icmp ult i8 %v5_4051c7, %v2_4051c7
  %v11_4051c7 = xor i8 %v5_4051c7, %v2_4051c7
  %v12_4051c7 = xor i8 %v5_4051c7, %v4_4051c7
  %v13_4051c7 = and i8 %v11_4051c7, %v12_4051c7
  %v14_4051c7 = icmp slt i8 %v13_4051c7, 0
  store i1 %v9_4051c7, i1* %az.global-to-local, align 1
  store i1 %v10_4051c7, i1* %cf.global-to-local, align 1
  store i1 %v14_4051c7, i1* %of.global-to-local, align 1
  %v15_4051c7 = icmp eq i8 %v5_4051c7, 0
  store i1 %v15_4051c7, i1* %zf.global-to-local, align 1
  %v16_4051c7 = icmp slt i8 %v5_4051c7, 0
  store i1 %v16_4051c7, i1* %sf.global-to-local, align 1
  store i8 %v5_4051c7, i8* %v1_4051c7, align 1
  %v22_4051c7 = load i32, i32* @eax, align 4
  ret i32 %v22_4051c7

dec_label_pc_4051ca:                              ; preds = %dec_label_pc_405152
  %v1_4051ca = inttoptr i32 %v0_4051ca to i8*
  %v2_4051ca = load i8, i8* %v1_4051ca, align 1
  %v4_4051ca = trunc i32 %v0_4051ca to i8
  %v5_4051ca = add i8 %v2_4051ca, %v4_4051ca
  %v6_4051ca = urem i8 %v2_4051ca, 16
  %v7_4051ca = urem i8 %v4_4051ca, 16
  %v8_4051ca = add nuw nsw i8 %v6_4051ca, %v7_4051ca
  %v9_4051ca = icmp ugt i8 %v8_4051ca, 15
  %v10_4051ca = icmp ult i8 %v5_4051ca, %v2_4051ca
  %v11_4051ca = xor i8 %v5_4051ca, %v2_4051ca
  %v12_4051ca = xor i8 %v5_4051ca, %v4_4051ca
  %v13_4051ca = and i8 %v11_4051ca, %v12_4051ca
  %v14_4051ca = icmp slt i8 %v13_4051ca, 0
  store i1 %v9_4051ca, i1* %az.global-to-local, align 1
  store i1 %v10_4051ca, i1* %cf.global-to-local, align 1
  store i1 %v14_4051ca, i1* %of.global-to-local, align 1
  %v15_4051ca = icmp eq i8 %v5_4051ca, 0
  store i1 %v15_4051ca, i1* %zf.global-to-local, align 1
  %v16_4051ca = icmp slt i8 %v5_4051ca, 0
  store i1 %v16_4051ca, i1* %sf.global-to-local, align 1
  store i8 %v5_4051ca, i8* %v1_4051ca, align 1
  %v22_4051ca = load i32, i32* @eax, align 4
  ret i32 %v22_4051ca
}

define i32 @entry_point() local_unnamed_addr {
dec_label_pc_4055de:
  %v0_4055de = call i32 @_CorExeMain()
  store i32 %v0_4055de, i32* @eax, align 4
  ret i32 %v0_4055de
}

define i32 @function_7ffffff(i16 %arg1, i16 %arg2) local_unnamed_addr {
dec_label_pc_7ffffff:
  %tmp = load i32, i32* @eax, align 4
  ret i32 %tmp
}

define i32 @function_28000002(i16 %arg1) local_unnamed_addr {
dec_label_pc_28000002:
  %tmp = load i32, i32* @eax, align 4
  ret i32 %tmp
}

define i32 @function_2856a04a() local_unnamed_addr {
dec_label_pc_2856a04a:
  %tmp = load i32, i32* @eax, align 4
  ret i32 %tmp
}

define i32 @function_28589d3b() local_unnamed_addr {
dec_label_pc_28589d3b:
  %tmp = load i32, i32* @eax, align 4
  ret i32 %tmp
}

define i32 @function_28589d68(i16 %arg1, i16 %arg2) local_unnamed_addr {
dec_label_pc_28589d68:
  %tmp = load i32, i32* @eax, align 4
  ret i32 %tmp
}

define i32 @function_285c9d7e() local_unnamed_addr {
dec_label_pc_285c9d7e:
  %tmp = load i32, i32* @eax, align 4
  ret i32 %tmp
}

define i32 @function_285d9f39() local_unnamed_addr {
dec_label_pc_285d9f39:
  %tmp = load i32, i32* @eax, align 4
  ret i32 %tmp
}

define i32 @function_2940a30d() local_unnamed_addr {
dec_label_pc_2940a30d:
  %tmp = load i32, i32* @eax, align 4
  ret i32 %tmp
}

define i32 @function_38403c77() local_unnamed_addr {
dec_label_pc_38403c77:
  %tmp = load i32, i32* @eax, align 4
  ret i32 %tmp
}

define i32 @function_47fffffe() local_unnamed_addr {
dec_label_pc_47fffffe:
  %tmp = load i32, i32* @eax, align 4
  ret i32 %tmp
}

define i32 @function_a940307d() local_unnamed_addr {
dec_label_pc_a940307d:
  %tmp = load i32, i32* @eax, align 4
  ret i32 %tmp
}

; Function Attrs: nounwind readnone speculatable
declare i8 @llvm.ctpop.i8(i8) #0

declare i32 @__asm_insd(i16) local_unnamed_addr

declare i32 @__asm_iretd() local_unnamed_addr

declare i32 @__asm_int3() local_unnamed_addr

declare void @__asm_arpl(i16, i16) local_unnamed_addr

declare void @__asm_into(i32) local_unnamed_addr

declare void @__asm_outsd(i16, i32) local_unnamed_addr

declare void @__asm_outsb(i16, i8) local_unnamed_addr

declare i32 @__asm_bound(i64) local_unnamed_addr

declare i8 @__asm_insb(i16) local_unnamed_addr

declare i8 @__readgsbyte(i32) local_unnamed_addr

declare void @__writegsbyte(i32, i8) local_unnamed_addr

declare i16 @__readgsword(i32) local_unnamed_addr

attributes #0 = { nounwind readnone speculatable }

!0 = !{i8 0, i8 9}
